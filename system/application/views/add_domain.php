<!DOCTYPE HTML>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <title>Add Domain RSS</title>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-responsive.css">
  <link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url() ?>assets/ui.datepicker.css" />
  <style type="text/css">
.page.container {
  margin-top: 55px;
}
  #page {
height: 200px;
padding-left: 200px;
}
  th {
color: 
#424242;
text-transform: uppercase;
font-size: 12px;
}
  .table-condensed th, .table-condensed td {
padding: 10px 5px;
}
  .table tbody tr:hover td, .table tbody tr:hover th {
background-color: #FCFCFC;
}
  .nav > li > a:hover {
background-color: #FCFCFC;
}
  .datepicker_header select {
background: 
#333;
color: 
white;
border: 0px;
font-weight: bold;
width: auto;
height: auto;
line-height: 10px;
margin-bottom: 0px;
padding: 0px;
}
  .page_link.active-page {
background: #E7E7E7;
}
  .tdlink {
color: #333;
}
  </style>
  <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/ui.datepicker.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/js/fancybox/jquery.fancybox.pack.js"></script>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="brand" href="<?php echo site_url('domain/home') ?>">Lintas Domain Source</a>
      <div class="nav-collapse">
        <ul class="nav">
                </ul>
      </div>
    </div>
  </div>
</div><div class="page container">
  <?php $this->load->view('left_menu') ?>  
  <style type="text/css">
	.rand_user, .chsel, .chselactive,.selch,.selsubch,.sel_upload {
	    background: none repeat scroll 0 0 #F7F7F7;
	    border: 1px solid #CCCCCC;
	    color: #B5B5B5;
	    cursor: pointer;
	    display: inline-block;
	    margin-top: -3px;
	    padding: 1px 5px;
	    margin-bottom: 10px;
	}
	.rand_user.active, .chsel.active, .active.chselactive,.selch.active,.selsubch.active,.sel_upload.active {
	    background: none repeat scroll 0 0 #88A991;
	    border-color: #4B6853;
	    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.4);
	    color: white;
	}
	</style>
  <div id="page">
  <form action="<?php echo site_url('domain/do_add') ?>" class="form-horizontal span6" method="post">
    <fieldset>
      <div class="page-header">
        <h1>Add <small>Domain</small></h1>
      </div>
                  <div class="control-group">
        <label for="" class="control-label">Category</label>
        <div class="controls">
        				  <?php
        				  //print '<pre>';print_r($category); print '</pre>';
        				  foreach($category as $cat):
        				  ?>
                          <span class="selch" data-chid="<?php echo $cat['id'] ?>" data-name="<?php echo $cat['name'] ?>"><?php echo $cat['displayName'] ?></span>
                          <?php
                          endforeach;
                          ?>
                          <input type="hidden" class="input-xlarge" name="category" id="category" value="" />
                          <input type="hidden" class="input-xlarge" name="category_name" id="category_name" value="" />
        </div>
      </div>
      <div class="control-group topic_select" style="display:none;">
        <label for="" class="control-label">Topic</label>
        <div class="controls">
              <div class="subchcon">
                Select Category First
              </div>
              <?php 
              foreach($category as $cat):
              ?>
              <div id="subch_<?php echo $cat['id'] ?>" class="subchcon" style="display:none;">
              <?php 
              $ch = curl_init();
              
              curl_setopt($ch,CURLOPT_URL, "http://www.lintas.me/rest/explore_topic/".$cat['name']);
              curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);
              
              //execute post
              $topics = json_decode(curl_exec($ch), TRUE);
              $topics = $topics['topic'];
              
              //close connection
              curl_close($ch);
              
              if(count($topics) > 0):
              foreach ($topics as $topic):
              ?>
              <span class="selsubch" data-chid="<?php echo $topic['tagname'] ?>"><?php echo ucfirst($topic['tagname']) ?></span>
              <?php 
              endforeach;
              endif;
              ?>
			  </div>              
              <?php 
              endforeach;
              ?>
			  <input type="hidden" class="input-xlarge" name="topic" id="topic" value="" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Domain Name</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <input class="span3" id="appendedPrependedInput" name="domain" size="16" type="text" value="">
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">RSS URL</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <span class="add-on">http://</span><input class="span3" id="appendedPrependedInput" name="rss_url" size="16" type="text" value="">
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Update Period </label>
        <div class="controls">
          <div class="input-prepend input-append">
            <input class="span1" id="appendedPrependedInput" name="update_every" type="text" value="">
            <select class="span1" name="period_type">
            	<option value="hour">Hours</option>
            	<option value="day">Days</option>
            	<option value="week">Weeks</option>
            </select>
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Crawl Method</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <select name="crawl_type">
            	<option value="0">Parse RSS Feed directly (default)</option>
            	<option value="1">Parse RSS Feed using Reader</option>
            	<option value="2">Parse RSS Feed using Reader and find image in original URL</option>
            	<option value="3">Parse RSS Feed directly and find image in original URL</option>
            </select>
            <input type="button" name="test_pattern" value="Test Pattern" />
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Title Pattern</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <input class="span4" id="appendedPrependedInput" name="title_pattern" size="16" type="text" value=''>
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Description Pattern</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <input class="span4" id="appendedPrependedInput" name="desc_pattern" size="16" type="text" value=''>
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Image Pattern</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <input class="span4" id="appendedPrependedInput" name="image_pattern" size="16" type="text" value=''>
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label"></label>
        <div class="controls">
          <label class="checkbox">
            <input type="checkbox" id="optionsCheckbox" name="whitelist" value="1">
            HOME POPULAR
          </label>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label"></label>
        <div class="controls">
          <label class="checkbox">
            <input type="checkbox" id="optionsCheckbox" name="channelpop" value="1">
            CHANNEL POPULAR
          </label>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label"></label>
        <div class="controls">
          <label class="checkbox">
            <input type="checkbox" id="optionsCheckbox" name="autotweet" value="1">
            AUTOTWEET
          </label>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label"></label>
        <div class="controls">
          <label class="checkbox">
            <input type="checkbox" id="optionsCheckbox" name="socialeffect" checked="checked" value="1">
            SOCIAL EFFECT
          </label>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label"></label>
        <div class="controls">
          <label class="checkbox">
            <input type="checkbox" id="optionsCheckbox" name="active" checked="checked" value="1">
            ACTIVE
          </label>
        </div>
      </div>
      
      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </fieldset>
  </form>
  </div>
</div>
<a id="pattern_tester" href="#pattern_tester_w"></a>
<div id="pattern_tester_w" style="display:none">
	<h4>Pattern Test Result</h4>
	<table border="0" cellpadding="1">
		<tr>
			<td width="100">Title</td>
			<td valign="top">:</td>
			<td valign="top"><span id="titlep"></span></td>
		</tr>
		<tr>
			<td valign="top">Description</td>
			<td valign="top">:</td>
			<td valign="top"><span id="descp"></span></td>
		</tr>
		<tr>
			<td valign="top">Orig link</td>
			<td valign="top">:</td>
			<td valign="top"><span id="linkp"></span></td>
		</tr>
		<tr>
			<td valign="top">Link</td>
			<td valign="top">:</td>
			<td valign="top"><span id="guidp"></span></td>
		</tr>
		<tr>
			<td valign="top">GUID</td>
			<td valign="top">:</td>
			<td valign="top"><span id="pubdatep"></span></td>
		</tr>
		<tr>
			<td valign="top">Pub date</td>
			<td valign="top">:</td>
			<td valign="top"><span id="pubdatep"></span></td>
		</tr>
		<tr>
			<td valign="top">Image</td>
			<td valign="top">:</td>
			<td valign="top"><span id="imgp"><img src="" width="200" /></span></td>
		</tr>
		<tr>
			<td valign="top">Author</td>
			<td valign="top">:</td>
			<td valign="top"><span id="authorp"></span></td>
		</tr>
		<tr>
			<td valign="top">Format</td>
			<td valign="top">:</td>
			<td valign="top"><span id="formatp"></span></td>
		</tr>
		<tr>
			<td valign="top">Language</td>
			<td valign="top">:</td>
			<td valign="top"><span id="langp"></span></td>
		</tr>
	</table>
</div>
<script type="text/javascript">
  $(document).ready(function(){
  	$("a#pattern_tester").fancybox();
  	$('input[name="test_pattern"]').click(function(){
  		$(this).val('Process...');
  		$.post('<?php echo base_url() ?>tools/pattern_tester.php',{
  			url: $('input[name="rss_url"]').val(),
  			type: $('select[name="crawl_type"]').val(),
  			title_pattern: $('input[name="title_pattern"]').val(),
  			desc_pattern: $('input[name="desc_pattern"]').val(),
  			image_pattern: $('input[name="image_pattern"]').val()
  		}).done(function(resp) {
  			$('input[name="test_pattern"]').val('Test Pattern');
  			$('span#titlep').text(resp.title);
  			$('span#descp').text(resp.description);
  			$('span#origlinkp').text(resp.origlink);
  			$('span#linkp').text(resp.link);
  			$('span#guidp').text(resp.guid);
  			$('span#pubdatep').text(resp.pubdate);
  			$('span#imgp img').attr('src', resp.image);
  			$('span#authorp').text(resp.dc_author);
  			$('span#langp').text(resp.dc_language);
  			$('span#formatp').text(resp.dc_format);
  			$("a#pattern_tester").click();
  		});
  	});
    $("span.selch").click(function() {
      $('span.selch').removeClass('active');
      $("div.subchcon").hide();
      $("div.topic_select").show();
      $(this).addClass('active');
      var subch_sel = $(this).attr('data-chid');
      var subch_name = $(this).attr('data-name');
      $("#category").val(subch_sel);
      $("#category_name").val(subch_name);
      $("#subch_" + subch_sel).show();
      $("span.selsubch").click(function() {
        $('span.selsubch').removeClass('active');
        $(this).addClass('active');
        var subch2_sel = $(this).attr('data-chid');
        $("#topic").val(subch2_sel);
      });
    });
  });
</script>
</body>
</html>