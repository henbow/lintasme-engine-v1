<!DOCTYPE html>
<!-- saved from url=(0057)<?php echo base_url() ?>assets/publisher/listpublisher_feed/all -->
<html lang="en-US"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="UTF-8">
  <title>Reports All Domain</title>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-responsive.css">
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <style type="text/css">
.page.container {
  margin-top: 55px;
}
  #page {
height: 200px;
padding-left: 200px;
}
  th {
color: 
#424242;
text-transform: uppercase;
font-size: 12px;
}
  .table-condensed th, .table-condensed td {
padding: 10px 5px;
}
  .table tbody tr:hover td, .table tbody tr:hover th {
background-color: #FCFCFC;
}
  .nav > li > a:hover {
background-color: #FCFCFC;
}
  .datepicker_header select {
background: 
#333;
color: 
white;
border: 0px;
font-weight: bold;
width: auto;
height: auto;
line-height: 10px;
margin-bottom: 0px;
padding: 0px;
}
  .page_link.active-page {
background: #E7E7E7;
}
  .tdlink {
color: #333;
}
</style>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/tablesorter/jquery.tablesorter.js"></script>
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="brand" href="<?php echo site_url('domain/home') ?>">Lintas Domain Source</a>
      <div class="nav-collapse">
        <ul class="nav">
                </ul>
      </div>
    </div>
  </div>
</div><div class="page container">
	<?php $this->load->view('left_menu') ?>  
    <div id="page">
    <div class="container-fluid">
    	<div class="page-header">
        <h1>Reports <small> Domains Per Category <?php echo "from $startdate to $enddate" ?></small></h1>
      </div>
        <div id="container">

        </div>
      <form method="get" action="<?php echo site_url('domain/domain_category_report/').'/' ?>">
    Display from <input type="text" class="span2 datepicker" name="start_date" value="<?php echo $startdate ?>" />&nbsp;&nbsp;to&nbsp;&nbsp;
    <input type="text" class="span2 datepicker" name="end_date" value="<?php echo $enddate ?>" />
    <input type="submit" name="go" value=" Go " /> </form>
    <table class="table table-condensed tablesorter" id="all_report">
      <thead>
      <tr>    
        <th>Category</th>
        <th width="40%">Domains</th>
        <th width="40%">Post/Domain</th>
      </tr>
      </thead>
      <tbody>
      	<?php 
      	foreach($categories as $cat): 
			$domain = $this->report->get_domain_by_topic($cat['name']);
      	?>
        <tr>        
	        <td <?php echo 'rowspan="'.count($domain).'"' ?>><strong><?php echo $cat['displayName'] ?></strong></td>
	        <td><?php echo count($domain) > 0 ? anchor('domain/detail_report/'.$domain[0]['dl_ID'].'/?start_date='.$startdate.'&end_date='.$enddate, $domain[0]['dl_domain']) : "" ?></td>
	        <td><?php echo count($domain) > 0 ? $this->report->get_total_crawled_domain($domain[0]['dl_ID'], $startdate, $enddate) : "" ?></td>
	    </tr>
	    <?php
	    if(count($domain) > 1):
			for($i = 1; $i < count($domain); $i++):
	    ?>
	    <tr>
	        <td><?php echo anchor('domain/detail_report/'.$domain[$i]['dl_ID'].'/?start_date='.$startdate.'&end_date='.$enddate, $domain[$i]['dl_domain']) ?></td>
	        <td><?php echo $this->report->get_total_crawled_domain($domain[$i]['dl_ID'], $startdate, $enddate) ?></td>	        
      	</tr>
	    <?php
	    	endfor;
		endif;
	    ?>
      	<?php 
	      	$sub_category = $this->domain->get_sub_category($cat['name']);
			foreach ($sub_category['topic'] as $sub):
				$domainsub = $this->report->get_domain_by_topic($sub['tagname'], TRUE);
				//echo $this->db->last_query().'<br>';
		?>
		<tr>        
	        <td <?php echo 'rowspan="'.count($domainsub).'"' ?>>&rArr;&nbsp;<?php echo ucwords($sub['tagname']) ?></td>
	        <td><?php echo count($domainsub) > 0 ? anchor('domain/detail_report/'.$domainsub[0]['dl_ID'].'/?start_date='.$startdate.'&end_date='.$enddate, $domainsub[0]['dl_domain']) : "" ?></td>
	        <td><?php echo count($domainsub) > 0 ? $this->report->get_total_crawled_domain($domainsub[0]['dl_ID'], $startdate, $enddate) : "" ?></td>
	    </tr>
	    <?php
			    if(count($domainsub) > 1):
					for($i = 1; $i < count($domainsub); $i++):
	    ?>
	    <tr>
	        <td><?php echo anchor('domain/detail_report/'.$domainsub[$i]['dl_ID'].'/?start_date='.$startdate.'&end_date='.$enddate, $domainsub[$i]['dl_domain']) ?></td>
	        <td><?php echo $this->report->get_total_crawled_domain($domainsub[$i]['dl_ID'], $startdate, $enddate) ?></td>	        
      	</tr>
	    <?php
			    	endfor;
				endif;
	    ?>
		<?php
			endforeach;
      	endforeach; 
      	?>        
      </tbody>
    </table>
    <style type="text/css">
    .table th {
    	cursor: pointer;
    }
	.pagination ul {
		display: inline-block;
		margin-left: 0;
		margin-bottom: 0;
		-webkit-border-radius: 3px;
		-moz-border-radius: 3px;
		border-radius: 3px;
		-webkit-box-shadow: 0 0px 0px 
		rgba(0, 0, 0, 0.05);
		-moz-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.05);
		box-shadow: 0 0px 0px 
		rgba(0, 0, 0, 0.05);
		width: 100%;
	}
	</style>
    </div>
  </div>
</div>
  <script>
  $(function() {
    $( ".datepicker" ).datepicker();
    $( ".datepicker" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );
  });
  </script>

<div id="datepicker_div"></div></body></html>