<div class="nav_left" style="width: 200px;float: left;">
    <a href="<?php echo site_url('domain/add') ?>" class="btn btn-large btn-danger" style="display:block;margin-bottom: 10px;">Add New Domain</a>
    <div class="well" style="padding: 8px 0;border-radius: 0px;background: white;">
    	<ul class="nav nav-list">
        	<li class="nav-header">Daemon Information</li>
        	<li>Status: <span class="level3 status_level statusinfo blink"><a href="<?php echo site_url('daemon') ?>">ALL STOPPED</a></span></li>
        	<li>Total Daemon: <span class="total_daemon statusinfo"><?php echo $this->daemon->get_daemons_total() ?></span></li>
        	<li>Running Daemon: <span class="running_daemon statusinfo">0</span></li>        	
        	<li>CPU Usage: <span class="cpu_usage statusinfo">0.0%</span></li>        	
        	<li>Memory Usage: <span class="mem_usage statusinfo">0.0%</span></li>        	
      	</ul>
    </div>
    <div class="well" style="padding: 8px 0;border-radius: 0px;background: white;">
      <ul class="nav nav-list">
        <li class="nav-header">List Domain</li>
        <li><a href="<?php echo site_url('domain/list_domain') ?>">All (<?php echo $this->domain->get_total_domain() ?>)</a></li>
        <li><a href="<?php echo site_url('domain/search') ?>">Search</a></li>
        <li class="divider"></li>
      </ul>
      <ul class="nav nav-list">
        <li class="nav-header">Local Domain</li>
        <li><a href="<?php echo site_url('local/domain') ?>">Domain (<?php echo $this->localm->get_total_domain() ?>)</a></li>
        <li><a href="<?php echo site_url('local/city') ?>">City List (<?php echo $this->localm->get_total_city() ?>)</a></li>
        <li><a href="<?php echo site_url('local/add_domain') ?>">Add Domain</a></li>
        <li><a href="<?php echo site_url('local/add_city') ?>">Add City</a></li>
        <li class="divider"></li>
      </ul>
      <ul class="nav nav-list">
        <li class="nav-header">Feeds Generator</li>
        <li  ><a href="<?php echo site_url('feeds_generator/list_rss') ?>">All Feed</a></li>
        <li  ><a href="<?php echo site_url('feeds_generator/add_page') ?>">Add Feed</a></li>
        <li class="divider"></li>
      </ul>
      <ul class="nav nav-list">
        <li class="nav-header">Classifier</li>
        <li><a href="<?php echo site_url('content_classification/train') ?>">Train</a></li>
        <li><a href="<?php echo site_url('content_classification/test_classifier') ?>">Tester</a></li>
        <li><a href="<?php echo site_url('content_classification/stopwords') ?>">Stopwords</a></li>
        <li><a href="<?php echo site_url('backend/badkeyword/index') ?>">Bad Keyword</a></li>
        <li class="divider"></li>
      </ul>
      
      <ul class="nav nav-list">
        <li class="nav-header">Autotag</li>
        <li><a href="<?php echo site_url('content_classification/tags') ?>">Add Tags</a></li>
        <li class="divider"></li>
      </ul>
       
      <ul class="nav nav-list">
        <li class="nav-header">Reports</li>
        <li><a href="<?php echo site_url('domain/report') ?>">All</a></li>
        <li><a href="<?php echo site_url('domain/home_popular_report') ?>">Home Popular</a></li> 
        <li><a href="<?php echo site_url('domain/domain_category_report') ?>">Domains per Category</a></li>
        <li class="divider"></li>
      </ul>
      
      <?php
      if($level == "SUPERADMIN"):
      ?>
      <ul class="nav nav-list">
        <li class="nav-header">Daemon</li>
        <li><a href="<?php echo site_url('daemon') ?>">Manage Daemons</a></li>
        <li><a href="<?php echo site_url('daemon/add') ?>">Add Daemon</a></li>
        <li class="divider"></li>
      </ul>
      <ul class="nav nav-list">
        <li class="nav-header">Setting</li>
        <li><a href="<?php echo site_url('domain/autotweet_setting') ?>">Auto-tweet</a></li>
        <li><a href="<?php echo site_url('domain/setting') ?>">Setting</a></li>
        <li class="divider"></li>
      </ul>
      <?php
	  endif;
      ?>
      
       <ul class="nav nav-list">
        <li><a href="<?php echo site_url('home/logout') ?>">Logout</a></li>
      </ul>      
  </div>
   
</div>
  <script>
  $(document).ready(function(){
  	var totalDaemon = <?php echo $this->daemon->get_daemons_total() ?>;
  	$.get('<?php echo site_url('tools/psax.php') ?>', function(resp){
  		if(resp.ps_items != undefined) {
	  		var psItems = resp.ps_items;
	  		var running = psItems.length;
	  		var cpuUsg = 0;
	  		var memUsg = 0;
	  		
	  		for(var i=0,j=running; i<j; i++){
				cpuUsg = cpuUsg + parseFloat(psItems[i].cpu_usage);
				memUsg = memUsg + parseFloat(psItems[i].memory_usage);
		    };
		    
		    statusLevel(totalDaemon, running);
		    
		    $('span.running_daemon').text(running);
		    $('span.cpu_usage').text((Math.round(cpuUsg * 100) / 100) + "%");
		    $('span.mem_usage').text((Math.round(memUsg * 100) / 100) + "%");
	    }
  	});
  	
  	setInterval(function(){
  		$.get('<?php echo site_url('tools/psax.php') ?>', function(resp){
	  		if(resp.ps_items != undefined) {	
	  			var psItems = resp.ps_items;
		  		var running = psItems.length;
		  		var cpuUsg = 0;
		  		var memUsg = 0;
		  		
		  		for(var i=0,j=running; i<j; i++){
					cpuUsg = cpuUsg + parseFloat(psItems[i].cpu_usage);
					memUsg = memUsg + parseFloat(psItems[i].memory_usage);
			    };
			    
			    statusLevel(totalDaemon, running);
			    
			    $('span.running_daemon').text(running);
			    $('span.cpu_usage').text((Math.round(cpuUsg * 100) / 100) + "%");
		    	$('span.mem_usage').text((Math.round(memUsg * 100) / 100) + "%");
		    }
	  	});
  	},3000);
  });
  
  function statusLevel(total, running) {
  	var per = Math.round((running / total) * 100);
  	
  	$('span.status_level').removeClass('level0');
	$('span.status_level').removeClass('level1');
	$('span.status_level').removeClass('level2');
	$('span.status_level').removeClass('level3');
	$('span.status_level').removeClass('blink');
	$('span.status_level a').text(running + '/' + total + ' RUNNING');
	
  	switch(true) {
  		case (per == 100):
  			$('span.status_level').addClass('level0');
  			break;
  			
  		case (per < 100 && per >= 75):
  			$('span.status_level').addClass('level1');
  			break;
  			
  		case (per < 75 && per >= 50):
  			$('span.status_level').addClass('level2');
  			break;
  			
  		case (per < 50 && per >= 25):
  			$('span.status_level').addClass('level3');
  			break;
  			
  		default:
  			$('span.status_level').addClass('level3 blink');
  			break;
  	}
  	
  	return per;
  }
  </script>
  <style type="text/css">
  .statusinfo, .statusinfo a {font-weight: bold; font-size: 12px;}
  .level0, .level0 a {color: #46A546;}
  .level1, .level1 a {color: #AD6704;}
  .level2, .level2 a {color: #B94A48;}
  .level3, .level3 a {color: #CC0000;}
	  @-webkit-keyframes blinker {  
	  from { opacity: 1.0; }
	  to { opacity: 0.0; }
   }
   .blink, .blink a {
	  -webkit-animation-name: blinker;  
	  -webkit-animation-iteration-count: infinite;  
	  -webkit-animation-timing-function: cubic-bezier(1.0,0,0,1.0);
	  -webkit-animation-duration: 1s; 
   }
  </style>