<div class="page container">
  <?php $this->load->view('left_menu') ?>  
  <div id="page">
  <form action="<?php echo site_url('content_classification/stopwords') ?>" class="form-horizontal span6" method="post">
    <fieldset>
      <div class="page-header">
        <h1>Manage <small>Stopwords</small></h1>
      </div>
                 
      <div class="control-group">
        <label for="" class="control-label">New Stopword</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <input class="span3" id="appendedPrependedInput" name="stopword" size="16" type="text" value="">
          </div>
        </div>
      </div>      
      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Add Stopword</button>
      </div>
    </fieldset>
  </form>
  <div class="container-fluid">
    <table class="table table-condensed tablesorter" id="all_domain">
      <tbody>
      	<tr><td><h4>Total stopwords: <?php echo $total_words ?></h4></td></tr>
      	<?php 
      	$cols = 5;
      	$per_cols = ceil($total_words / $cols);
		$offset = 0;
		
		sort($stopword_data);
		
		for($i = 0; $i < $per_cols; $i++):
			$current_row = array_slice($stopword_data, $offset, $cols);
		?>		
        <tr>
		<?php
      		foreach ($current_row as $stopword):
      	?>
	        <td width="<?php echo (100 / $cols) ?>%"><?php echo $stopword."  ".anchor('content_classification/delete_stopword/'.$stopword, '[X]', array('class' => "delete_stopwords", "data-text" => $stopword)); ?></td>
      	<?php 
      		endforeach; 
		?>
		</tr>
		<?php
			$offset = $offset + $cols;
			flush();
		endfor;
      	?>        
      </tbody>
    </table>
    <style type="text/css">
	.pagination ul {
		display: inline-block;
		margin-left: 0;
		margin-bottom: 0;
		-webkit-border-radius: 3px;
		-moz-border-radius: 3px;
		border-radius: 3px;
		-webkit-box-shadow: 0 0px 0px 
		rgba(0, 0, 0, 0.05);
		-moz-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.05);
		box-shadow: 0 0px 0px 
		rgba(0, 0, 0, 0.05);
		width: 100%;
	}
	</style>
    </div>
  </div>
</div>
<script>
	$('input[name="stopword"]').focus();
	$('.delete_stopwords').click(function(e){
		e.preventDefault();
		if(confirm('Delete "'+$(this).attr('data-text')+'"?')) {
			window.location.href = $(this).attr('href');
		}
	});
</script>
<?php if($message): ?>
<script>//alert('<?php echo $message ?>');</script>
<?php endif; ?>