<!DOCTYPE html>
<!-- saved from url=(0057)<?php echo base_url() ?>assets/publisher/listpublisher_feed/all -->
<html lang="en-US"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="UTF-8">
  <title>All Domain List</title>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-responsive.css">
  <link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url() ?>assets/ui.datepicker.css" />
  <style type="text/css">
.page.container {
  margin-top: 55px;
}
  #page {
height: 200px;
padding-left: 200px;
}
  th {
color: 
#424242;
text-transform: uppercase;
font-size: 12px;
}
  .table-condensed th, .table-condensed td {
padding: 10px 5px;
}
  .table tbody tr:hover td, .table tbody tr:hover th {
background-color: #FCFCFC;
}
  .nav > li > a:hover {
background-color: #FCFCFC;
}
  .datepicker_header select {
background: 
#333;
color: 
white;
border: 0px;
font-weight: bold;
width: auto;
height: auto;
line-height: 10px;
margin-bottom: 0px;
padding: 0px;
}
  .page_link.active-page {
background: #E7E7E7;
}
  .tdlink {
color: #333;
}
  </style>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/ui.datepicker.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/tablesorter/jquery.tablesorter.js"></script>
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="brand" href="<?php echo site_url('domain/home') ?>">Lintas Domain Source</a>
      <div class="nav-collapse">
        <ul class="nav">
                </ul>
      </div>
    </div>
  </div>
</div><div class="page container">
	<?php $this->load->view('left_menu') ?>  
    <div id="page">
    <div class="container-fluid">
    <table class="table table-condensed tablesorter" id="all_domain">
      <thead>
      <tr>      
        
        <th>Domain</th>
        <th>topic</th>
        <th>rss url</th> 
        <th>Home</th>
        <th>Channel</th>
        <th>Autotweet</th>
        <th>Social Effect</th>
        <th>status</th>
        <th></th>
      </tr>
      </thead>
      <tbody>
      	<?php 
      	foreach($domain_list as $domain): 
      	?>
        <tr>        
	        <td><?php echo $domain['dl_domain'] ?></td>
	        <td><?php echo $category[$domain['dl_category']] . ($domain['dl_topic'] != "" ? " > " : "") . ucfirst($domain['dl_topic']) ?></td>
	        <td><?php echo $domain['dl_rss_url'] ?></td>
	        <td><?php echo $domain['dl_white_list'] == '1' ? 'YES' : 'NO' ?></td>
	        <td><?php echo $domain['dl_channel_popular'] == '1' ? 'YES' : 'NO' ?></td>
	        <td><?php echo $domain['dl_autotweet'] == '1' ? 'YES' : 'NO' ?></td>
	        <td><?php echo $domain['dl_social_effect'] == '1' ? 'YES' : 'NO' ?></td>
	        <td><?php echo $domain['dl_status'] ?></td>
	        <td>
            <a href="<?php echo site_url('domain/edit/'.$domain['dl_ID'].'/'.$current_page) ?>" class="detail">Detail</a> |                       
            <a href="<?php echo site_url('domain/delete/'.$domain['dl_ID']) ?>" class="delete" data-name="<?php echo $domain['dl_domain'] ?>">Delete</a>	                      
	        </td>
      	</tr>
      	<?php endforeach; ?>        
      </tbody>
    </table>
    <style type="text/css">
	.pagination ul {
		display: inline-block;
		margin-left: 0;
		margin-bottom: 0;
		-webkit-border-radius: 3px;
		-moz-border-radius: 3px;
		border-radius: 3px;
		-webkit-box-shadow: 0 0px 0px 
		rgba(0, 0, 0, 0.05);
		-moz-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.05);
		box-shadow: 0 0px 0px 
		rgba(0, 0, 0, 0.05);
		width: 100%;
	}
	</style>
		<div class="pagination">
		<?php echo $pages ?>
        </div>
    </div>
  </div>
</div>
  <script src="<?php echo base_url() ?>assets/bootstrap.js"></script>
  <script>
  	$("#all_domain").tablesorter();
	$('.delete').click(function(e){
		e.preventDefault();
		
		if(confirm('Are you sure to delete ' + $(this).attr('data-name'))) {
			window.location.href = $(this).attr('href');
		} 
	});
  </script>

<div id="datepicker_div"></div></body></html>