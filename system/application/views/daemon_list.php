<!DOCTYPE html>
<!-- saved from url=(0057)<?php echo base_url() ?>assets/publisher/listpublisher_feed/all -->
<html lang="en-US"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="UTF-8">
  <title>Manage Daemon</title>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-responsive.css">
  <link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url() ?>assets/css/ui.datepicker.css" />
  <style type="text/css">
.page.container {
  margin-top: 55px;
}
  #page {
height: 200px;
padding-left: 200px;
}
  th {
color: 
#424242;
text-transform: uppercase;
font-size: 12px;
}
  .table-condensed th, .table-condensed td {
padding: 10px 5px;
}
  .table tbody tr:hover td, .table tbody tr:hover th {
background-color: #FCFCFC;
}
  .nav > li > a:hover {
background-color: #FCFCFC;
}
  .datepicker_header select {
background: 
#333;
color: 
white;
border: 0px;
font-weight: bold;
width: auto;
height: auto;
line-height: 10px;
margin-bottom: 0px;
padding: 0px;
}
  .page_link.active-page {
background: #E7E7E7;
}
  .tdlink {
color: #333;
}
  </style>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/ui.datepicker.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/tablesorter/jquery.tablesorter.js"></script>
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="brand" href="<?php echo site_url() ?>">Lintas Domain Source</a>
      <div class="nav-collapse">
        <ul class="nav">
                </ul>
      </div>
    </div>
  </div>
</div><div class="page container">
	<?php $this->load->view('left_menu') ?>  
    <div id="page">
    <div class="container-fluid">
    <table class="table table-condensed daemon-list" id="daemon-list">
      <thead>
      <tr>              
        <th>Daemon Name</th>
        <th>CPU Usage (%)</th> 
        <th>Memory Usage (%)</th>
        <th>Running Time</th>
        <th>status</th>
        <th></th>
      </tr>
      </thead>
      <tbody>
      	<?php 
      	foreach($daemons as $daemon): 
			$params = $daemon['script_params'] ? " ".$daemon['script_params'] : '';
      	?>
        <tr id="<?php echo $daemon['daemon_id'] ?>"  data-psname="<?php echo substr($daemon['script_path'], strrpos($daemon['script_path'], '/') + 1).$params ?>">        
	        <td class="dname"><?php echo $daemon['name'] ?></td>
	        <td class="dcpu">0.0</td>
	        <td class="dmem">0.0</td>
	        <td class="druntime">0 seconds</td>
	        <td class="dstatus"><span class="statusinfo level3">STOPPED</span></td>
	        <td class="daction">
	            <a style="display: none" class="stop">Stop</a><a class="start">Start</a> | 
	            <a class="restart disabled">Restart</a> | 
	            <a href="<?php echo site_url('daemon/edit/'.$daemon['daemon_id']) ?>" class="edit">Edit</a> | 
	            <a href="<?php echo site_url('daemon/view_log/'.$daemon['daemon_id']) ?>" class="log">View Log</a> 
	        </td>
      	</tr>
      	<?php endforeach; ?>        
      </tbody>
    </table>
    
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url() ?>assets/bootstrap.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  	var daemonList = $('table#daemon-list tbody tr').map(function(){return $(this).attr('data-psname');});
  	var psData = function(current){
 		$.get('<?php echo site_url('tools/psax.php?ps=') ?>'+current, function(resp) {
 			var psid = $('tr[data-psname="'+current+'"]').attr('id');
 			
			if(resp.ps_items != undefined) {
				var ps = resp.ps_items[0];
				var psname = ps.process_name.replace(/\s+/gi, ' ').split('/');
				
				if(current==psname[psname.length - 1]) {
					var runtime = ps.runtime;
					var timeString = "0 seconds";
										
					if(runtime >= 60 && runtime < 3600) {
						timeString = Math.round(runtime / 60) + " minutes";
					} else if(runtime >= 3600 && runtime < 86400) {
						timeString = Math.round(runtime / 3600) + " hours";
					} else if(runtime >= 86400) {
						timeString = Math.round(runtime / 86400) + " days";
					} else {
						timeString = runtime + " seconds";
					}
					
					if(timeString == '3 days') {
						//$('tr[data-psname="'+psname[psname.length - 1]+'"] td.daction a.restart').click();
					}
					
					$('tr[data-psname="'+psname[psname.length - 1]+'"] td.dcpu').text(ps.cpu_usage);
					$('tr[data-psname="'+psname[psname.length - 1]+'"] td.dmem').text(ps.memory_usage);
					$('tr[data-psname="'+psname[psname.length - 1]+'"] td.druntime').text(timeString);
					$('tr[data-psname="'+psname[psname.length - 1]+'"] td.dstatus').html('<span class="statusinfo level0">RUNNING</span>');
														
					$('tr[data-psname="'+psname[psname.length - 1]+'"] td.daction a.stop').show();
					$('tr[data-psname="'+psname[psname.length - 1]+'"] td.daction a.start').hide();
					$('tr[data-psname="'+psname[psname.length - 1]+'"] td.daction a.stop').attr('href','<?php echo site_url('daemon/stop') ?>/'+ps.pid+'/'+psid);
					$('tr[data-psname="'+psname[psname.length - 1]+'"] td.daction a.restart').attr('href','<?php echo site_url('daemon/restart') ?>/'+ps.pid+'/'+psid);
				} else {
					$('tr[data-psname="'+psname[psname.length - 1]+'"] td.dcpu').text('0.0');
					$('tr[data-psname="'+psname[psname.length - 1]+'"] td.dmem').text('0.0');
					$('tr[data-psname="'+psname[psname.length - 1]+'"] td.druntime').text('0 seconds');
					$('tr[data-psname="'+psname[psname.length - 1]+'"] td.dstatus').html('<span class="statusinfo level3">STOPPED</span>');
														
					$('tr[data-psname="'+psname[psname.length - 1]+'"] td.daction a.stop').hide();
					$('tr[data-psname="'+psname[psname.length - 1]+'"] td.daction a.start').show();
					$('tr[data-psname="'+psname[psname.length - 1]+'"] td.daction a.start').attr('href','<?php echo site_url('daemon/start') ?>/'+psid);
				}
			} else {
				$('tr[data-psname="'+current+'"] td.dcpu').text('0.0');
				$('tr[data-psname="'+current+'"] td.dmem').text('0.0');
				$('tr[data-psname="'+current+'"] td.druntime').text('0 seconds');
				$('tr[data-psname="'+current+'"] td.dstatus').html('<span class="statusinfo level3">STOPPED</span>');
				$('tr[data-psname="'+current+'"] td.daction a.start').attr('href','<?php echo site_url('daemon/start') ?>/'+psid);
				
				$('tr[data-psname="'+current+'"] td.daction a.stop').hide();
				$('tr[data-psname="'+current+'"] td.daction a.start').show();
			}
		});
 	};
 	$('a.start').click(function(e){
 		e.preventDefault();
 		var self = $(this);
 		self.text('Loading...');
 		$.get(self.attr('href'), function(resp){
 			self.text('Start');
 			self.hide();
 			$('a.stop').show();
 		});
 	});
 	$('a.stop').click(function(e){
 		e.preventDefault();
 		var self = $(this);
 		self.text('Loading...');
 		$.get(self.attr('href'), function(resp){
 			self.text('Stop');
 			self.hide();
 			$('a.start').show();
 		});
 	});
 	$('a.restart').click(function(e){
 		e.preventDefault();
 		var self = $(this);
 		self.text('Loading...');
 		$.get(self.attr('href'), function(resp){
 			self.text('Restart');
 		});
 	});
 	for(var j=0; j<daemonList.length; j++) psData(daemonList[j]);
    setInterval(function(){for(var j=0; j<daemonList.length; j++) psData(daemonList[j])},3000);
});
</script>
</body></html>