<!DOCTYPE html>
<!-- saved from url=(0040)<?php echo base_url() ?>assets/publisher/login -->
<html lang="en-US"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
    <meta charset="UTF-8">
    <title>Admin Lintas.me</title>
    <link rel="stylesheet" href="/assets/css/bootstrap.css">
    <link rel="stylesheet" href="/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="/assets/css/backend.css">
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <style type="text/css">
        #post_loader {
            display: inline-block;
            width: 15px;
            height: 15px;
            vertical-align: middle;
            margin-left: 5px;
        }
        .post_image {
            display: inline-block;
            vertical-align: top;
            margin-bottom: 10px;
            margin-right: 5px;
            background: #FFF;
            padding: 4px;
            box-shadow: 1px 1px 3px #838383;
            cursor: pointer;
        }
        .post_image.active {
            background: #008000;
        }
        .pimg {
            background: #FFF;
            padding: 10px;
            border: 1px solid #CCC;
        }
        .post_img_url_options {
            margin-bottom: 10px;
            height: 28px;
            margin-left: -10px;
            margin-top: -10px;
        }
        .select_img {
            display: inline-block;
            padding: 4px 10px;
            float: left;
            border: 1px solid #EEE;
            cursor: pointer;
            border-top: none;
            border-left: none;
            color: #A0A0A0;
            font-size: 12px;
        }
        .select_img:hover {
            background: #F8F8F8;
        }
    </style>
</head>
<body>
<div id="wraper">
    <div id="menuwrap">
        <div id="logo">
            backend
        </div>
        <div id="mainmenucon">
            <a href=""><span class="ico"></span>Menu 1</a>
            <a href=""><span class="ico"></span>Menu 1</a>
            <a href=""><span class="ico"></span>Menu 1</a>
            <a href=""><span class="ico"></span>Menu 1</a>
        </div>
    </div>
    <div id="contenwrap">
        <div id="topbar">
            <h1>New Post</h1>
        </div>
        <div id="content">
            <form action="" class="form-horizontal" method="post" id="formposturl" enctype="multipart/form-data">
                <fieldset>
                    <div class="control-group">
                        <label for="" class="control-label">Post As</label>
                        <div class="controls">
                                <?php
                                foreach($random_user as $key => $val){
                                ?>
                                <span class="rand_user <?php if($key == 0){ echo "active"; }?>" data-userid="<?=$val['id']?>">
                                    <?=$val['name']?>
                                </span>
                                <?php
                                }
                                ?>
                            <input type="hidden" value="<?=$random_user[0]['id']?>" id="rand_user" name="rand_user">
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="" class="control-label">URL</label>
                        <div class="controls">
                            <div class="input-prepend input-append">
                                <input class="span5" id="posturl" name="url" size="16" type="text" value="">
                                <button class="btn" id="crawl" type="button">Get Detail</button>
                                <div id="post_loader" style="background-image:url('http://i.brta.in/images/ajax-loader-admin.gif');display: none;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="" class="control-label">Tipe</label>
                        <div class="controls">
                            <div class="input-prepend input-append">
                                <select name="kind"> <option value="article">Article</option> <option value="photo">Images</option> <option value="video">Video</option> </select>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="" class="control-label">Title</label>
                        <div class="controls">
                            <div class="input-prepend input-append">
                                <input id="post_title" class="span5" name="post_title" value="">
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="" class="control-label">Content</label>
                        <div class="controls">
                            <div class="input-prepend input-append">
                                <textarea name="post_content" spellcheck="false" id="texarea_post_content" rows="7" class="input_post span6"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="" class="control-label">Thumbnail</label>
                        <div class="controls">
                            <div class="pimg">
                                <div class="post_img_url_options">
                                    <span class="select_img" data-sel="ava">Select Available</span>
                                    <span class="select_img" data-sel="url">URL</span>
                                    <span class="select_img" data-sel="upl">Upload </span>
                                </div>

                                <div class="post_img_url_con ava">
                                    <div class="img_slideleft img_nav" style=""></div>
                                    <div class="img_slideright img_nav" style=""></div>
                                    <div class="post_image_con" id="post_image_con">

                                    </div>
                                </div>
                                <div class="post_img_url_con hide url">
                                    <input name="post_img_url" id="post_img_url" value="" class="span8" placeholder="Paste Image url">
                                </div>
                                <div class="post_img_url_con hide upl">
                                    <div id="imupload" style="display:inline-block;" class="opt_hide">
                                        <input type="file" id="thumb_pic" name="thumb_pic">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="" class="control-label">Topic</label>
                        <div class="controls">
                            <span class="selch" data-chid="4d8269937205fb62580031c6">fun</span>
                            <span class="selch" data-chid="4d82a1d17205fb674a00000b">lifestyle</span>
                            <span class="selch" data-chid="4d82a1d17205fb674a000005">woman</span>
                            <span class="selch" data-chid="4d82a1d07205fb674a000001">entertainment</span>
                            <span class="selch" data-chid="4d82a1d47205fb674a000032">technology</span>
                            <span class="selch" data-chid="4d82a1d37205fb674a000023">sports</span>
                            <span class="selch" data-chid="4d82a1d67205fb674a00003f">bisnis</span>
                            <span class="selch" data-chid="4d82a1d57205fb674a000039">internet</span>
                            <span class="selch" data-chid="4d82a1d47205fb674a00002e">otomotif</span>
                            <span class="selch" data-chid="4ef94f8c7085e24e7c000031">design</span>
                            <span class="selch" data-chid="4d82a1d57205fb674a00003a">news</span>
                            <span class="selch" data-chid="4d82a1d37205fb674a00001d">games</span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="" class="control-label">Sub Topic</label>
                        <div class="controls">
                            <div id="subch_4d8269937205fb62580031c6" class="subchcon" style=""> <span class="selsubch" data-chid="4d8295177205fb62580076b7">unik-aneh</span> <span class="selsubch" data-chid="4d82953d7205fb6258007747">humor</span> <span class="selsubch" data-chid="4d82a13f7205fb674a000000">misteri</span> </div> <div id="subch_4d82a1d17205fb674a00000b" class="subchcon" style="display: none;"> <span class="selsubch" data-chid="4d82a1d17205fb674a00000c">nightlife</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c00001b">events</span> <span class="selsubch" data-chid="4d82a1d27205fb674a000014">travelling</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c00001c">food</span> <span class="selsubch" data-chid="4d82a1d27205fb674a000010">shopping</span> <span class="selsubch" data-chid="4d82a1d47205fb674a00002a">health</span> </div> <div id="subch_4d82a1d17205fb674a000005" class="subchcon" style="display: none;"> <span class="selsubch" data-chid="4d82a1d17205fb674a000006">beauty</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c00002d">relationship</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c00002c">parenting</span> <span class="selsubch" data-chid="4d82a1d17205fb674a000009">family</span> <span class="selsubch" data-chid="4f226f94600c9dac47000189">wedding</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000022">woman-story</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c00002f">horoscope</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000037">recipes</span> <span class="selsubch" data-chid="4f210ba6600c9d5a2a000072">career-woman</span> <span class="selsubch" data-chid="4f47559ce0c8ba5d120090a7">fashion</span> </div> <div id="subch_4d82a1d07205fb674a000001" class="subchcon" style="display: none;"> <span class="selsubch" data-chid="4d82a1d07205fb674a000002">movie</span> <span class="selsubch" data-chid="4d82a1d17205fb674a000003">music</span> <span class="selsubch" data-chid="4d82a1d17205fb674a000004">celebrities</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000016">tv</span> </div> <div id="subch_4d82a1d47205fb674a000032" class="subchcon" style="display: none;"> <span class="selsubch" data-chid="4d82a1d57205fb674a000033">gadget</span> <span class="selsubch" data-chid="4d82a1d57205fb674a000035">computer</span> <span class="selsubch" data-chid="4d82a1d57205fb674a000036">software</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000017">apps</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000018">hi-fi</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000019">science</span> </div> <div id="subch_4d82a1d37205fb674a000023" class="subchcon" style="display: none;"> <span class="selsubch" data-chid="4d82a1d37205fb674a000024">sepakbola</span> <span class="selsubch" data-chid="4d82a1d37205fb674a000025">basketball</span> <span class="selsubch" data-chid="4d82a1d37205fb674a000026">raket</span> <span class="selsubch" data-chid="4d82a1d47205fb674a000027">racing</span> </div> <div id="subch_4d82a1d67205fb674a00003f" class="subchcon" style="display: none;"> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000020">financial</span> <span class="selsubch" data-chid="4d82a1d67205fb674a000041">forex-saham</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000021">ekonomi</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c00002e">inspirasi</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000023">entrepreneurship</span> <span class="selsubch" data-chid="4f210b5f600c9d4a2a00004f">marketing</span> <span class="selsubch" data-chid="513d4f721b971a9b4700011b">jualan</span> </div> <div id="subch_4d82a1d57205fb674a000039" class="subchcon" style="display: none;"> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000024">online-marketing</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000025">social-media</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000026">start-up</span> <span class="selsubch" data-chid="4f2108be600c9d4520000251">online-media</span> </div> <div id="subch_4d82a1d47205fb674a00002e" class="subchcon" style="display: none;"> <span class="selsubch" data-chid="4d82a1d47205fb674a00002f">mobil</span> <span class="selsubch" data-chid="4d82a1d47205fb674a000030">motor</span> <span class="selsubch" data-chid="4d82a1d47205fb674a000031">modifikasi</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000030">listing</span> <span class="selsubch" data-chid="4d82a1d47205fb674a000027">racing</span> </div> <div id="subch_4ef94f8c7085e24e7c000031" class="subchcon" style="display: none;"> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000032">architecture</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000033">infographic</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000034">interior</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000035">ads</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c000036">cool-stuff</span> </div> <div id="subch_4d82a1d57205fb674a00003a" class="subchcon" style="display: none;"> <span class="selsubch" data-chid="4d82a1d57205fb674a00003b">politik-hukum</span> <span class="selsubch" data-chid="4ef94f8c7085e24e7c00001f">indonesiaku</span> <span class="selsubch" data-chid="4d82a1d57205fb674a00003d">nasional</span> <span class="selsubch" data-chid="4d82a1d57205fb674a00003e">dunia</span> </div> <div id="subch_4d82a1d37205fb674a00001d" class="subchcon" style="display: none;"> <span class="selsubch" data-chid="4d82a1d37205fb674a00001e">pc</span> <span class="selsubch" data-chid="4d82a1d37205fb674a00001f">consoles</span> <span class="selsubch" data-chid="4d82a1d37205fb674a000020">reviews</span> <span class="selsubch" data-chid="4d82a1d37205fb674a000021">cheats</span> <span class="selsubch" data-chid="4d82a1d57205fb674a000038">mobile</span> </div>
                        </div>
                    </div>
                    <input type="hidden" name="category_id" id="category_id" value="" />
                    <input type="hidden" name="origin_id" id="origin_id" value="<?php echo $_SESSION['backend_user']['post_only_ch'];?>" />
                    <div class="control-group">
                        <label for="" class="control-label">Tags</label>
                        <div class="controls">
                            <div class="input-prepend input-append">
                                <input id="post_tag_no_auto" class="span5" name="post_tag_no_auto" value="">
                            </div>
                              <span class="help-block">
                                Tags dipisahkan dengan koma (,) contoh : <b>windows,microsoft,bing</b>
                              </span>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn">Cancel</button>
                        <input type="hidden" name="is_backecd" id="is_backecd" value="1" />
                    </div>
                </fieldset>
            </form>
            <script>
                $(document).ready(function(){
                    $('#crawl').click(function(){
                        post_url = $('#posturl').val();
                        $.ajax({
                            type: "POST",
                            url: 'http://api.lintas.me/crawl/index/format.json',
                            data : {url:post_url},
                            beforeSend: function(){
                                $("#crawl").hide();
                                $("#post_loader").css("display","inline-block");
                            },
                            success: function(data){
                                $("#crawl").show();
                                $("#post_loader").hide();
                                if(data.result.error){
                                    if(data.result.status == "DUPLICATE"){
                                        alert('already posted');
                                    }
                                }else{
                                    $('#post_title').val(data.result.title);
                                    $('#texarea_post_content').val(data.result.content);
                                }

                                $('#post_image_con').html('');
                                $('.img_nav').show();
                                $('#img_jml').html(data.result.images.length);
                                var jimages = data.result.images.length;

                                if(jimages > 0){
                                    $("#post_img_url").val(data.result.images[0]);
                                }

                                $.each(data.result.images, function(key, value) {
                                    if(key > 0){
                                        $('#post_image_con').append('<div class="post_image hide image_'+ key +'"><img id="pimage'+ key +'" src="'+ value +'" width="160px"/></div>');
                                    }else{
                                        $('#post_image_con').append('<div class="post_image active image_'+ key +'"><img id="pimage'+ key +'" src="'+ value +'" width="160px"/></div>');
                                    }
                                });

                                $(".post_image").click(function() {

                                    $(".post_image").removeClass('active');
                                    $(this).removeClass('active').addClass('active');
                                    var img_src = $('.active img').attr("src") ;
                                    $("#post_img_url").val(img_src);
                                });
                            }
                        });
                    });

                    $('.rand_user').click(function(){
                        $('.rand_user').removeClass('active')
                        $(this).addClass('active');
                        $('#rand_user').val($(this).attr('data-userid'));
                    });

                    $('.select_img').click(function(){
                        $('.post_img_url_con').addClass('hide');
                        var sel = $(this).attr('data-sel');
                        $('.' + sel).removeClass('hide');
                    });

                    $("span.selch").click(function() {
                        $('span.selch').removeClass('active');
                        $("div.subchcon").hide();
                        $(this).addClass('active');
                        var subch_sel = $(this).attr('data-chid');
                        $("#origin_id").val(subch_sel);
                        $("#category_id").val(subch_sel);
                        $("#subch_" + subch_sel).show();
                        $("span.selsubch").click(function() {
                            $('span.selsubch').removeClass('active');
                            $(this).addClass('active');
                            var subch2_sel = $(this).attr('data-chid');
                            $("#origin_id").val(subch2_sel);
                        });
                    });
                });
            </script>
        </div>
    </div>
</div>
<script src="http://engine.lintas.me/assets/js/bootstrap.js"></script>
</body>
</html>