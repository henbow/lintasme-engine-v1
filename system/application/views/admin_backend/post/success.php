<!DOCTYPE html>
<!-- saved from url=(0040)<?php echo base_url() ?>assets/publisher/login -->
<html lang="en-US"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
    <meta charset="UTF-8">
    <title>Admin Lintas.me</title>
    <link rel="stylesheet" href="/assets/css/bootstrap.css">
    <link rel="stylesheet" href="/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="/assets/css/backend.css">
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <style type="text/css">
        #post_loader {
            display: inline-block;
            width: 15px;
            height: 15px;
            vertical-align: middle;
            margin-left: 5px;
        }
        .post_image {
            display: inline-block;
            vertical-align: top;
            margin-bottom: 10px;
            margin-right: 5px;
            background: #FFF;
            padding: 4px;
            box-shadow: 1px 1px 3px #838383;
            cursor: pointer;
        }
        .post_image.active {
            background: #008000;
        }
        .pimg {
            background: #FFF;
            padding: 10px;
            border: 1px solid #CCC;
        }
        .post_img_url_options {
            margin-bottom: 10px;
            height: 28px;
            margin-left: -10px;
            margin-top: -10px;
        }
        .select_img {
            display: inline-block;
            padding: 4px 10px;
            float: left;
            border: 1px solid #EEE;
            cursor: pointer;
            border-top: none;
            border-left: none;
            color: #A0A0A0;
            font-size: 12px;
        }
        .select_img:hover {
            background: #F8F8F8;
        }
        .popdetail.bigdetail {
            position: relative;
            top: 0px;
            left: 0px;
        }
        .alert.alert-success {
            margin-bottom: -1px;
            width: 614px;
            border-radius: 0px;
            border: 1px solid #CCC;
            padding: 10px;
            color: black;
        }
    </style>
</head>
<body>
<div id="wraper">
<div id="menuwrap">
    <div id="logo">
        backend
    </div>
    <div id="mainmenucon">
        <a href=""><span class="ico"></span>Menu 1</a>
        <a href=""><span class="ico"></span>Menu 1</a>
        <a href=""><span class="ico"></span>Menu 1</a>
        <a href=""><span class="ico"></span>Menu 1</a>
    </div>
</div>
<div id="contenwrap">
<div id="topbar">
    <h1>New Post</h1>
</div>
<div id="content">
    <div class="alert alert-success">
        <strong>Post successs!</strong> You successfully read this important alert message.
    </div>
    <div class="popdetail bigdetail">
        <div class="article-img">
            <a class="shadow" href="http://celebrities.lintas.me/article/nusantara.tvonenews.tv/ayu-azhari-merasa-jadi-korban-janji-fathanah?utm_source=latest_channel_&amp;utm_medium=latest_channel__13&amp;utm_campaign=stream_lintas">
                <img id="stream_home_image" alt="Ayu Azhari Merasa Jadi Korban Janji Fathanah" title="Ayu Azhari Merasa Jadi Korban Janji Fathanah" width="100" height="66" src="http://i.brta.in/images/2013-05/med/ac72c9a7093dfcd4c65194b2171f8cbf.jpg">
            </a>
        </div>
        <div class="article-text ">
            <h2 class="article-title">
                <a id="stream_home_title" href="http://celebrities.lintas.me/article/nusantara.tvonenews.tv/ayu-azhari-merasa-jadi-korban-janji-fathanah?utm_source=latest_channel_&amp;utm_medium=latest_channel__13&amp;utm_campaign=stream_lintas">Ayu Azhari Merasa Jadi Korban Janji Fathanah</a>
            </h2>
            <div class="article-meta">
                <a id="stream_home_source" class="article-source" title="nusantara.tvonenews.tv" href="publisher/nusantara.tvonenews.tv">nusantara.tvonenews.tv</a>
                <span class="article-time"> - 2 minute ago</span>
            </div>
            <p class="artilce-snippet">Jakrta, (tvOne)
                Artis Ayu Azhari mengaku telah menjadi korban dari perbuatan orang dekat mantan Presiden Partai Keadil...
            </p>
        </div>
        <div class="article-action">
            <a class="" href="">Edit</a>
            <a class="" href="">Delete</a>
            <a class="" href="">Set Popular</a>
        </div>
        <div class="clear"></div>
    </div>
</div>
</div>
</div>
<script src="http://engine.lintas.me/assets/js/bootstrap.js"></script>
</body>
</html>