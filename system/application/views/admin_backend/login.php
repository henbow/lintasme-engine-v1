<!DOCTYPE html>
<html lang="en-US"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
    <meta charset="UTF-8">
    <title>Admin Lintas.me</title>
    <link rel="stylesheet" href="/assets/css/bootstrap.css">
    <link rel="stylesheet" href="/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="/assets/css/backend.css">
    <style type="text/css">
        #logincon{
            float: none;
            margin: 0 auto;
        }
    </style>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
</head>
<body>
<div class="container">
    <div class="row-fluid">
        <div class="span6" id="logincon">
            <form action="" class="form-horizontal" method="post">
                <fieldset>
                    <div class="page-header">
                        <h1>Login</h1>
                    </div>
                    <div class="control-group">
                        <label for="" class="control-label">Username</label>
                        <div class="controls">
                            <input type="text" class="input-xlarge" name="lg_user" value="">
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="" class="control-label">Password</label>
                        <div class="controls">
                            <input type="password" class="input-xlarge" name="lg_pass" value="">
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="submit" name="action" class="btn btn-primary" value="Login">
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<script src="http://engine.lintas.me/assets/js/bootstrap.js"></script>
</body>
</html>