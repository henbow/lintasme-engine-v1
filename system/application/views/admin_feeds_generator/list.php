<?php
$this->load->view('admin_feeds_generator/header');
?>
    <div class="page container">
        <?php
        $this->load->view('left_menu');
        ?>
        <div id="page">
            <div class="container-fluid">
                <div class="page-header">
                    <h1>List <small>Feed</small></h1>
                </div>
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>domain</th>
                            <th>url</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach($feeds as $key => $val){
                        ?>
                        <tr>
                            <td><?=$val['domain']?></td>
                            <td><?=$val['url']?></td>
                            <td>
                                <a href="/feeds_generator/feed/<?=$val['id']?>" target="_blank">View</a>
                                <a href="/feeds_generator/edit/<?=$val['id']?>">Edit</a>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
                <style type="text/css">
                    .pagination ul {
                        display: inline-block;
                        margin-left: 0;
                        margin-bottom: 0;
                        -webkit-border-radius: 3px;
                        -moz-border-radius: 3px;
                        border-radius: 3px;
                        -webkit-box-shadow: 0 0px 0px
                        rgba(0, 0, 0, 0.05);
                        -moz-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.05);
                        box-shadow: 0 0px 0px
                        rgba(0, 0, 0, 0.05);
                        width: 100%;
                    }
                </style>
                <div class="pagination">
                    <?php echo $pages ?>
                </div>
            </div>
        </div>
    </div>
<?php
//$this->load->view('admin_feeds_generator/footer');
?>