<?php
$this->load->view('admin_feeds_generator/header');
?>
    <div class="page container">
        <?php
        $this->load->view('admin_feeds_generator/nav_left');
        ?>
        <div id="page">
            <div class="container-fluid">
                <table class="table table-condensed">
                    <thead>
                    <tr>
                        <th>domain</th>
                        <th>url</th>
                        <th>avg_elapsed_time</th>
                        <th>daily_run_times</th>
                        <th>daily_new_post</th>
                        <th>fromtime</th>
                        <th>lastrun</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($feeds as $key => $val){
                        ?>
                        <tr>
                            <td><?=$val['domain']?></td>
                            <td><?=$val['url']?></td>
                            <td><?=$val['avg_elapsed_time']?></td>
                            <td><?=$val['daily_run_times']?></td>
                            <td><?=$val['daily_new_post']?></td>
                            <td><?=$val['fromtime']?></td>
                            <td><?=$val['lastrun']?></td>
                        </tr>

                    <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php
//$this->load->view('admin_feeds_generator/footer');
?>