<?php
$this->load->view('admin_feeds_generator/header');
?>
    <div class="page container">
        <?php
        $this->load->view('left_menu');
        ?>
        <style type="text/css">
            .rand_user, .chsel, .chselactive,.selch,.selsubch,.sel_upload {
                background: none repeat scroll 0 0 #F7F7F7;
                border: 1px solid #CCCCCC;
                color: #B5B5B5;
                cursor: pointer;
                display: inline-block;
                margin-top: -3px;
                padding: 1px 5px;
                margin-bottom: 10px;
            }
            .rand_user.active, .chsel.active, .active.chselactive,.selch.active,.selsubch.active,.sel_upload.active {
                background: none repeat scroll 0 0 #88A991;
                border-color: #4B6853;
                box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.4);
                color: white;
            }
        </style>
        <div id="page">
            <form action="" class="form-horizontal span8" method="post">
                <fieldset>
                    <div class="page-header">
                        <h1>Add <small>Page Patern</small></h1>
                    </div>

                    <div class="control-group">
                        <label for="" class="control-label">Page URL</label>
                        <div class="controls">
                            <div class="input-prepend input-append">
                                <input class="span5" name="page_url" size="16" type="text" value="">
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="" class="control-label">Domain</label>
                        <div class="controls">
                            <div class="input-prepend input-append">
                                <input class="span5" name="domain" size="16" type="text" value="">
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="" class="control-label">Xpath container</label>
                        <div class="controls">
                                <input class="span5" name="xpath_block" size="16" type="text" value="">
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="" class="control-label">Xpath href</label>
                        <div class="controls">
                            <input class="span5" name="xpath_href" size="16" type="text" value="">
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="" class="control-label">Xpath title</label>
                        <div class="controls">
                            <input class="span5" name="xpath_title" size="16" type="text" value="">
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="" class="control-label">Xpath description</label>
                        <div class="controls">
                            <input class="span5" name="xpath_description" size="16" type="text" value="">
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="" class="control-label">Xpath images</label>
                        <div class="controls">
                            <input class="span5" name="xpath_images" size="16" type="text" value="">
                        </div>
                    </div>

                    <div class="form-actions">
                        <input type="submit" class="btn btn-primary" name="act" value="Save">
                        <input type="submit" class="btn" name="act" value="Preview">
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
<?php
//$this->load->view('admin_feeds_generator/footer');
?>