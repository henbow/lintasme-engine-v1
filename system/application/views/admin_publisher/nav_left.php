<?php
$navigate = $this->uri->segment(2)."/".$this->uri->segment(3);
?>
<div class="nav_left" style="width: 200px;float: left;">
    <a href="/publisher/addpublisher_feed" class="btn btn-large btn-danger" style="display:block;margin-bottom: 10px;">Add New Feed</a>
    <div class="well" style="padding: 8px 0;border-radius: 0px;background: white;">    
      <ul class="nav nav-list">
        <li class="nav-header">List feed</li>
        <li <?php if($navigate == 'listpublisher_feed/all'){echo "class=\"active\"";}?> ><a href="/publisher/listpublisher_feed/all">All</a></li>
        <li <?php if($navigate == 'listpublisher_feed/request'){echo "class=\"active\"";}?>><a href="/publisher/listpublisher_feed/request">Request</a></li>
        <li <?php if($navigate == 'listpublisher_feed/approve'){echo "class=\"active\"";}?>><a href="/publisher/listpublisher_feed/approve">Approve</a></li>
        <li <?php if($navigate == 'listpublisher_feed/reject'){echo "class=\"active\"";}?>><a href="/publisher/listpublisher_feed/reject">Rejected</a></li>
        <?php
        if($_SESSION['publisher_user']['is_admin']){
        ?>
        <li <?php if($navigate == 'findpublisher_feed/'){echo "class=\"active\"";}?>><a href="/publisher/findpublisher_feed">FInd</a></li>
        <?php
        }
        ?>
        <?php
        if($_SESSION['publisher_user']['is_admin']){
          ?>
        <li class="nav-header">List Member</li>
        <li <?php if($navigate == 'listpublisher_member/'){echo "class=\"active\"";}?>><a href="/publisher/listpublisher_member">All</a></li>
        <li <?php if($navigate == 'addpublisher_member/'){echo "class=\"active\"";}?>><a href="/publisher/addpublisher_member">Add New</a></li>
        <?php
        }
        ?>
        <li class="nav-header">Reporting</li>
        <li <?php if($this->uri->segment(2) == 'reporting'){echo "class=\"active\"";}?> ><a href="/publisher/reporting">All Post</a></li>
        <li <?php if($this->uri->segment(2) == 'reportingpopular'){echo "class=\"active\"";}?> ><a href="/publisher/reportingpopular">Popular Post</a></li>
        <li class="nav-header">Another list</li>
        <li <?php if($navigate == 'editpublisher_member/'){echo "class=\"active\"";}?>><a href="/publisher/editpublisher_member">Profile</a></li>
        <li <?php if($navigate == 'settingpublisher_member/'){echo "class=\"active\"";}?>><a href="/publisher/settingpublisher_member">Settings</a></li>
        <li class="divider"></li>
        <li><a href="/publisher/logout">Logout</a></li>
      </ul>
    </div>
  </div>