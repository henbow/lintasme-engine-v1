<?php
$this->load->view('admin_publisher/header');
?>
<div class="page container">
  <?php
//$this->load->view('admin_publisher/nav_left');
?>
  <div id="page">
  <form action="" class="form-horizontal span6" method="post">
    <fieldset>
      <div class="page-header">
        <h1>Login</h1>
      </div>
      <?php echo validation_errors(); ?>
      <div class="control-group">
        <label for="" class="control-label">Username</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="username" value="<?php echo set_value('username'); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Password</label>
        <div class="controls">
          <input type="password" class="input-xlarge" name="password" value="" />
        </div>
      </div>
      <div class="form-actions">
        <input type="submit" name="action" class="btn btn-primary" value="Login"/>
      </div>
    </fieldset>
  </form>
  </div>
</div>
<?php
$this->load->view('admin_publisher/footer');
?>