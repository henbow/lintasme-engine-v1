<?php
$this->load->view('admin_publisher/header');
?>
<div class="page container">
  <?php
$this->load->view('admin_publisher/nav_left');
?>
  <div id="page">
  <form action="" class="form-horizontal span6" method="post">
    <fieldset>
      <div class="page-header">
        <h1>Setting <small>Publisher Member</small></h1>
      </div>
      <?php
      if($saved){
        ?>
      <div class="alert alert-success">
        <b>Well done!</b> 
      </div>
        <?php
      }
      ?>
      <?php echo validation_errors(); ?>
      <div class="control-group">
        <label for="" class="control-label">Set Password</label>
        <div class="controls">
          <input type="password" class="input-xlarge" name="password" value="<?php echo set_value('password'); ?>" />
        </div>
      </div>

      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="/publisher/listpublisher_member" class="btn">Cancel</a>
      </div>
    </fieldset>
  </form>
  </div>
</div>
<?php
$this->load->view('admin_publisher/footer');
?>
