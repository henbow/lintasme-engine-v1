<?php
$this->load->view('admin_publisher/header');
?>
<link rel="stylesheet" href="http://www.lintas.me/assets/datepicker/css/datepicker.css" type="text/css" />
<link rel="stylesheet" href="http://www.lintas.me/assets/tablesort/style.css" type="text/css" />

<style type="text/css">
.formfilter {
padding: 5px 15px;
display: block;
margin: 10px 0px;
background: #FAF8F8;
border-bottom: 1px solid #CCC;
border-top: 1px solid #CCC;
}
.legendoptions {
font-weight: bold;
font-size: 15px;
margin-right: 10px;
}
.showoptions {
padding: 10px;
text-align: right;
}
.showoptions a {
display: inline-block;
color: black;
padding: 0px 10px;
background: #F3F3F3;
}
.showoptions a.active {
background: #08C;
color: white;
text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.2);
}
.tdlinkdetail {
margin-left: 10px;
font-size: 11px;
color: #999;
}
.header {
cursor: pointer;
}
div.datepicker a {
font-size: 12px;
}
#table_sort td {
position: relative;
}
#table_sort td a:hover {
background: rgb(245, 245, 245);
text-decoration: none;
}
#table_sort td a.active {
background: #EEE;
}
</style>


<div class="page container">
  <?php
$this->load->view('admin_publisher/nav_left');
?>
  <div id="page">
    <div class="container-fluid">
    <h1>Report - Publisher Member</h1>
    <form class="form-inline formfilter" method="post">
      <div class="input-prepend">
        <span class="add-on">From</span><input class="span2 picker" id="stst_from" name="stat_from" size="16" type="text" placeholder="Date Start" value="<?php echo date("Y/m/d",$params['stat_from']);?>">
      </div>
      <div class="input-prepend">
        <span class="add-on">End</span><input class="span2 picker" id="stst_end" name="stat_end" size="16" type="text" placeholder="Date End"  value="<?php echo date("Y/m/d",$params['stat_end']);?>">
      </div>
      
      <button type="submit" class="btn">Update Report</button>
    </form>
    <div id="container"></div>
    <table class="table table-condensed" id="table_sort">
      <thead>
      <tr>      
        
        <th>Domain</th>
        <th>Total</th>
        <th>Not OK</th>
        <th>Duplicate</th>
        <th>Img FAIL</th>
        <th>No Img</th>
        <th>Latest</th>
        <th>Channel</th>
        <th>Popular</th>              
      </tr>
      </thead>
      <tbody>
      <?php
      $offset = 1;
      foreach($stream as $key => $val){
        $numb = $offset + $key;
		  
        ?>
      <tr>
        <td><?=$val['domain']?></td>
        <td><a <?php if($type == "all"){ ?> class="active" <?php } ?>  href="/publisher/member_report_detail/<?=$val['pnr_pub_member']?>/<?=$params['stat_from']?>/<?=$params['stat_end']?>/all"><?=$val['total']?></a></td>
        <td><a <?php if($type == "nok"){ ?> class="active" <?php } ?> href="/publisher/member_report_detail/<?=$val['pnr_pub_member']?>/<?=$params['stat_from']?>/<?=$params['stat_end']?>/nok"><?=$val['not_ok']?></a></td>
        <td><a <?php if($type == "duplicate"){ ?> class="active" <?php } ?> href="/publisher/member_report_detail/<?=$val['pnr_pub_member']?>/<?=$params['stat_from']?>/<?=$params['stat_end']?>/duplicate"><?=$val['count_duplicate']?></a></td>
        <td><a <?php if($type == "imgfail"){ ?> class="active" <?php } ?> href="/publisher/member_report_detail/<?=$val['pnr_pub_member']?>/<?=$params['stat_from']?>/<?=$params['stat_end']?>/imgfail"><?=$val['count_img_fail']?></a></td>
        <td><a <?php if($type == "noimg"){ ?> class="active" <?php } ?> href="/publisher/member_report_detail/<?=$val['pnr_pub_member']?>/<?=$params['stat_from']?>/<?=$params['stat_end']?>/noimg"><?=$val['count_no_img']?></a></td>
        <td><a <?php if($type == "latest"){ ?> class="active" <?php } ?> href="/publisher/member_report_detail/<?=$val['pnr_pub_member']?>/<?=$params['stat_from']?>/<?=$params['stat_end']?>/latest"><?=$val['sum_latest']?></a></td>
        <td><a <?php if($type == "channel"){ ?> class="active" <?php } ?> href="/publisher/member_report_detail/<?=$val['pnr_pub_member']?>/<?=$params['stat_from']?>/<?=$params['stat_end']?>/channel"><?=$val['sum_channel']?></a></td>
        <td><a <?php if($type == "home"){ ?> class="active" <?php } ?> href="/publisher/member_report_detail/<?=$val['pnr_pub_member']?>/<?=$params['stat_from']?>/<?=$params['stat_end']?>/home"><?=$val['sum_home']?></a></td>
      </tr>
        <?php
      }
      ?>
      </tbody>
    </table>
    
    <?php
    $graph_data = "";
    if(!empty($stream_detail)){
    	$graph_data = "/publisher/publishergraph_range/".$params['stat_from']."/".$params['stat_end']."/".$stream[0]['domain'];
    	?>
    <table class="table table-condensed" id="table_sort">
      <thead>
      <tr>
        <th>Post Id</th>
        <th>Date</th>
        <th>Title</th>
        <th>Status</th>
      </tr>
      </thead>
      <?php
      foreach ($stream_detail as $key => $val) {
          ?>
          <tr>
          	<td>
                <a class="tdlink" target="_blank" href="<?php echo base_url() ?>article/<?=$val['pnr_post_ID']?>"><?=$val['pnr_post_ID']?></a>
            </td>
          	<td><?=$val['pnr_date']?></td>
          	<td>
                <a class="tdlink" target="_blank" href="<?php echo base_url() ?>article/<?=$val['pnr_post_ID']?>"><?=$val['pnr_title']?></a>
            </td>
          	<td><?=$val['pnr_status']?></td>
          </tr>
          <?php
      }
      ?>
      <tbody>
      	
      </tbody>
    </table>
    	<?php
    }
    ?>
    <?php
    $this->load->view('admin_publisher/paging');
    ?>
    </div>
  </div>
</div>
<script type="text/javascript" src="http://www.lintas.me/assets/include/jquery.js"></script>
<script type="text/javascript" src="http://www.lintas.me/assets/scripts/highcharts.js"></script>
<script type="text/javascript" src="http://www.lintas.me/assets/datepicker/js/datepicker.js"></script>
<script type="text/javascript" src="http://www.lintas.me/assets/datepicker/js/eye.js"></script>
<script type="text/javascript" src="http://www.lintas.me/assets/datepicker/js/utils.js"></script>
<script type="text/javascript" src="http://www.lintas.me/scripts/newlintasme_script/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(function () {
    var chart;
    $(document).ready(function() {
        $("#table_sort").tablesorter();
        $('#stst_from').DatePicker({
          format:'Y/m/d',
          date: $(this).val(),
          current: $(this).val(),
          starts: 1,
          onBeforeShow: function(){
          },
          onChange: function(formated, dates){
            $('#stst_from').val(formated);
            $('#stst_from').DatePickerHide();
          }
        });
        $('#stst_end').DatePicker({
          format:'Y/m/d',
          date: $(this).val(),
          current: $(this).val(),
          starts: 1,
          onBeforeShow: function(){
          },
          onChange: function(formated, dates){
            $('#stst_end').val(formated);
            $('#stst_end').DatePickerHide();
          }
        });
        
        <?php
        if(!empty($graph_data)){
        ?>
        $.ajax({
                type: "GET",
                url: '<?=$graph_data;?>',
                beforeSend: function(){
                },
                success: function(data){
                    var graphdata = data;
                    chart = new Highcharts.Chart({
                      chart: {
                          renderTo: 'container',
                          type: 'area',
                          spacingBottom: 30
                      },
                      title: {
                          text: 'Graph Domain Total View Article'
                      },
                      legend: {
                          enabled: false
                      },
                      xAxis: {
                          categories: graphdata.axis
                      },
                      yAxis: {
                          title: {
                              text: 'Total View'
                          },
                          labels: {
                              formatter: function() {
                                  return this.value;
                              }
                          }
                      },
                      tooltip: {
                          formatter: function() {
                              return this.y;
                          }
                      },
                      plotOptions: {
                          area: {
                              fillOpacity: 0.5
                          }
                      },
                      credits: {
                          enabled: false
                      },
                      series: [{
                          name: 'John',
                          data: graphdata.view
                      }]
                  });
                }
        });
        <?php
        }
        ?>
        
    });
    
});
</script>
<?php
$this->load->view('admin_publisher/footer');
?>
