<?php
$this->load->view('admin_publisher/header');
?>
<div class="page container">
  <?php
$this->load->view('admin_publisher/nav_left');
?>
  <div id="page">
    <div class="container-fluid">
    <table class="table table-condensed">
      <thead>
      <tr>      
        
        <th>Title</th>
        <th>Channel name</th>
        <th>Views</th>
        <th>Post time</th>
        <th>Popular Since</th>              
      </tr>
      </thead>
      <tbody>
      <?php
      $offset = 1;
      foreach($stream as $key => $val){
        $numb = $offset + $key;
        ?>
      <tr>
        
        <td>
        <a class="tdlink" href="<?php echo "http://".$val['channel_name'].".lintas.me/article/".$val['domain']."/".$val['link_title_url'];?>"><?=$val['title']?></a>
        
        </td>
        <td><?=$val['channel_name']?></td>
        <td><?=$val['views']?></td>
        <td><?=date('Y-m-d H:i:s',$val['creation_date'])?></td>
        <td><?=date('Y-m-d H:i:s',$val['setpopular'][0]['logtime'])?></td>
      </tr>
        <?php
      }
      ?>
      </tbody>
    </table>
    <?php
    $this->load->view('admin_publisher/paging');
    ?>
    </div>
  </div>
</div>
<?php
$this->load->view('admin_publisher/footer');
?>
