<?php
$this->load->view('admin_publisher/header');
?>
<div class="page container">
  
  <form action="" class="form-horizontal span6" method="post">
    <fieldset>
      <div class="page-header">
        <h1>Edit <small>video channel</small></h1>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Title</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="title" value="<?=$channel['title']?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Description</label>
        <div class="controls">
          <textarea name="description" class="input-xlarge" id="" cols="30" rows="3"><?=$channel['description']?></textarea>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Recomended</label>
        <div class="controls">
          <select name="recomended" id="recomended">
            <option value="0" <?php if($channel['recomended'] == 0){?> selected="selected" <?php } ?>>No</option>
            <option value="1" <?php if($channel['recomended'] == 1){?> selected="selected" <?php } ?>>Yes</option>
          </select>
        </div>
      </div>
      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Save</button>
        <button class="btn">Cancel</button>
      </div>
    </fieldset>
  </form>
</div>
<?php
$this->load->view('admin_publisher/footer');
?>
