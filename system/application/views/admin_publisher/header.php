<!DOCTYPE HTML>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <title></title>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-responsive.css">
  <link rel="stylesheet" media="all" type="text/css" href="http://www.lintas.me/assets/styles/ui.datepicker.css" />
  <style type="text/css">
.page.container {
  margin-top: 55px;
}
  #page {
height: 200px;
padding-left: 200px;
}
  th {
color: 
#424242;
text-transform: uppercase;
font-size: 12px;
}
  .table-condensed th, .table-condensed td {
padding: 10px 5px;
}
  .table tbody tr:hover td, .table tbody tr:hover th {
background-color: #FCFCFC;
}
  .nav > li > a:hover {
background-color: #FCFCFC;
}
  .datepicker_header select {
background: 
#333;
color: 
white;
border: 0px;
font-weight: bold;
width: auto;
height: auto;
line-height: 10px;
margin-bottom: 0px;
padding: 0px;
}
  .page_link.active-page {
background: #E7E7E7;
}
  .tdlink {
color: #333;
}
  </style>
  <script src="/assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="http://www.lintas.me/assets/scripts/ui.datepicker.js"></script>
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="brand" href="#">Lintas Publisher</a>
      <div class="nav-collapse">
        <ul class="nav">
        <?php
        /*
          <li class="active"><a href="<?php echo site_url('pidio/admin_channel')?>">Channel</a></li>
         */
        ?>
        </ul>
      </div>
    </div>
  </div>
</div>