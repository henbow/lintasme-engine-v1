<?php
$this->load->view('admin_publisher/header');
?>
<div class="page container">
  <?php
$this->load->view('admin_publisher/nav_left');
?>
  <div id="page">
    <div class="container-fluid">
    <table class="table table-condensed">
      <thead>
      <tr>      
        
        <th>publisher_member</th>
        <th>category</th>
        <th>topic</th>
        <th>rss_url</th>              
        <th>status</th>
        <th></th>
      </tr>
      </thead>
      <tbody>
      <?php
      $offset = 1;
      foreach($stream as $key => $val){
        $numb = $offset + $key;
        ?>
      <tr>
        
        <td><?=$val['publisher_name']?></td>
        <td><?=$val['category_name']?></td>
        <td><?=$val['topic_name']?></td>
        <td><?=$val['rss_url']?></td>
        <td><?=$val['status']?></td>
        <td>
          <?php
          if($val['status'] == "Draff"){
            ?>
            <a href="/publisher/reqpublisher_feed/<?=$val['id']?>">Send Request</a>
            <?php
          }else{
            ?>
            <a href="/publisher/editpublisher_feed/<?=$val['id']?>">Detail</a>
            <?php
          }
          ?>
          
        </td>
      </tr>
        <?php
      }
      ?>
      </tbody>
    </table>
    <?php
    $this->load->view('admin_publisher/paging');
    ?>
    </div>
  </div>
</div>
<?php
$this->load->view('admin_publisher/footer');
?>
