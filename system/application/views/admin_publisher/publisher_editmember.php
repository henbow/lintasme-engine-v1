<?php
$this->load->view('admin_publisher/header');
?>
<div class="page container">
  <?php
$this->load->view('admin_publisher/nav_left');
?>
  <div id="page">
  <form action="" class="form-horizontal span6" method="post">
    <fieldset>
      <div class="page-header">
        <h1>Edit <small>Publisher Member</small></h1>
      </div>
      <?php
      if($saved){
        ?>
      <div class="alert alert-success">
        <b>Well done!</b> 
      </div>
        <?php
      }
      ?>
      <?php echo validation_errors(); ?>
      <div class="control-group">
        <label for="" class="control-label">Name</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="nama" value="<?php echo set_value('nama',$member['nama']); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Description</label>
        <div class="controls">
          <textarea class="input-xlarge" name="description"><?php echo set_value('description',$member['description']); ?></textarea>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Administrator Name</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="admin_nm" value="<?php echo set_value('admin_nm',$member['admin_nm']); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Company Website Url</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="company_url" value="<?php echo set_value('company_url',$member['company_url']); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Company Contact</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="contact_office" value="<?php echo set_value('contact_office',$member['contact_office']); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Call me at mobile</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="contact_mobile" value="<?php echo set_value('contact_mobile',$member['contact_mobile']); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Twitter Account</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <span class="add-on">@</span><input class="span2" id="appendedPrependedInput" name="contact_tw" size="16" type="text" value="<?php echo set_value('contact_tw',$member['contact_tw']); ?>">
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Facebook Profile URL</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="fb_profile_url" value="<?php echo set_value('fb_profile_url',$member['fb_profile_url']); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Google Plus URL</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="google_plus_url" value="<?php echo set_value('google_plus_url',$member['google_plus_url']); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Email</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="contact_email" value="<?php echo set_value('contact_email',$member['contact_email']); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Background Image</label>
        <div class="controls">
          <input type="file" name="bg_file" value="" id="bg_file"/>
        </div>
      </div>
      <?php
      if($_SESSION['publisher_user']['is_admin']){
        ?>
      <div class="control-group">
        <label for="" class="control-label"></label>
        <div class="controls">
          <a href="/publisher/deletepublisher_member/<?=$member['id']?>" class="btn">Delete</a>
        </div>
      </div>
        <?php
      }
      ?>
      

      <div class="form-actions">
        
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="/publisher/listpublisher_member" class="btn">Cancel</a>
      </div>
    </fieldset>
  </form>
  </div>
</div>

<?php
$this->load->view('admin_publisher/footer');
?>
