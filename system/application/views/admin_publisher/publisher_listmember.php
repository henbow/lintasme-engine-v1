<?php
$this->load->view('admin_publisher/header');
?>
<div class="page container">
  <?php
$this->load->view('admin_publisher/nav_left');
?>
  <div id="page">
    <div class="container-fluid">
    <table class="table table-condensed">
      <thead>
      <tr>      
        <th>create dt</th>
        <th>nama</th>
        <th>username</th>
        <th>admin name</th>
        <th>contact_mobile</th>
        <th>email</th>              
        <th></th>
      </tr>
      </thead>
      <tbody>
      <?php
      $offset = 1;
      foreach($stream as $key => $val){
        $numb = $offset + $key;
        ?>
      <tr>
        <td><?=$val['create_dt']?></td>
        <td><?=$val['nama']?></td>
        <td><?=$val['username']?></td>
        <td><?=$val['admin_nm']?></td>
        <td><?=$val['contact_mobile']?></td>
        <td><?=$val['contact_email']?></td>
        <td><a href="/publisher/editpublisher_member/<?=$val['id']?>">Detail</a></td>
      </tr>
        <?php
      }
      ?>
      </tbody>
    </table>
    <?php
    $this->load->view('admin_publisher/paging');
    ?>
    </div>
  </div>
</div>
<?php
$this->load->view('admin_publisher/footer');
?>
