<?php
$this->load->view('admin_publisher/header');
?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/datepicker/css/datepicker.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/tablesort/style.css" type="text/css" />

<style type="text/css">
.formfilter {
padding: 5px 15px;
display: block;
margin: 10px 0px;
background: #FAF8F8;
border-bottom: 1px solid #CCC;
border-top: 1px solid #CCC;
}
.legendoptions {
font-weight: bold;
font-size: 15px;
margin-right: 10px;
}
.showoptions {
padding: 10px;
text-align: right;
}
.showoptions a {
display: inline-block;
color: black;
padding: 0px 10px;
background: #F3F3F3;
}
.showoptions a.active {
background: #08C;
color: white;
text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.2);
}
.tdlinkdetail {
margin-left: 10px;
font-size: 11px;
color: #999;
}
.header {
cursor: pointer;
}
</style>


<div class="page container">
  <?php
$this->load->view('admin_publisher/nav_left');
?>
  <div id="page">
    <div class="container-fluid">
    <h1>Report - <?php echo $domain;?></h1>
    <form class="form-inline formfilter" method="post">
      <select class="span2" name="category" id="category">
          <option value="">All Channel</option>
          <?php foreach($channel_tree as $parent => $children): ?>
          <optgroup label='<?php echo ucfirst($children['name'])?>'>
              <option value="<?php echo $children['id']?>" <?php if($channel == $children['id']){ echo "selected=\"selected\""; } ?>><?php echo ucfirst($children['name'])?> All</option>
              <?php foreach($children['child'] as $key => $val) :?>
              <option value="<?php echo $val['id']?>" <?php if($channel == $val['id']){ echo "selected=\"selected\""; } ?>><?php echo ucfirst($val['name'])?></option>
              <?php endforeach;?>
          </optgroup>
          <?php endforeach;?>
      </select>
      <select class="span2" name="source" id="source">
        <option value="">All Source</option>
        
      </select>
      <div class="input-prepend">
        <span class="add-on">From</span><input class="span2 picker" id="stst_from" name="stat_from" size="16" type="text" placeholder="Date Start" value="<?=$start?>">
      </div>
      <div class="input-prepend">
        <span class="add-on">End</span><input class="span2 picker" id="stst_end" name="stat_end" size="16" type="text" placeholder="Date End"  value="<?=$end?>">
      </div>
      
      <button type="submit" class="btn">Update Report</button>
    </form>
    <div id="container"></div>
    <div class="showoptions">
      <span class="legendoptions">Show :</span>
      <a href="<?php echo '/publisher/reporting/'.$page_data['channel']."/".$page_data['source']."/".$page_data['start'].'/'.$page_data['end'].'/20';?>" <?php if($page_data['limit'] == '20'){ echo " class=\"active\"" ;};?>>20</a>
      <a href="<?php echo '/publisher/reporting/'.$page_data['channel']."/".$page_data['source']."/".$page_data['start'].'/'.$page_data['end'].'/50';?>" <?php if($page_data['limit'] == '50'){ echo " class=\"active\"" ;};?>>50</a>
      <a href="<?php echo '/publisher/reporting/'.$page_data['channel']."/".$page_data['source']."/".$page_data['start'].'/'.$page_data['end'].'/100';?>" <?php if($page_data['limit'] == '100'){ echo " class=\"active\"" ;};?>>100</a>
    </div>
    <table class="table table-condensed" id="table_sort">
      <thead>
      <tr>      
        
        <th>Title</th>
        <th>Channel name</th>
        <th>Views</th>
        <th>Post time</th>              
      </tr>
      </thead>
      <tbody>
      <?php
      $offset = 1;
      foreach($stream as $key => $val){
        $numb = $offset + $key;
        ?>
      <tr>
        
        <td>
        <a class="tdlink" href="<?php echo "http://".$val['channel_name'].".lintas.me/article/".$val['domain']."/".$val['link_title_url'];?>"><?=$val['title']?></a>
        <a class="tdlinkdetail" href="/publisher/reportingdetail/<?php echo $val['id'];?>">View detail</a>
        </td>
        <td><?=$val['channel_name']?></td>
        <td><?=$val['views']?></td>
        <td><?=date('Y-m-d H:i:s',$val['creation_date'])?></td>
      </tr>
        <?php
      }
      ?>
      </tbody>
    </table>
    <?php
    $this->load->view('admin_publisher/paging');
    ?>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url() ?>assets/include/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/scripts/highcharts.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/datepicker/js/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/datepicker/js/eye.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/datepicker/js/utils.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>scripts/newlintasme_script/jquery.tablesorter.min.js"></script>  
<script type="text/javascript">
$(function () {
    var chart;
    $(document).ready(function() {
        $("#table_sort").tablesorter();
        $('#stst_from').DatePicker({
          format:'Y/m/d',
          date: $(this).val(),
          current: $(this).val(),
          starts: 1,
          onBeforeShow: function(){
          },
          onChange: function(formated, dates){
            $('#stst_from').val(formated);
            $('#stst_from').DatePickerHide();
          }
        });
        $('#stst_end').DatePicker({
          format:'Y/m/d',
          date: $(this).val(),
          current: $(this).val(),
          starts: 1,
          onBeforeShow: function(){
          },
          onChange: function(formated, dates){
            $('#stst_end').val(formated);
            $('#stst_end').DatePickerHide();
          }
        });
        <?php
        if(empty($end_timestamp)){
          if(empty($start_timestamp)){
            $graph_data = "/publisher/publishergraph/8";
          }else{
            $graph_data = "/publisher/publishergraph_range/".$start_timestamp."/".time();
          }
          
        }else{
          $graph_data = "/publisher/publishergraph_range/".$start_timestamp."/".$end_timestamp;
        }
        ?>
        $.ajax({
                type: "GET",
                url: '<?=$graph_data;?>',
                beforeSend: function(){
                },
                success: function(data){
                    var graphdata = data;
                    chart = new Highcharts.Chart({
                      chart: {
                          renderTo: 'container',
                          type: 'area',
                          spacingBottom: 30
                      },
                      title: {
                          text: 'Graph Domain Total View Article'
                      },
                      legend: {
                          enabled: false
                      },
                      xAxis: {
                          categories: graphdata.axis
                      },
                      yAxis: {
                          title: {
                              text: 'Total View'
                          },
                          labels: {
                              formatter: function() {
                                  return this.value;
                              }
                          }
                      },
                      tooltip: {
                          formatter: function() {
                              return this.y;
                          }
                      },
                      plotOptions: {
                          area: {
                              fillOpacity: 0.5
                          }
                      },
                      credits: {
                          enabled: false
                      },
                      series: [{
                          name: 'John',
                          data: graphdata.view
                      }]
                  });
                }
        });
        
    });
    
});
</script>
<?php
$this->load->view('admin_publisher/footer');
?>
