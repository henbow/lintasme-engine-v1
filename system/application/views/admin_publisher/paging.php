<style type="text/css">
.pagination ul {
display: inline-block;
margin-left: 0;
margin-bottom: 0;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;
-webkit-box-shadow: 0 0px 0px 
rgba(0, 0, 0, 0.05);
-moz-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.05);
box-shadow: 0 0px 0px 
rgba(0, 0, 0, 0.05);
width: 100%;
}
</style>
<?
if($page['totalpage'] > 1){
?>
<div class="pagination">
        <ul class="stream_page">
         <?php
         //$page['totalpage'] = 1000;
$start = 1;
$slice = 4;
$limit = 5;

if($page['position']!= 1){
$pageprev = $page['position'] - 1;
  $nofirst_round = TRUE;
    //if(! $this->uri->segment(1) == 'source') {
?>
            <li class="round-left"><a class="page_link round-left" href="<?php echo site_url($page['control']."/1");?>"> First</a></li>
<?php
    //} 
}

if (($page['position'] + $slice) < $page['totalpage']) {
$this_far = $page['position'] + $slice;
} else {
$this_far = $page['totalpage'];
}

if (($start + $page['position']) >= 4 && ($page['position'] - 4) > 0) {
$start = $page['position'] - 4;
}

for ($i = $start; $i <= $this_far; $i++){
  if($i==$start){
    if(empty($nofirst_round)){
      $cl="round-left";
    }else{
      $cl="";
    }
    }else {
        $cl="";
  }
if($i == $page['position']){
?>
          <li class="<?=$cl;?>"><div class="css-arrow"></div><a class="page_link active-page <?=$cl;?>" href="<?php echo site_url($page['control']."/".$i);?>"><strong><?php echo $i;?></strong></a></li>
<?php
}else{
?>
          <li class="<?=$cl;?>"><a class="page_link active <?=$cl;?>" href="<?php echo site_url($page['control']."/".$i);?>"><?php echo $i;?></a></li>
<?php
}
}

if(($page['totalpage'] - ($limit * $page['position'])) > 0){
$pagenext = $page['position'] + 1;
?>

          <li class="round-right"><a class="page_link round-right" href="<?php echo site_url($page['control']."/".$page['totalpage']);?>">Last</a></li>
<?php
}else{
}
         
         
         
        ?>
        </ul>
        </div>
<?php
}
?>