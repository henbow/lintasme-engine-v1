<?php
$this->load->view('admin_publisher/header');
?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/datepicker/css/datepicker.css" type="text/css" />
<style type="text/css">
.formfilter {
padding: 5px 15px;
display: block;
margin: 10px 0px;
background: #FAF8F8;
border-bottom: 1px solid #CCC;
border-top: 1px solid #CCC;
}
.legendoptions {
font-weight: bold;
font-size: 15px;
margin-right: 10px;
}
.showoptions {
padding: 10px;
text-align: right;
}
.showoptions a {
display: inline-block;
color: black;
padding: 0px 10px;
background: #F3F3F3;
}
.showoptions a.active {
background: #08C;
color: white;
text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.2);
}
.tdlinkdetail {
margin-left: 10px;
font-size: 11px;
color: #999;
}
</style>


<div class="page container">
  <?php
$this->load->view('admin_publisher/nav_left');
?>
  <div id="page">
    <div class="container-fluid">
    <h1>Report - <?php echo $domain;?></h1>
    <h3><?php echo $detail['article']['title'];?></h3>
    <div id="container"></div>
    <table  class="table table-condensed span5">
    <tbody>
      <tr>
        <td>Total View</td>
        <td><?php echo $detail['article']['views'];?></td>
      </tr>
      <tr>
        <td>Total Share</td>
        <td>
          <?php echo $detail['article']['social_count']['fb'];?> Facebook
          <?php echo $detail['article']['social_count']['tw'];?> Twitter
          <?php echo $detail['article']['social_count']['gg'];?> Google
        </td>
      </tr>
    </tbody>
    </table>
    
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url() ?>assets/include/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/scripts/highcharts.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/datepicker/js/datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/datepicker/js/eye.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/datepicker/js/utils.js"></script>
  
<script type="text/javascript">
$(function () {
    var chart;
    $(document).ready(function() {
        $('#stst_from').DatePicker({
          format:'Y/m/d',
          date: $(this).val(),
          current: $(this).val(),
          starts: 1,
          onBeforeShow: function(){
          },
          onChange: function(formated, dates){
            $('#stst_from').val(formated);
            $('#stst_from').DatePickerHide();
          }
        });
        $('#stst_end').DatePicker({
          format:'Y/m/d',
          date: $(this).val(),
          current: $(this).val(),
          starts: 1,
          onBeforeShow: function(){
          },
          onChange: function(formated, dates){
            $('#stst_end').val(formated);
            $('#stst_end').DatePickerHide();
          }
        });
                    chart = new Highcharts.Chart({
                      chart: {
                          renderTo: 'container',
                          type: 'area',
                          spacingBottom: 30
                      },
                      title: {
                          text: 'Graph Article View'
                      },
                      subtitle: {
                          text: '<?php echo $detail['article']['title'];?>'
                      },
                      legend: {
                          enabled: false
                      },
                      xAxis: {
                          categories: <?php echo json_encode($detail['graph']['axis']);?>
                      },
                      yAxis: {
                          title: {
                              text: 'Total Views'
                          },
                          labels: {
                              formatter: function() {
                                  return this.value;
                              }
                          }
                      },
                      tooltip: {
                          formatter: function() {
                              return this.y;
                          }
                      },
                      plotOptions: {
                          area: {
                              fillOpacity: 0.5
                          }
                      },
                      credits: {
                          enabled: false
                      },
                      series: [{
                          name: 'John',
                          data: <?php echo json_encode($detail['graph']['view']);?>
                      }]
                  });

        
    });
    
});
</script>
<?php
$this->load->view('admin_publisher/footer');
?>
