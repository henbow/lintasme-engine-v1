<?php
$this->load->view('admin_publisher/header');
?>
<div class="page container">
  <?php
$this->load->view('admin_publisher/nav_left');
?>
  <div id="page">
  <form action="" class="form-horizontal span6" method="post">
    <fieldset>
      <div class="page-header">
        <h1>Detail <small>Publisher Feed</small></h1>
      </div>
      <?php echo validation_errors(); ?>
      <div class="control-group">
        <label for="" class="control-label">Publisher</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="publisher_name" disabled="true" value="<?php echo set_value('publisher_member',$member['publisher_name']); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Category</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="category_name" disabled="true" value="<?php echo set_value('category_name',$member['category_name']); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Topic</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="topic_name" disabled="true" value="<?php echo set_value('topic_name',$member['topic_name']); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">RSS URL</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="rss_url" disabled="true" value="<?php echo set_value('rss_url',$member['rss_url']); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Status</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="status" disabled="true" value="<?php echo set_value('status',$member['status']); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Create Date</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="create_dt" disabled="true" value="<?php echo set_value('create_dt',$member['create_dt']); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Request Date</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="request_dt" disabled="true" value="<?php echo set_value('request_dt',$member['request_dt']); ?>" />
        </div>
      </div>
      <?php
        if($_SESSION['publisher_user']['is_admin']){
      ?>
      <div class="control-group">
        <label for="" class="control-label">Start Date</label>
        <div class="controls">
          <input type="text" class="input-xlarge date" name="start_dt" value="<?php echo set_value('start_dt',substr($member['start_dt'],0,10)); ?>" />
          <span class="help-block">Format : yyyy-mm-dd</span>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">End Date</label>
        <div class="controls">
          <input type="text" class="input-xlarge date" name="end_dt" value="<?php echo set_value('end_dt',substr($member['end_dt'],0,10)); ?>" />
          <span class="help-block">Format : yyyy-mm-dd</span>
        </div>
      </div>
            <div class="control-group">
                <label for="" class="control-label">Crawl Method</label>
                <div class="controls">
                    <div class="input-prepend input-append">
                        <select name="crawl_type">
                            <option <?php if($member['use_reader'] == 0 ) { ?> selected="selected" <?php } ?> value="0">Parse RSS Feed directly (default)</option>
                            <option <?php if($member['use_reader'] == 1 ) { ?> selected="selected" <?php } ?> value="1">Parse RSS Feed using Reader</option>
                            <option <?php if($member['use_reader'] == 2 ) { ?> selected="selected" <?php } ?> value="2">Parse RSS Feed using Reader and find image in original URL</option>
                            <option <?php if($member['use_reader'] == 3 ) { ?> selected="selected" <?php } ?> value="3">Parse RSS Feed directly and find image in original URL</option>
                        </select>
                        <input type="button" name="test_pattern" value="Test Pattern" />
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label for="" class="control-label">Title Pattern</label>
                <div class="controls">
                    <div class="input-prepend input-append">
                        <input class="span4" id="appendedPrependedInput" name="title_pattern" size="16" type="text" value='<?php echo set_value('title_pattern',$member['title_tag']); ?>'>
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label for="" class="control-label">Description Pattern</label>
                <div class="controls">
                    <div class="input-prepend input-append">
                        <input class="span4" id="appendedPrependedInput" name="desc_pattern" size="16" type="text" value='<?php echo set_value('desc_pattern',$member['description_tag']); ?>'>
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label for="" class="control-label">Image Pattern</label>
                <div class="controls">
                    <div class="input-prepend input-append">
                        <input class="span4" id="appendedPrependedInput" name="image_pattern" size="16" type="text" value='<?php echo set_value('image_pattern',$member['image_tag']); ?>'>
                    </div>
                </div>
            </div>
      <?php
        }
      ?>
      <div class="form-actions">
        <input type="submit" name="action" class="btn btn-danger" value="Delete"/>
        <?php
        if($_SESSION['publisher_user']['is_admin']){
          ?>
        <input type="submit" name="action" class="btn btn-primary" value="Approve"/>
        <input type="submit" name="action" class="btn" value="Reject"/>
        <?php
        }
        ?>
      </div>
    </fieldset>
  </form>
  </div>
</div>
<a id="pattern_tester" href="#pattern_tester_w"></a>
<div id="pattern_tester_w" style="display:none">
    <h4>Pattern Test Result</h4>
    <table border="0" cellpadding="1">
        <tr>
            <td width="100">Title</td>
            <td valign="top">:</td>
            <td valign="top"><span id="titlep"></span></td>
        </tr>
        <tr>
            <td valign="top">Description</td>
            <td valign="top">:</td>
            <td valign="top"><span id="descp"></span></td>
        </tr>
        <tr>
            <td valign="top">Orig link</td>
            <td valign="top">:</td>
            <td valign="top"><span id="linkp"></span></td>
        </tr>
        <tr>
            <td valign="top">Link</td>
            <td valign="top">:</td>
            <td valign="top"><span id="guidp"></span></td>
        </tr>
        <tr>
            <td valign="top">GUID</td>
            <td valign="top">:</td>
            <td valign="top"><span id="pubdatep"></span></td>
        </tr>
        <tr>
            <td valign="top">Pub date</td>
            <td valign="top">:</td>
            <td valign="top"><span id="pubdatep"></span></td>
        </tr>
        <tr>
            <td valign="top">Image</td>
            <td valign="top">:</td>
            <td valign="top"><span id="imgp"><img src="" width="200" /></span></td>
        </tr>
        <tr>
            <td valign="top">Author</td>
            <td valign="top">:</td>
            <td valign="top"><span id="authorp"></span></td>
        </tr>
        <tr>
            <td valign="top">Format</td>
            <td valign="top">:</td>
            <td valign="top"><span id="formatp"></span></td>
        </tr>
        <tr>
            <td valign="top">Language</td>
            <td valign="top">:</td>
            <td valign="top"><span id="langp"></span></td>
        </tr>
    </table>
</div>
<script type="text/javascript" src="http://engine.lintas.me/assets/js/fancybox/jquery.fancybox.pack.js"></script>
<link rel="stylesheet" href="http://engine.lintas.me/assets/js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<script type="text/javascript">
$(document).ready(function (){
    $(".date").datepicker({
      dateFormat : 'yy-mm-dd'
      }
    );

    $("a#pattern_tester").fancybox();
    $('input[name="test_pattern"]').click(function(){
        $(this).val('Process...');
        $.post('http://engine.lintas.me/tools/pattern_tester.php',{
            url: '<?=$member['rss_url']?>',
            type: $('select[name="crawl_type"]').val(),
            title_pattern: $('input[name="title_pattern"]').val(),
            desc_pattern: $('input[name="desc_pattern"]').val(),
            image_pattern: $('input[name="image_pattern"]').val()
        }).done(function(resp) {
                $('input[name="test_pattern"]').val('Test Pattern');
                $('span#titlep').text(resp.title);
                $('span#descp').text(resp.description);
                $('span#origlinkp').text(resp.origlink);
                $('span#linkp').text(resp.link);
                $('span#guidp').text(resp.guid);
                $('span#pubdatep').text(resp.pubdate);
                $('span#imgp img').attr('src', resp.image);
                $('span#authorp').text(resp.dc_author);
                $('span#langp').text(resp.dc_language);
                $('span#formatp').text(resp.dc_format);
                $("a#pattern_tester").click();
            });
    });
    $("span.selch").click(function() {
        $('span.selch').removeClass('active');
        $("div.subchcon").hide();
        $("div.topic_select").show();
        $(this).addClass('active');
        var subch_sel = $(this).attr('data-chid');
        var subch_name = $(this).attr('data-name');
        $("#category").val(subch_sel);
        $("#category_name").val(subch_name);
        $("#subch_" + subch_sel).show();
        $("span.selsubch").click(function() {
            $('span.selsubch').removeClass('active');
            $(this).addClass('active');
            var subch2_sel = $(this).attr('data-chid');
            $("#topic").val(subch2_sel);
        });
    });
});
</script>
<?php
$this->load->view('admin_publisher/footer');
?>
