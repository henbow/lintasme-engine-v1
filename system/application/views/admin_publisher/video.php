<?php
$this->load->view('admin/header');
?>
<div class="page container">
  <div class="page-header">
    <h1>List <small>video channel</small></h1>
  </div>
  <table class="table table-striped">
    <thead>
    <tr>
      <th>No</th>
      <th>Author</th>
      <th>Title</th>
      <th>Channel</th>
      <th>Description</th>
      <th>Recomended</th>      
      <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $offset = 1;
    foreach($channel as $key => $val){
      $numb = $offset + $key;
      ?>
    <tr>
      <td><?=$numb?></td>
      <td><?=$val['author']?></td>
      <td><?=$val['title']?></td>
      <td><?=$val['chname']?></td>
      <td><?=$val['description']?></td>
      <td>
      <?php
      if($val['recomended']){
        echo "Yes";
      }else{
        echo "No";
      }
      ?>
      </td>
      <td>
        <div class="btn-group">
          <a class="btn" href="<?php echo site_url('pidio/channel_edit/'.$val['channel_id']);?>"><i class="icon-pencil"></i> Edit</a>
          <a class="btn" href="#"><i class="icon-remove"></i> Delete</a>
        </div>
      </td>
    </tr>
      <?php
    }
    ?>
    </tbody>
  </table>
</div>  
<?php
$this->load->view('admin/footer');
?>