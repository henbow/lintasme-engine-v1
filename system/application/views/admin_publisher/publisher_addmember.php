<?php
$this->load->view('admin_publisher/header');
?>
<div class="page container">
  <?php
$this->load->view('admin_publisher/nav_left');
?>
  <div id="page">
  <form action="" class="form-horizontal span6" method="post">
    <fieldset>
      <div class="page-header">
        <h1>Add <small>Publisher Member</small></h1>
      </div>
      <?php
      if($saved){
        ?>
      <div class="alert alert-success">
        <b>Well done!</b> 
      </div>
        <?php
      }
      ?>
      <?php echo validation_errors(); ?>
      <div class="control-group">
        <label for="" class="control-label">Name</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="nama" value="<?php echo set_value('nama'); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Domain</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="domain" value="<?php echo set_value('domain'); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Username</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="username" value="<?php echo set_value('username'); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Password</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="password" value="<?php echo set_value('password'); ?>" />
        </div>
      </div>
      
      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="/publisher/listpublisher_member" class="btn">Cancel</a>
      </div>
    </fieldset>
  </form>
  </div>
</div>
<?php
$this->load->view('admin_publisher/footer');
?>
