<?php
$this->load->view('admin_publisher/header');
?>
<div class="page container">
  <?php
$this->load->view('admin_publisher/nav_left');
?>
  <div id="page">
  <form action="" class="form-horizontal span6" method="post">
    <fieldset>
      <div class="page-header">
        <h1>Filter <small>Publisher Feed</small></h1>
      </div>
      <?php
      if($saved){
        ?>
      <div class="alert alert-success">
        <b>Well done!</b> 
      </div>
        <?php
      }
      ?>


      <?php echo validation_errors(); ?>
      <div class="control-group">
        <label for="" class="control-label">Member</label>
        <div class="controls">
          <select name="publisher_member" id="">
            <option value="">All</option>
            <?php foreach($member as $key => $val){ ?>
            <option value="<?=$val['id']?>"><?=$val['nama']?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Category</label>
        <div class="controls">
          <select name="category" id="">
            <option value="">All</option>
            <?php foreach($channel_tree as $key => $val){ ?>
            <option value="<?=$val['id']?>"><?=$val['name']?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Status</label>
        <div class="controls">
          <select name="status" id="">
            <option value="">All</option>
            <option value="1">Requested</option>
            <option value="3">Approve</option>
            <option value="2">Reject</option>
            <option value="4">Deleted</option>
          </select>
        </div>
      </div>
      
      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="/publisher/listpublisher_member" class="btn">Cancel</a>
      </div>
    </fieldset>
  </form>
  </div>
</div>
<?php
$this->load->view('admin_publisher/footer');
?>
