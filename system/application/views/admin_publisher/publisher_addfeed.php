<?php
$this->load->view('admin_publisher/header');
?>
<div class="page container">
  <?php
$this->load->view('admin_publisher/nav_left');
?>
<style type="text/css">
.rand_user, .chsel, .chselactive,.selch,.selsubch,.sel_upload {
    background: none repeat scroll 0 0 #F7F7F7;
    border: 1px solid #CCCCCC;
    color: #B5B5B5;
    cursor: pointer;
    display: inline-block;
    margin-top: -3px;
    padding: 1px 5px;
    margin-bottom: 10px;
}
.rand_user.active, .chsel.active, .active.chselactive,.selch.active,.selsubch.active,.sel_upload.active {
    background: none repeat scroll 0 0 #88A991;
    border-color: #4B6853;
    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.4);
    color: white;
}
</style>
  <div id="page">
  <form action="" class="form-horizontal span6" method="post">
    <fieldset>
      <div class="page-header">
        <h1>Register <small>Publisher RSS Feed</small></h1>
      </div>
      <?php
      if($saved){
        ?>
      <div class="alert alert-success">
        <b>Well done!</b> 
      </div>
        <?php
      }
      ?>
      <?php echo validation_errors(); ?>
      <div class="control-group">
        <label for="" class="control-label">Category</label>
        <div class="controls">
          <?php
            foreach($channel_tree as $key => $val){
                ?>
                <span class="selch" data-chid="<?php echo $val['id'];?>"><?php echo ucfirst($val['name']);?></span>
                <?php
            }
          ?>
          <input type="hidden" class="input-xlarge" name="category" id="category" value="<?php echo set_value('category'); ?>" />
        </div>
      </div>
      <div class="control-group topic_select" style="display:none;">
        <label for="" class="control-label">Topic</label>
        <div class="controls">
              <div class="subchcon">
                Select Category First
              </div>
          <?php
          foreach($channel_tree as $key => $val){
              ?>
              <div id="subch_<?php echo $val['id'];?>" class="subchcon" style="display:none;">
                <?php
                foreach($val['child'] as $key2 => $val2){
                  ?>
                  <span class="selsubch" data-chid="<?php echo $val2['id'];?>"><?php echo ucfirst($val2['name']);?></span>
                  <?php
                }
                ?>
              </div>
              
              <?php
          }
          ?>
          <input type="hidden" class="input-xlarge" name="topic" id="topic" value="<?php echo set_value('topic'); ?>" />
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">RSS URL</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <span class="add-on">http://</span><input class="span3" id="appendedPrependedInput" name="rss_url" size="16" type="text" value="<?php echo str_replace("http://","",set_value('rss_url')); ?>">
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label"></label>
        <div class="controls">
          <label class="checkbox">
            <input type="checkbox" id="optionsCheckbox" name="request" checked="checked" value="1">
            Request for approval
          </label>
        </div>
      </div>
      
      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="/publisher/listpublisher_feed/all" class="btn">Cancel</a>
      </div>
    </fieldset>
  </form>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){    
    $("span.selch").click(function() {
      $('span.selch').removeClass('active');
      $("div.subchcon").hide();
      $("div.topic_select").show();
      $(this).addClass('active');
      var subch_sel = $(this).attr('data-chid');
      $("#category").val(subch_sel);
      $("#subch_" + subch_sel).show();
      $("span.selsubch").click(function() {
        $('span.selsubch').removeClass('active');
        $(this).addClass('active');
        var subch2_sel = $(this).attr('data-chid');
        $("#topic").val(subch2_sel);
      });
    });
  });
</script>
<?php
$this->load->view('admin_publisher/footer');
?>