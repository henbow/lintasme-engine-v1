<div class="page-header">
    <h3>
        Detail Post All Domain
    </h3>
</div>
<table class="table table-condensed tablesorter">
    <thead>
    <tr>
        <th>
            Domain Name
        </th>
        <?php
        foreach($xAxis['categories'] as $key => $val){
        ?>
            <th><?=$val?></th>
        <?php
        }
        ?>

    </tr>
    </thead>
    <tbody>
    <?php
    foreach($series as $key => $val){
        ?>
        <tr>
            <td><?=$val['name']?></td>
            <?php
            foreach($val['data'] as $key2 => $val2){
            ?>
                <td><?=$val2?></td>
            <?php
            }
            ?>
        </tr>
    <?php
    }
    ?>
    </tbody>
</table>