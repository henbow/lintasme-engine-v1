<div class="page container">
  <?php $this->load->view('left_menu') ?>  
  <div id="page">
  <form action="<?php echo site_url('content_classification/train') ?>" class="form-horizontal span6" method="post">
    <fieldset>
      <div class="page-header">
        <h1>Train <small>Classifier</small></h1>
      </div>    
      <fieldset>  
      	<legend style="margin-bottom: 5px;">Input Text</legend>
      <div class="control-group">
            <textarea class="span6" name="content" rows="10" cols="60"></textarea>          
      </div>
      </fieldset>
      <fieldset>
      	<legend style="margin-bottom: 5px;">Classify as</legend>
	      <div class="control-group">
			  <?php
			  foreach($category as $cat):
			  ?>
              <span class="selch" data-chid="<?php echo $cat['name'] ?>"><?php echo $cat['displayName'] ?></span>
              <?php
              endforeach;
              ?>      
	      </div>
	      <div class="control-group topic_select" style="display:none;">
	              <div class="subchcon">
	                Select Category First
	              </div>
	              <?php 
	              foreach($category as $cat):
	              ?>
	              <div id="subch_<?php echo $cat['name'] ?>" class="subchcon" style="display:none;">
	              <?php 
	              $ch = curl_init();
	              
	              curl_setopt($ch,CURLOPT_URL, "http://www.lintas.me/rest/explore_topic/".$cat['name']);
	              curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);
	              
	              //execute post
	              $topics = json_decode(curl_exec($ch), TRUE);
	              $topics = $topics['topic'];
	              
	              //close connection
	              curl_close($ch);
	              
	              if(count($topics) > 0):
	              foreach ($topics as $topic):
	              ?>
	              <span class="selsubch" data-chid="<?php echo $topic['tagname'] ?>"><?php echo ucfirst($topic['tagname']) ?></span>
	              <?php 
	              endforeach;
	              endif;
	              ?>
				  </div>              
	              <?php 
	              endforeach;
	              ?>
	      </div>
	      <div class="control-group">
              <span class="badcontent" data-chid="bet">Bet</span>
              <span class="badcontent" data-chid="porno">Porn</span>
              <span class="badcontent" data-chid="bajakan">Bajakan</span>
              <span class="badcontent" data-chid="sara">SARA</span>
              <span class="badcontent" data-chid="jualobat">Jual Obat</span>
	      </div>
	      <div class="control-group">
              <span class="additionalcontent" data-chid="bursatransfer">Bursa Transfer</span>
	      </div>
	      <input type="hidden" class="input-xlarge" name="classified_as" id="topic" value="" />
      </fieldset>
      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Train Classifier</button>
      </div>
    </fieldset>
  </form>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("span.selch").click(function() {
    	$('span.selch').removeClass('active');
    	$('span.badcontent').removeClass('active');
    	$('span.additionalcontent').removeClass('active');
    	$("div.subchcon").hide();
	    $("div.topic_select").show();
	    $(this).addClass('active');
	    var subch_sel = $(this).attr('data-chid');
      	var subch_name = $(this).attr('data-name');
      	$("#category").val(subch_sel);
      	$("#category_name").val(subch_name);
      	$("#subch_" + subch_sel).show();
    	$('#topic').val($(this).attr('data-chid'));
    });
    $("span.selsubch").click(function() {
        $('span.selsubch').removeClass('active');
        $('span.badcontent').removeClass('active');
        $('span.additionalcontent').removeClass('active');
        $(this).addClass('active');
        var subch2_sel = $(this).attr('data-chid');
        $("#topic").val(subch2_sel);
  	});
  	$("span.badcontent").click(function() {
        $('span.selch').removeClass('active');
        $('span.selsubch').removeClass('active');
        $('span.badcontent').removeClass('active');
        $('span.additionalcontent').removeClass('active');
        $(this).addClass('active');
        $("div.subchcon").hide();
        $("div.topic_select").hide();
        var badcontent_sel = $(this).attr('data-chid');
        $("#topic").val(badcontent_sel);
  	});
  	$("span.additionalcontent").click(function() {
        $('span.selch').removeClass('active');
        $('span.selsubch').removeClass('active');
        $('span.badcontent').removeClass('active');
        $('span.additionalcontent').removeClass('active');
        $(this).addClass('active');
        $("div.subchcon").hide();
        $("div.topic_select").hide();
        var additionalcontent_sel = $(this).attr('data-chid');
        $("#topic").val(additionalcontent_sel);
  	});
  });
  
<?php
if($train_message != "") {
?>
alert("<?php echo $train_message ?>");
// window.location.href = "<?php echo current_url() ?>";
<?php
}
?>
</script>