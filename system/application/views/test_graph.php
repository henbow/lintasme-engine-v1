<html>
<head>
    <title></title>
</head>
<body>
<div id="container">

</div>

<script type="text/javascript" src="<?php echo base_url() ?>assets/include/jquery.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var chart = <?php echo json_encode($chart);?>;

        chart.yAxis.labels.formatter = function() {
            return this.value ;
        };
        $('#container').highcharts(chart);
    });

</script>
</body>
</html>