<!DOCTYPE HTML>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <title>Dashboard</title>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-responsive.css">
  <link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url() ?>assets/ui.datepicker.css" />
  <style type="text/css">
.page.container {
  margin-top: 55px;
}
  #page {
height: 200px;
padding-left: 200px;
}
  th {
color: 
#424242;
text-transform: uppercase;
font-size: 12px;
}
  .table-condensed th, .table-condensed td {
padding: 10px 5px;
}
  .table tbody tr:hover td, .table tbody tr:hover th {
background-color: #FCFCFC;
}
  .nav > li > a:hover {
background-color: #FCFCFC;
}
  .datepicker_header select {
background: 
#333;
color: 
white;
border: 0px;
font-weight: bold;
width: auto;
height: auto;
line-height: 10px;
margin-bottom: 0px;
padding: 0px;
}
  .page_link.active-page {
background: #E7E7E7;
}
  .tdlink {
color: #333;
}
  </style>
  <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/ui.datepicker.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.masonry.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/js/fancybox/jquery.fancybox.pack.js"></script>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="brand" href="<?php echo site_url('domain/home') ?>">Lintas Domain Source</a>
      <div class="nav-collapse">
        <ul class="nav">
                </ul>
      </div>
    </div>
  </div>
</div><div class="page container">
  <?php $this->load->view('left_menu') ?>  
  <style type="text/css">
	.dashbox {
	    background: none repeat scroll 0 0 #F7F7F7;
	    border: 1px solid #CCCCCC;
	    width: 300px;
	    min-height: 150px;
	    float: left;
	    margin: 0px 0px 10px 0;
	}
	.dashbox p.dashbox-title{
		width: auto;
		height: auto;
	    background: none repeat scroll 0 0 #2C2C2C;
	    padding: 5px 5px;
	    color: white;
		font-weight: bold;
	}
	.dashbox p.dashbox-view-all{
		width: auto;
		height: auto;
	    background: none repeat scroll 0 0 #8DDBF8;
	    padding: 5px 5px;
	    margin: 0px;
	}
	.dashbox p.dashbox-view-all a{
		display: block;
	}
	.dashbox ul{
		list-style: none;
	}
	.dashbox ul li{
		margin: 2px 5px 2px -20px;
		border-bottom: #CCC 1px solid;
		color: #777777;
		font-size: 12px;
	}
	.dashbox small {
		font-size: 10px;
		font-weight: bold;
	}
	</style>
  <div id="page" style="padding-left: 230px;">
      <div class="page-header">
        <h1>Dashboard</h1>
      </div>
      <div id="graph_most_viewed"></div>
      <div id="graph_engine_post"></div>
      <div id="graph_domain_post"></div>

      <div id="container">
	      <div class="dashbox" id="newest-feeds">
	      	<p class="dashbox-title">Newest Feed Sources</p>
	      	<ul>
	      		<?php
	      		if(count($newest_feeds) > 0):
					foreach ($newest_feeds as $item):
	      		?>
	      		<li class="dashbox-item"><a href="<?php echo site_url('domain/detail_report/'.$item['dl_ID']) ?>"><?php echo $item['dl_domain'] ?> - <small><?php echo date('d/m/Y H:i:s', strtotime($item['dl_insert_date'])) ?></small></a></li>
	      		<?php
	      			endforeach;
				endif;
				?>
	      	</ul>
	      	<p class="dashbox-view-all"><a href="<?php echo site_url('domain/report') ?>">View All</a></p>
	      </div>
	      <div class="dashbox" id="latest-posts">
	      	<p class="dashbox-title">Latest Posts</p>
	      	<ul>
	      		<?php
	      		if(count($latest_posts) > 0):
					foreach ($latest_posts as $post):
	      		?>
	      		<li class="dashbox-item"><a target="_blank" href="http://lintas.me/article/<?php echo $post['se_post_ID'] ?>"><?php echo $post['se_title'] ?> - <small><?php echo date('d/m/Y H:i:s', strtotime($post['se_posted_date'])) ?></small></a></li>
	      		<?php
	      			endforeach;
				endif;
				?>
	      	</ul>
	      	<p class="dashbox-view-all"><a href="<?php echo site_url('domain/latest_posts') ?>">View All</a></p>
	      </div>
	      <div class="dashbox" id="home-popular">
	      	<p class="dashbox-title">Latest Home Popular</p>
	      	<ul>
	      		<?php
	      		if(count($home_popular) > 0):
					foreach ($home_popular as $homepop):
	      		?>
	      		<li class="dashbox-item"><a target="_blank" href="http://lintas.me/article/<?php echo $homepop['se_post_ID'] ?>"><?php echo $homepop['se_title'] ?> - <small><?php echo date('d/m/Y H:i:s', strtotime($homepop['se_homepop_date'])) ?> (<?php echo $this->report->post_viewed($homepop['se_post_ID']) ?> views)</small></a></li>
	      		<?php
	      			endforeach;
				endif;
				?>	
	      	</ul>
	      	<p class="dashbox-view-all"><a href="<?php echo site_url('domain/latest_home_popular') ?>">View All</a></p>
	      </div>
	      <div class="dashbox" id="most-active-source">
	      	<p class="dashbox-title">Most Active Source (<?php echo date('d-M-y', time() - ( 60 * 60 * 24 * 7 )) . " to " . date('d-M-y') ?>)</p>
	      	<ul>
	      		<?php
	      		if(count($most_active_source) > 0):
					foreach ($most_active_source as $as_key => $as_val):
	      		?>
	      		<li class="dashbox-item"><a target="_blank" href="#"><?php echo "{$as_val} - <strong>{$as_key} articles</strong>" ?></a></li>
	      		<?php
	      			endforeach;
				endif;
				?>	
	      	</ul>
	      	<p class="dashbox-view-all"><a href="#">View All</a></p>
	      </div>
	      <div class="dashbox" id="most-tweeted-article">
	      	<p class="dashbox-title">Most Tweeted Articles (last 24 hours)</p>
	      	<ul>
	      		<?php
	      		if(count($most_tweeted_news) > 0):
					foreach ($most_tweeted_news as $tw_news):
	      		?>
	      		<li class="dashbox-item"><a target="_blank" href="http://lintas.me/article/<?php echo $tw_news['se_post_ID'] ?>"><?php echo "{$tw_news['se_title']} - <strong>{$tw_news['se_social_effect']} tweets</strong>" ?></a></li>
	      		<?php
	      			endforeach;
				endif;
				?>	
	      	</ul>
	      	<p class="dashbox-view-all"><a href="#">View All</a></p>
	      </div>
	      <div class="dashbox" id="most-tweeted-article">
	      	<p class="dashbox-title">Most Tweeted Source (last 24 hours)</p>
	      	<ul>
	      		<?php
	      		if(count($most_tweeted_source) > 0):
					foreach ($most_tweeted_source as $ts_key => $ts_val):
	      		?>
	      		<li class="dashbox-item"><a target="_blank" href="#"><?php echo "{$ts_val} - <strong>{$ts_key} tweets</strong>" ?></a></li>
	      		<?php
	      			endforeach;
				endif;
				?>	
	      	</ul>
	      	<p class="dashbox-view-all"><a href="#">View All</a></p>
	      </div>
	      <div class="dashbox" id="most-tweeted-article">
	      	<p class="dashbox-title">Most Viewed (last 24 hours)</p>
	      	<ul>
	      		<?php
	      		if(count($most_viewed_today) > 0):
					foreach ($most_viewed_today as $mvt):
	      		?>
	      		<li class="dashbox-item"><a target="_blank" href="http://lintas.me/article/<?php echo $mvt['post_id'] ?>"><?php echo "{$mvt['title']} - <small> ".date('d/m/Y H:i:s', strtotime($mvt['posted_date']))." ({$mvt['view']} views)</small>" ?></a></li>
	      		<?php
	      			endforeach;
				endif;
				?>	
	      	</ul>
	      	<p class="dashbox-view-all"><a href="#">View All</a></p>
	      </div>
	      <div class="dashbox" id="most-tweeted-article">
	      	<p class="dashbox-title">Most Active Category (last 24 hours)</p>
	      	<ul>
	      		<?php
	      		if(count($most_active_category) > 0):
					foreach ($most_active_category as $ac_key => $ac_val):
	      		?>
	      		<li class="dashbox-item"><a target="_blank" href="#"><?php echo "{$ac_val} - <strong>{$ac_key} articles</strong>" ?></a></li>
	      		<?php
	      			endforeach;
				endif;
				?>	
	      	</ul>
	      	<p class="dashbox-view-all"><a href="#">View All</a></p>
	      </div>
      </div>
      <div id="detail_domain_post"></div>
  </div>
</div>
</body>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script>
$(function(){
  $('#container').masonry({
    // options
    itemSelector : '.dashbox',
    columnWidth: 310
  });
});
$(document).ready(function() {
    /*
    $.ajax({
        type:"GET",
        url:"/domain/graph_most_viewed",
        beforeSend:function(){

        },
        success:function(chart){
            chart.yAxis.labels.formatter = function() {
                return this.value ;
            };
            $('#graph_most_viewed').highcharts(chart);
        }
    });

    $.ajax({
        type:"GET",
        url:"/domain/graph_engine_post",
        beforeSend:function(){

        },
        success:function(chart){
            chart.yAxis.labels.formatter = function() {
                return this.value ;
            };
            $('#graph_engine_post').highcharts(chart);
        }
    });


    $.ajax({
        type:"GET",
        url:"/domain/graph_domain_post",
        beforeSend:function(){

        },
        success:function(chart){
            chart.yAxis.labels.formatter = function() {
                return this.value ;
            };
            $('#graph_domain_post').highcharts(chart);
        }
    });

    $.ajax({
        type:"GET",
        url:"/domain/detail_domain_post",
        beforeSend:function(){

        },
        success:function(data){
            $('#detail_domain_post').html(data);
        }
    });
*/
});
</script>
</html>