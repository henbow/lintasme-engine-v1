<!DOCTYPE html>
<!-- saved from url=(0057)<?php echo base_url() ?>assets/publisher/listpublisher_feed/all -->
<html lang="en-US"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="UTF-8">
  <title>Reports All Domain</title>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-responsive.css">
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <style type="text/css">
.page.container {
  margin-top: 55px;
}
  #page {
height: 200px;
padding-left: 200px;
}
  th {
color: 
#424242;
text-transform: uppercase;
font-size: 12px;
}
  .table-condensed th, .table-condensed td {
padding: 10px 5px;
}
  .table tbody tr:hover td, .table tbody tr:hover th {
background-color: #FCFCFC;
}
  .nav > li > a:hover {
background-color: #FCFCFC;
}
  .datepicker_header select {
background: 
#333;
color: 
white;
border: 0px;
font-weight: bold;
width: auto;
height: auto;
line-height: 10px;
margin-bottom: 0px;
padding: 0px;
}
  .page_link.active-page {
background: #E7E7E7;
}
  .tdlink {
color: #333;
}
</style>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/tablesorter/jquery.tablesorter.js"></script>
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="brand" href="<?php echo site_url('domain/home') ?>">Lintas Domain Source</a>
      <div class="nav-collapse">
        <ul class="nav">
                </ul>
      </div>
    </div>
  </div>
</div><div class="page container">
	<?php $this->load->view('left_menu') ?>  
    <div id="page">
    <div class="container-fluid">
    	<div class="page-header">
    		<?php //echo $startdate;die; ?>
        <h1>Reports <small> All domains <?php echo "from $startdate to $enddate" ?></small></h1>
      </div>
        <div id="container">

        </div>
      <form method="get" action="<?php echo site_url('domain/report/').'/' ?>">
    Display from <input type="text" class="span2 datepicker" name="start_date" value="<?php echo $startdate ?>" />&nbsp;&nbsp;to&nbsp;&nbsp;
    <input type="text" class="span2 datepicker" name="end_date" value="<?php echo $enddate ?>" />
    <input type="submit" name="go" value=" Go " /> </form>
    <table class="table table-condensed tablesorter" id="all_report">
      <thead>
      <tr>    
        <th>Domain</th>
        <th>Crawler</th>
        <th>Home</th>    
        <th>Channel</th>    
        <th>Autotweet</th>
      </tr>
      </thead>
      <tbody>
      	<?php 
      	foreach($domain_list as $domain): 
      	?>
        <tr>        
	        <td><?php echo anchor('domain/detail_report/'.$domain['dl_ID'].'/?start_date='.$startdate.'&end_date='.$enddate,$domain['dl_domain']) ?></td>
	        <td><?php echo $this->report->get_total_crawled_domain($domain['dl_ID'], $startdate, $enddate) ?></td>
	        <td><?php echo $this->report->get_total_home_popular_domain($domain['dl_ID'], $startdate, $enddate) ?></td>
	        <td><?php echo $this->report->get_total_channel_popular_domain($domain['dl_ID'], $startdate, $enddate) ?></td>
	        <td><?php echo $this->report->get_total_autotweet_domain($domain['dl_ID'], $startdate, $enddate) ?></td>
      	</tr>
      	<?php endforeach; ?>        
      </tbody>
    </table>
    <style type="text/css">
    .table th {
    	cursor: pointer;
    }
	.pagination ul {
		display: inline-block;
		margin-left: 0;
		margin-bottom: 0;
		-webkit-border-radius: 3px;
		-moz-border-radius: 3px;
		border-radius: 3px;
		-webkit-box-shadow: 0 0px 0px 
		rgba(0, 0, 0, 0.05);
		-moz-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.05);
		box-shadow: 0 0px 0px 
		rgba(0, 0, 0, 0.05);
		width: 100%;
	}
	</style>
		<div class="pagination">
		<?php echo $pages ?>
        </div>
    </div>
  </div>
</div>
<script src="http://code.highcharts.com/highcharts.js"></script>
  <script>
  $(function() {
    $( "#all_report" ).tablesorter();
    $( ".datepicker" ).datepicker();
    $( ".datepicker" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );

      var chart = <?php echo json_encode($chart);?>;

      chart.yAxis.labels.formatter = function() {
          return this.value ;
      };
      $('#container').highcharts(chart);
  });
  </script>

<div id="datepicker_div"></div></body></html>