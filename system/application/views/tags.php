<?php
$cols = 5;
$per_cols = ceil($total_words / $cols);
$offset = 0;
sort($tag_data);
?>
<div class="page container">
  <?php $this->load->view('left_menu') ?>  
  <div id="page">
  <form action="<?php echo site_url('content_classification/tags') ?>" class="form-horizontal span6" method="post">
    <fieldset>
      <div class="page-header">
        <h1>Manage <small>Tags</small></h1>
      </div>                 
      <div class="control-group">
        <label for="" class="control-label">New Tag</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <input class="span3" id="appendedPrependedInput" name="tags" size="16" type="text" value="">
          </div>
        </div>
      </div>      
      <div class="form-actions">
        <button type="submit" name="submit" class="btn btn-primary">Add Tag</button>
      </div>
    </fieldset>
  </form>
  
  <div class="container-fluid">
  	<table class="table table-condensed tablesorter" id="all_domain">
      <tbody>
      	<tr><td colspan="<?php echo $cols ?>">
      		<form action="<?php echo site_url('content_classification/tags') ?>/" method="get" style="margin: 0px;">
      			<input type="text" class="span3" name="search_tags" style="float:left;margin: 0px;" /><button style="float:left;margin-left: 10px" type="submit" name="submit" class="btn btn-primary">Search</button>
      		</form>
  		</td></tr>
      	<tr><td colspan="<?php echo $cols ?>">      		
      		<div class="pagination" style="height: 75px; margin: 10px 0;">
				<ul class="stream_page">			
					<?php
					foreach(range('a','x') as $alpha):
						if($alpha == 'a'):
					?>
					<li class="round-left">
						<a class="page_link <?php echo $page == 'a' ? 'active-page' : '' ?> round-left" href="<?php echo site_url('content_classification/tags/?alphabet=a') ?>">A</a>
					</li>
					<?php
						elseif($alpha == 'z'):
					?>
					<li class="round-right">
						<a class="page_link <?php echo $page == 'x' ? 'active-page' : '' ?> round-right" href="<?php echo site_url('content_classification/tags/?alphabet=x') ?>">X</a>
					</li>
					<?php
						else:
					?>
					<li>
						<a class="page_link <?php echo $page == $alpha ? 'active-page' : '' ?>" href="<?php echo site_url('content_classification/tags/?alphabet='.$alpha) ?>"><?php echo strtoupper($alpha) ?></a>
					</li>
					<?php
						endif;
					endforeach;
					?>			
				</ul>    
				<ul class="stream_page">			
					<li class="round-left">
						<a class="page_link <?php echo $page == 'y' ? 'active-page' : '' ?> round-left" href="<?php echo site_url('content_classification/tags/?alphabet=y') ?>">Y</a>
					</li>
					<li class="round-right">
						<a class="page_link <?php echo $page == 'z' ? 'active-page' : '' ?> round-right" href="<?php echo site_url('content_classification/tags/?alphabet=z') ?>">Z</a>
					</li>	
				</ul>        
			</div>
      	</td>
     </tr>
     <tr><td colspan="<?php echo $cols ?>"><h4>Total Tags: <?php echo $total_words ?></h4></td></tr>      	
      	<?php 
		for($i = 0; $i < $per_cols; $i++):
			$current_row = array_slice($tag_data, $offset, $cols);
		?>		
        <tr>
		<?php
      		foreach ($current_row as $tag):
      	?>
	        <td width="<?php echo (100 / $cols) ?>%"><?php echo $tag."  ".anchor('content_classification/delete_tag/?tag='.$tag, '[X]', array('class' => "delete_tags", "data-text" => $tag)); ?></td>
      	<?php 
      		flush();
      		endforeach; 
		?>
		</tr>
		<?php
			$offset = $offset + $cols;
			flush();
		endfor;
      	?>   
      	<tr><td colspan="<?php echo $cols ?>">      		
      		<div class="pagination" style="height: 75px; margin: 10px 0;">
				<ul class="stream_page">			
					<?php
					foreach(range('a','x') as $alpha):
						if($alpha == 'a'):
					?>
					<li class="round-left">
						<a class="page_link <?php echo $page == 'a' ? 'active-page' : '' ?> round-left" href="<?php echo site_url('content_classification/tags/?alphabet=a') ?>">A</a>
					</li>
					<?php
						elseif($alpha == 'z'):
					?>
					<li class="round-right">
						<a class="page_link <?php echo $page == 'x' ? 'active-page' : '' ?> round-right" href="<?php echo site_url('content_classification/tags/?alphabet=x') ?>">X</a>
					</li>
					<?php
						else:
					?>
					<li>
						<a class="page_link <?php echo $page == $alpha ? 'active-page' : '' ?>" href="<?php echo site_url('content_classification/tags/?alphabet='.$alpha) ?>"><?php echo strtoupper($alpha) ?></a>
					</li>
					<?php
						endif;
					endforeach;
					?>			
				</ul>    
				<ul class="stream_page">			
					<li class="round-left">
						<a class="page_link <?php echo $page == 'y' ? 'active-page' : '' ?> round-left" href="<?php echo site_url('content_classification/tags/?alphabet=y') ?>">Y</a>
					</li>
					<li class="round-right">
						<a class="page_link <?php echo $page == 'z' ? 'active-page' : '' ?> round-right" href="<?php echo site_url('content_classification/tags/?alphabet=z') ?>">Z</a>
					</li>	
				</ul>        
			</div>
      	</td>
     </tr>     
      </tbody>
    </table>
    <style type="text/css">
	.pagination ul {
		display: inline-block;
		margin-left: 0;
		margin-bottom: 0;
		-webkit-border-radius: 3px;
		-moz-border-radius: 3px;
		border-radius: 3px;
		-webkit-box-shadow: 0 0px 0px 
		rgba(0, 0, 0, 0.05);
		-moz-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.05);
		box-shadow: 0 0px 0px 
		rgba(0, 0, 0, 0.05);
		width: 100%;
	}
	</style>	
    </div>
  </div>
</div>
<script>
	$('input[name="tag"]').focus();
	$('.delete_tags').click(function(e){
		e.preventDefault();
		if(confirm('Delete "'+$(this).attr('data-text')+'"?')) {
			window.location.href = $(this).attr('href');
		}
	});
</script>
<?php if($message): ?>
<script>//alert('<?php echo $message ?>');</script>
<?php endif; ?>