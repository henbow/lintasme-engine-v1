<!DOCTYPE HTML>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <title>Edit City</title>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-responsive.css">
  <link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url() ?>assets/ui.datepicker.css" />
  <style type="text/css">
.page.container {
  margin-top: 55px;
}
  #page {
height: 200px;
padding-left: 200px;
}
  th {
color: 
#424242;
text-transform: uppercase;
font-size: 12px;
}
  .table-condensed th, .table-condensed td {
padding: 10px 5px;
}
  .table tbody tr:hover td, .table tbody tr:hover th {
background-color: #FCFCFC;
}
  .nav > li > a:hover {
background-color: #FCFCFC;
}
  .datepicker_header select {
background: 
#333;
color: 
white;
border: 0px;
font-weight: bold;
width: auto;
height: auto;
line-height: 10px;
margin-bottom: 0px;
padding: 0px;
}
  .page_link.active-page {
background: #E7E7E7;
}
  .tdlink {
color: #333;
}
  </style>
  <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/ui.datepicker.js"></script>
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="brand" href="<?php echo site_url('domain/home') ?>">Lintas Domain Source</a>
      <div class="nav-collapse">
        <ul class="nav">
                </ul>
      </div>
    </div>
  </div>
</div><div class="page container">
  <?php $this->load->view('left_menu') ?>  
  <style type="text/css">
	.rand_user, .chsel, .chselactive,.selch,.selsubch,.sel_upload {
	    background: none repeat scroll 0 0 #F7F7F7;
	    border: 1px solid #CCCCCC;
	    color: #B5B5B5;
	    cursor: pointer;
	    display: inline-block;
	    margin-top: -3px;
	    padding: 1px 5px;
	    margin-bottom: 10px;
	}
	.rand_user.active, .chsel.active, .active.chselactive,.selch.active,.selsubch.active,.sel_upload.active {
	    background: none repeat scroll 0 0 #88A991;
	    border-color: #4B6853;
	    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.4);
	    color: white;
	}
	</style>
  <div id="page">
  <form enctype="multipart/form-data" action="<?php echo site_url('local/do_edit_city/'.$id) ?>" class="form-horizontal span6" method="post">
    <fieldset>
      <div class="page-header">
        <h1>Edit <small>City: <?php echo ucwords($city['kota']) ?></small></h1>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Country</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <select name="country">
            	<?php
            	foreach ($countries as $country) {
				?>
				<option <?php echo $city['country_id'] == $country['id'] ? 'selected="selected"' : '' ?> value="<?php echo $country['id'] ?>" <?php echo $country['id'] == '1' ? 'selected="selected"' : "" ?>><?php echo ucwords($country['name']) ?></option>
				<?php
				}
            	?>
            </select>
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Province</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <select name="province">
            	<option value=""></option>
            	<?php
            	foreach ($provinces as $prov) {
				?>
				<option <?php echo $city['province_id'] == $prov['id'] ? 'selected="selected"' : '' ?> value="<?php echo $prov['id'] ?>"><?php echo ucwords($prov['name']) ?></option>
				<?php
				}
            	?>
            </select>
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">City Name</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <input class="span3" id="appendedPrependedInput" name="name" size="16" type="text" value="<?php echo $city['kota'] ?>">
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Description</label>
        <div class="controls">
          <div class="input-prepend input-append">
          	<textarea class="span4" name="desc" rows="5" cols="60"><?php echo $city['desc'] ?></textarea>
          </div>
        </div>
      </div>
      <div class="control-group">        
        <label for="" class="control-label">Photo</label>
        <div class="controls">
          <div class="input-prepend input-append">
          	<?php if($city['image'] != ''): ?>
          	<!--<img src="<?php echo base_url() . "assets/city/" . $city['image'] ?>" width="270" />-->
          	<?php endif; ?>
            <input class="span3" id="appendedPrependedInput" name="photo" size="16" type="file" value="">
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Latitude</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <input class="span3" id="appendedPrependedInput" name="lat" size="16" type="text" value="<?php echo $city['latitude'] ?>">
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Longitude</label>
        <div class="controls">
          <div class="input-prepend input-append">
          	<input class="span3" id="appendedPrependedInput" name="long" size="16" type="text" value="<?php echo $city['longitude'] ?>"><br>
            <input type="button" name="find_position" value="Find Position" style="margin-top:5px;" />
            
            <script>
            	$('input[name="find_position"]').click(function(){
            		$.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address='+$('input[name="name"]').val()+'&sensor=true', function(resp){
            			console.log(resp);
            			if(resp.status == 'OK') {
            				$('input[name="lat"]').val(resp.results[0].geometry.location.lat);
            				$('input[name="long"]').val(resp.results[0].geometry.location.lng);
            			}
            		});
            	});
            </script>
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label"></label>
        <div class="controls">
          <label class="checkbox">
            <input type="checkbox" id="optionsCheckbox" name="active" <?php echo $city['status'] == '1' ? 'checked="checked"' : '' ?> value="1">
            ACTIVE
          </label>
        </div>
      </div>
      
      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="/publisher/listpublisher_feed/all" class="btn">Cancel</a>
      </div>
    </fieldset>
  </form>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){    
    $("span.selch").click(function() {
      $('span.selch').removeClass('active');
      $("div.subchcon").hide();
      $("div.topic_select").show();
      $(this).addClass('active');
      var subch_sel = $(this).attr('data-chid');
      $("#category").val(subch_sel);
      $("#subch_" + subch_sel).show();
      $("span.selsubch").click(function() {
        $('span.selsubch').removeClass('active');
        $(this).addClass('active');
        var subch2_sel = $(this).attr('data-chid');
        $("#topic").val(subch2_sel);
      });
     
    });
	 $('select#domaintype').change(function(){
	  	console.log($(this).val());
	  	if($(this).val() == 'website') $('div#pattern_rules').show();
	  	else $('div#pattern_rules').hide();
	  });
  });
</script>
  <script src="<?php echo base_url() ?>assets/js/bootstrap.js"></script>
</body>
</html>