<!DOCTYPE html>
<!-- saved from url=(0040)<?php echo base_url() ?>assets/publisher/login -->
<html lang="en-US"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="UTF-8">
  <title></title>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-responsive.css">
  <style type="text/css">
.page.container {
  margin-top: 55px;
}
  #page {
height: 200px;
padding-left: 200px;
}
  th {
color: 
#424242;
text-transform: uppercase;
font-size: 12px;
}
  .table-condensed th, .table-condensed td {
padding: 10px 5px;
}
  .table tbody tr:hover td, .table tbody tr:hover th {
background-color: #FCFCFC;
}
  .nav > li > a:hover {
background-color: #FCFCFC;
}
  .datepicker_header select {
background: 
#333;
color: 
white;
border: 0px;
font-weight: bold;
width: auto;
height: auto;
line-height: 10px;
margin-bottom: 0px;
padding: 0px;
}
  .page_link.active-page {
background: #E7E7E7;
}
  .tdlink {
color: #333;
}
  </style>
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="brand" href="<?php echo base_url() ?>assets/publisher/login#">Lintas Domain Source</a>
      <div class="nav-collapse">
        <ul class="nav">
                </ul>
      </div>
    </div>
  </div>
</div><div class="page container">
    <div id="page">
  <form action="<?php echo $login_url ?>" class="form-horizontal span6" method="post">
    <fieldset>
      <div class="page-header">
        <h1>Login</h1>
      </div>
            <div class="control-group">
        <label for="" class="control-label">Username</label>
        <div class="controls">
          <input type="text" class="input-xlarge" name="username" value="">
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">Password</label>
        <div class="controls">
          <input type="password" class="input-xlarge" name="password" value="">
        </div>
      </div>
      <div class="form-actions">
        <input type="submit" name="action" class="btn btn-primary" value="Login">
      </div>
    </fieldset>
  </form>
  </div>
</div>
  <script src="./login_files/bootstrap.js"></script>

<div id="datepicker_div"></div></body></html>