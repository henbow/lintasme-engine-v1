<div class="page container">
  <?php $this->load->view('left_menu') ?>  
  <div id="page">
  <form action="<?php echo site_url('content_classification/test_classifier') ?>" class="form-horizontal span6" method="post">
    <fieldset>
      <div class="page-header">
        <h1>Test <small>Classifier</small></h1>
      </div>
                 
      <div class="control-group">
        <label for="" class="control-label">URL</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <input class="span5" id="appendedPrependedInput" name="url" size="16" type="text" value="">
          </div>
        </div>
      </div>      
      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Do Test!</button>
      </div>
    </fieldset>
  </form>
  <?php 
  if($result != ""):
		$keys = array_keys($result['result']); 
  ?>
  <div class="container-fluid">
  	<table class="table table-condensed tablesorter" id="all_domain">
      <tbody>
      	<tr><td><h4>Test Result:</h4></td></tr>
      	<tr>
      		<td width="20%"><strong>Content</strong></td>
      		<td><?php echo $result['content'] ?></td>
  		</tr>      
      	<tr>
      		<td width="20%"><strong>Classified As</strong></td>
      		<td><?php echo $keys[0] . ' ('.$result['result'][$keys[0]].')' ?></td>
  		</tr>      
      	<tr>      		
      		<td><strong>Execution Time</strong></td>
      		<td><?php echo $result['performance']['time'] ?></td>
  		</tr>	
      </tbody>
    </table>
  </div>
  <?php endif; ?>
  </div>
</div>