<!DOCTYPE HTML>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <title>Edit Autotweet Setting</title>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-responsive.css">
  <link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url() ?>assets/ui.datepicker.css" />
  <style type="text/css">
.page.container {
  margin-top: 55px;
}
  #page {
height: 200px;
padding-left: 200px;
}
  th {
color: 
#424242;
text-transform: uppercase;
font-size: 12px;
}
  .table-condensed th, .table-condensed td {
padding: 10px 5px;
}
  .table tbody tr:hover td, .table tbody tr:hover th {
background-color: #FCFCFC;
}
  .nav > li > a:hover {
background-color: #FCFCFC;
}
  .datepicker_header select {
background: 
#333;
color: 
white;
border: 0px;
font-weight: bold;
width: auto;
height: auto;
line-height: 10px;
margin-bottom: 0px;
padding: 0px;
}
  .page_link.active-page {
background: #E7E7E7;
}
  .tdlink {
color: #333;
}
  </style>
  <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/ui.datepicker.js"></script>
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="brand" href="<?php echo site_url('domain/home') ?>">Lintas Domain Source</a>
      <div class="nav-collapse">
        <ul class="nav">
                </ul>
      </div>
    </div>
  </div>
</div><div class="page container">
  <?php $this->load->view('left_menu') ?>  
  <style type="text/css">
	.rand_user, .chsel, .chselactive,.selch,.selsubch,.sel_upload {
	    background: none repeat scroll 0 0 #F7F7F7;
	    border: 1px solid #CCCCCC;
	    color: #B5B5B5;
	    cursor: pointer;
	    display: inline-block;
	    margin-top: -3px;
	    padding: 1px 5px;
	    margin-bottom: 10px;
	}
	.rand_user.active, .chsel.active, .active.chselactive,.selch.active,.selsubch.active,.sel_upload.active {
	    background: none repeat scroll 0 0 #88A991;
	    border-color: #4B6853;
	    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.4);
	    color: white;
	}
	</style>
  <div id="page">
  <form action="<?php echo site_url('domain/autotweet_edit/'.$cat_id) ?>" class="form-horizontal span6" method="post">
    <fieldset>
      <?php
      $setting_data = $this->autotweet->get_channel_setting($cat_id);
      ?>
      <div class="page-header">
        <h1>AutoTweet Setting <small>Category <?php echo ucwords($cat_name) ?></small></h1>
      </div>
     
      <div class="control-group">
        <label for="" class="control-label">Start Time</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <input class="span1" id="appendedPrependedInput" name="starttime" size="16" type="text" value="<?php echo isset($setting_data['start_time']) ? $setting_data['start_time'] : "" ?>">
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label">End Time</label>
        <div class="controls">
          <div class="input-prepend input-append">
             <input class="span1" id="appendedPrependedInput" name="endtime" size="16" type="text" value="<?php echo isset($setting_data['end_time']) ? $setting_data['end_time'] : "" ?>">
          </div>
        </div>
      </div>
      <div class="control-group">
        <label for="" class="control-label"></label>
        <div class="controls">
          <label class="checkbox">
            <input type="checkbox" id="optionsCheckbox" <?php echo isset($setting_data['fullday_tweet']) ? $setting_data['fullday_tweet'] == '1' ? 'checked="checked"' : '' : "" ?> name="fullday" value="1">
            FULLDAY AUTOTWEET
          </label>
        </div>
      </div>
      <div class="form-actions">
      	<input name="category_id" size="16" type="hidden" value="<?php echo $cat_id ?>">
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </fieldset>
  </form>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){    
    $("span.selch").click(function() {
      $('span.selch').removeClass('active');
      $("div.subchcon").hide();
      $("div.topic_select").show();
      $(this).addClass('active');
      var subch_sel = $(this).attr('data-chid');
      $("#category").val(subch_sel);
      $("#subch_" + subch_sel).show();
      $("span.selsubch").click(function() {
        $('span.selsubch').removeClass('active');
        $(this).addClass('active');
        var subch2_sel = $(this).attr('data-chid');
        $("#topic").val(subch2_sel);
      });
     
    });
	 $('select#domaintype').change(function(){
	  	console.log($(this).val());
	  	if($(this).val() == 'website') $('div#pattern_rules').show();
	  	else $('div#pattern_rules').hide();
	  });
  });
</script>
  <script src="<?php echo base_url() ?>assets/js/bootstrap.js"></script>
</body>
</html>