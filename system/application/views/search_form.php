<!DOCTYPE HTML>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <title>Search</title>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-responsive.css">
  <link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url() ?>assets/ui.datepicker.css" />
  <style type="text/css">
.page.container {
  margin-top: 55px;
}
  #page {
height: 200px;
padding-left: 200px;
}
  th {
color: 
#424242;
text-transform: uppercase;
font-size: 12px;
}
  .table-condensed th, .table-condensed td {
padding: 10px 5px;
}
  .table tbody tr:hover td, .table tbody tr:hover th {
background-color: #FCFCFC;
}
  .nav > li > a:hover {
background-color: #FCFCFC;
}
  .datepicker_header select {
background: 
#333;
color: 
white;
border: 0px;
font-weight: bold;
width: auto;
height: auto;
line-height: 10px;
margin-bottom: 0px;
padding: 0px;
}
  .page_link.active-page {
background: #E7E7E7;
}
  .tdlink {
color: #333;
}
  </style>
  <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/ui.datepicker.js"></script>
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="brand" href="<?php echo site_url('domain/home') ?>">Lintas Domain Source</a>
      <div class="nav-collapse">
        <ul class="nav">
                </ul>
      </div>
    </div>
  </div>
</div><div class="page container">
  <?php $this->load->view('left_menu') ?>  
  <style type="text/css">
	.rand_user, .chsel, .chselactive,.selch,.selsubch,.sel_upload {
	    background: none repeat scroll 0 0 #F7F7F7;
	    border: 1px solid #CCCCCC;
	    color: #B5B5B5;
	    cursor: pointer;
	    display: inline-block;
	    margin-top: -3px;
	    padding: 1px 5px;
	    margin-bottom: 10px;
	}
	.rand_user.active, .chsel.active, .active.chselactive,.selch.active,.selsubch.active,.sel_upload.active {
	    background: none repeat scroll 0 0 #88A991;
	    border-color: #4B6853;
	    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.4);
	    color: white;
	}
	</style>
  <div id="page">
  <form action="<?php echo site_url('domain/search/') ?>" class="form-horizontal span6" method="post">
    <fieldset>
    
      <div class="page-header">
        <h1>Search </h1>
      </div>                 
      <div class="control-group">
        <label for="" class="control-label">Field</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <select class="span3" name="fieldname">
            	<option value="dl_domain">Domain</option>
            	<option value="dl_rss_url">RSS URL</option>
            	<option value="topic">Topic</option>
            </select>
          </div>
        </div>
      </div>
            
      <div class="control-group keyword">
        <label for="" class="control-label">Keyword</label>
        <div class="controls">
          <div class="input-prepend input-append">
            <input class="span3" id="appendedPrependedInput" name="keyword" type="text" value="">
          </div>
        </div>
      </div>  
      <div class="control-group topic" style="display:none">
	      <label for="" class="control-label">Category</label>
	      <div class="controls">
		  <?php
		  foreach($category as $cat):
		  ?>
	      <span class="selch" data-chid="<?php echo $cat['id'] ?>" data-name="<?php echo $cat['name'] ?>"><?php echo $cat['displayName'] ?></span>
	      <?php
	      endforeach;
	      ?>
	      <input type="hidden" class="input-xlarge" name="category" id="category" value="" />
	      <input type="hidden" class="input-xlarge" name="category_name" id="category_name" value="" />
	      </div>
      </div>
      <div class="control-group topic_select" style="display:none;">
        <label for="" class="control-label">Topic</label>
        <div class="controls">
              <div class="subchcon">
                Select Category First
              </div>
              <?php 
              foreach($category as $cat):
              ?>
              <div id="subch_<?php echo $cat['id'] ?>" class="subchcon" style="display:none;">
              <?php 
              $ch = curl_init();
              
              curl_setopt($ch,CURLOPT_URL, "http://www.lintas.me/rest/explore_topic/".$cat['name']);
              curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);
              
              //execute post
              $topics = json_decode(curl_exec($ch), TRUE);
              $topics = $topics['topic'];
              
              //close connection
              curl_close($ch);
              
              if(count($topics) > 0):
              foreach ($topics as $topic):
              ?>
              <span class="selsubch" data-chid="<?php echo $topic['tagname'] ?>"><?php echo ucfirst($topic['tagname']) ?></span>
              <?php 
              endforeach;
              endif;
              ?>
			  </div>              
              <?php 
              endforeach;
              ?>
			  <input type="hidden" class="input-xlarge" name="topic" id="topic" value="" />
        </div>
      </div>
      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Search</button>
      </div>
      
    </fieldset>
  </form>
  </div>
</div>
  <script src="<?php echo base_url() ?>assets/js/bootstrap.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){   
  	$('select[name="fieldname"]').change(function(){
  		var field = $(this).val();
  		// console.log($(this).val());
  		if(field == 'topic') {
  			$('.topic').show();
  			$('.keyword').hide();
  		} else {
  			$('.topic').hide();
  			$('.topic_select').hide();
  			$('.keyword').show();
  		}
  	}); 
    $("span.selch").click(function() {
      $('span.selch').removeClass('active');
      $("div.subchcon").hide();
      $("div.topic_select").show();
      $(this).addClass('active');
      var subch_sel = $(this).attr('data-chid');
      var subch_name = $(this).attr('data-name');
      $("#category").val(subch_sel);
      $("#category_name").val(subch_name);
      $("#subch_" + subch_sel).show();
      $("span.selsubch").click(function() {
        $('span.selsubch').removeClass('active');
        $(this).addClass('active');
        var subch2_sel = $(this).attr('data-chid');
        $("#topic").val(subch2_sel);
      });
    });
  });
</script>
</body>
</html>