<?php
// Connection details
$config['redis_host'] = '192.168.1.12';		// IP address or host
$config['redis_port'] = '6379';				// Default Redis port is 6379
$config['redis_password'] = '';				// Can be left empty when the server does not require AUTH

$config['rhost'] = "127.0.0.1";
$config['rport'] = "6379";
$config['rnamespace1'] = "lfe2";
$config['rnamespace2'] = "lat";
$config['rdebug'] = TRUE;
$config['rmintrainratio'] = 0;
 
/**
 * End of file redis.php
 * 
 */