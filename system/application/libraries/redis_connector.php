<?php
require 'redisent.php';

class Redis_Connector{
	var $CI;
	var $redis;
	
	function __construct() {
		$this->CI =& get_instance();
		$this->CI->config->load('redis');
		
		$host  = $this->CI->config->item('rhost');
		$port = $this->CI->config->item('rport');
		$namespace = $this->CI->config->item('rnamespace');
		$debug = $this->CI->config->item('rdebug');
		
		$this->redis = new redisent\Redis($host, $port);
	}
}
