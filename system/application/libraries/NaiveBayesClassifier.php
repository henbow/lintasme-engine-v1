<?php

require_once 'NaiveBayesClassifierException.php';
require_once 'wordanalyzer.php';

class NaiveBayesClassifier {
	private $thresholdRatio = 0;
	private $thresholdFrequency = 1;
	private $debug = TRUE;	
	private $ngram = NULL;
	private $ngramN = 3;
	public $store;
	private $P;
	
	/**
	 * Class constructor
	 * @access public
	 * @param Array $conf Redis configurations array 
	 */
	public function __construct($conf = array()) {
		if(empty($conf))
			throw new NaiveBayesClassifierException(1001);
		if(empty($conf['store']))
			throw new NaiveBayesClassifierException(1002);
		if(empty($conf['store']['mode']))
			throw new NaiveBayesClassifierException(1003);
		if(empty($conf['store']['db']))
			throw new NaiveBayesClassifierException(1004);
			
		if(!empty($conf['debug']) && $conf['debug'] === TRUE)
			$this->debug = TRUE;
		
		$this->ngram = new WordAnalyzer();
			
		switch($conf['store']['mode']) {
			case 'redis':
				require_once 'NaiveBayesClassifierStoreRedis.php';
				$this->store = new NaiveBayesClassifierStoreRedis($conf['store']['db']);
				break;
		}	
		
	}
	
	/**
	 * Train classifier
	 * @access public
	 * @param String $words the input text
	 * @param String $set which class will be trained
	 * @return void
	 */
	public function train($words, $set) {
		$words = $this->cleanKeywords($words);
		
		foreach($words as $w) {
			$this->store->trainTo(html_entity_decode($w), $set);
		}
	}

	/**
	 * Detrain classifier
	 * @access public
	 * @param String $words the input text
	 * @param String $set which class will detrained
	 * @return void
	 */
	public function deTrain($words, $set) {
		$words = $this->cleanKeywords($words); 
		                
		foreach($words as $w) {
			$this->store->deTrainFromSet(html_entity_decode($w), $set);
		}
	}

	/**
	 * Classify input text
	 * @access public
	 * @param String $words the input text
	 * @param Integer $count how many output classes will displayed
	 * @param Integer $offset 
	 * @param Boolean $train if true will self-training after classification
	 * @return array
	 */
	public function classify($words, $count = 1, $offset = 0, $train = TRUE) {
		$P = array();
		$score = array();
		
		// Break keywords
		$keywords = $this->cleanKeywords($words); 
				
		// All sets
		$sets = $this->store->getAllSets();
		$P['sets'] = array();
		
		// Word counts in sets
		$setWordCounts = $this->store->getSetWordCount($sets);
		$wordCountFromSet = $this->store->getWordCountFromSet($keywords, $sets);
				
		foreach($sets as $set) {
			$P['sets'][$set] = 0;
			
			foreach($keywords as $word) {
				$key = "{$word}{$this->store->delimiter}{$set}"; // P(Word|Class)
				
				if($wordCountFromSet[$key] > 0) {
					$P['sets'][$set] += $wordCountFromSet[$key] / $setWordCounts[$set];
				}
			}
			
			$this->P = $P;
			
			if(!is_infinite($P['sets'][$set]) && $P['sets'][$set] > 0)
				$score[$set] = $P['sets'][$set];
		}
		
		arsort($score);
		
		$max = max($score);
		
		// Train classifier after classification process
		if($max >= $this->thresholdRatio) {
			// Find keyword with maximum score
			$key = array_search($max, $score);
			
			if($train === TRUE) $this->train($words, $key);
			
			return array_slice($score, $offset, $count);
		}
		
		return array();
	}
	
	public function stopwords($words = array()) {
		$clean = array();
		
		if(is_string($words)) {
			$clean = array($words);
		}
		else if(is_array($words)) {
			$clean = $words;
		}
		
		// $clean = $this->cleanKeywords($clean);
		
		foreach($clean as $word) {
			$this->store->addToStopwords($word);
		}
	}
	
	public function getStopwords() {
		return $this->store->getStopwords();
	}
	
	public function removeStopword($word) {
		return $this->store->removeFromStopword($word);
	}

	public function saveClassificationResult($postID, $class) {
		return $this->store->saveResult($postID, $class);
	}
	
	private function cleanKeywords($words) {
		if(!empty($words)) { 
			$ret = array();
			$sw  = $this->store->getStopwords();
			
			$this->ngram->analyzeString($words, $this->ngramN);
			
			$kw = $this->ngram->getKeyWordDensityTable();
						
			for($i = 0; $i < $this->ngramN; $i++) {				
				foreach($kw[$i] as $k => $v) {
					$k = strtolower(trim($k));
					$k = preg_replace("/[^a-z]/i", "", $k);
					
					if(!empty($k) && strlen($k) > 2 && !in_array($k, $sw)) {							
						$k = strtolower($k);
						if(!empty($k))
							$ret[] = $k;
					}				
				}
			}
						
			return $ret;
		}
	}
	
	private function countWordFrequency($words, $sort = FALSE, $sortOrder = 'asc') {
		$words = array_count_values($words);
		
		if($sort == TRUE) {
			switch ($sortOrder) {
				case 'desc':
					arsort($words);
					break;
				
				case 'asc':
				default:
					asort($words);
					break;
			}
		}
		
		return $words;
	}
	
	private function isStopworded($word) {
		return $this->store->isStopworded($word);
	}
	
	private function _debug($msg) {
		if($this->debug)
			echo $msg . PHP_EOL;
	}
	
}
