<?php
class WordAnalyzer {
    protected $_wordDensityHashTable = array();
    protected $_tokenPattern = " ,.?\n\/\"'\\()-_|";
    protected $_stopWords = array();
    protected $_trimCharacters = " \t\r\n.!?:\x0B\0";
	protected $_ci;
	
	public $ngrams = 3;
	public $outputNum = 5;
	public $redisClass = NULL;

    public function __construct(){
    	$this->_ci =& get_instance();

		$this->_ci->load->model('tag_model', 'tag');
		
		$this->loadStopWordsText();
   	}

    public function loadStopWordsText() {
       	$this->_stopWords = $this->_ci->tag->get_stopwords();
    }

    /**
     * Set the characters that should always be trimmed from a word
     * @param string $trimCharacters 
     */
    public function setTrimCharacters($trimCharacters){
       $this->_trimCharacters = $trimCharacters;
    }

    /**
     * Split a string into tokens using the pattern splitter
     * @param string $string
     * @return array 
     */
    protected function tokenize($string){
     	//var_dump($string);
        $tokens = array();
        $token = strtok(strtolower($string), $this->_tokenPattern);

        while ($token !== false) {
            $tokens[] = $token;
            $token = strtok($this->_tokenPattern);
        }
        return $tokens;
    }

    /**
     * Returns an array the key is the word(s) the value is the frequency
     * @return array 
     */
    public function getKeyWordDensityTable() {
       return $this->_wordDensityHashTable;
    }

    /**
     * Clear the keyword density table 
     */
    public function clearKeyWordDensityTable() {
       unset($this->_wordDensityHashTable);
       $this->_wordDensityHashTable = array();
    }

    /**
     * Set the REGEX split pattern string
     * @param string $pattern 
     */
    public function setSplitPattern($pattern){
       $this->_splitPattern = $pattern;
    }

    /**
     * Iterate through the array of words that and make an n-gram lookup table
     * @param array $arrayOfWords
     * @param integer $maxWords 
     */
    protected function _keyWordDensityAnalysis($arrayOfWords = array(), $maxWords = 2){

       for($start = 0; $start < $maxWords; $start++){

          if(!isset($this->_wordDensityHashTable[$start])){
             $this->_wordDensityHashTable[$start] = array();
          }

          for($index = $start; $index < count($arrayOfWords)+$start; $index++){

             $words = array_slice($arrayOfWords,$index - $start, $start+1);

             if(count($words) < $start+1){
                continue;
             }

             $phrase = trim( implode(" ",$words), $this->_trimCharacters);

             if(empty($this->_wordDensityHashTable[$start][$phrase])){
                $this->_wordDensityHashTable[$start][$phrase] = 1;
             }
             else{
                 $this->_wordDensityHashTable[$start][$phrase]++;
             }
         }
       }
    }
	
	/**
     * Analyze the text passed in
     * @param type $string
     * @param type $maxWords 
     */
    public function analyzeString($string, $maxWords){
       $this->_keyWordDensityAnalysis( $this->_filterStopWords( $this->tokenize( $string ) ), $maxWords);
    }
	
	/**
	 * Generate article tags
	 * @return array
	 */
	public function generateTags($string, $limit = 5) {
		$words = $this->_wordDensityHashTable;
		$tags  = $this->_ci->tag->get_tags();
		$outputTags = array();
		$compWords = array();
		$num = 0;
		
		foreach ($words as $cword) {
			$compWords += $cword;
		}
		
		arsort($compWords);
		
		foreach ($compWords as $word => $count) {
			if($num == 5) break;
			if(preg_match('/[a-zA-Z0-9\+]/', $word)) {
				if(count(preg_grep("/^".strtolower(trim($word, ' +-\\/[]{}()*'))."\z/i", $tags)) > 0) {
					$outputTags[$word] = $count;
					$num++;
				}
			}
		}
		
		return $outputTags;
	}

    /**
     * Add stop words to the stop words lookup
     * @param type $stopWords 
     */
    public function addStopWords(array $stopWords){
       if(is_array($stopWords)){
           $this->_stopWords = array_unique(array_merge($stopWords, $this->_stopWords));
       }
    }

    /**
     * Remove all stop words from the text
     * @param array $arrayOfWords
     * @return array 
     */
    protected function _filterStopWords(&$arrayOfWords){
       $nonStopWords = array();
       for($index = 0; $index < count($arrayOfWords); $index++){

           if(!$this->_isStopWord($arrayOfWords[$index])){
              $nonStopWords[] = $arrayOfWords[$index];
           }
       }

       unset($arrayOfWords);
       return $nonStopWords;
    }

    /**
     * Get the list of stop words that are loaded into memory
     * @return array 
     */
    public function getStopWords(){
       return $this->_stopWords;
    }

    /**
     * Check if word is a stop word
     * @param string $word
     * @return boolean
     */
    protected function _isStopWord($word){
       return in_array($word,$this->_stopWords);
    }

}

