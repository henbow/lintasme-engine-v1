<?php
class NaiveBayesClassifierException extends Exception {
	
	function __construct($code = NULL, $else = NULL) {
		switch($code) {
			case 1001:
				parent::__construct('Empty configuration array for NaiveBayesClassifier constructor', $code);
				break;
			case 1002:
				parent::__construct('Store must be defined for NaiveBayesClassifier constructor', $code);
				break;
			case 1003:
				parent::__construct('Store Mode must be defined for NaiveBayesClassifier constructor', $code);
				break;
			case 1004:
				parent::__construct('Store DB Credentials must be defined for NaiveBayesClassifier constructor', $code);
				break;
				
			case 3001:
				parent::__construct('Empty configuration array for NBCStore', $code);
				break;
			
			case 3101:
				parent::__construct('NaiveBayesClassifierStoreMySQL needs a database host defined in the constructor', $code);
				break;
			case 3102:
				parent::__construct('NaiveBayesClassifierStoreMySQL needs a database port defined in the constructor', $code);
				break;
			case 3103:
				parent::__construct('NaiveBayesClassifierStoreMySQL needs a database name defined in the constructor', $code);
				break;
			case 3104:
				parent::__construct('NaiveBayesClassifierStoreMySQL needs a database user defined in the constructor', $code);
				break;
			case 3105:
				parent::__construct('NaiveBayesClassifierStoreMySQL needs a database password defined in the constructor', $code);
				break;
			case 3106:
				parent::__construct('Cannot connect to MySQL Host: ' . mysql_error(), $code);
				break;
			case 3107:
				parent::__construct('Cannot connect to MySQL Database', $code);
				break;
			
			case 3200:
				parent::__construct('HandlerSocket Extension for PHP not installed. Refer to http://code.google.com/p/php-handlersocket/ please.', $code);
				break;
			case 3201:
				parent::__construct('Cannot open specified Index', $code);
				break;
			case 3202:
				parent::__construct('NaiveBayesClassifierStoreHandlerSocket needs a database read port defined in the constructor', $code);
				break;
			case 3203:
				parent::__construct('NaiveBayesClassifierStoreHandlerSocket needs a database write port defined in the constructor', $code);
				break;
			case 3204:
				parent::__construct('NaiveBayesClassifierStoreHandlerSocket needs a database name defined in the constructor', $code);
				break;
			case 3205:
				parent::__construct('Opening Index for HandlerSocket failed, Table Select Fields must be an array', $code);
				break;
			
			default:
				parent::__construct('Uncategorized NaiveBayesClassifierException thrown, check Code Trace please');
		}
	}
	
}