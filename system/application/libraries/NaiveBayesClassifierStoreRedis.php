<?php
require_once 'redisent.php';

class NaiveBayesClassifierStoreRedis {	
	private $conn;

	private $namespace	= "lfe2";
	private $stopwords 	= "stopwords";
	private $blacklist  = "blacklist";
	private $blacklistCandidate = "blcandidate";
	private $words 		= "words";
	private $sets 		= "sets";
	private $cache		= "cache";
	private $result		= "results";
	public $delimiter	= ":";
	private $wordCount	= "--count--";
	
	function __construct($conf = array(), $redisClass = NULL) {
		if(empty($conf))
			throw new NaiveBayesClassifierException(3001);
		if(empty($conf['db_host']))
			throw new NaiveBayesClassifierException(3101);
		if(empty($conf['db_port']))
			throw new NaiveBayesClassifierException(3102);
		if(!empty($conf['namespace']))
			$this->namespace = $conf['namespace'];
		
		// Namespacing
		$this->stopwords = "{$this->namespace}-{$this->stopwords}";
		$this->blacklist = "{$this->namespace}-{$this->blacklist}";
		$this->blacklistCandidate = "{$this->namespace}-{$this->blacklistCandidate}";
		$this->words = "{$this->namespace}-{$this->words}";
		$this->sets	 = "{$this->namespace}-{$this->sets}";
		$this->cache = "{$this->namespace}-{$this->cache}";
		
		// Redis connection
        if($redisClass === NULL) 
        	$this->conn = new redisent\Redis($conf['db_host'], $conf['db_port']);
        else
        	$this->conn = $redisClass;
	}
	
	public function command($com, $words, $keys = '') {
		if($keys == '')
			return $this->conn->{$com}($words);
		else
			return $this->conn->{$com}($words, $keys);
	}
	
	public function close() {
		$this->conn->close();
	}
	
	public function addToStopwords($word) {
		return $this->conn->sadd($this->stopwords, $word);
	}
	
	public function getStopwords() {
		return $this->conn->smembers($this->stopwords);
	}
	
	public function removeFromStopword($word) {
		return $this->conn->srem($this->stopwords, $word);
	}
	
	public function addToBlacklist($word) {
		return $this->conn->hIncrBy($this->blacklist, $word, 0);
	}
	
	public function addToBlacklistFromCandidate($word) {
		$this->conn->srem($this->blacklistCandidate, $word);
		return $this->conn->hIncrBy($this->blacklist, $word, 0);
	}
	
	public function addToBlacklistCandidate($word) {
		return $this->conn->sadd($this->blacklistCandidate, $word);
	}
	
	public function removeFromBlacklist($word) {
		return $this->conn->hdel($this->words, $word);
	}
	
	public function isBlacklisted($word) {
		return $this->conn->hexists($this->words, $word);
	}
	
	public function trainTo($word, $set) {
		// Words
		$this->conn->hIncrBy($this->words, $word, 1);
		$this->conn->hIncrBy($this->words, $this->wordCount, 1);

		// Sets
		$key = "{$word}{$this->delimiter}{$set}";
		$this->conn->hIncrBy($this->words, $key, 1);
		$this->conn->hIncrBy($this->sets, $set, 1);
	}

	public function deTrainFromSet($word, $set) {
		$key = "{$word}{$this->delimiter}{$set}";

		$check = $this->conn->hExists($this->words, $word) &&
			$this->conn->hExists($this->words, $this->wordCount) &&
			$this->conn->hExists($this->words, $key) &&
			$this->conn->hExists($this->sets, $set);

		if($check) {
                    // Words
                    $this->conn->hIncrBy($this->words, $word, -1);
                    $this->conn->hIncrBy($this->words, $this->wordCount, -1);

                    // Sets
                    $this->conn->hIncrBy($this->words, $key, -1);
                    $this->conn->hIncrBy($this->sets, $set, -1);

                    return TRUE;
		}
		
		return FALSE;
	}
	
	public function getAllSets() {
		return $this->conn->hKeys($this->sets);
	}
	
	public function getSetCount() {
		return $this->conn->hLen($this->sets);
	}
	
	public function getWordCount($words) {
		$results = array();
		
		foreach ($words as $word) {
			$res = $this->conn->hMGet($this->words, $word);
			$results[$word] = $res[0];
		}
		
		return $results;
	}
	
	public function getAllWordsCount() {
		return $this->conn->hGet($this->wordCount, $this->wordCount);
	}
	
	public function getSetWordCount($sets) {
		$results = array();
		
		foreach ($sets as $set) {
			$res = $this->conn->hMGet($this->sets, $set);
			$results[$set] = $res[0];
		}
		
		return $results;
	}
	
	public function getWordCountFromSet($words, $sets) {
		$results = array();
		foreach($words as $word) {
			foreach($sets as $set) {
				$key = "{$word}{$this->delimiter}{$set}";
				$res = $this->conn->hMGet($this->words, $key);
				$results[$key] = (int) $res[0];
			}
		}
		
		return $results;
	}
	
	public function saveResult($post_id, $class) {
		return $this->conn->hSet($this->result, $post_id, $class);
	}
}