<?php

class Filter extends Controller {
    var $offset = 0;
    var $row = 1;

    function __construct() {
        parent::__construct();

        $this->config->load('redis');
        $this->load->library('NaiveBayesClassifier', array(
            'store' => array(
                'mode'	=> 'redis',
                'mintrainratio' => $this->config->item('rmintrainratio'),
                'db'	=> array(
                    'db_host'	=> $this->config->item('rhost'),
                    'db_port'	=> $this->config->item('rport'),
                    'namespace'	=> $this->config->item('rnamespace')
                )
            ),
            'debug' => $this->config->item('rdebug')
        ), 'bayesian');		
    }

    function index() {
        $this->output->set_output('Hallooo.');
    }

    function classify() {
        $this->benchmark->mark('lfe_start');

        $detrain = $this->input->post('detrain_from', TRUE);
        $train   = $this->input->post('train', TRUE) === '1' ? TRUE : FALSE;
        $words   = $this->input->post('content', TRUE);
        $result  = array();

        if($words != '') {
            if($detrain) {
                $this->bayesian->deTrain($words, $detrain);
            }
            
            $result = $this->bayesian->classify($words, $this->row, $this->offset, $train);
        }

        $this->benchmark->mark('lfe_end');

        $benchmark = array(
            'time'   => $this->benchmark->elapsed_time('lfe_start', 'lfe_end') . " seconds",
            'memory' => $this->benchmark->memory_usage()
        );

        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode(array(
               'result' => $result, 
               'performance' => $benchmark
             )));
    }

    function autotag_classify() {
        $this->benchmark->mark('lfe_start');

        $this->load->library('WordAnalyzer', '', 'analyzer');
        $this->load->model('tag_model', 'tag');	

        $train  = isset($_GET['train']) ? ( $_GET['train'] == 'true' ? TRUE : FALSE ) : TRUE;
        $words  = $this->input->post('content', TRUE);
        $result = array();
        $keywords = array();
        $benchmark = array();
        $tags = array();

        if($words != '') {
            $text   = preg_replace('/[^a-z0-9]+/i',' ',strip_tags($words));
            $result = $this->bayesian->classify($text, $this->row, $this->offset, $train);			

            $this->analyzer->analyzeString($text, 3);

            $tags = $this->analyzer->generateTags($text);

            if(count($tags) > 0) {
                foreach ($tags as $tag => $count) {
                    $tags[$tag] = $count;
                }
            } 
        }

        $this->benchmark->mark('lfe_end');

        $benchmark = array(
            'time'   => $this->benchmark->elapsed_time('lfe_start', 'lfe_end') . " seconds",
            'memory' => $this->benchmark->memory_usage()
        );

        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode(array(
                'result' => array(
                    'filtered_as' => $result,
                    'tags' => $tags
                ), 
                'performance' => $benchmark
             )));
    }

    function train() {
            $this->benchmark->mark('lfe_start');

            $words = $this->input->post('content', TRUE);
            $sets  = $this->input->post('classified_as', TRUE);

            $this->bayesian->train($words, $sets);		
            $this->benchmark->mark('lfe_end');	

            $benchmark = array (
                'time'   => $this->benchmark->elapsed_time('lfe_start', 'lfe_end') . " seconds",
                'memory' => $this->benchmark->memory_usage()
            );

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode(array(
                    'message' => "Success train classifier to {$sets}.",
                    'performance' => $benchmark
                 )));
    }

    function detrain() {
        $this->benchmark->mark('lfe_start');

        $pid = $this->input->post('post_id', TRUE);
        $sets  = $this->input->post('classified_as', TRUE);

        if($pid != '' && $sets != '') {	
            $url = 'http://www.lintas.me/rest/getsinglestream/&pid='.$pid;
            $fields = array( 'url' => $url );

            $ch = curl_init();

            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

            $result = json_decode(curl_exec($ch), TRUE);

            curl_close($ch);

            $words = $result['result']['message'];

            $this->bayesian->deTrain($words, $sets);

            $status = 'OK';
            $message = "Detraining success.";
        } else {
            $status = 'FAILED';
            $message = "Parameters is empty.";
        }

        $this->benchmark->mark('lfe_end');	
        $benchmark = array(
                        'time'   => $this->benchmark->elapsed_time('lfe_start', 'lfe_end') . " seconds",
                        'memory' => $this->benchmark->memory_usage()
                    );

        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode(array(
                'result' => array('status' => $status, 'message' => $message),
                'performance' => $benchmark
             )));
    }

    function addstopwords() {
        $words = $this->input->post('word', TRUE);

        $this->bayesian->stopwords($words);
    }

    function form() {
        $this->load->helper('url');
        $form = "
            <form method=\"post\" action=\"".site_url('filter/autotag_classify')."\">
            <textarea name=\"content\" cols=\"50\" rows=\"10\"></textarea>
            <br> <input type=\"submit\" name=\"submit\" value=\"Submit\" />
            </form>
        ";

        echo $form;
    }
}
