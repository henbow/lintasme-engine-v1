<?php
class Publisher extends Controller {
    var $data;

    function __construct() {
        parent::__construct();

        $this->load->database("default");
        $this->load->library('mongo_db');

        date_default_timezone_set('Asia/Jakarta');
    }

    function test(){
        $ret['status'] = 200;
        $ret['result'] = "test ok";

        $this->_output($ret,TRUE);
    }

    function popular(){
        $rv = $this->db->query("
            SELECT `pnr_post_ID`,`pnr_pub_member` FROM `publisher_social_effect`
            WHERE `pnr_home_popular` = 1 AND `pnr_homepop_date` >= DATE(NOW())
            ORDER BY `pnr_ID` DESC LIMIT 0,10
        ")->result_array();

        foreach($rv as $key => $val){

            $det = $this->mongo_db->where(array(
                'activity' => 'popular',
                'post_id' => $val['pnr_post_ID']
            ))->limit(1)->get('backend_user_stat');

            $rv[$key]['popular_log'] = $det;
        }

        $ret['status'] = 200;
        $ret['result'] = $rv;

        $this->_output($ret,TRUE);
    }

    function _output($input, $format_json = FALSE) {
        $this->load->helper('json');

        $this->output->set_header("HTTP/1.0 200 OK");
        $this->output->set_header("HTTP/1.1 200 OK");
        $this->output->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s', time()).' GMT');
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Content-type: application/json");
        if($format_json === TRUE) {
            $this->output->set_output(json_readable_encode($input));
        } else {
            $this->output->set_output(json_encode($input));
        }
    }
}