<?php
class Domain extends Controller {
	var $data;
	
	function __construct() {
		parent::__construct();
		
		$this->load->database("default");
		$this->load->model('domain_model', 'domain');
		
		date_default_timezone_set('Asia/Jakarta');
	}
	
	function update_post_view($domain = '', $post_id = '') {
		$dom_view = $this->domain->update_domain_view($domain);
		$post_view = $this->domain->update_post_view($post_id);
		
		if($dom_view == FALSE && $post_view == FALSE) {
			$result = array(
				'status' => "FAILED",
				'views' => array(
					$domain => "",
					$post_id => ""
				)
			);
		} else {
			$result = array(
				'status' => "OK",
				'views' => array(
					$domain => $dom_view,
					$post_id => $post_view
				)
			);
		}
		
		$this->_output($result);
	}

    function unset_engine_popular($postid){
        $ret = $this->domain->unset_popular_social_effect($postid);
        echo $ret;
    }

    function most_tweeted_news($limit = 10){
        $this->load->model('report_model', 'report');
        $startdate = date("Y-m-d H:i:s", time() - ( 60 * 60 * 24 * 1 ));
        $enddate = date("Y-m-d H:i:s", time() - ( 60 * 60 * 24 * 0 ));

        $sql = "SELECT `se_post_ID`,`se_title`,`se_domain_ID`,`se_source`,se_social_effect FROM engine_social_effect WHERE se_tweeted = '1'
 AND se_tweet_date BETWEEN '{$startdate}' AND '{$enddate}' GROUP BY `se_source` ORDER BY se_social_effect DESC LIMIT 0,5";

        $this->default_db = $this->load->database("default", TRUE);
        $most_tweeted_news = $this->default_db->query($sql)->result_array();

        //$most_tweeted_news = $this->report->get_most_tweeted_news($limit);
        $this->_output($most_tweeted_news,TRUE);
    }
	
	function _output($input, $format_json = FALSE) {
		$this->load->helper('json');
		
		$this->output->set_header("HTTP/1.0 200 OK");
		$this->output->set_header("HTTP/1.1 200 OK");
		$this->output->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s', time()).' GMT');
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Content-type: application/json");
		if($format_json === TRUE) {
			$this->output->set_output(json_readable_encode($input));
		} else {
			$this->output->set_output(json_encode($input));
		}
	}
}
