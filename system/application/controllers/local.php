<?php 
session_start();

class Local extends Controller {
	var $maps_api_url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&sensor=true&language=id";
	var $data;
	
	function __construct() {
		parent::__construct();
		
		$this->load->model('local_model', 'localm');
		$this->load->model('domain_model', 'domain');
		$this->load->model('daemon_model', 'daemon');
		
		date_default_timezone_set('Asia/Jakarta');
		
		$logged_in = $_SESSION['logged_in'];
		
		$this->data['uid']   = $_SESSION['uid'];
		$this->data['uname'] = $_SESSION['uname'];
		$this->data['level'] = $_SESSION['level'];
		
		if($logged_in == '') {
			redirect('home');
			exit();
		}
	}
	
	function index() {
		redirect('local/domain');
	}
	
	function domain() {		
		$page = isset($_GET['page_num']) ? $_GET['page_num'] : 0;
		$category = array();
		
		foreach($this->domain->get_category('array') as $cat) {
			$category[$cat['id']] = $cat['displayName'];
		}
		
		$this->load->library('pagination');
		
		$config['base_url'] = rtrim(site_url(), '/').'?c=local&m=domain';
		$config['total_rows'] = $this->localm->get_total_domain();
		$config['per_page'] = 30;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'page_num';
		$config['full_tag_open'] = '<ul class="stream_page">';
		$config['full_tag_close'] = '</ul>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="round-left"><a class="page_link active-page round-left">';
		$config['cur_tag_close'] = '</a></li>';
		
		$this->pagination->initialize($config);
		
		$domain = $this->localm->limit($config['per_page'], $page)->get_domain();
		
		$this->data['domain_list'] = $domain;
		$this->data['category'] = $category;
		$this->data['current_page'] = base64_encode(current_url()."?".$_SERVER['QUERY_STRING']);
		$this->data['pages'] = $this->pagination->create_links();
		
		$this->load->view('local/list_domain', $this->data);
	}

	function add_domain() {				
		$this->data['category'] = $this->domain->get_category('array');
		$this->data['cities'] = $this->localm->get_city();
		
		$this->load->view('local/add_domain', $this->data);
	}
	
	function do_add_domain() {						
		$domain = trim($this->input->post('domain'));
		$category = $this->input->post('category');		
		$category_name = $this->input->post('category_name');
		$topic = $this->input->post('topic');	
		$topic_name = $this->input->post('topic_name');	
		$city = $this->input->post('city');				
		$rss_url = trim(str_replace('http://', '', $this->input->post('rss_url')));	
		$crawl_type = $this->input->post('crawl_type');
		$title_pattern = $this->input->post('title_pattern');
		$desc_pattern = $this->input->post('desc_pattern');
		$image_pattern = $this->input->post('image_pattern');
		$status = $this->input->post('active') == '1' ? 'ACTIVE' : 'INACTIVE';
		
		$insert = $this->localm->add_domain($domain, $category, $category_name, $topic, $topic_name, $city, $rss_url, date('Y-m-d H:i:s'), $crawl_type, $title_pattern, $desc_pattern, $image_pattern, $status);
		
		if($insert) redirect('local/domain');
	}
	
	function edit_domain($id, $ref = '') {		
		$domain = $this->localm->get_domain($id);
		$this->data['domain'] = $domain[0];
		$this->data['category'] = $this->domain->get_category('array');
		$this->data['cities'] = $this->localm->get_city();
		$this->data['dom_ID'] = $id;
		$this->data['referer'] = $ref;
		
		$this->load->view('local/edit_domain', $this->data);
	}
	
	function do_edit_domain($id, $ref = '') {							
		$domain = trim($this->input->post('domain'));
		$category = $this->input->post('category');		
		$category_name = $this->input->post('category_name');
		$topic = $this->input->post('topic');	
		$topic_name = $this->input->post('topic_name');	
		$city = $this->input->post('city');				
		$rss_url = trim(str_replace('http://', '', $this->input->post('rss_url')));
		$crawl_type = $this->input->post('crawl_type');
		$title_pattern = $this->input->post('title_pattern');
		$desc_pattern = $this->input->post('desc_pattern');
		$image_pattern = $this->input->post('image_pattern');
		$status = $this->input->post('active') == '1' ? 'ACTIVE' : 'INACTIVE';
	
		$update = $this->localm->edit_domain($id, $domain, $category, $category_name, $topic, $topic_name, $city, $rss_url, $crawl_type, $title_pattern, $desc_pattern, $image_pattern, $status);
	
		if($update) redirect(str_replace('//&', '', base64_decode($ref)));
	}
	
	function delete_domain($id) {		
		$delete = $this->localm->delete_domain($id);
		
		if($delete) redirect('local/domain');
	}

	function city() {
		$page = isset($_GET['page_num']) ? $_GET['page_num'] : 0;
				
		$this->load->library('pagination');
		
		$config['base_url'] = rtrim(site_url(), '/').'?c=local&m=city';
		$config['total_rows'] = $this->localm->get_total_city();
		$config['per_page'] = 20;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'page_num';
		$config['full_tag_open'] = '<ul class="stream_page">';
		$config['full_tag_close'] = '</ul>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="round-left"><a class="page_link active-page round-left">';
		$config['cur_tag_close'] = '</a></li>';
		
		$this->pagination->initialize($config);
		
		$cities = $this->localm->limit($config['per_page'], $page)->get_city();
				
		$this->data['cities'] = $cities;
		$this->data['pages'] = $this->pagination->create_links();
		
		$this->load->view('local/list_city', $this->data);
	}
	
	function add_city() {
		$this->data['provinces'] = $this->localm->get_province();
		$this->data['countries'] = $this->localm->get_country();
				
		$this->load->view('local/add_city', $this->data);
	}
	
	function do_add_city() {				
		$country = trim($this->input->post('country'));
		$province = $this->input->post('province');
		$name = $this->input->post('name');
		$desc = $this->input->post('desc');
		$long = $this->input->post('long');
		$lat = $this->input->post('lat');
		$status = $this->input->post('active') ? $this->input->post('active') : 'INACTIVE';
		$upload_data = $this->do_city_photo_upload(str_replace(' ', '_', strtolower($name)));
		$image = $upload_data === FALSE ? "" : $upload_data['file_name'];
		
		$insert = $this->localm->add_city($country, $province, $name, $desc, $long, $lat, $image, $status);
		
		if($insert) redirect('local/city');
	}
		
	function edit_city($id) {
		$city = $this->localm->get_city($id);
				
		$this->data['id'] = $id;
		$this->data['city'] = $city[0];
		$this->data['provinces'] = $this->localm->get_province();
		$this->data['countries'] = $this->localm->get_country();
				
		$this->load->view('local/edit_city', $this->data);
	}
	
	function do_edit_city($id, $uri = '') {					
		$country = trim($this->input->post('country'));
		$province = $this->input->post('province');
		$name = $this->input->post('name');
		$desc = $this->input->post('desc');
		$long = $this->input->post('long');
		$lat = $this->input->post('lat');
		$status = $this->input->post('active') ? $this->input->post('active') : 'INACTIVE';
		$upload_data = $this->do_city_photo_upload(str_replace(' ', '_', strtolower($name)));
		$image = $upload_data === FALSE ? "" : $upload_data['file_name'];
		
		$insert = $this->localm->edit_city($id, $country, $province, $name, $desc, $long, $lat, $image, $status);
		
		if($insert) redirect('local/city');
	}
	
	function do_city_photo_upload($city_name)
	{
		$config['upload_path'] = './assets/city/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '100';
		$config['max_width'] = '1024';
		$config['max_height'] = '768';
		$config['overwrite'] = TRUE;
		$config['file_name'] = $city_name;
		
		$this->load->library('upload', $config);
	
		if ( ! $this->upload->do_upload('photo')) {
			// print_r($this->upload->display_errors());die;
			return FALSE;
		} else {
			return $this->upload->data();
		}
	}	
	
	function delete_city($id) {		
		$delete = $this->localm->delete_city($id);
		
		if($delete) redirect('local/city');
	}
}