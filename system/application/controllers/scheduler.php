<?php
session_start();

class Scheduler extends Controller {
	var $_data;
	
	function __construct() {
		parent::__construct();
		
		$this->load->model('domain_model', 'domain');
		$this->load->model('daemon_model', 'daemon');
		$this->load->model('local_model', 'localm');
		
		date_default_timezone_set('Asia/Jakarta');
		
		$logged_in = $_SESSION['logged_in'];
		
		$this->_data['uid']   = $_SESSION['uid'];
		$this->_data['uname'] = $_SESSION['uname'];
		$this->_data['level'] = $_SESSION['level'];
		
		if($logged_in == '') {
			redirect('home');
			exit();
		}		
	}
	
	function index() {
		
	}
}
