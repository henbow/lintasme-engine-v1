<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autotag extends Controller {
	
	function __construct() {
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->helper('file');
	}
	
	function index() {
		$this->load->model('tag_model', 'tag');
		print '<pre>';
		print_r($this->tag->get_tags());
		print '</pre>';
	}
	
	function process() {
		$input = isset($_GET['content']) ? $_GET['content'] : $this->input->post('content');
		$tags['result'] = array();
		
		if($input != '') {
			$this->load->library('WordAnalyzer', '', 'analyzer');
			$this->load->model('tag_model', 'tag');
	
			$result = array();
			$keywords = array();
			
			$text = preg_replace('/[^a-z0-9]+/i',' ',strip_tags($input));
			
			$this->analyzer->analyzeString($text, 3);
			
			//$keywords = $this->analyzer->getKeyWordDensityTable();			
			$result = $this->analyzer->generateTags($text);
			
			$tags = array();
			if(count($result) > 0) {
				foreach ($result as $word => $count) {
					$tags['result'][$word] = $count;
				}
			} 
		}
		
		$this->__response($tags['result']);
	}
	
	/**
	 * Add tag via API
	 * @access public
	 * @method POST
	 * @param tags using format tag1,tag2,tag3,...
	 * @return JSON
	 */
	function do_add_tag() {
		$this->load->model('tag_model', 'tag');
		
		$words = explode(",", trim(strtolower($this->input->post('tag'))));
		$this->tag->add_tag($words);
		
		$this->__response(array('status' => 'OK', 'message' => "Success add $words as tags."));
	}
	
	function add_tag($display = 'false') {
            $this->load->model('tag_model', 'tag');
		
            if($this->input->post('submit')) {
                $words = explode(",", strtolower($this->input->post('tag')));
                $added = $this->tag->add_tag($words);

                $this->load->database("default");
                foreach($added as $key => $val){
                    $this->db->insert('autoaddtags_log',array('tagname' => $val));
                }
            }

            if($this->input->post('api')) {
                $words = explode(",", strtolower($this->input->post('tag')));
                $added = $this->tag->add_tag($words);

                $this->load->database("default");
                foreach($added as $key => $val){
                    $this->db->insert('autoaddtags_log',array('tagname' => $val));
                }

                echo json_encode($added);
                return;
            }
		
            $display_tags = isset($_GET['display']) ? $_GET['display'] : $display;

            echo '<strong>Tag List</strong><br><form method="post" action="'.site_url().'?c=autotag&m=add_tag&display='.$display_tags.'">';
            echo '<input type="text" name="tag" value="" /><input type="submit" name="submit" value="Add Tag" /><br>';
            echo '</form>';
            echo 'Tags count: <strong>' . $this->tag->get_tags_count() . ' words</strong><br>';

            if(($display_tags == 'true')) {
                $tags = $this->tag->get_tags();

                foreach ($tags as $tag) {
                    echo $tag.' '.anchor('?c=autotag&m=delete_tag&tag='.$tag,'[x]').'<br>';
                }
            }
	}
	
	function add_tag_from_file($display = 'false') {
            $this->load->model('tag_model', 'tag');

            if($this->input->post('submit')) {
                $tag_data = explode("\n",read_file('./files/tags1.txt'));
                $this->tag->add_tag($tag_data);	
            }

            $display_tags = isset($_GET['display']) ? $_GET['display'] : $display;

            echo '<strong>Add Tag From File</strong><br><form method="post" action="'.site_url().'?c=autotag&m=add_tag_from_file&display='.$display_tags.'">';
            echo '<input type="submit" name="submit" value="Add Tag" /><br>';
            echo '</form>';
            echo 'Tags count: <strong>' . $this->tag->get_tags_count() . ' words</strong><br>';

            $tags = $this->tag->get_tags();
            foreach ($tags as $tag) {
                echo $tag.' '.anchor('?c=autotag&m=delete_tag&tag='.$tag,'[x]').'<br>';
            }
	}
	
	function delete_tag($tag = '') {
            $tag = isset($_GET['tag']) ? $_GET['tag'] : $tag;

            $this->load->model('tag_model', 'tag');

            $this->tag->delete_tags($tag);

            redirect('?c=autotag&m=add_tag&display=true');
	}
	
	function tag_to_lower() {
            $this->load->model('tag_model', 'tag');

            $tags = $this->tag->get_tags();

            foreach ($tags as $tag) {
                $lower_tag = strtolower($tag);
                $this->tag->delete_tags($tag);
                $words = explode(",", $lower_tag);
                $this->tag->add_tag($words);
            }

            $tags = $this->tag->get_tags();
            foreach ($tags as $tag) {
                echo $tag.' '.anchor('?c=autotag&m=delete_tag&tag='.$tag,'[x]').'<br>';
            }
	}

	function add_stopwords() {
            $this->load->model('tag_model', 'tag');

            if($this->input->post('submit')) {
                $word = explode(",", $this->input->post('words'));
                $this->tag->add_stopwords($word);			
            }

            $words = $this->tag->get_stopwords();

            echo '<strong>Stopwords List</strong><br><form method="post" action="'.current_url().'">';
            echo '<input type="text" name="words" value="" /><input type="submit" name="submit" value="Add Stopwords" />';
            echo '</form>';
            echo 'Stopwords count: <strong>' . $this->tag->get_stopwords_count() . ' words</strong><br>';

            foreach ($words as $word) {
                echo $word.' '.anchor('autotag/delete_stopwords/'.$word,'[x]').'<br>';
            }
	}
	
	function add_stopwords_from_file() {
            $this->load->model('tag_model', 'tag');
            $this->load->helper('form');

            if($this->input->post('submit')) {		
                $config['upload_path'] = './files/';
                $config['allowed_types'] = '*';
                
		$this->load->library('upload', $config);
                
                if ($this->upload->do_upload('sw_file'))
		{
                    $upload_data = $this->upload->data();
                    $stopwords_data = explode("\n",read_file('./files/'.$upload_data['file_name']));
                    $this->tag->add_stopwords($stopwords_data);	
		}
                else
                {
                    $error = array('error' => $this->upload->display_errors());
                    print '<pre>';print_r($error);print '</pre>';
                }
            }

            echo '<strong>Add Stopwords From File</strong><br>'.form_open_multipart(current_url());
            echo '<input type="file" name="sw_file" /><br>';
            echo '<input type="submit" name="submit" value="Add Stopwords" /><br>';

            $words = $this->tag->get_stopwords();
            foreach ($words as $word) {
                echo $word.' '.anchor('autotag/delete_stopwords/'.$word,'[x]').'<br>';
            }

            echo '</form>';
	}
	
	function export_tags() {
		$this->load->model('tag_model', 'tag');
	
		if($this->input->post('submit')) {
			$config['upload_path'] = './files/';
			$config['allowed_types'] = 'txt';
	
			$this->load->library('upload', $config);
	
			$stopword = "";
			$words = $this->tag->get_tags();
			$fp = fopen('/var/www/html/engine.lintas.me/files/tags.txt', 'w');
			
			foreach ($words as $word) {
				$stopword .= $word . "\n";
			}
				
			fwrite($fp, rtrim($stopword, "\n"));
			fclose($fp);
				
			echo "Exported file: " . anchor(base_url().'files/tags.txt',"tags.txt") . "<br />";
		}
	
		echo '<strong>Export Stopwords</strong><br><form enctype="multipart/form-data" method="post" action="'.current_url().'">';
		echo '<input type="submit" name="submit" value="Export Stopwords" /><br>';
		echo '</form>';
	}
	
	function export_stopwords() {
		$this->load->model('tag_model', 'tag');
	
		if($this->input->post('submit')) {
			$config['upload_path'] = './files/';
			$config['allowed_types'] = 'txt';
				
			$this->load->library('upload', $config);
	
			$stopword = "";
			$words = $this->tag->get_stopwords();
			$fp = fopen('/var/www/html/engine.lintas.me/files/stopwords.txt', 'w');
			foreach ($words as $word) {
				$stopword .= $word . "\n";
			}		
			
			fwrite($fp, rtrim($stopword, "\n"));
			fclose($fp);
			
			echo "Exported file: " . anchor(base_url().'files/stopwords.txt',"Stopwords.txt") . "<br />";
		}
	
		echo '<strong>Export Stopwords</strong><br><form enctype="multipart/form-data" method="post" action="'.current_url().'">';
		echo '<input type="submit" name="submit" value="Export Stopwords" /><br>';
		echo '</form>';
	}
	
	function delete_stopwords($word = '') {
		$word = isset($_GET['word']) ? $_GET['word'] : $word;
		
		$this->load->model('tag_model', 'tag');
		
		$this->tag->delete_stopwords($word);
		
		redirect('autotag/add_stopwords');
	}
		
	/**
	 * Process array that returned by API method to JSON
	 * @access private
	 * @param array
	 * @param boolean Is pretty print?
	 * @return JSON
	 */
	function __response($result, $is_pretty_print = FALSE) {
		$this->output->set_header("HTTP/1.0 200 OK");
		$this->output->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s', time()).' GMT');
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Content-Type: application/json");
		if($is_pretty_print === FALSE) {
			$this->output->set_output(json_encode($result));
		} else {
			$this->output->set_output(json_readable_encode($result));
		}
	}
}
