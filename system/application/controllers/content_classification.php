<?php
session_start();

class Content_classification extends Controller {
	var $_data;
	var $train_url = "";
	var $classifier_url = "";
		
	function __construct() {
		parent::__construct();
		
		$this->load->model('domain_model', 'domain');
		$this->load->model('daemon_model', 'daemon');
		$this->load->model('local_model', 'localm');
		
		
		$this->train_url = site_url("filter/train");
		$this->classifier_url = site_url("filter/classify");
		
		date_default_timezone_set('Asia/Jakarta');
		
		$logged_in = $_SESSION['logged_in'];
		
		$this->_data['category'] = $this->domain->get_category('array');
		$this->_data['uid']   = $_SESSION['uid'];
		$this->_data['uname'] = $_SESSION['uname'];
		$this->_data['level'] = $_SESSION['level'];
		
		if($logged_in == '') {
			redirect('home');
			exit();
		}		
	}
	
	function _view() {
		$this->load->view('main_machine_learning', $this->_data);
	}
	
	function index() {
		redirect('content_classification/train');
	}
	
	function train() {
            $content = $this->input->post('content'); 
            $class   = $this->input->post('classified_as');
            $train   = "";

            if($this->input->post('content')) {
                $fields = array('content' => $content, 'classified_as' => $class);
                $fields_string = '';

                foreach($fields as $key => $value) {
                    $fields_string .= $key.'='.$value.'&';
                }

                rtrim($fields_string, '&');

                $fch = curl_init();

                curl_setopt($fch,CURLOPT_URL, $this->train_url);
                curl_setopt($fch,CURLOPT_POST, count($fields));
                curl_setopt($fch,CURLOPT_POSTFIELDS, $fields_string);
                curl_setopt($fch,CURLOPT_RETURNTRANSFER, TRUE);

                $train = json_decode(curl_exec($fch), TRUE);
                $train = $train['message'];

                curl_close($fch);
            }

            $this->_data['train_message'] = $train;
            $this->_data['module'] = "train_form";

            $this->_view();
	}

	function test_classifier() {
		$url = $this->input->post('url');
		$result = "";
		
		if($url) {
                    if (!preg_match('!^https?://!i', $url)) $url = 'http://'.$url;

                    $html = file_get_contents($url);

                    require_once(APPPATH . 'libraries/Readability.php');

                    $readability = new Readability($html);			
                    $readability->init();			
                    $content = preg_replace('/[^a-z0-9\s\-\:\'\"\*\(\)\[\]\.\,]+/i', '', html_entity_decode(strip_tags($readability->articleContent->innerHTML)));

                    $fields = array('content' => $content);
                    $fields_string = '';

                    foreach($fields as $key => $value) {
                            $fields_string .= $key.'='.$value.'&';
                    }

                    rtrim($fields_string, '&');

                    $fch = curl_init();

                    curl_setopt($fch,CURLOPT_URL, $this->classifier_url);
                    curl_setopt($fch,CURLOPT_POST, count($fields));
                    curl_setopt($fch,CURLOPT_POSTFIELDS, $fields_string);
                    curl_setopt($fch,CURLOPT_RETURNTRANSFER, TRUE);

                    $result = json_decode(curl_exec($fch), TRUE);
                    $result['content'] = $content;

                    curl_close($fch);
		}
		
		$this->_data['result'] = $result;
		$this->_data['module'] = "ml_tester";
		
		$this->_view();
	}
	
	function tags() {
		$alphabet = isset($_GET['alphabet']) ? $_GET['alphabet'] : 'a';
		$keyword  = isset($_GET['search_tags']) ? $_GET['search_tags'] : '';
		
		$this->load->model('tag_model', 'tag');
				
		$word = $this->input->post('tags', TRUE);
		$message = "";
		
		if($word != '') {
			$words = explode(",", strtolower($word));
			
			$this->tag->add_tag($words);
			
			$message = "Success add tags.";
		}
		
		if($keyword != '') {
			$tags = $this->tag->get_tags_by_keyword($keyword);
			$count = count($tags);
		} else {
			$tags = $this->tag->get_tags_by_alphabet($alphabet);
			$count = $this->tag->get_tags_count();
		}
		
		$this->_data['message'] = $message;
		$this->_data['tag_data'] = $tags;
		$this->_data['total_words'] = $count;
		$this->_data['page'] = $alphabet;
		$this->_data['module'] = "tags";
		
		$this->_view();
	}
	
	function delete_tag() {
		$this->load->model('tag_model', 'tag');
		
		$tag = $_GET['tag'];
		
		$this->tag->delete_tags($tag);
		
		redirect('content_classification/tags');
	}

	function stopwords() {
		$this->config->load('redis');
		$this->load->library('NaiveBayesClassifier', array(
			'store' => array(
				'mode'	=> 'redis',
				'mintrainratio' => $this->config->item('rmintrainratio'),
				'db'	=> array(
					'db_host'	=> $this->config->item('rhost'),
					'db_port'	=> $this->config->item('rport'),
					'namespace'	=> $this->config->item('rnamespace')
				)
			),
			'debug' => $this->config->item('rdebug')
		), 'bayesian');		
		
		$word = $this->input->post('stopword', TRUE);
		$message = "";
		
		if($word) {
			$this->bayesian->stopwords($word);
		}
		
		$this->_data['message'] = $message;
		$this->_data['stopword_data'] = $this->bayesian->getStopWords();
		$this->_data['total_words'] = count($this->_data['stopword_data']);
		$this->_data['module'] = "stopwords";
		
		$this->_view();
	}
	
	function delete_stopword($word) {
		$this->config->load('redis');
		$this->load->library('NaiveBayesClassifier', array(
			'store' => array(
				'mode'	=> 'redis',
				'mintrainratio' => $this->config->item('rmintrainratio'),
				'db'	=> array(
					'db_host'	=> $this->config->item('rhost'),
					'db_port'	=> $this->config->item('rport'),
					'namespace'	=> $this->config->item('rnamespace')
				)
			),
			'debug' => $this->config->item('rdebug')
		), 'bayesian');
		
		$this->bayesian->removeStopword($word);
		
		redirect('content_classification/stopwords');
	}
}
