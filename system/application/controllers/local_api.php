<?php 
class Local_api extends Controller {
	var $maps_api_url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&sensor=true&language=id";
	var $limit = 5;
	
	function __construct() {
		parent::__construct();
		
		$this->load->database("local");
		$this->load->model('local_model', 'localm');
		$this->load->helper('inflector');
		
		date_default_timezone_set('Asia/Jakarta');
	}
	
	function index() {
		redirect('local/domain');
	}
	
	function get_local_area($lat = '', $lng = '') {
		$lat = isset($_GET['lat']) ? $_GET['lat'] : $lat;
		$lng = isset($_GET['lng']) ? $_GET['lng'] : $lng;
		$preserve_latlng = isset($_GET['preserve_latlng']) ? $_GET['preserve_latlng'] : 'true';
		$reverse = isset($_GET['reverse']) ? $_GET['reverse'] : 'false';
		
		$result = $this->_get_location($lat, $lng, ($preserve_latlng == 'true' ? TRUE : FALSE), ($reverse == 'true' ? TRUE : FALSE));
		
		$this->_output($result, TRUE);
	}
	
	function get_news_by_location($lat = '', $lng = '') {
		$lat = isset($_GET['lat']) ? $_GET['lat'] : $lat;
		$lng = isset($_GET['lng']) ? $_GET['lng'] : $lng;
		$location_name = isset($_GET['location']) ? $_GET['location'] : '';
		$page  = isset($_GET['page']) ? ($_GET['page'] == 0 ? 0 : $_GET['page'] - 1) : 0;
		$limit = isset($_GET['limit']) ? $_GET['limit'] : $this->limit;
		$offset = $limit * $page;
		
		if($location_name != '') {
			$search_param = array($location_name);
		} else {
			$location = $this->_get_location($lat, $lng, FALSE, FALSE);
			$search_param = array();
			$prevloop = "";
			
			foreach ($location['results'] as $loc) {
				$local   = explode(', ', $loc);
				$locname = trim(preg_replace('/[^a-zA-Z ]/', '', $local[0]));
				
				if($prevloop == $locname) continue; 
				
				$search_param[] = $locname;
				
				$prevloop = $locname;
			}
		}
		
		$news_data  = array();
		$news_data['locations']  = isset($location['results']) ? $location['results'] : array($location_name);
		$news_data['total_news'] = $this->localm->get_total_news_by_location($search_param);
		$news_data['total_page'] = ceil(intval($news_data['total_news']) / $limit);
		$news_data['limit_per_page'] = $limit;
		$news_data['current_page'] = $page + 1;
		
		$news_items = $this->localm->limit($limit, $offset)->get_news_by_location($search_param);
		$news_data['result'] = array();
		
		foreach($news_items as $news) {
			$parse_url = parse_url($news['origlink']);
			$tags = explode(',', trim($news['tags'] . ',' . $this->localm->get_current_location(),', '));
			
			if(strlen($news['title']) > 5) {
				$news_data['result'][] = array(
							'id' => $news['id'],
							'title' => $news['title'],
							'link_title_url' => preg_replace('/[^a-z0-9\-]/i', '', dash($news['title'])),
							'link_url' => $news['origlink'],
							'message' => $news['description'],
							'creator_id' => "",
							'creator_name' => "",
							'creation_date' => strtotime($news['insert_date']),
							'creation_date_txt' => "",
							'last_update' => "",
							'last_update_txt' => "",
							'pubdate' => strtotime($news['pubdate']),
							'channel_id' => "",
							'channel_name' => $news['category'],
							'domain' => $parse_url['host'],
							'small_images' => $news['image'],
							'medium_images' => $news['image'],
							'large_images' => $news['image'],
							'type' => "article",
							'tag' => $tags,
							'tag_counter' => array(),
							'tag2' => array(),
							'views' => 0
						);
			}
		}
		
		$news_data = count($news_data) > 0 ? $news_data : array('message'=>'No news found for this location.');
		
		$this->_output($news_data, TRUE);
	}

	function get_city() {
		$country = isset($_GET['country']) ? $_GET['country'] : '';
		$province = isset($_GET['province']) ? $_GET['province'] : '';
		$city = isset($_GET['city']) ? $_GET['city'] : '';
		
		
	}
	
	function _get_location($lat, $lng, $include_latlng = TRUE, $reverse = TRUE) {
		$area = $this->localm->get_city_by_position($lat, $lng);
		
		if(count($area) > 0) {
			if($include_latlng === TRUE) {
				$location = ucwords($area['kota']).', '.ucwords($this->localm->get_province_name($area['province_id'])).', '.ucwords($this->localm->get_country_name($area['country_id'])).', '.$area['latitude'].', '.$area['longitude'];
			} else {
				$location = ucwords($area['kota']).', '.ucwords($this->localm->get_province_name($area['province_id'])).', '.ucwords($this->localm->get_country_name($area['country_id']));
			}
			$result['results'] = array($location);
			return $result;
		} else {
			$area = file_get_contents(sprintf($this->maps_api_url, $lat, $lng));
			$area = json_decode($area, TRUE);
			$city = "";
			$province = "";
			$country = "";
			$lat = "";
			$lng = "";
			$formatted_address = array();
			
			foreach ($area['results'] as $location) {
				if($include_latlng === TRUE) {
					$formatted_address[] = $location['formatted_address'].', '.$location['geometry']['location']['lat'].', '.$location['geometry']['location']['lng'];
				} else {
					$formatted_address[] = $location['formatted_address'];
				}
			}
			
			$result['results'] = $reverse === TRUE ? array_reverse($formatted_address) : $formatted_address;
			
			return $result;
		}
	}
	
	function _output($input, $format_json = FALSE) {
		$this->load->helper('json');
		
		$this->output->set_header("HTTP/1.0 200 OK");
		$this->output->set_header("HTTP/1.1 200 OK");
		$this->output->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s', time()).' GMT');
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Content-type: application/json");
		if($format_json === TRUE) {
			$this->output->set_output(json_readable_encode($input));
		} else {
			$this->output->set_output(json_encode($input));
		}
	}
}