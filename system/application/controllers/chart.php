<?php
session_start();

class Chart extends Controller {

    function __construct() {
        parent::__construct();

        $this->load->database("default");
        $this->load->model('domain_model', 'domain');
    }


    function chart_test($start_date = '', $end_date = '') {

        $start_date = isset($_GET['start_date']) ? $_GET['start_date'] : date('Y-m-d', time() - 60 * 60 * 24 * 7);
        $end_date = isset($_GET['end_date']) ? $_GET['end_date'] : date('Y-m-d');

        $this->load->model('report_model', 'report');

        $popular = $this->report->get_total_home_popular_daily($start_date, $end_date);
        $totalpost = $this->report->get_total_post_daily($start_date, $end_date);
        $total_autotweet = $this->report->get_total_autotweet_alldomain_daily($start_date, $end_date);

        $seri['name'] = 'Popular';
        $data['xAxis'] = array(
            'categories' => array(),
            'tickmarkPlacement' => 'on',
            'title' => array(
                'enabled' => false
            )
        );



        foreach($popular['results'] as $key => $val){
            $seri['data'][] = (int) $val['total_home_popular'];
            $data['xAxis']['categories'][] = $val['date'];
        }



        $data['series'][] = $seri;

        unset($seri);

        foreach($totalpost['results'] as $key => $val){
            $seri['data'][] = (int) $val['total_post'];
            $data['xAxis']['categories'][] = $val['date'];
        }

        $seri['name'] = 'Total Post';
        $data['series'][] = $seri;

        unset($seri);

        foreach($total_autotweet['results'] as $key => $val){
            $seri['data'][] = (int) $val['total_autotweet'];
            $data['xAxis']['categories'][] = $val['date'];
        }

        $seri['name'] = 'Total Autotweet';
        $data['series'][] = $seri;


        $data['plotOptions'] = array(
            'area' => array(
                'fillOpacity' => 0.5
            )
        );

        $data['title']['text'] = "Lintas Engine Statistic";
        $data['subtitle']['text'] = "Source: lintas.me";

        //return $this->chart_data($data);
        print '<pre>';print_r($data);print '</pre>';
    }

    function chart_data($setdata = array()){
        $data = array();

        $data['chart']['type'] = "area";
        $data['title']['text'] = "Historic and Estimated Worldwide Population Growth by Region";
        $data['subtitle']['text'] = "Source: Wikipedia.org";

        $data['xAxis'] = array(
            'categories' => array(
                '1750', '1800', '1850', '1900', '1950', '1999', '2050'
            ),
            'tickmarkPlacement' => 'on',
            'title' => array(
                'enabled' => false
            )
        );

        $data['yAxis'] = array(
            'title' => array(
                'text' => 'Post'
            ),
            'labels' => array(
                'formatter' => ''
            )
        );

        $data['tooltip'] = array(
            'shared' => true,
            'valueSuffix' => ' post'
        );

        $data['plotOptions'] = array(
            'area' => array(
                'stacking' => 'normal',
                'lineColor' => '#666666',
                'lineWidth' => 1,
                'marker' => array(
                    'lineWidth' => 1,
                    'lineColor' => '#666666'
                )
            )
        );

        $data['series'][] = array(
            'name' => 'Asia',
            'data' => array(
                502, 635, 809, 947, 1402, 3634, 5268
            )
        );

        $data['series'][] = array(
            'name' => 'Africa',
            'data' => array(
                106, 107, 111, 133, 221, 767, 1766
            )
        );

        $data['series'][] = array(
            'name' => 'Europe',
            'data' => array(
                163, 203, 276, 408, 547, 729, 628
            )
        );

        if(! empty($setdata) ){
            foreach($setdata as $key => $val){
                $data[$key] = $val;
            }
        }

        //return $data;
        $rv['chart'] = $data;
        //print_r($rv);
        //die();
        $this->load->view('test_graph',$rv);
    }


}