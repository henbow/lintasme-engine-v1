<?php
class Crawler extends Controller {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
		echo "Access is not allowed.";
	}
	
	/**
	 * Process URL crawling job and automatically set content tags
	 * Usage example: http://engine.lintas.me/?c=crawler&m=process&url=http://www.bolalob.com/futsal/news/berita-tim-futsal-tax-trisakti-diendorse-apparel-mitre-20129?utm_source=futsal&utm_medium=article_1&utm_campaign=headline&utm_content=Tim+Futsal+Tax+Trisakti+Diendorse+Apparel+Mitre
	 * 
	 * @access public
	 * @method GET
	 * @param string url. URL to be crawled.
	 * @return JSON
	 * 
	 */
	function process() {
		$start = microtime(TRUE);
		$url = $_GET['url'];
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, 'http://localhost:8080/get_image/'.$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		
		$res = json_decode(curl_exec($ch), TRUE);
		curl_close($ch);
		
		print "<pre>";
		print_r($res);
		print "</pre>";
		die;
		
		$this->load->library('WordAnalyzer', '', 'analyzer');
		$this->load->model('tag_model', 'tag');
	
		$result = array();
		$tags = array();
		
		$this->analyzer->analyzeString($res[0]['title'] . " " . $res[0]['content'], 3);
		
		$result = $this->analyzer->generateTags($res[0]['title'] . " " . $res[0]['content']);
			
		if(count($result) > 0) {
			foreach ($result as $word => $count) {
				$tags[] = $word;
			}
		} 
		
		$end = microtime(TRUE);
		
		$imgs = $res[0]['image'];
		$images = NULL;
		
		if(count($imgs) > 1) {
			foreach ($imgs as $img => $width) {
				$images[] = $img;
			} 
		} else {
			$images = $imgs[0];
		}
				
		$result = array(
					'title' => $res[0]['title'],
					'images' => $images,
					'content' => $res[0]['content'],
					'autotag' => array(),
					'favicon' => "",
					'metaimg' => "",
					'term' => $tags,
					'posted' => "",
					'exec_time' => ($end - $start) . " seconds"
				);
		
		$this->__response($result, FALSE);
	}
	
	/**
	 * Process array that returned by API method to JSON
	 * 
	 * @access private
	 * @param array
	 * @param boolean Is pretty print?
	 * @return JSON
	 */
	function __response($result, $is_pretty_print = FALSE) {
		$this->output->set_header("HTTP/1.0 200 OK");
		$this->output->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s', time()).' GMT');
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Content-Type: application/json");
		if($is_pretty_print === FALSE) {
			$this->output->set_output(json_encode($result));
		} else {
			$this->load->helper('json');
			
			$this->output->set_output(json_readable_encode($result));
		}
	}
}