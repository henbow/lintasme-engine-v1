<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post extends Controller  {

    function __construct() {
        parent::__construct();
        session_start();
    }

    function index(){
        $this->load->view('admin_backend/login');
    }

    function add(){
        if(!isset($_SESSION['backend_user']['post_only_ch'])){
            $_SESSION['backend_user']['post_only_ch'] = "";
        }
        if(!empty($_POST)){
            //print_r($_POST);
            $ret = $this->_addpost();
            $ret = json_decode($ret,TRUE);

            if($ret['status'] != 200){
                print_r($ret);
                die();
            }

            if($ret['result']['is_duplicate']){

            }

            $this->data['post'] = $ret['result'];
            print_r($ret);
            echo $ret['result']['id'];
            //redirect('backend/post/success/',$ret['result']['id']);
            return;
        }

        $api_randomuser = json_decode(file_get_contents('http://api.lintas.me/user/fake/format.json'),TRUE);

        $this->data['random_user'] = $api_randomuser['result'];

        $this->load->view('admin_backend/post/post',$this->data);
    }

    function success($id){
        $this->load->view('admin_backend/post/success',$this->data);
    }

    function _addpost(){
        $url = 'http://api.lintas.me/post/new/format.json';

        $params['link_url']         = $_POST['url'];
        $params['message']          = $_POST['post_content'];
        $params['kind']             = $_POST['kind'];
        $params['title']            = $_POST['post_title'];
        $params['idchannel']        = $_POST['origin_id'];
        $params['userid']           = $_POST['rand_user'];
        $params['post_tag_no_auto'] = $_POST['post_tag_no_auto'];

        if(!$_FILES['thumb_pic']['size']){
            $params['thumbnail_url']    = $_POST['post_img_url'];
        }else{
            $localFile = $_FILES['thumb_pic']['tmp_name']; // This is the entire file that was uploaded to a temp location.
            $fileType = $_FILES['thumb_pic']['type'];
            $params['thumb_pic'] = '@'.$localFile.';type='.$fileType;
            $params['thumb_pic_name'] = $_FILES['thumb_pic']['name'];
        }

        $params['key']              = '4p1l1nt45b3t4';


        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
        curl_setopt($ch,CURLOPT_POST, count($params));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_VERBOSE, 1);
        curl_setopt($ch,CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $params);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);
        //$result = json_decode($result,TRUE);
        return $result;
    }
}