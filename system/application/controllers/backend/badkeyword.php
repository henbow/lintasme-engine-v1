<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();

class Badkeyword extends Controller  {

    function __construct() {
        parent::__construct();
        $this->load->model('backend/keyword_model',"keyword");
        $this->load->model('domain_model', 'domain');
        $this->load->model('daemon_model', 'daemon');
        $this->load->model('local_model', 'localm');
        $this->load->model('redis_model');

        date_default_timezone_set('Asia/Jakarta');

        $logged_in = $_SESSION['logged_in'];

        $this->data['uid']   = $_SESSION['uid'];
        $this->data['uname'] = $_SESSION['uname'];
        $this->data['level'] = $_SESSION['level'];

        if($logged_in == '') {
            redirect('home');
            exit();
        }

        require_once APPPATH."libraries/redisent.php";

        $this->config->load('redis');
        $this->redis = new redisent\Redis($this->config->item('rhost'), $this->config->item('rport'));
    }

    function index(){
        $page = isset($_GET['page_num']) ? $_GET['page_num'] : 0;
        $this->data['bad_keyword'] = $this->keyword->setOffset($page)->getBadKeywordList();

        $this->load->library('pagination');

        $config['base_url'] = '/backend/badkeyword/index/';
        $config['total_rows'] = $this->keyword->getBadKeywordTotal();
        $config['per_page'] = $this->keyword->limit;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'page_num';
        $config['full_tag_open'] = '<ul class="stream_page">';
        $config['full_tag_close'] = '</ul>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="round-left"><a class="page_link active-page round-left">';
        $config['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($config);
        $this->data['pages'] = $this->pagination->create_links();

        $this->load->view('admin_backend/badkeyword',$this->data);
    }

    function add(){
        if($_POST['keyword']){
            $ret = $this->keyword->addBadKeyword($_POST['keyword']);
            $this->redis_model->storeCache('engine-test',array("test"=> "save"));
            redirect('backend/badkeyword');
        }

    }


    function delete($id){
        $this->keyword->deleteBadKeyword($id);
        redirect($_SERVER['HTTP_REFERER']);
    }



    function test_get(){

        $ret = $this->redis_model->getCache('engine-test');
        print_r($ret);
    }

    function test_store(){
        $ret = $this->redis_model->storeCache('engine-test',array("test"=> "save"));
        print_r($ret);
    }

    function test_del(){
        $this->redis_model->delCache('newest-article');
        echo "ok";
    }
}