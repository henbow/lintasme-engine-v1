<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends Controller  {

    function __construct() {
        parent::__construct();
        $this->load->model('backend/auth');
    }

    function index(){
        $logout = $this->auth->setLogout();
        if($logout){
            redirect('backend');
        }
    }
}