<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends Controller  {

    function __construct() {
        parent::__construct();
        $this->load->model('backend/auth');
    }

    function index(){
        if(empty($_POST)){
            $this->load->view('admin_backend/login');
        }else{
            $params = array(
                'username' => $_POST['lg_user'],
                'password' => $_POST['lg_pass']
            );
            $user_detail = $this->auth->setLogin($params);
            if(isset($user_detail[0])){
                redirect('backend');
            }else{
                $this->load->view('admin_backend/login');
            }
        }
    }
}