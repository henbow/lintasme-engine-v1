<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Controller  {

    function __construct() {
        parent::__construct();
        $this->load->model('backend/auth');
        $this->isLogin = $this->auth->isLogin();

        if(!$this->isLogin){
            redirect('backend/login');
        }
    }

    function index(){
        $this->load->view('admin_backend/home');
    }

    function test(){
        $ret = json_decode(file_get_contents('http://api.lintas.me/stream/popular/offset/0/limit/20/format.json'),true);
        $this->data['stream'] = $ret['result'];

        $this->load->view('admin_backend/table_view',$this->data);
    }



}