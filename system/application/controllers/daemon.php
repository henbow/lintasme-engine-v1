<?php
session_start();

class Daemon extends Controller {
	var $_data;
	
	function __construct() {
		parent::__construct();
		
		$this->load->database("default");
		$this->load->model('domain_model', 'domain');
		$this->load->model('daemon_model', 'daemon');
		$this->load->model('local_model', 'localm');
		
		date_default_timezone_set('Asia/Jakarta');
		
		$this->_data['category'] = $this->domain->get_category('array');
		$this->_data['uid']   = $_SESSION['uid'];
		$this->_data['uname'] = $_SESSION['uname'];
		$this->_data['level'] = $_SESSION['level'];
	}
	
	function index() {
		if($_SESSION['logged_in'] == '') {
			redirect('home');
			exit();
		}
		
		$this->_data['daemons'] = $this->daemon->get_daemons();
		$this->load->view('daemon_list', $this->_data);
	}
	
	function add() {
		if($_SESSION['logged_in'] == '') {
			redirect('home');
			exit();
		}
		
		$this->load->view('daemon_add', $this->_data);
	}
	
	function do_add() {
		if($_SESSION['logged_in'] == '') {
			redirect('home');
			exit();
		}
		
		$name = $this->input->post('daemon_name');
		$script = $this->input->post('script_path');
		$log = $this->input->post('log_path');
		$param = $this->input->post('parameter');
		$type = $this->input->post('type');
		
		$this->daemon->add_daemon(array(
			'name' => $name,
			'type' => $type,
			'script_path' => $script,
			'log_path' => $log,
			'script_params' => $param
		));
		
		redirect('daemon');
	}
	
	function edit($id) {
		if($_SESSION['logged_in'] == '') {
			redirect('home');
			exit();
		}
		
		$this->_data['daemon'] = $this->daemon->get_daemon_by_id($id);
		$this->load->view('daemon_edit', $this->_data);
	}
	
	function do_edit($id) {
		if($_SESSION['logged_in'] == '') {
			redirect('home');
			exit();
		}
		
		$name = $this->input->post('daemon_name');
		$script = $this->input->post('script_path');
		$log = $this->input->post('log_path');
		$param = $this->input->post('parameter');
		$type = $this->input->post('type');
		
		$this->daemon->edit_daemon($id, array(
			'name' => $name,
			'type' => $type,
			'script_path' => $script,
			'log_path' => $log,
			'script_params' => $param
		));
		
		redirect('daemon');
	}
	
	function start($id, $redirect = FALSE) {
		$daemon = $this->daemon->get_daemon_by_id($id);
		$filename = trim(substr($daemon['script_path'], strrpos($daemon['script_path'], '/')), '/');
		$logfname = trim(substr($daemon['log_path'], strrpos($daemon['log_path'], '/')), '/');
		$params = $daemon['script_params'] ? " ".$daemon['script_params'] : '';
		
		echo '/var/www/engine.lintas.me/tools/execute-daemon.exp '.$filename.' '.$logfname.' '.$daemon['type'].$params;
		//echo exec('/var/www/engine.lintas.me/tools/execute-daemon.exp '.$filename.' '.$logfname.' '.$daemon['type'].$params, $output);
	
		if($redirect === TRUE) redirect('daemon');
	}
	
	function stop($pid, $id, $redirect = FALSE) {
		$daemon = $this->daemon->get_daemon_by_id($id);
		$type = $daemon['type'];
		$logfname = trim(substr($daemon['log_path'], strrpos($daemon['log_path'], '/')), '/');
		$newlog = substr($logfname, 0, strrpos($logfname, '.')).'.'.date('YmdHis').'.log';
				
		echo exec('/var/www/engine.lintas.me/tools/stop-daemon.exp '.$pid, $output1);
		echo exec('/var/www/engine.lintas.me/tools/log-backup.exp '.$logfname.' '.$newlog.' '.$type, $output2);
		
		if($redirect === TRUE) redirect('daemon');
	}
	
	function restart($pid, $id) {
		$this->stop($pid, $id);
		$this->start($id);
	}
}

/**
 * End of file
 * 
 */