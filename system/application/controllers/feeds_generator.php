<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


session_start();

class feeds_generator extends Controller {

    const HTTP_CONNECTTIMEOUT	= 60;
    const HTTP_TIMEOUT		    = 120;
    const HTTP_USERAGENT		= 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.100 Safari/534.30';
    const EXC_HARD			    = 0;
    const EXC_SOFT			    = 1;


    function __construct() {
        parent::__construct();
        $this->load->database("default");
        $this->load->model('domain_model', 'domain');
        $this->load->model('daemon_model', 'daemon');
        $this->load->model('local_model', 'localm');

        date_default_timezone_set('Asia/Jakarta');


        error_reporting(0);
        $this->data['uid']   = $_SESSION['uid'];
        $this->data['uname'] = $_SESSION['uname'];
        $this->data['level'] = $_SESSION['level'];
		

    }

    function index(){
        $logged_in = $_SESSION['logged_in'];
        if($logged_in == '') {
            redirect('home');
            exit();
        }
        $this->list_rss();
    }

    function feed($id = 1) {
        $this->benchmark->mark('code_start');
        error_reporting(0);
        $params = array(
            'page' => "http://www.tribunnews.com/metropolitan",
            'xpath_area' => "//ul[@id='latestul']/li",
            'xpath' => array(
                'href' => "//div/div[@class='fbo f16 ln20']/a/@href",
                'title' => "//div/div[@class='fbo f16 ln20']/a",
                'description' => "//div[@class='pt5']/div[@class='grey2 pt5 f13']",
                'images' => "//div/a/img/@src"
            )
        );

        $params = $this->read_config($id);

        $ret = $this->get_data($params);
        $this->benchmark->mark('code_end');

        $this->elapsed_time = $this->benchmark->elapsed_time('code_start', 'code_end');

        $this->create_log();
        echo $ret;
    }

    function add_page($id = ""){
        $logged_in = $_SESSION['logged_in'];
        if($logged_in == '') {
            redirect('home');
            exit();
        }
        if(!empty($_POST)){
            if($_POST['act'] == "Save"){

                if(!empty($_POST['page_url'])){
                    $ins['url'] = $_POST['page_url'];
                }

                if(!empty($_POST['domain'])){
                    $ins['domain'] = $_POST['domain'];
                }

                $ins['xpath_block'] = $_POST['xpath_block'];
                $ins['xpath_href'] = $_POST['xpath_href'];
                $ins['xpath_title'] = $_POST['xpath_title'];
                $ins['xpath_description'] = $_POST['xpath_description'];
                $ins['xpath_images'] = $_POST['xpath_images'];

                if(!empty($id)){
                    $ret = $this->db->where(array('id' => $id))->update('page2rss_list',$ins);
                    redirect('feeds_generator/list_rss');
                    return;
                }


                $cek = $this->db->where(array('url' => $ins['url']))->get('page2rss_list')->result_array();

                if(empty($cek)){
                    $this->db->insert('page2rss_list',$ins);
                    redirect('feeds_generator/list_rss');
                }else{
                    //echo "update";
                    //print_r($ins);
                    $this->db->where(array('url' => $ins['url']))->update('page2rss_list',$ins);
                    //echo mysql_error();
                    redirect('feeds_generator/list_rss');
                }


            }else{
                $this->preview();
            }

            return;
        }
        $this->load->view('admin_feeds_generator/add_page',$this->data);
    }

    function test(){
        $params = array(
            'page' => "http://www.tribunnews.com/metropolitan",
            'xpath_area' => "//ul[@id='latestul']/li",
            'xpath' => array(
                'href' => "//div/div[@class='fbo f16 ln20']/a/@href",
                'title' => "//div/div[@class='fbo f16 ln20']/a",
                'description' => "//div[@class='pt5']/div[@class='grey2 pt5 f13']",
                'images' => "//div/a/img/@src"
            )
        );

        $ret = $this->get_data($params);
        echo $ret;
    }

    function preview(){
        error_reporting(0);
        $params = array(
            'page' => $_POST['page_url'],
            'xpath_area' => $_POST['xpath_block']
        );

        if(!empty($_POST['xpath_href'])){
            $params['xpath']['href'] = $_POST['xpath_href'];
        }

        if(!empty($_POST['xpath_title'])){
            $params['xpath']['title'] = $_POST['xpath_title'];
        }

        if(!empty($_POST['xpath_description'])){
            $params['xpath']['description'] = $_POST['xpath_description'];
        }

        if(!empty($_POST['xpath_images'])){
            $params['xpath']['images'] = $_POST['xpath_images'];
        }


        $ret = $this->get_data($params);
        echo $ret;
    }

    function list_rss(){
        $logged_in = $_SESSION['logged_in'];
        if($logged_in == '') {
            redirect('home');
            exit();
        }
        $limit = 50;
        $this->load->library('pagination');

        $config['base_url'] = '/feeds_generator/list_rss_page/';
        $config['total_rows'] = $this->db->from('page2rss_list')->count_all_results();
        $config['per_page'] = $limit;
        $config['full_tag_open'] = '<ul class="stream_page">';
        $config['full_tag_close'] = '</ul>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="round-left"><a class="page_link active-page round-left">';
        $config['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($config);

        $this->data['pages'] = $this->pagination->create_links();

        if(empty($_GET['per_page'])){
            $offset = 0;
        }else{
            $offset = $_GET['per_page'];
        }

        $this->data['feeds'] = $this->db->order_by("id", "desc")->limit($limit)->offset($offset)->get('page2rss_list')->result_array();
        $this->load->view('admin_feeds_generator/list',$this->data);
    }

    function stat_rss(){
        $this->data['feeds'] = $this->db->query("
SELECT summary_7days.*,engine_page2rss_list.`url`,engine_page2rss_list.`domain` FROM (
SELECT `page2rss_id`,
AVG(`elapsed_time`) AS avg_elapsed_time,
(COUNT(`id`)/7) AS daily_run_times,
(SUM(`result_new_data_count`)/7) AS daily_new_post,
MIN(`insert_dt`) AS fromtime,
MAX(`insert_dt`) AS lastrun
FROM `engine_page2rss_log`
WHERE `engine_page2rss_log`.`insert_dt` >= ( CURDATE() - INTERVAL 7 DAY )
GROUP BY page2rss_id,DATE_FORMAT(`engine_page2rss_log`.`insert_dt`, '%d/%m/%Y')
) summary_7days
LEFT JOIN `engine_page2rss_list` ON `engine_page2rss_list`.`id` = summary_7days.page2rss_id
        ")->result_array();
        $this->load->view('admin_feeds_generator/stat',$this->data);
    }

    function edit($id){
        $logged_in = $_SESSION['logged_in'];
        if($logged_in == '') {
            redirect('home');
            exit();
        }
        $ret['detail'] = $this->db->where(array('id' => $id))->get('page2rss_list')->result_array();
        if(empty($ret['detail'])){
            return;
        }

        $this->data['id'] = $id;

        $this->data['detail'] = $ret['detail'][0];

        $this->load->view('admin_feeds_generator/edit_page',$this->data);
    }

    function read_config($id){

        $ret = $this->db->where(array('id' => $id))->get('page2rss_list')->result_array();
        if(empty($ret)){
            echo "invalid id";
            die();
            return;
        }

        $this->page2rss_id = $id;
        $this->page2rss_domain = $ret[0]['domain'];

        $params = array(
            'page' => $ret[0]['url'],
            'xpath_area' => $ret[0]['xpath_block'],
            'xpath' => array(
                'href' => $ret[0]['xpath_href'],
                'title' => $ret[0]['xpath_title'],
                'description' => $ret[0]['xpath_description'],
                'images' => $ret[0]['xpath_images']
            )
        );

        return $params;
    }

    function create_log(){
        if(empty($this->page2rss_id)){
            return;
        }

        $log['page2rss_id'] = $this->page2rss_id;
        $log['result_data_json'] = json_encode($this->xpathData);
        $log['result_data_count'] = count($this->xpathData);
        $log['result_new_data_json'] = json_encode($this->xpathDataNew);
        $log['result_new_data_count'] = count($this->xpathDataNew);
        $log['elapsed_time'] = $this->elapsed_time;

        $this->db->insert('page2rss_log',$log);
    }

    function create_result_log($data){
        if(empty($this->page2rss_id)){
            return;
        }
        $log['page2rss_id'] = $this->page2rss_id;
        $log['url'] = $data['href'];
        $log['title'] = $data['title'];
        $log['shortdescription'] = $data['description'];
        if(!empty($data['images'])){
            $log['image_url'] = $data['images'];
        }

        $dbkey = array();
        $dbval = array();

        foreach ($log as $key => $val) {
            $dbkey[] = $key;
            $dbval[] = "'".mysql_real_escape_string($val)."'";
        }

        $q = "INSERT INTO engine_page2rss_result (".implode(', ', $dbkey).") VALUES (".implode(', ', $dbval).")";

        $ret = mysql_query($q);
        if($ret){
            $this->xpathDataNew[] = $data;
        }

        return $ret;
    }

    function get_data($config){
        $this->loadHTML($config['page']);
        $ret = $this->xpathBuildData($config['xpath_area'],$config['xpath'])->xpathBuildRss();
        return $ret;
    }

    function wget($url) {
        if (!function_exists('curl_init'))
            return @file_get_contents($url);

        $curlOptions = array(
            CURLOPT_USERAGENT	=> self::HTTP_USERAGENT,	// The contents of the "User-Agent: " header to be used in a HTTP request.
            CURLOPT_URL		=> $url,			// The URL to fetch.
            CURLOPT_HEADER		=> false,			// TRUE to include the header in the output.
            CURLOPT_FOLLOWLOCATION	=> false,			// TRUE to follow any "Location: " header that the server sends as part of the HTTP header.
            CURLOPT_CONNECTTIMEOUT	=> self::HTTP_CONNECTTIMEOUT,	// The number of seconds to wait while trying to connect. Use 0 to wait indefinitely.
            CURLOPT_TIMEOUT		=> self::HTTP_TIMEOUT,		// The maximum number of seconds to allow cURL functions to execute.
            CURLOPT_RETURNTRANSFER	=> true,			// TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
        );

        $handle = curl_init();

        curl_setopt_array($handle, $curlOptions);

        $result		= curl_exec($handle);
        $curlInfo	= curl_getinfo($handle);

        curl_close($handle);

        if (empty($curlInfo['http_code']))
            throw new Exception('Connection error', self::EXC_SOFT);

        if ($curlInfo['http_code'] != 200){
            echo json_encode($curlInfo);
            //print_r($curlInfo);
            die();
        }

        $this->curl_result = $result;

        return $result;

    }

    function loadHTML($fromURL) {
        $this->pageUrl = $fromURL;
        @$this->doc = DOMDocument::loadHTML($this->wget($fromURL));
        return $this;
    }

    function xpathBuildData($expression,$xpath_config){
        $xpath = new DOMXPath($this->doc);
        $elements = $xpath->query($expression);

        $dom = array();
        foreach($elements as $n){
            $temp_dom = new DOMDocument();
            $temp_dom->appendChild($temp_dom->importNode($n,true));

            $items = array();
            foreach($xpath_config as $key => $val){
                @$this->doc = DOMDocument::loadHTML($temp_dom->saveHTML());
                $xpath = new DOMXPath($this->doc);
                $element_items = $xpath->query($val);
                foreach($element_items as $o){
                    $items[$key] =  trim($o->nodeValue);
                }
            }

            if(!empty($items)){
                $dom[] = $items;
            }
        }

        $this->xpathData = $dom;

        return $this;
    }

    function xpathBuildRss($feedTitle = "Lintas Auto Page to Rss",$feedURL = "http://engine.lintas.me/"){
        if(empty($this->xpathData)){
            header('Content-type: text/json');
            echo $this->curl_result;
            return;
        }


        header('Content-type: application/xml');

        $feedTitle = htmlspecialchars($feedTitle);
        $feedURL   = htmlspecialchars($feedURL);

        $rss = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>
			<rss version=\"2.0\">
				<channel>
					<title>$feedTitle</title>
					<link>$feedURL</link>
					<description>".$this->pageUrl."</description>";

        $data = $this->xpathData;
        foreach ( $data as $key => $val){
            $this->create_result_log($val);

            if(!empty($val['images'])){
                if(substr($val['images'],0,1) == '/'){
                    $val['images'] = "http://".$this->page2rss_domain.$val['images'];
                }elseif(substr($val['images'],0,4) != 'http'){
                    $val['images'] = "http://".$this->page2rss_domain."/".$val['images'];
                }

                $val['description'] = "<img src =\"".$val['images']."\"/>".$val['description'];
            }

            $val['href'] = trim($val['href']);

            if(substr($val['href'],0,1) == '/'){
                $val['href'] = "http://".$this->page2rss_domain.$val['href'];
            }elseif(substr($val['href'],0,4) != 'http'){
                $val['href'] = "http://".$this->page2rss_domain."/".$val['href'];
            }

            $rss .= "
				<item>
					<title>".$val['title']."</title>
					<link>".$val['href']."</link>
					<description>".$val['description']."</description>
					<guid isPermaLink=\"false\">".$val['href']."</guid>
				</item>";
        }

        $rss .= '</channel></rss>';

        $doc = new DOMDocument();
        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;
        $doc->loadXML($rss);

        return $doc->saveXML(); // return a pretty-printed RSS XML string
    }

}
