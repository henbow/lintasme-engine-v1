<?php
session_start();

class Home extends Controller {
	function __construct() {
		parent::__construct();
		
		$this->load->database("default");
		$this->load->model('user_model', 'user');
		
		date_default_timezone_set('Asia/Jakarta');
	}
	
	function index() {
        //mysql_connect("192.168.1.3","lcm","lcm12345") or die(mysql_error());
        //mysql_select_db("lintasengine");
        //echo "connect";
		$logged_in = isset($_SESSION['logged_in']);
				
		if($logged_in) {
			redirect('domain/search');
			exit();
		}
		
		$data['login_url'] = site_url('home/login');

		$this->load->view('login', $data);
	}
	
	function login() {		
		$username = $this->input->post('username', TRUE);
		$password = $this->input->post('password', TRUE);
		
		$userdata = $this->user->auth($username, $password);
		
		if($userdata !== FALSE) {
			$_SESSION = array (
							'uid' => $userdata['user_ID'],
							'uname' => $username,
							'level' => $userdata['level'],
							'logged_in' => TRUE
						);
			
			$user_id = $userdata['user_ID'];
			$login_date = date('Y-m-d H:i:s');
			$ip_address = $this->input->ip_address();
			$user_agent = $this->input->user_agent();
			
			$this->user->set_activity_log($user_id, $login_date, $ip_address, $user_agent);
			
			redirect('domain/search');
		} else {
			redirect('home');
		}
		
		exit();
	}
	
	function logout() {
		$user_id = $_SESSION['uid'];
		$logout_date = date('Y-m-d H:i:s');
		
		$this->user->update_activity_log($user_id, $logout_date);
		
		$_SESSION = array(
						'uid' => "",
						'uname' => "",
						'level' => "",
						'logged_in' => ""
					);
					
		session_destroy();
		
		redirect('home');
	}
}

/**
 * End of file
 */