<?php
class publisher extends Controller {

    function publisher(){
        parent::Controller();
        session_start();
        $this->db = $this->load->database('noprefix', TRUE);
        $this->load->library('mongo_db');
        $login_url = $this->uri->segment(2);
        if($login_url != 'login'){
            if(empty($_SESSION['publisher_user'])){
                redirect('publisher/login');
            }
        }
    }

    function index() {
        echo "ok test";
    }

    function login(){
        $this->load->library('form_validation');


        $this->form_validation->set_rules( 'username','Username', 'required');
        $this->form_validation->set_rules( 'password','Password', 'required');

        if($this->form_validation->run()== false){
            $data['error'] = true;
            if(!empty($_POST)) $data['post'] = $_POST;
            $this->load->view('admin_publisher/publisher_login');
        }else{
            $params['username'] = $this->input->post('username');
            $params['password'] = md5($this->input->post('password'));
            $ret = $this->db->where($params)->from('publisher_member')->get()->result_array();
            if(!empty($ret)){
                $_SESSION['publisher_user'] = $ret[0];
                redirect('publisher/listpublisher_feed/all');
            }else{
                $this->load->view('admin_publisher/publisher_login');
            }

        }
    }

    function listpublisher_feed($filter = 'all',$page_pos= '1'){


        $where = array();
        $limit = 20;
        $offset = ($page_pos - 1)*$limit;

        switch($filter){
            case 'draff':
                $where['status'] = 0;
                break;
            case 'request':
                $where['status'] = 1;
                break;
            case 'reject':
                $where['status'] = 2;
                break;
            case 'approve':
                $where['status'] = 3;
                break;
        }

        if($_SESSION['publisher_user']['is_admin']){
            $where['publisher_member'] = "";
        }else{
            $where['publisher_member'] = " AND publisher_feed.publisher_member = '".$_SESSION['publisher_user']['id']."'";
        }

        if($filter != 'all'){
            $list = $this->db->query("SELECT
            publisher_feed.*,
            lintasengine.publisher_member.nama as publisher_name
        FROM
            publisher_feed
        LEFT JOIN publisher_member
        ON (publisher_feed.publisher_member = publisher_member.id)
        WHERE
            publisher_feed.status = '".$where['status']."'
            AND publisher_feed.status != '4'
            ".$where['publisher_member']."
        ORDER BY
            publisher_feed.create_dt DESC
            limit ".$offset.",".$limit."
        ")->result_array();

            $list_all = $this->db->query("SELECT
            publisher_feed.*
        FROM
            publisher_feed

        WHERE
            publisher_feed.status = '".$where['status']."'
            AND publisher_feed.status != '4'
            ".$where['publisher_member']."
        ORDER BY
            publisher_feed.create_dt DESC
        ")->result_array();

            $total_page = ceil(count($list_all)/$limit);

        }else{


            $list = $this->db->query("SELECT
            publisher_feed.*,
            lintasengine.publisher_member.nama as publisher_name
        FROM
            publisher_feed
        LEFT JOIN publisher_member
        ON (publisher_feed.publisher_member = publisher_member.id)
        WHERE
            publisher_feed.status != '4'
            ".$where['publisher_member']."
        ORDER BY
            publisher_feed.create_dt DESC

            limit ".$offset.",".$limit."
        ")->result_array();

        $list_all = $this->db->query("SELECT
            publisher_feed.*,
            lintasengine.publisher_member.nama as publisher_name
        FROM
            publisher_feed
        LEFT JOIN publisher_member
        ON (publisher_feed.publisher_member = publisher_member.id)
        WHERE
            publisher_feed.status != '4'
            ".$where['publisher_member']."
        ORDER BY
            publisher_feed.create_dt DESC

        ")->result_array();

            $total_page = ceil(count($list_all)/$limit);

        }

        $data['page'] = array(
            'control' => '/publisher/listpublisher_feed/'.$filter,
            'position' => $page_pos,
            'totalpage' => $total_page
        );


        foreach($list as $key => $val){
            $list[$key]['category_name'] = $this->_getChannelName($val['category']);
            $list[$key]['topic_name'] = $this->_getChannelName($val['topic']);
            switch($val['status']){
                case '0':
                    $list[$key]['status'] = "Draff";
                    break;
                case '1':
                    $list[$key]['status'] = "Requested";
                    break;
                case '2':
                    $list[$key]['status'] = "Reject";
                    break;
                case '3':
                    $list[$key]['status'] = "Approve";
                    break;
                case '4':
                    $list[$key]['status'] = "Deleted";
                    break;
            }
        }




        $data['stream'] = $list;
        $this->load->view('admin_publisher/publisher_listfeed',$data);
    }

    function logout(){
        unset($_SESSION['publisher_user']);
        redirect('publisher');
    }

    function admin_only(){

        if($_SESSION['publisher_user']['is_admin']){
            return true;
        }else{
            header("Status: 403 Forbidden");
            die();
        }
    }

    function findpublisher_feed(){
        $this->admin_only();
        $this->load->library('form_validation');

        if(empty($_POST)){
            $data['channel_tree']                     = $this->_getChannelTree();
            $data['member'] = $this->db->where(array('is_active' => 1))->get('publisher_member')->result_array();
            $data['error'] = true;
            if(!empty($_POST)) $data['post'] = $_POST;
            $this->load->view('admin_publisher/publisher_findfeed', $data);
        }else{
            $params['publisher_member'] = $this->input->post('publisher_member');
            $params['category'] = $this->input->post('category');
            $params['status'] = $this->input->post('status');

            if(!empty($params['publisher_member'])){
                $where['publisher_member'] = $params['publisher_member'];
                $q_where['publisher_member'] = " AND publisher_feed.publisher_member = '".$params['publisher_member']."' ";
            }

            if(!empty($params['category'])){
                $where['category'] = $params['category'];
                $q_where['category'] = " AND publisher_feed.category = '".$params['category']."' ";
            }

            if(!empty($params['status'])){
                $where['status'] = $params['status'];
                $q_where['status'] = " AND publisher_feed.status = '".$params['status']."' ";
            }
            $list = $this->db->query("SELECT
            publisher_feed.*,
            lintasengine.publisher_member.nama as publisher_name
        FROM
            publisher_feed
        LEFT JOIN publisher_member
        ON (publisher_feed.publisher_member = publisher_member.id)
        WHERE
            publisher_feed.id is not null
            ".$q_where['publisher_member']."
            ".$q_where['category']."
            ".$q_where['status']."
        ORDER BY
            publisher_feed.create_dt DESC
        ")->result_array();

            foreach($list as $key => $val){
                $list[$key]['category_name'] = $this->_getChannelName($val['category']);
                $list[$key]['topic_name'] = $this->_getChannelName($val['topic']);
                switch($val['status']){
                    case '0':
                        $list[$key]['status'] = "Draff";
                        break;
                    case '1':
                        $list[$key]['status'] = "Requested";
                        break;
                    case '2':
                        $list[$key]['status'] = "Reject";
                        break;
                    case '3':
                        $list[$key]['status'] = "Approve";
                        break;
                    case '4':
                        $list[$key]['status'] = "Deleted";
                        break;
                }
            }

            $data['stream'] = $list;
            $this->load->view('admin_publisher/publisher_listfeed',$data);

        }
    }

    function listpublisher_member($page_pos = '1'){
        $this->admin_only();
        $limit = 10;
        $offset = ($page_pos - 1)*$limit;
        $list = $this->db->where(array('is_active' => 1))->offset($offset)->limit($limit)->get('publisher_member')->result_array();
        $list_all = $this->db->where(array('is_active' => 1))->get('publisher_member')->result_array();
        $data['stream'] = $list;
        $total_page = ceil(count($list_all)/$limit);

        $data['page'] = array(
            'control' => '/publisher/listpublisher_member',
            'position' => $page_pos,
            'totalpage' => $total_page
        );
        $this->load->view('admin_publisher/publisher_listmember',$data);
    }

    function editpublisher_member($id){


        $this->load->library('form_validation');

        if(empty($id)){
            $id = $_SESSION['publisher_user']['id'];
        }else{
            $this->admin_only();
        }


        if(empty($_POST)){
            $this->form_validation->run()== false;
            $data['saved'] = false;
            if(!empty($_POST)) $data['post'] = $_POST;
            $member = $this->db->where(array('id'=> $id))->get('publisher_member')->result_array();
            if(!empty($member[0])){
                $data['member'] = $member[0];
                $_POST = $data['member'];
            }else{
                echo "member detail not found";
                die();
            }
            $this->load->view('admin_publisher/publisher_editmember', $data);
        }else{

            $params['admin_nm'] = $this->input->post('admin_nm');
            $params['company_url'] = $this->input->post('company_url');
            $params['contact_office'] = $this->input->post('contact_office');
            $params['contact_mobile'] = $this->input->post('contact_mobile');
            $params['contact_email'] = $this->input->post('contact_email');
            $params['contact_tw'] = $this->input->post('contact_tw');

            $params['fb_profile_url'] = $this->input->post('fb_profile_url');
            $params['google_plus_url'] = $this->input->post('google_plus_url');
            $params['description'] = $this->input->post('description');

            if(!empty($_FILES['bg_file'])){
                $upl = $this->_uploadIbrta($_FILES['bg_file'],true,true);
                $params['bg_profile'] = $upl['result']['url_path'];
            }

            $update = $this->db->where(array('id' => $id))->update('publisher_member',$params);
            $data['member'] = $_POST;
            if($update){
                $data['saved'] = true;
                $this->load->view('admin_publisher/publisher_editmember', $data);
            }
        }
    }

    function addpublisher_feed(){
        $memberid = $_SESSION['publisher_user']['id'];
        $this->load->library('form_validation');

        $this->form_validation->set_rules( 'category','Category', 'required');
        $this->form_validation->set_rules( 'topic','Topic', 'required');
        $this->form_validation->set_rules( 'rss_url','RSS URL', 'required');

        $data['channel_tree']                     = $this->_getChannelTree();

        if($this->form_validation->run()== false){
            $data['saved'] = false;
            if(!empty($_POST)) $data['post'] = $_POST;
            $this->load->view('admin_publisher/publisher_addfeed',$data);
        }else{
            $params['publisher_member'] = $memberid;
            $params['category'] = $this->input->post('category');
            $params['topic'] = $this->input->post('topic');
            $params['rss_url'] = $this->input->post('rss_url');
            if(!empty($params['rss_url'])){
                $params['rss_url'] = "http://".$params['rss_url'];
            }

            $request = $this->input->post('request');
            if($request == 1){
                $params['status'] = 1 ;
                $this->db->set('request_dt', 'NOW()', FALSE);
            }else{
                $params['status'] = 0 ;
            }

            $insert = $this->db->insert('publisher_feed',$params);
            if($insert){

                $this->load->library('email');
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from('noreply@lintas.me', 'Lintas.me');
                $to = array(
                    'eka.setyobudi@lintas.me',
                    'satria.pinandita@lintas.me',
                    'ardian@lintas.me',
                    'erwin@lintas.me',
                    'hendro@lintas.me'
                );

                $this->email->to('eka.setyobudi@lintas.me,hendro@lintas.me');

                $this->email->subject("New feed from ".$_SESSION['publisher_user']['domain']);

                $msg = "URL : ".$params['rss_url'];
                $this->email->message($msg);

                $this->email->send();

                $data['saved'] = true;
                $this->load->view('admin_publisher/publisher_addfeed',$data);
            }
        }
    }

    function addpublisher_member(){
        $this->admin_only();
        $this->load->library('form_validation');

        //$_POST['nama'] = 'Republika.co.id';
        //$_POST['username'] = 'republika';
        //$_POST['password'] = 'test';

        $this->form_validation->set_rules( 'nama','nama', 'required');
        $this->form_validation->set_rules( 'username','username', 'required');
        $this->form_validation->set_rules( 'password','password', 'required');
        $this->form_validation->set_rules( 'domain','domain', 'required');

        if($this->form_validation->run()== false){
            $data['saved'] = false;
            if(!empty($_POST)) $data['post'] = $_POST;
            $this->load->view('admin_publisher/publisher_addmember',$data);
            //echo validation_errors();
        }else{
            $params['nama'] = $this->input->post('nama');
            $params['username'] = $this->input->post('username');
            $params['password'] = md5($this->input->post('password'));
            $params['domain'] = $this->input->post('domain');
            $insert = $this->db->insert('publisher_member',$params);
            if($insert){
                $data['saved'] = true;
                $this->load->view('admin_publisher/publisher_addmember',$data);
            }
        }
    }

    function settingpublisher_member($id){
        $this->load->library('form_validation');

        if(empty($id)){
            $id = $_SESSION['publisher_user']['id'];
        }else{
            $this->admin_only();
        }

        $this->form_validation->set_rules( 'password','password', 'required');

        if($this->form_validation->run()== false){
            $data['saved'] = false;
            if(!empty($_POST)) $data['post'] = $_POST;
            $member = $this->db->where(array('id'=> $id))->get('publisher_member')->result_array();
            if(!empty($member[0])){
                $data['member'] = $member[0];
                $_POST = $data['member'];
            }else{
                echo "member detail not found";
                die();
            }
            $this->load->view('admin_publisher/publisher_editpassword', $data);
        }else{
            $params['password'] = md5($this->input->post('password'));
            $update = $this->db->where(array('id' => $id))->update('publisher_member',$params);
            $data['member'] = $_POST;
            if($update){
                $data['saved'] = true;
                $this->load->view('admin_publisher/publisher_editpassword', $data);
            }
        }
    }

    function reportingpopular($page_pos = '1',$debug = FALSE){
        $source = $_SESSION['publisher_user']['domain'];
        if(empty($source)){
            echo "domain user empty";
            die();
        }else{
            $limit = 15;
            $offset = ($page_pos - 1)*$limit;
            $params = array(
                'source' => $source,
                'offset' => $offset,
                'limit' => $limit,
                'page' => TRUE,
            );


            $regexObj         = strtolower(trim($params['source']));

            $wheres['source']           = $regexObj;
            $wheres['_deleted'] = FALSE;
            $wheres['_top_article'] = TRUE;
            $order['_last_update']      = -1;

            $rt  = $this->mongo_db->select(array('_id','_top_article'))->where($wheres)->order_by($order)->offset($params['offset'])->limit($params['limit'])->get('user_post');

            $rtall = $this->mongo_db->select(array('_id'))->where($wheres)->count('user_post');


            foreach($rt as $key => $val){
                $this->data['stream'][$key] = $this->_getStreamSingle(array('id' => $val['_id']->{'$id'}));
                $this->data['stream'][$key]['setpopular'] = $this->mongo_db->where(array('activity' => 'popular','post_id' => $val['_id']->{'$id'}))->get('backend_user_stat');
            }

            if($debug){
                print_r($this->data['stream']);
                die();
            }

            $total_page = ceil($rtall/$limit);
            unset($this->data['stream']['totalpage']);



            $this->data['page'] = array(
                'control' => '/publisher/reportingpopular',
                'position' => $page_pos,
                'totalpage' => $total_page
            );
        }

        $this->load->view('admin_publisher/publisher_listpost_popular',$this->data);
    }

    function reporting($channel = 'all',$source = 'all',$start = 'all',$end = 'all',$limit = 20,$page_pos = '1',$debug = FALSE){
        if($_SESSION['publisher_user']['is_admin']){
            redirect('publisher/member_report');
        }
        $paging_page = 'publisher/reporting/'.$channel."/".$source."/".$start.'/'.$end.'/'.$limit;
        $this->data['page_data'] = array(
            'channel' => $channel,
            'source' => $source,
            'start' => $start,
            'end' => $end,
            'limit' => $limit
        );
        if(!empty($_POST)){
            $params['stat_from'] = strtotime($this->input->post('stat_from'));
            $params['stat_from_txt'] = date("Y-m-d",$params['stat_from']);
            $params['stat_end'] = strtotime($this->input->post('stat_end'));
            $params['stat_end_txt'] = date("Y-m-d",$params['stat_end']);
            $params['category'] = $this->input->post('category');
            $params['source'] = $this->input->post('source');

            if(empty($params['category'])){
                $params['category'] = 'all';
            }

            if(empty($params['source'])){
                $params['source'] = 'all';
            }

            if(!empty($_POST['stat_from']) or !empty($_POST['stat_end'])){
                if($params['stat_from'] > $params['stat_end']){
                    $params['stat_end'] = $params['stat_from'];
                    $params['stat_from'] = strtotime($this->input->post('stat_end'));
                }
                $diff = (($params['stat_end']-$params['stat_from'])/86400) + 1;
                $now = $params['stat_end'];
                for($i=0;$i<$diff;$i++){
                    if($i > 0){
                        $day['timestamp'] = $now - 86400;
                    }else{
                        $day['timestamp'] = $now;
                    }
                    $day['day_txt'] = date('Y-m-d',$day['timestamp']);
                    $days[] = $day;
                    $now = $day['timestamp'];
                }
                //print_r($days);
                //secho $diff;
                //die();
                redirect('publisher/reporting/'.$params['category']."/".$params['source']."/".$params['stat_from'].'/'.$params['stat_end']);
                die();
            }else{
                redirect('publisher/reporting/'.$params['category']."/".$params['source']."/".$params['stat_from'].'/'.$params['stat_end']);
                die();
            }


        }

        $limit = $limit;
        $offset = ($page_pos - 1)*$limit;
        $source = $_SESSION['publisher_user']['domain'];
        $params = array(
            'source' => $source,
            'offset' => $offset,
            'limit' => $limit,
            'page' => TRUE
        );

        if($start != 'all'){
            $this->data['start'] = date("Y/m/d",$start);
            $this->data['start_timestamp'] = $start;
            $params['gt'] = $start;
        }

        if($end != 'all'){
            $this->data['end'] = date("Y/m/d",$end);
            $this->data['end_timestamp'] = $end;
            $params['lt'] = $end;
        }

        if($channel != 'all'){
            $params['chid'] = $channel;
            $chname = $this->_getChannelName($params['chid']);
            $isparrent = $this->_isChannelParent($chname);
            if(!empty($isparrent)){
                $params['flagch'] = $chname."-all";
                $params['chid'] = array('$in' => $isparrent);
            }
        }

        if($source == 'all'){

        }

        $this->data['source'] = $source;
        $this->data['channel'] = $channel;

        $this->data['channel_tree']                     = $this->_getChannelTree();


        $this->data['domain'] = $source;
        if(empty($source)){
            echo "domain user empty";
            die();
        }else{
            $this->data['stream']                               = $this->_getStreamBySource($params);

            if($debug){
                print_r($this->data['stream']);
                die();
            }

            $total_page = ceil($this->data['stream']['totalpage']/$limit);
            unset($this->data['stream']['totalpage']);



            $this->data['page'] = array(
                'control' => $paging_page,
                'position' => $page_pos,
                'totalpage' => $total_page
            );
        }

        $this->load->view('admin_publisher/publisher_listpost',$this->data);
        //$this->load->view('admin_publisher/publisher_chart',$this->data);
    }

    function member_report($start="",$end=""){
        if(!empty($_POST)){
            $params['stat_from'] = strtotime($this->input->post('stat_from'));
            $params['stat_end'] = strtotime($this->input->post('stat_end'));
            redirect('publisher/member_report/'.$params['stat_from']."/".$params['stat_end']);
        }else{
            $params['stat_from'] = strtotime("-1 day");
            $params['stat_end'] = time();
        }

        if(!empty($start) and !empty($end)){
            $params['stat_from'] = $start;
            $params['stat_end'] = $end;
        }
        $this->data['params'] = $params;
        $this->data['stream'] = $this->db->query("
			SELECT publisher_member.nama,publisher_member.domain,a.* FROM
			(
				SELECT `pnr_pub_member`, COUNT(*) AS total,
				SUM(IF(`pnr_status` != 'OK',1,0)) AS not_ok,
				SUM(IF(`pnr_status` = 'DUPLICATE',1,0)) AS count_duplicate,
				SUM(IF(`pnr_status` = 'OK_NO_ID_IMAGE',1,0)) AS count_img_fail,
				SUM(IF(`pnr_status` = 'OK_NO_IMAGE',1,0)) AS count_no_img,
				SUM(`pnr_latest`) AS sum_latest ,
				SUM(`pnr_channel_popular`) AS sum_channel,
				SUM(`pnr_home_popular`) AS sum_home
				FROM `publisher_social_effect`
				WHERE `pnr_date` > '".date("Y-m-d",$params['stat_from'])."' and
				`pnr_date` < '".date("Y-m-d",$params['stat_end'])."'
				GROUP BY `pnr_pub_member`
				ORDER BY
				SUM(`pnr_home_popular`) DESC
			) AS a
			LEFT JOIN `lintasengine`.`publisher_member`
			ON (`a`.`pnr_pub_member` = `publisher_member`.`id`)
		")->result_array();

        $this->load->view('admin_publisher/member_report',$this->data);
    }

    function member_report_detail($id_member,$start,$end,$type = 'all'){
        if(!empty($_POST)){
            $params['stat_from'] = strtotime($this->input->post('stat_from'));
            $params['stat_end'] = strtotime($this->input->post('stat_end'));
            redirect('publisher/member_report/'.$params['stat_from']."/".$params['stat_end']);
        }else{
            $params['stat_from'] = strtotime("-1 day");
            $params['stat_end'] = time();
        }

        if(!empty($start) and !empty($end)){
            $params['stat_from'] = $start;
            $params['stat_end'] = $end;
        }
        $this->data['params'] = $params;
        $this->data['type'] = $type;
        $filter = "";
        if($type == "nok"){
            $filter = "and `pnr_status` != 'OK'";
        }

        if($type == "duplicate"){
            $filter = "and `pnr_status` = 'DUPLICATE'";
        }

        if($type == "imgfail"){
            $filter = "and `pnr_status` = 'OK_NO_ID_IMAGE'";
        }

        if($type == "noimg"){
            $filter = "and `pnr_status` = 'OK_NO_IMAGE'";
        }

        if($type == "latest"){
            $filter = "and `pnr_latest` = 1";
        }

        if($type == "channel"){
            $filter = "and `pnr_channel_popular` = 1";
        }

        if($type == "home"){
            $filter = "and `pnr_home_popular` = 1";
        }

        $this->data['stream_detail'] = $this->db->query("SELECT * FROM `publisher_social_effect` WHERE `pnr_pub_member` = '".$id_member."'
		and `pnr_date` > '".date("Y-m-d",$start)."'
		and `pnr_date` < '".date("Y-m-d",$end)."'
		".$filter."
		 order by pnr_ID desc limit 0,5000")->result_array();

        $this->data['stream'] = $this->db->query("
			SELECT publisher_member.nama,publisher_member.domain,a.* FROM
			(
				SELECT `pnr_pub_member`, COUNT(*) AS total,
				SUM(IF(`pnr_status` != 'OK',1,0)) AS not_ok,
				SUM(IF(`pnr_status` = 'DUPLICATE',1,0)) AS count_duplicate,
				SUM(IF(`pnr_status` = 'OK_NO_ID_IMAGE',1,0)) AS count_img_fail,
				SUM(IF(`pnr_status` = 'OK_NO_IMAGE',1,0)) AS count_no_img,
				SUM(`pnr_latest`) AS sum_latest ,
				SUM(`pnr_channel_popular`) AS sum_channel,
				SUM(`pnr_home_popular`) AS sum_home
				FROM `publisher_social_effect`
				WHERE
				`pnr_pub_member` = '".$id_member."'
				and `pnr_date` > '".date("Y-m-d",$start)."'
				and `pnr_date` < '".date("Y-m-d",$end)."'
				GROUP BY `pnr_pub_member`
				ORDER BY
				SUM(`pnr_home_popular`) DESC
			) AS a
			LEFT JOIN `lintasengine`.`publisher_member`
			ON (`a`.`pnr_pub_member` = `publisher_member`.`id`)
		")->result_array();

        $this->load->view('admin_publisher/member_report',$this->data);
    }

    function publishergraph_range($start,$end,$domain = ""){
        if(empty($start)){
            return;
        }
        if(empty($end)){
            return;
        }
        $this->load->model('modelpublisher');
        if(empty($domain)){
            $domain = $_SESSION['publisher_user']['domain'];
        }else{
            $domain = str_replace("_",".",$domain);
        }

        if(empty($domain)){
            die();
        }
        $ret = $this->modelpublisher->getDomainStat($domain,'',array(
            'range' => array(
                'start' => $start,
                'end' => $end
            )
        ));
        $this->_send_json(json_encode($ret));
    }

    function _send_json($data){
        $this->output->set_header("HTTP/1.1 200 OK");
        $this->output->set_header("Content-Type: application/json;");
        $this->output->set_header("Cache-Control: no-store");
        $this->output->set_output($data);
    }

    /**
     * _getChannelTree
     * this function to get channels tree from lintas.me
     * no @param needed to using this function
     *
     */
    function _getChannelTree() {
        $ret = file_get_contents("http://api.lintas.me/categories/format.json");
        $ret = json_decode($ret,TRUE);
        return $ret['result'];
    }

    /**
     * getChannelName
     * This function is to getting channel Name from ID channel become Channel Name
     * @param   $id as channel ID
     * @return  string Channel Name
     *
     */
    function _getChannelName($id = '') {
        $where          = array("_id" => new MongoId($id));
        $result         = $this->mongo_db->where($where)->get('channel');
        return $result[0]["name"];
    }

    function _getStreamSingle($params = array()){
        if(!empty($params['id'])){
            $ret = file_get_contents("http://api.lintas.me/post/detail/id/".$params['id']."/format.json");
            $ret = json_decode($ret,TRUE);
            return $ret['result'];
        }
    }

    function _getStreamBySource($params = array()){
        if(!empty($params['gt']) and !empty($params['lt'])){
            $ret = file_get_contents("http://api.lintas.me/stream/publisher/domain/".$params['source']."/offset/".$params['offset']."/limit/".$params['limit']."/start_dt/".$params['gt']."/end_dt/".$params['lt']."/force_limit/y/format.json");
        }else{
            $ret = file_get_contents("http://api.lintas.me/stream/publisher/domain/".$params['source']."/offset/".$params['offset']."/limit/".$params['limit']."/force_limit/y/format.json");
        }

        $ret = json_decode($ret,TRUE);
        return $ret['result'];
    }

    /**
     *
     * uploadIbrta
     * this function is for upload images to sparda server / lintas.me CDN images
     *
     * @param 	$file 		filename
     * @param 	$resize		images size width x height
     * @param	$cloudit 	boolean send to AWS | not
     *
     * @return boolean TRUE | FALSE
     *
     **/

    function _uploadIbrta($file = array(),$resize = false,$cloudit = false){
        $localFile = $file['tmp_name']; // This is the entire file that was uploaded to a temp location.
        $fileType = $file['type'];
        $fileName = $file['name'];

        $post_data['upload_file'] = '@'.$localFile.';type='.$fileType;
        $post_data['upload_filename'] = $fileName;
        $post_data['upload_resize'] = $resize;
        $post_data['upload_cloud'] = $cloudit;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'http://di.lintas.me/uploadfile');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

        $result = curl_exec($ch);
        if (curl_errno($ch)){
            //$msg = curl_error($ch);
            //echo $msg;
        }
        curl_close ($ch);
        //return $result;
        return json_decode($result,true);
    }

    function info($id = ""){
        $where          = array("_id" => new MongoId($id));
        $result         = $this->mongo_db->where($where)->get('channel');
        print_r($result);
        echo "ok";
    }

    function editpublisher_feed($id){

        $this->load->library('form_validation');

        if(empty($id)){
            $id = $_SESSION['publisher_user']['id'];
        }


        if(empty($_POST)){
            $_SESSION['ref'] = $_SERVER['HTTP_REFERER'];
            $data['error'] = true;
            if(!empty($_POST)) $data['post'] = $_POST;
            //$member = $this->db->where(array('id'=> $id))->get('publisher_feed')->result_array();
            $member = $this->db->query("SELECT publisher_feed.*, lintasengine.publisher_member.nama as publisher_name FROM publisher_feed  LEFT JOIN publisher_member ON (publisher_feed.publisher_member = publisher_member.id) WHERE publisher_feed.id = '".$id."'")->result_array();
            if(!empty($member[0])){
                $data['member'] = $member[0];
                switch($data['member']['status']){
                    case '0':
                        $data['member']['status'] = "Draff";
                        break;
                    case '1':
                        $data['member']['status'] = "Requested";
                        break;
                    case '2':
                        $data['member']['status'] = "Reject";
                        break;
                    case '3':
                        $data['member']['status'] = "Approve";
                        break;
                    case '4':
                        $data['member']['status'] = "Deleted";
                        break;
                }
                $data['member']['category_name'] = $this->_getChannelName($data['member']['category']);
                $data['member']['topic_name'] = $this->_getChannelName($data['member']['topic']);
                $_POST = $data['member'];

            }else{
                echo "feed detail not found";
                die();
            }
            $this->load->view('admin_publisher/publisher_editfeed', $data);
        }else{
            $params['action'] = $this->input->post('action');

            switch($params['action']){
                case 'Delete':
                    $update = $this->db->where(array('id' => $id))->update('publisher_feed',array('status' => 4));
                    redirect($_SESSION['ref']);
                    break;
                case 'Approve':

                    $this->form_validation->set_rules( 'start_dt','start_dt', 'required');
                    $this->form_validation->set_rules( 'end_dt','end_dt', 'required');

                    if($this->form_validation->run()== false){
                        $data['error'] = true;
                        if(!empty($_POST)) $data['post'] = $_POST;
                        $member = $this->db->query("SELECT publisher_feed.*, lintasengine.publisher_member.nama as publisher_name FROM publisher_feed  LEFT JOIN publisher_member ON (publisher_feed.publisher_member = publisher_member.id) WHERE publisher_feed.id = '".$id."'")->result_array();
                        if(!empty($member[0])){
                            $data['member'] = $member[0];
                            switch($data['member']['status']){
                                case '0':
                                    $data['member']['status'] = "Draff";
                                    break;
                                case '1':
                                    $data['member']['status'] = "Requested";
                                    break;
                                case '2':
                                    $data['member']['status'] = "Reject";
                                    break;
                                case '3':
                                    $data['member']['status'] = "Approve";
                                    break;
                                case '4':
                                    $data['member']['status'] = "Deleted";
                                    break;
                            }
                            $data['member']['category_name'] = $this->_getChannelName($data['member']['category']);
                            $data['member']['topic_name'] = $this->_getChannelName($data['member']['topic']);
                        }
                        $this->load->view('admin_publisher/publisher_editfeed', $data);
                    }else{
                        $params = array();
                        $params['start_dt'] = $this->input->post('start_dt');
                        $params['end_dt'] = $this->input->post('end_dt');
                        $params['status'] = 3;

                        $params['use_reader'] = $this->input->post('crawl_type');
                        $params['title_tag'] = $this->input->post('title_pattern');
                        $params['description_tag'] = $this->input->post('desc_pattern');
                        $params['image_tag'] = $this->input->post('image_pattern');

                        $update = $this->db->where(array('id' => $id))->update('publisher_feed', $params);
                        //print_r($params);
                        //die();
                        redirect($_SESSION['ref']);
                    }


                    break;
                case 'Reject':
                    $update = $this->db->where(array('id' => $id))->update('publisher_feed',array('status' => 2));
                    redirect($_SESSION['ref']);
                    break;
            }

        }
    }
}