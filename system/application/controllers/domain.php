<?php
session_start();

class Domain extends Controller {
	var $data;
	var $default_title_pattern = '//channel/item/title';
	var $default_desc_pattern = '//channel/item/description';
	var $default_img_pattern = '//channel/item/description';
	var $rssAPI = "http://www.lintas.me/assets/reader/makefulltextfeed.php?url=%s&max=%s&links=preserve&exc=&format=json&submit=Create+Feed";

	function __construct() {
		parent::__construct();

		$this->load->model('domain_model', 'domain');
		$this->load->model('daemon_model', 'daemon');
		$this->load->model('local_model', 'localm');

		date_default_timezone_set('Asia/Jakarta');

		$logged_in = $_SESSION['logged_in'];

		$this->data['uid']   = $_SESSION['uid'];
		$this->data['uname'] = $_SESSION['uname'];
		$this->data['level'] = $_SESSION['level'];

		if($logged_in == '') {
			redirect('home');
			exit();
		}
	}

	function index() {
		$this->home();
	}

	function home() {
		$this->load->model('report_model', 'report');

		$newest_feeds = $this->report->get_newest_feeds();
		$latest_posts = $this->report->get_latest_post();
		$home_popular = $this->report->get_latest_home_popular();
		$most_active_source = $this->report->get_most_active_source();
		$most_tweeted_news = $this->report->get_most_tweeted_news();
		$most_tweeted_source = $this->report->get_most_tweeted_source();
		$most_viewed_today = $this->report->get_most_viewed_today();
		$most_active_category = $this->report->get_most_active_category();
    $get_most_viewed_last_hour = $this->report->get_most_viewed_last_hour();

		$this->data['newest_feeds'] = $newest_feeds;
		$this->data['latest_posts'] = $latest_posts;
		$this->data['home_popular'] = $home_popular;
		$this->data['most_active_source'] = $most_active_source;
		$this->data['most_tweeted_news'] = $most_tweeted_news;
		$this->data['most_tweeted_source'] = $most_tweeted_source;
		$this->data['most_viewed_today'] = $most_viewed_today;
		$this->data['most_active_category'] = $most_active_category;
        //$this->data['graph_most_viewed']= $this->graph_most_viewed($get_most_viewed_last_hour);

		$this->load->view('dashboard.php', $this->data);
	}

	function list_domain() {
		$page = isset($_GET['page_num']) ? $_GET['page_num'] : 0;
		$category = array();

		foreach($this->domain->get_category('array') as $cat) {
			$category[$cat['id']] = $cat['displayName'];
		}

		$this->load->library('pagination');

		$config['base_url'] = rtrim(site_url(), '/').'?c=domain&m=list_domain';
		$config['total_rows'] = $this->domain->get_total_domain();
		$config['per_page'] = 30;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'page_num';
		$config['full_tag_open'] = '<ul class="stream_page">';
		$config['full_tag_close'] = '</ul>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="round-left"><a class="page_link active-page round-left">';
		$config['cur_tag_close'] = '</a></li>';

		$this->pagination->initialize($config);

		$domains = $this->domain->limit($config['per_page'], $page)->get_domain();

		$this->data['domain_list'] = $domains;
		$this->data['category'] = $category;
		$this->data['current_page'] = base64_encode(current_url()."?".$_SERVER['QUERY_STRING']);
		$this->data['pages'] = $this->pagination->create_links();

		$this->load->view('list', $this->data);
	}

	function add() {
		$this->data['category'] = $this->domain->get_category('array');

		$this->load->view('add_domain', $this->data);
	}

	function do_add() {
		$domain = trim($this->input->post('domain'));
		$category = $this->input->post('category');
		$category_name = $this->input->post('category_name');
		$topic = $this->input->post('topic');
		$rss_url = trim(str_replace('http://', '', $this->input->post('rss_url')));
		$update_every = $this->input->post('update_every');
		$update_every = $update_every == '' ? '0' : trim($update_every);
		$update_type = $this->input->post('period_type');
		$type = "rss"; // $this->input->post('domaintype');
		$whitelist = $this->input->post('whitelist');
		$channel = $this->input->post('channelpop');
		$autotweet = $this->input->post('autotweet');
		$socialeffect = $this->input->post('socialeffect');
		$crawl_type = $this->input->post('crawl_type');
		$title_pattern = $this->input->post('title_pattern');
		$title_pattern = $title_pattern ? $title_pattern : $this->default_title_pattern;
		$desc_pattern = $this->input->post('desc_pattern');
		$desc_pattern = $desc_pattern ? $desc_pattern : $this->default_desc_pattern;
		$img_pattern = $this->input->post('image_pattern');
		$img_pattern = $img_pattern ? $img_pattern : $this->default_img_pattern;
		$status = $this->input->post('active') ? $this->input->post('active') : 'INACTIVE';

		$insert = $this->domain->add_domain(
										$domain,
										$category,
										$category_name,
										$topic,
										$rss_url,
										date('Y-m-d H:i:s'),
										$update_every,
										$update_type,
										$type,
										$whitelist,
										$channel,
										$autotweet,
										$socialeffect,
										$crawl_type,
										$title_pattern,
										$desc_pattern,
										$img_pattern,
										$status);

		if($insert) redirect('domain/index/all');
	}

	function edit($id, $ref = '') {
		$this->data['category'] = $this->domain->get_category('array');
		$this->data['domain'] = $this->domain->get_domain($id);
		$this->data['dom_ID'] = $id;
		$this->data['referer'] = $ref;

		$this->load->view('edit_domain', $this->data);
	}

	function do_edit($id, $ref = '') {
		$domain = trim($this->input->post('domain'));
		$category = $this->input->post('category');
		$category_name = $this->input->post('category_name');
		$topic = $this->input->post('topic');
		$topic = $topic == '' ? $category_name : $topic;
		$rss_url = trim(str_replace('http://', '', $this->input->post('rss_url')));
		$update_every = $this->input->post('update_every');
		$update_every = $update_every == '' ? '0' : trim($update_every);
		$update_type = $this->input->post('period_type');
		$type = "rss"; // $this->input->post('domaintype');
		$whitelist = $this->input->post('whitelist');
		$channel = $this->input->post('channelpop');
		$autotweet = $this->input->post('autotweet');
		$socialeffect = $this->input->post('socialeffect');
		$crawl_type = $this->input->post('crawl_type');
		$title_pattern = $_POST['title_pattern'];
		$title_pattern = $title_pattern ? $title_pattern : $this->default_title_pattern;
		$desc_pattern = $_POST['desc_pattern'];
		$desc_pattern = $desc_pattern ? $desc_pattern : $this->default_desc_pattern;
		$img_pattern = $_POST['image_pattern'];
		$img_pattern = $img_pattern ? $img_pattern : $this->default_img_pattern;
		$status = $this->input->post('active') ? $this->input->post('active') : 'INACTIVE';

		$update = $this->domain->edit_domain(
											$id,
											$domain,
											$category,
											$category_name,
											$topic,
											$rss_url,
											$update_every,
											$update_type,
											$type,
											$whitelist,
											$channel,
											$autotweet,
											$socialeffect,
											$crawl_type,
											$title_pattern,
											$desc_pattern,
											$img_pattern,
											$status);

		if($update) redirect(str_replace('//&', '', base64_decode($ref)));
	}

	function delete($id) {
		$delete = $this->domain->delete_domain($id);

		if($delete) redirect('domain/index/all');
	}

	function setting() {
		$this->data['settings'] = $this->domain->get_setting();

		$this->load->view('setting', $this->data);
	}

	function do_edit_setting() {
		foreach ($_POST as $key => $value) {
			$data[$key] = $value;
		}

		$data['post_home_popular'] = isset($_POST['post_home_popular']) ? $_POST['post_home_popular'] : '0';

		$update = $this->domain->edit_setting($data);

		if($update) redirect('domain/setting');
	}

	function search() {
		$type = $this->input->post('fieldname');

		if($type) {
			if($type == 'topic') {
				$catname = $this->input->post('category_name', TRUE);
				$topic = $this->input->post('topic', TRUE);

				redirect('?c=domain&m=do_search&field=topic&category='.$catname.'&topic='.$topic);
			} else {
				redirect('?c=domain&m=do_search&field='.$type.'&keyword='.urlencode($this->input->post('keyword')));
			}
		}

		$this->data['category'] = $this->domain->get_category('array');

		$this->load->view('search_form', $this->data);
	}

	function do_search() {
		$type = isset($_GET['field']) ? $_GET['field'] : '';
		$keyword = '';

		if($type) {
			if($type == 'topic') {
				$catname = isset($_GET['category']) ? $_GET['category'] : '';
				$topic = isset($_GET['topic']) ? $_GET['topic'] : '';
				$keyword = $catname.'%%'.$topic;
			} else {
				$keyword = isset($_GET['keyword']) ? $_GET['keyword'] : '';
			}
		}

		$paramurl = '?c=domain&m=do_search&field='.$type.'&keyword='.$keyword;
		$page = isset($_GET['page_num']) ? $_GET['page_num'] : 0;
		$category = array();
		foreach($this->domain->get_category('array') as $cat) {
			$category[$cat['id']] = $cat['displayName'];
		}

		$this->load->library('pagination');

		$config['base_url'] = rtrim(site_url(), '/').$paramurl;
		$config['total_rows'] = $this->domain->get_total_search($keyword, $type);
		$config['per_page'] = 20;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'page_num';
		$config['full_tag_open'] = '<ul class="stream_page">';
		$config['full_tag_close'] = '</ul>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="round-left"><a class="page_link active-page round-left">';
		$config['cur_tag_close'] = '</a></li>';

		$this->pagination->initialize($config);

		$domains = $this->domain->limit($config['per_page'], $page)->get_search($keyword, $type);

		//echo $this->db->last_query();die;

		$this->data['domain_list'] = $domains;
		$this->data['category'] = $category;
		$this->data['current_page'] = base64_encode(current_url()."?".$_SERVER['QUERY_STRING']);
		$this->data['pages'] = $this->pagination->create_links();

		$this->load->view('list', $this->data);
	}

	function autotweet_setting() {
		$this->load->model('autotweet_model', 'autotweet');

		$this->data['categories'] = $this->domain->get_category_setting('array');
		//print '<pre>';print_r($this->data['categories']);print '</pre>';
		//print '<pre>';print_r($this->domain->get_sub_category('sports'));print '</pre>';
		//die;

		$this->load->view('autotweet_cat_list', $this->data);
	}

	function autotweet_edit($id) {
		$this->load->model('autotweet_model', 'autotweet');

		if($this->input->post('starttime')) {
			$start = $this->input->post('starttime');
			$end = $this->input->post('endtime');
			$fullday = $this->input->post('fullday');

			$this->autotweet->update_autotweet_setting($id, $start, $end, $fullday);

			redirect('domain/autotweet_setting');
			exit();
		}

		$this->data['cat_id'] = $id;
		$this->data['cat_name'] = $this->domain->get_category_name($id);

		$this->load->view('autotweet_cat_detail', $this->data);
	}

	function autotweet_fullday($id, $type = 'set') {
		$this->load->model('autotweet_model', 'autotweet');

		if($this->autotweet->{$type."_fullday_tweet"}($id)) {
			redirect('domain/autotweet_setting');
		}
	}

	function latest_posts() {
		$this->load->model('report_model', 'report');

		$page = isset($_GET['page_num']) ? intval($_GET['page_num']) : 0;
		$startdate = isset($_GET['start_date']) ? $_GET['start_date'] : date('Y-m-d', time() - 60 * 60 * 24 * 7);
		$enddate = isset($_GET['end_date']) ? $_GET['end_date'] : date('Y-m-d');

		$this->load->library('pagination');

		$query_string = '?c=domain&m=latest_posts';

		if($startdate && $enddate) {
			$query_string .= "&start_date=$startdate&end_date=$enddate";
		}

		$config['base_url'] = rtrim(site_url(), '/').$query_string;
		$config['total_rows'] = $this->report->get_total_post($startdate, $enddate);
		$config['per_page'] = 20;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'page_num';
		$config['full_tag_open'] = '<ul class="stream_page">';
		$config['full_tag_close'] = '</ul>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="round-left"><a class="page_link active-page round-left">';
		$config['cur_tag_close'] = '</a></li>';

		$this->pagination->initialize($config);

		$this->data['latest_posts'] = $this->report->get_latest_post($config['per_page'], $page, $startdate, $enddate);;
		$this->data['pages'] = $this->pagination->create_links();
		$this->data['startdate'] = $startdate;
		$this->data['enddate'] = $enddate;

        $this->load->view('latest_posts', $this->data);
	}

	function latest_home_popular() {
		$this->load->model('report_model', 'report');

		$page = isset($_GET['page_num']) ? $_GET['page_num'] : 0;
		$startdate = isset($_GET['start_date']) ? $_GET['start_date'] : date('Y-m-d', time() - 60 * 60 * 24 * 7);
		$enddate = isset($_GET['end_date']) ? $_GET['end_date'] : date('Y-m-d');

		$this->load->library('pagination');

		$query_string = '?c=domain&m=latest_home_popular';

		if($startdate && $enddate) {
			$query_string .= "&start_date=$startdate&end_date=$enddate";
		}

		$config['base_url'] = rtrim(site_url(), '/').$query_string;
		$config['total_rows'] = $this->report->get_total_home_popular();
		$config['per_page'] = 20;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'page_num';
		$config['full_tag_open'] = '<ul class="stream_page">';
		$config['full_tag_close'] = '</ul>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="round-left"><a class="page_link active-page round-left">';
		$config['cur_tag_close'] = '</a></li>';

		$this->pagination->initialize($config);

		$this->data['latest_home_popular'] = $this->report->limit($config['per_page'], $page)->get_latest_home_popular($config['per_page'], $startdate, $enddate);;
		$this->data['pages'] = $this->pagination->create_links();
		$this->data['startdate'] = $startdate;
		$this->data['enddate'] = $enddate;

        $this->load->view('latest_home_popular', $this->data);
	}

	function most_active_source() {
		$this->load->model('report_model', 'report');

		$page = isset($_GET['page_num']) ? $_GET['page_num'] : 0;
		$startdate = isset($_GET['start_date']) ? $_GET['start_date'] : date('Y-m-d', time() - 60 * 60 * 24 * 7);
		$enddate = isset($_GET['end_date']) ? $_GET['end_date'] : date('Y-m-d');

		$this->load->library('pagination');

		$query_string = '?c=domain&m=latest_home_popular';

		if($startdate && $enddate) {
			$query_string .= "&start_date=$startdate&end_date=$enddate";
		}

		$config['base_url'] = rtrim(site_url(), '/').$query_string;
		$config['total_rows'] = $this->report->get_total_home_popular();
		$config['per_page'] = 20;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'page_num';
		$config['full_tag_open'] = '<ul class="stream_page">';
		$config['full_tag_close'] = '</ul>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="round-left"><a class="page_link active-page round-left">';
		$config['cur_tag_close'] = '</a></li>';

		$this->pagination->initialize($config);

		$this->data['latest_home_popular'] = $this->report->limit($config['per_page'], $page)->get_latest_home_popular($config['per_page'], $startdate, $enddate);;
		$this->data['pages'] = $this->pagination->create_links();
		$this->data['startdate'] = $startdate;
		$this->data['enddate'] = $enddate;

        $this->load->view('latest_home_popular', $this->data);
	}

	function report() {
		$this->load->model('report_model', 'report');

		$page = isset($_GET['page_num']) ? $_GET['page_num'] : 0;
		$startdate = isset($_GET['start_date']) ? $_GET['start_date'] : date('Y-m-d', time() - 60 * 60 * 24 * 7);
		$enddate = isset($_GET['end_date']) ? $_GET['end_date'] : date('Y-m-d');

		$this->load->library('pagination');

		$query_string = '?c=domain&m=report';

		if($startdate && $enddate) {
			$query_string .= "&start_date=$startdate&end_date=$enddate";
		}

		$config['base_url'] = rtrim(site_url(), '/').$query_string;
		$config['total_rows'] = $this->domain->domain_active_only()->get_total_domain();
		$config['per_page'] = 20;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'page_num';
		$config['full_tag_open'] = '<ul class="stream_page">';
		$config['full_tag_close'] = '</ul>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="round-left"><a class="page_link active-page round-left">';
		$config['cur_tag_close'] = '</a></li>';

		$this->pagination->initialize($config);

		$domains = $this->domain->limit($config['per_page'], $page)->domain_active_only()->get_domain();

		$this->data['domain_list'] = $domains;
		$this->data['pages'] = $this->pagination->create_links();
		$this->data['startdate'] = $startdate;
		$this->data['enddate'] = $enddate;
		// $this->data['total_records'] = $config['total_rows'];

        $this->data['chart'] = $this->chart($startdate,$enddate);
		$this->load->view('report_all_domain', $this->data);
	}

	function detail_report($id = '') {
		$this->load->model('report_model', 'report');

		$page = isset($_GET['page_num']) ? $_GET['page_num'] : 0;
		$id = isset($_GET['domid']) ? $_GET['domid'] : $id;
		$startdate = isset($_GET['start_date']) ? $_GET['start_date'] : date('Y-m-d', time() - 60 * 60 * 24 * 7);
		$enddate = isset($_GET['end_date']) ? $_GET['end_date'] : date('Y-m-d');

		$this->load->library('pagination');

		$query_string = '?c=domain&m=detail_report&domid='.$id;

		if($startdate && $enddate) {
			$query_string .= "&start_date=$startdate&end_date=$enddate";
		}

		$config['base_url'] = rtrim(site_url(), '/').$query_string;
		$config['total_rows'] = $this->report->range_rss_date($startdate, $enddate)->get_total_rss_items_domain($id);
		$config['per_page'] = 20;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'page_num';
		$config['full_tag_open'] = '<ul class="stream_page">';
		$config['full_tag_close'] = '</ul>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="round-left"><a class="page_link active-page round-left">';
		$config['cur_tag_close'] = '</a></li>';

		$this->pagination->initialize($config);

		$rss_item = $this->report->limit($config['per_page'], $page)->range_rss_date($startdate, $enddate)->get_rss_items_domain($id);

		$this->data['domain_name'] = $this->domain->get_domain_name($id);
		$this->data['total_records'] = $config['total_rows'];
		$this->data['rss_items'] = $rss_item;
		$this->data['pages'] = $this->pagination->create_links();
		$this->data['startdate'] = $startdate;
		$this->data['enddate'] = $enddate;
		$this->data['id'] = $id;

		$this->load->view('report_detail_domain', $this->data);
	}

	function home_popular_report() {
		$this->load->model('report_model', 'report');

		$page = isset($_GET['page_num']) ? $_GET['page_num'] : 0;
		$startdate = isset($_GET['start_date']) ? $_GET['start_date'] : date('Y-m-d', time() - 60 * 60 * 24 * 7);
		$enddate = isset($_GET['end_date']) ? $_GET['end_date'] : date('Y-m-d');

		$this->load->library('pagination');

		$query_string = '?c=domain&m=home_popular_report';

		if($startdate && $enddate) {
			$query_string .= "&start_date=$startdate&end_date=$enddate";
		}

		$config['base_url'] = rtrim(site_url(), '/').$query_string;
		$config['total_rows'] = $this->report->range_rss_date($startdate, $enddate)->get_total_home_popular();
		$config['per_page'] = 35;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'page_num';
		$config['full_tag_open'] = '<ul class="stream_page">';
		$config['full_tag_close'] = '</ul>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="round-left"><a class="page_link active-page round-left">';
		$config['cur_tag_close'] = '</a></li>';

		$this->pagination->initialize($config);

		$homepop = $this->report->limit($config['per_page'], $page)->range_rss_date($startdate, $enddate)->get_home_popular();

		$this->data['homepop'] = $homepop;
		$this->data['pages'] = $this->pagination->create_links();
		$this->data['startdate'] = $startdate;
		$this->data['enddate'] = $enddate;
		// $this->data['total_records'] = $config['total_rows'];

		$this->load->view('report_home_popular', $this->data);
	}

	function domain_category_report() {
		$this->load->model('report_model', 'report');

		$startdate = isset($_GET['start_date']) ? $_GET['start_date'] : date('Y-m-d', time() - 60 * 60 * 24 * 7);
		$enddate = isset($_GET['end_date']) ? $_GET['end_date'] : date('Y-m-d');

		$this->data['categories'] = $this->domain->get_category('array');
		$this->data['startdate'] = $startdate;
		$this->data['enddate'] = $enddate;

		$this->load->view('report_domain_channel', $this->data);
	}

	function chart($start_date = '', $end_date = '') {
		$this->load->model('report_model', 'report');

        $popular = $this->report->get_total_home_popular_daily($start_date, $end_date);
        $totalpost = $this->report->get_total_post_daily($start_date, $end_date);
        $total_autotweet = $this->report->get_total_autotweet_alldomain_daily($start_date, $end_date);

        $seri['name'] = 'Popular';
        $data['xAxis'] = array(
            'categories' => array(),
            'tickmarkPlacement' => 'on',
            'title' => array(
                'enabled' => false
            )
        );



        foreach($popular['results'] as $key => $val){
            $seri['data'][] = (int) $val['total_home_popular'];
            $data['xAxis']['categories'][] = $val['date'];
        }



        $data['series'][] = $seri;

        unset($seri);

        foreach($totalpost['results'] as $key => $val){
            $seri['data'][] = (int) $val['total_post'];
            $data['xAxis']['categories'][] = $val['date'];
        }

        $seri['name'] = 'Total Post';
        $data['series'][] = $seri;

        unset($seri);

        foreach($total_autotweet['results'] as $key => $val){
            $seri['data'][] = (int) $val['total_autotweet'];
            $data['xAxis']['categories'][] = $val['date'];
        }

        $seri['name'] = 'Total Autotweet';
        $data['series'][] = $seri;


        $data['plotOptions'] = array(
            'area' => array(
                'fillOpacity' => 0.5
            )
        );

        $data['title']['text'] = "Lintas Engine Statistic";
        $data['subtitle']['text'] = "Source: lintas.me";

        return $this->chart_data($data);
		//print '<pre>';print_r($data);print '</pre>';
	}

    function chart_data($setdata = array()){
        $data = array();

        $data['chart']['type'] = "area";
        $data['title']['text'] = "Historic and Estimated Worldwide Population Growth by Region";
        $data['subtitle']['text'] = "Source: Wikipedia.org";

        $data['xAxis'] = array(
            'categories' => array(
                '1750', '1800', '1850', '1900', '1950', '1999', '2050'
            ),
            'tickmarkPlacement' => 'on',
            'title' => array(
                'enabled' => false
            )
        );

        $data['yAxis'] = array(
            'title' => array(
                'text' => 'Post'
            ),
            'labels' => array(
                'formatter' => ''
            )
        );

        $data['tooltip'] = array(
            'shared' => true,
            'valueSuffix' => ' post'
        );

        $data['plotOptions'] = array(
            'area' => array(
                'stacking' => 'normal',
                'lineColor' => '#666666',
                'lineWidth' => 1,
                'marker' => array(
                    'lineWidth' => 1,
                    'lineColor' => '#666666'
                )
            )
        );

        $data['series'][] = array(
            'name' => 'Asia',
            'data' => array(
                502, 635, 809, 947, 1402, 3634, 5268
            )
        );

        $data['series'][] = array(
            'name' => 'Africa',
            'data' => array(
                106, 107, 111, 133, 221, 767, 1766
            )
        );

        $data['series'][] = array(
            'name' => 'Europe',
            'data' => array(
                163, 203, 276, 408, 547, 729, 628
            )
        );

        if(! empty($setdata) ){
            foreach($setdata as $key => $val){
                $data[$key] = $val;
            }
        }

        return $data;
        //$rv['chart'] = $data;
        //print_r($rv);
        //die();
        //$this->load->view('test_graph',$rv);
    }

    function graph_most_viewed(){
    	$this->load->helper('msgpack');
        $this->load->model('report_model', 'report');

        $most_viewed_today = $this->report->get_most_viewed_today();

		$in_post = array();

        foreach($most_viewed_today as $key => $val){
            $in_post[] = "'".$val['post_id']."'";
            $key_title = $val['post_id'];
            $title_arr[$key_title] = $val['title'];
        }

        $in_post_txt = implode(",",$in_post);

        $this->db_local = $this->load->database('local',TRUE);

        $table_source = array();
        $timeline = array();
        for ($i=24; $i > 0; $i--) {
            $timekey = (floor(time()/3600)*3600)+3600+3600 - ($i * 3600);
            $table_source[date("Ym",$timekey)] = "post_analytics_".date("Ym",$timekey);
            $timeline[$timekey] = 0;
        }

        require_once APPPATH."libraries/redisent.php";

        $this->config->load('redis');
        $this->redis = new redisent\Redis($this->config->item('rhost'), $this->config->item('rport'));

        $key = 'graph_most_viewed';

        $cache = $this->redis->get("lcache-nv2-" . $key);
        if($cache) {
            $ret = msgpack_unpack($cache);
        }else{
            $ret = $this->db_local->query("
                SELECT
                  `pid`,
                  (FLOOR(`time`/3600)*3600)+3600 AS timekey,
                  DATE_FORMAT(FROM_UNIXTIME(FLOOR(`time`/3600)*3600), '%d/%m/%Y %T') AS tm_end,
                  DATE_FORMAT(FROM_UNIXTIME((FLOOR(`time`/3600)*3600)+3600), '%d/%m/%Y %T') AS tm_stat,
                  COUNT(*) as count_view
                FROM `post_analytics_".date("Ym")."`
                WHERE
                  `pid` IN(".$in_post_txt.")
                  AND `time` > (UNIX_TIMESTAMP(NOW())-(60*60*24))
                GROUP BY FLOOR(`time`/3600),`pid`
                ORDER BY
                    (FLOOR(`time`/3600)*3600)+3600 ASC
            ")->result_array();
            $this->redis->setex("lcache-nv2-" . $key, 3600, msgpack_pack($ret));
        }

        $stat = array();
        foreach($ret as $key => $val){
            $key = $val['timekey'];
            $pid = $val['pid'];
            if(!isset($tot_count[$pid])){
                $tot_count[$pid] = null;
            }
            $stat[$pid][$key] = $tot_count[$pid] + $val['count_view'];
            $tot_count[$pid] = $stat[$pid][$key];
        }


        $timeline_post_last = array();
        $timeline_post = array();

        foreach($timeline as $key => $val){
            $data['xAxis']['categories'][] = date("d H:i",$key);
            foreach($most_viewed_today as $key2 => $val2){
                $keypid = $val2['post_id'];
                if(!isset($timeline_post_last[$keypid])){
                    $timeline_post_last[$keypid] = 0;
                }
                if(!empty($stat[$keypid][$key])){
                    $timeline_post[$keypid][$key] = $stat[$keypid][$key];
                    $timeline_post_last[$keypid] = $timeline_post[$keypid][$key];
                }else{
                    $timeline_post[$keypid][$key] = $timeline_post_last[$keypid];
                }
            }
        }

        $series = array();
        foreach($timeline_post as $key => $val){
            $seri = array();

            $seri['name'] = $title_arr[$key];
            foreach($val as $key2 => $val2){
                $seri['data'][] = $val2;
            }

            $series[] = $seri;
        }

        $data['series'] = $series;

        $seri['name'] = 'Total Autotweet';


        $data['plotOptions'] = array(
            'area' => array(
                'fillOpacity' => 0.5
            )
        );

        $data['title']['text'] = "Lintas Engine Statistic Most Viewed Post Last 24 hour";
        $data['subtitle']['text'] = "Source: lintas.me";

        $data['yAxis'] = array(
            'min' => 0,
            'title' => array(
                'text' => 'Views'
            ),
            'labels' => array(
                'formatter' => ''
            )
        );

        $data['chart']['type'] = "line";
        $data['tooltip'] = array(
            'valueSuffix' => ' view'
        );

        $this->domain->json_output($this->chart_data($data));
    }

    function graph_engine_post(){
        $this->default_db = $this->load->database("default", TRUE);
        $ret = $this->default_db->query("SELECT
        DATE_FORMAT(`se_insert_date`,'%Y-%m-%d %H:00') AS period,
        DATE_FORMAT(`se_insert_date`,'%H') AS period_short,
        COUNT(`se_ID`) as total
        FROM `engine_social_effect` WHERE
        `se_status` = 'OK' AND `se_insert_date` > FROM_UNIXTIME((UNIX_TIMESTAMP(NOW())-(60*60*24)))
        GROUP BY DATE_FORMAT(`se_insert_date`,'%Y%m%d%H')
        ORDER BY `se_insert_date` ASC ")->result_array();

        $seri = array();
        $seri['name'] = "Total Post";
        foreach($ret as $key => $val){
            $data['xAxis']['categories'][] = $val['period_short'];

            $seri['data'][] = (int) $val['total'];

        }

        $series[] = $seri;

        $data['series'] = $series;

        $data['plotOptions'] = array(
            'area' => array(
                'fillOpacity' => 0.5
            )
        );

        $data['title']['text'] = "Lintas Engine Statistic Total Post Engine Last 24 hour";
        $data['subtitle']['text'] = "Source: lintas.me";

        $data['yAxis'] = array(
            'min' => 0,
            'title' => array(
                'text' => 'Post'
            ),
            'labels' => array(
                'formatter' => ''
            )
        );

        $data['chart']['type'] = "line";
        $data['tooltip'] = array(
            'valueSuffix' => ' Total Post'
        );

        $this->domain->json_output($this->chart_data($data));

    }

    function graph_domain_post(){
        error_reporting(0);

        $this->default_db = $this->load->database("default", TRUE);

        $this->load->model('report_model', 'report');

		$most_active_source = $this->report->get_most_active_source();
        $in = array();
        foreach($most_active_source as $key => $val){
            $in[] = "'".$val."'";
        }

        $a = implode(",",$in);

        $ret = $this->default_db->query(
        "SELECT `se_domain_ID`,
`engine_domain_rss_list`.`dl_domain`,
DATE_FORMAT(`se_insert_date`,'%d') AS period,
COUNT(`se_ID`) AS total
FROM `engine_social_effect`
LEFT JOIN `engine_domain_rss_list` ON `engine_domain_rss_list`.`dl_ID` = `engine_social_effect`.`se_domain_ID`
WHERE
`se_status` = 'OK' AND `se_insert_date` >= DATE(FROM_UNIXTIME((UNIX_TIMESTAMP(NOW())-(60*60*24*7))))
AND
`engine_domain_rss_list`.`dl_domain` IN (".$a.")
GROUP BY `se_domain_ID`,DATE_FORMAT(`se_insert_date`,'%Y%m%d')
ORDER BY `se_insert_date` ASC

 ")->result_array();


        $domain = array();
        $domain_name = array();
        $time_frame = array();

        foreach($ret as $key => $val){
            $key_id = $val['se_domain_ID'];
            $key_period = $val['period'];
            $domain[$key_id][$key_period] = (int) $val['total'];
            $domain_name[$key_id] = $val['dl_domain'];
            $time_frame[$key_period] = 1;
        }


        $series = array();

        foreach($domain_name as $key => $val){
            $seri = array();
            $seri['name'] = $val;
            foreach($time_frame as $key2 => $val2){
                if(!empty($domain[$key][$key2])){
                    $seri['data'][] = (int) $domain[$key][$key2];
                }else{
                    $seri['data'][] = null;
                }

            }
            $series[] = $seri;
        }


        foreach($time_frame as $key2 => $val2){
            $data['xAxis']['categories'][] = $key2;
        }

        $data['series'] = $series;

        $data['plotOptions'] = array(
            'area' => array(
                'fillOpacity' => 0.5
            )
        );

        $data['title']['text'] = "Lintas Engine Statistic Total Post Active Source Engine Last 7 days";
        $data['subtitle']['text'] = "Source: lintas.me";

        $data['yAxis'] = array(
            'min' => 0,
            'title' => array(
                'text' => 'Post'
            ),
            'labels' => array(
                'formatter' => ''
            )
        );

        $data['chart']['type'] = "line";
        $data['tooltip'] = array(
            'valueSuffix' => ' Total Post'
        );

        $this->domain->json_output($this->chart_data($data));
    }

    function detail_domain_post(){
            error_reporting(0);

            $this->default_db = $this->load->database("default", TRUE);


            $ret = $this->default_db->query("SELECT `se_domain_ID`,
        `engine_domain_rss_list`.`dl_domain`,
        DATE_FORMAT(`se_insert_date`,'%d') AS period,
        DATE_FORMAT(`se_insert_date`,'%d %b') AS period2,
        COUNT(`se_ID`) AS total
        FROM `engine_social_effect`
        LEFT JOIN `engine_domain_rss_list` ON `engine_domain_rss_list`.`dl_ID` = `engine_social_effect`.`se_domain_ID`
        WHERE
                `se_status` = 'OK' AND `se_insert_date` >= DATE(FROM_UNIXTIME((UNIX_TIMESTAMP(NOW())-(60*60*24*7))))
        GROUP BY `se_domain_ID`,DATE_FORMAT(`se_insert_date`,'%Y%m%d')
        ORDER BY `se_insert_date` ASC")->result_array();


            $domain = array();
            $domain_name = array();
            $time_frame = array();
            $time_frame2 = array();

            foreach($ret as $key => $val){
                $key_id = $val['se_domain_ID'];
                $key_period = $val['period'];
                $key_period2 = $val['period2'];
                $domain[$key_id][$key_period] = (int) $val['total'];
                $domain_name[$key_id] = $val['dl_domain'];
                $time_frame[$key_period] = 1;
                $time_frame2[$key_period2] = 1;
            }


            $series = array();

            foreach($domain_name as $key => $val){
                $seri = array();
                $seri['name'] = $val;
                foreach($time_frame as $key2 => $val2){
                    if(!empty($domain[$key][$key2])){
                        $seri['data'][] = (int) $domain[$key][$key2];
                    }else{
                        $seri['data'][] = null;
                    }

                }
                $series[] = $seri;
            }


            foreach($time_frame2 as $key2 => $val2){
                $data['xAxis']['categories'][] = $key2;
            }

            $data['series'] = $series;

            $this->load->view('xhr_view/detail_domain_post',$data);
    }
}
