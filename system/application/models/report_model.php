<?php
require_once APPPATH."libraries/redisent.php";

class Report_model extends Model {
	var $redis;
	var $cache_namespace = "lcache";
	var $cache_ttl = 300;
	var $default_db = NULL;
	var $_limit = "";
	var $_offset = "";
	
	function __construct() {
		parent::__construct();

		$this->load->helper('msgpack');
		$this->config->load('redis');
		
		$this->default_db = $this->load->database("default", TRUE);
		$this->redis = new redisent\Redis($this->config->item('rhost'), $this->config->item('rport'));
		
		$logged_in = $_SESSION['logged_in'];
	}
	
	function limit($limit, $offset, $is_active_record = TRUE) {
		if($is_active_record === TRUE) {
			$this->default_db->limit($limit, $offset);
		} else {
			$this->_limit = $limit;
			$this->_offset = $offset;
		}	
			
		return $this;
	}
	
	function order_by($field, $sort) {
		$this->default_db->order_by($field, $sort);		
		return $this;
	}
	
	function range_domain_date($start, $end) {
		$this->default_db->where("DATE(dl_insert_date) BETWEEN '{$start}' AND '{$end}'");
		return $this;
	}
	
	function range_rss_date($start, $end) {
		$this->default_db->where("DATE(se_insert_date) BETWEEN '{$start}' AND '{$end}'");
		return $this;
	}
	
	function get_total_crawled_domain($domain_ID, $start_date = '', $end_date = '') {
		$where = "";
		
		if($start_date != '' && $end_date != '') {
			$where = "AND DATE(se_insert_date) BETWEEN '{$start_date}' AND '{$end_date}'";
		}
		
		$data = $this->default_db->query('SELECT COUNT(se_ID) AS total_count FROM engine_social_effect WHERE se_domain_ID=\''.$domain_ID.'\' '.$where)->row_array();
		return $data['total_count'];
	}
	
	function get_total_posted_domain($domain_ID, $start_date = '', $end_date = '') {
		$where = "";
		
		if($start_date != '' && $end_date != '') {
			$where = "AND DATE(se_insert_date) BETWEEN '{$start_date}' AND '{$end_date}'";
		}
		
		$data = $this->default_db->query('SELECT COUNT(se_ID) AS total_count FROM engine_social_effect WHERE (se_status = \'OK\' OR se_status = \'DUPLICATE\') AND se_domain_ID=\''.$domain_ID.'\' '.$where)->row_array();
		return $data['total_count'];
	}
	
	function get_total_pending_domain($domain_ID, $start_date = '', $end_date = '') {
		$where = "";
		
		if($start_date != '' && $end_date != '') {
			$where = "AND DATE(se_insert_date) BETWEEN '{$start_date}' AND '{$end_date}'";
		}
		
		$data = $this->default_db->query('SELECT COUNT(se_ID) AS total_count FROM engine_social_effect WHERE se_status = \'PENDING\' AND se_domain_ID=\''.$domain_ID.'\' '.$where)->row_array();
		return $data['total_count'];
	}
	
	function get_total_post($start_date = '', $end_date = '') {
		$where = "";
		
		if($start_date != '' && $end_date != '') {
			$where = "WHERE DATE(se_insert_date) BETWEEN '{$start_date}' AND '{$end_date}'";
		}
		
		$data = $this->default_db->query('SELECT COUNT(se_ID) AS total_count FROM engine_social_effect '.$where)->row_array();
		return $data['total_count'];
	}
	
	function get_total_post_per_day($date, $status = '') {
		$where = "";		
		if($status != '') {
			$where = "AND se_status = '{$status}'";
		}
		
		$data = $this->default_db->query('SELECT COUNT(se_ID) AS total_count FROM engine_social_effect WHERE DATE(se_insert_date) = \''.$date.'\' '.$where)->row_array();
		return $data['total_count'];
	}
	
	function get_total_channel_popular_domain($domain_ID, $start_date = '', $end_date = '') {
		$where = "";
		
		if($start_date != '' && $end_date != '') {
			$where = "AND DATE(se_insert_date) BETWEEN '{$start_date}' AND '{$end_date}'";
		}
		
		$data = $this->default_db->query('SELECT COUNT(se_ID) AS chpop_count FROM engine_social_effect WHERE se_domain_ID=\''.$domain_ID.'\' AND se_channel_popular = \'1\' '.$where)->row_array();
		return $data['chpop_count'];
	}
	
	function get_total_home_popular_domain($domain_ID, $start_date = '', $end_date = '') {
		$where = "";
		
		if($start_date != '' && $end_date != '') {
			$where = "AND DATE(se_insert_date) BETWEEN '{$start_date}' AND '{$end_date}'";
		}
		
		$data = $this->default_db->query('SELECT COUNT(se_ID) AS hmpop_count FROM engine_social_effect WHERE se_domain_ID=\''.$domain_ID.'\' AND se_home_popular = \'1\' '.$where)->row_array();
		return $data['hmpop_count'];
	}
	
	function get_total_home_popular_per_day($date) {		
		$data = $this->default_db->query('SELECT COUNT(se_ID) AS hmpop_count FROM engine_social_effect WHERE DATE(se_insert_date) = \''.$date.'\' AND se_home_popular = \'1\'')->row_array();
		return $data['hmpop_count'];
	}
	
	function get_total_autotweet_domain($domain_ID, $start_date = '', $end_date = '') {
		$where = "";
		
		if($start_date != '' && $end_date != '') {
			$where = "AND DATE(se_insert_date) BETWEEN '{$start_date}' AND '{$end_date}'";
		}
		
		$data = $this->default_db->query('SELECT COUNT(se_ID) AS tweet_count FROM engine_social_effect WHERE se_domain_ID=\''.$domain_ID.'\' AND se_status <> \'FAILED_TWEET\' AND se_tweeted = \'1\' '.$where)->row_array();
		return $data['tweet_count'];
	}

    function get_total_autotweet_domain_per_day($domain_ID,$date) {
        $data = $this->default_db->query('SELECT COUNT(se_ID) AS tweet_count FROM engine_social_effect WHERE se_domain_ID=\''.$domain_ID.'\' AND DATE(se_insert_date) = \''.$date.'\' AND se_status <> \'FAILED_TWEET\' AND se_tweeted = \'1\' ')->row_array();
        return $data['tweet_count'];
    }

    function get_total_autotweet_alldomain_per_day($date) {
        $data = $this->default_db->query('SELECT COUNT(se_ID) AS tweet_count FROM engine_social_effect WHERE DATE(se_insert_date) = \''.$date.'\' AND se_status <> \'FAILED_TWEET\' AND se_tweeted = \'1\' ')->row_array();
        return $data['tweet_count'];
    }
	
	function get_total_rss_items_domain($dom_id) {
		return $this->default_db->where('se_domain_ID', $dom_id)->get('social_effect')->num_rows();
	}
	
	function get_rss_items_domain($dom_id) {
		return $this->default_db->where('se_domain_ID', $dom_id)->order_by('se_insert_date', 'DESC')->get('social_effect')->result_array();
	}
	
	function get_total_home_popular() {
		return $this->default_db->where('se_home_popular', '1')->get('social_effect')->num_rows();
	}
	
	function get_home_popular() {
		return $this->default_db->where('se_home_popular', '1')->order_by('se_homepop_date', 'DESC')->get('social_effect')->result_array();
	}
	
	function get_total_post_domain($start_date = '', $end_date = '') {
		$domain = $this->default_db->select('dl_ID, dl_domain')->from('domain_rss_list')->get()->result_array();
		$start  = $start_date;
		$end    = $end_date;
		$result = array();
		
		if($start_date == '' && $end_date == '') {
			$start = date('Y-m-d', time() - 60 * 60 * 24 * 7);
			$end = date('Y-m-d');
		}
		
		$result['result'] = array();
		$result['from_date'] = $start;
		$result['end_date'] = $end;
		
		if(count($domain) > 0) {
			foreach ($domain as $dom) {
				$post_total = $this->get_total_crawled_domain($dom['dl_ID'], $start_date, $end_date);
				$result['result'][] = array(
										'domain' => $dom['dl_domain'],
										'total_post' => $post_total
									  );
			}
		}
		
		return $result;
	}
	
	function get_total_post_daily($start_date = '', $end_date = '') {
		$start  = $start_date;
		$end    = $end_date;
		$result = array();
		
		if($start_date == '' && $end_date == '') {
			$start = date('Y-m-d', time() - 60 * 60 * 24 * 6);
			$end = date('Y-m-d');
		}
		
		$start_timestamp = strtotime($start);
		$end_timestamp = strtotime($end);
		$time_range = $end_timestamp - $start_timestamp;
		$seconds_per_day = 24 * 60 * 60;
		$days_total = $time_range / $seconds_per_day;
		$current_timestamp = $start_timestamp;
		
		$result['results'] = array();
		$result['from_date'] = $start;
		$result['end_date'] = $end;
		
		for($i = 0; $i <= $days_total; $i++) {			
			$current_date = date('Y-m-d', $current_timestamp);
			$current_timestamp = $current_timestamp + $seconds_per_day;
			
			$result['results'][] = array(
										'date' => $current_date,
										'total_post' => $this->get_total_post_per_day($current_date)
								   );			
		}
		
		return $result;
	}
	
	function get_total_home_popular_daily($start_date = '', $end_date = '') {
		$start  = $start_date;
		$end    = $end_date;
		$result = array();
		
		if($start_date == '' && $end_date == '') {
			$start = date('Y-m-d', time() - 60 * 60 * 24 * 6);
			$end = date('Y-m-d');
		}
		
		$start_timestamp = strtotime($start);
		$end_timestamp = strtotime($end);
		$time_range = $end_timestamp - $start_timestamp;
		$seconds_per_day = 24 * 60 * 60;
		$days_total = $time_range / $seconds_per_day;
		$current_timestamp = $start_timestamp;
		
		$result['results'] = array();
		$result['from_date'] = $start;
		$result['end_date'] = $end;
		
		for($i = 0; $i <= $days_total; $i++) {			
			$current_date = date('Y-m-d', $current_timestamp);
			$current_timestamp = $current_timestamp + $seconds_per_day;
			
			$result['results'][] = array(
										'date' => $current_date,
										'total_home_popular' => $this->get_total_home_popular_per_day($current_date)
								   );
		}
		
		return $result;
	}

    function get_total_autotweet_alldomain_daily( $start_date = '', $end_date = '') {
        $start  = $start_date;
        $end    = $end_date;
        $result = array();

        if($start_date == '' && $end_date == '') {
            $start = date('Y-m-d', time() - 60 * 60 * 24 * 6);
            $end = date('Y-m-d');
        }

        $start_timestamp = strtotime($start);
        $end_timestamp = strtotime($end);
        $time_range = $end_timestamp - $start_timestamp;
        $seconds_per_day = 24 * 60 * 60;
        $days_total = $time_range / $seconds_per_day;
        $current_timestamp = $start_timestamp;

        $result['results'] = array();
        $result['from_date'] = $start;
        $result['end_date'] = $end;

        for($i = 0; $i <= $days_total; $i++) {
            $current_date = date('Y-m-d', $current_timestamp);
            $current_timestamp = $current_timestamp + $seconds_per_day;

            $result['results'][] = array(
                'date' => $current_date,
                'total_autotweet' => $this->get_total_autotweet_alldomain_per_day($current_date)
            );
        }

        return $result;
    }

	function get_domain_by_topic($topic, $is_child = FALSE) {
		if($is_child === FALSE) {
			$this->default_db->where('dl_category_name', $topic);
			$this->default_db->where('dl_topic', '');
		} else {
			$this->default_db->where('dl_topic', $topic);
		}
		
		return $this->default_db->where('dl_status', 'ACTIVE')->get('domain_rss_list')->result_array();
	}
	
	function get_newest_feeds($num = 10) {
		$sql = "SELECT * FROM engine_domain_rss_list WHERE dl_status = 'ACTIVE' ORDER BY dl_insert_date DESC LIMIT {$num}";
		$key = md5($sql);
		$cache = $this->redis->get($this->cache_namespace . "-nf-" . $key);
		
		if($cache) {
			return msgpack_unpack($cache);
		} else {
			$data = $this->default_db->query($sql)->result_array();
			
			$this->redis->setex($this->cache_namespace . "-nf-" . $key, $this->cache_ttl, msgpack_pack($data));
			
			return $data;
		}
	}
	
	function get_latest_post($num = 10, $offset = 0, $startdate = '', $enddate = '') {
		$where = "";
		if($startdate != '' && $enddate != '') {
			$where = "AND se_posted_date BETWEEN '{$startdate}' AND '{$enddate}'";
		}
		
		$sql = "SELECT * FROM engine_social_effect WHERE se_status = 'OK' {$where} ORDER BY se_posted_date DESC LIMIT {$offset}, {$num}";
				
		$key = md5($sql);
		$cache = $this->redis->get($this->cache_namespace . "-lp-" . $key);
		
		if($cache) {
			return msgpack_unpack($cache);
		} else {
			$data = $this->default_db->query($sql)->result_array();
			
			$this->redis->setex($this->cache_namespace . "-lp-" . $key, $this->cache_ttl, msgpack_pack($data));
			
			return $data;
		}
	}
	
	function get_latest_home_popular($num = 10, $startdate = '', $enddate = '') {
		$where = "";
		if($startdate != '' && $enddate != '') {
			$where = "AND se_homepop_date BETWEEN '{$startdate}' AND '{$enddate}'";
		}
		
		$sql = "SELECT * FROM engine_social_effect WHERE se_status = 'OK' {$where} ORDER BY se_homepop_date DESC LIMIT {$num}";
		$key = md5($sql);
		$cache = $this->redis->get($this->cache_namespace . "-hp-" . $key);
		
		if($cache) {
			return msgpack_unpack($cache);
		} else {
			$data = $this->default_db->query($sql)->result_array();
			
			$this->redis->setex($this->cache_namespace . "-hp-" . $key, $this->cache_ttl, msgpack_pack($data));
			
			return $data;
		}			
	}
	
	function get_most_active_source($limit = 10, $days_range = 7) {
		$startdate = date("Y-m-d H:i:s", time() - ( 60 * 60 * 24 * $days_range ));
		$enddate = date("Y-m-d H:i:s", time() - ( 60 * 60 * 24 * 0 ));
		$key = $this->cache_namespace."-mas";
		$cache =  $this->redis->get($key);
		
		if($cache) {
			$item_count = msgpack_unpack($cache);
		} else {
			$sources = $this->default_db->select('dl_ID, dl_domain')->from('domain_rss_list')->where('dl_status', 'ACTIVE')->get()->result_array();
			$item_count = array();
					
			foreach ($sources as $source) {
				$sql = "SELECT COUNT(se_ID) AS total FROM engine_social_effect WHERE se_domain_ID = '{$source['dl_ID']}' AND (se_homepop_date BETWEEN '{$startdate}' AND '{$enddate}')";
				$count = $this->default_db->query($sql)->row_array();	
				$item_count[$count['total']] = $source['dl_domain'];
			}
			
			$this->redis->setex($key, $this->cache_ttl, msgpack_pack($item_count));
		}
		
		krsort($item_count);
		
		return array_slice($item_count, 0, $limit, TRUE);
	}
	
	function get_most_tweeted_news($limit = 10) {
		$startdate = date("Y-m-d H:i:s", time() - ( 60 * 60 * 24 * 1 ));
		$enddate = date("Y-m-d H:i:s", time() - ( 60 * 60 * 24 * 0 ));
		
		$where = "";
		if($startdate != '' && $enddate != '') {
			$where = "AND se_tweet_date BETWEEN '{$startdate}' AND '{$enddate}'";
		}
		
		$sql = "SELECT * FROM engine_social_effect WHERE se_tweeted = '1' {$where} ORDER BY se_social_effect DESC LIMIT {$limit}";
		$key = md5($sql);
		$cache = $this->redis->get($this->cache_namespace . "-mtn-" . $key);
		
		if($cache) {
			return msgpack_unpack($cache);
		} else {
			$data = $this->default_db->query($sql)->result_array();
			
			$this->redis->setex($this->cache_namespace . "-mtn-" . $key, $this->cache_ttl, msgpack_pack($data));
			
			return $data;
		}
	}

	function get_most_tweeted_source($limit = 10) {
		$startdate = date("Y-m-d H:i:s", time() - ( 60 * 60 * 24 * 1 ));
		$enddate = date("Y-m-d H:i:s", time() - ( 60 * 60 * 24 * 0 ));
		$key = $this->cache_namespace."-mts";
		$cache = $this->redis->get($key);
		
		if($cache) {
			$item_count = msgpack_unpack($cache);
		} else {
			$sources = $this->default_db->select('dl_ID, dl_domain')->from('domain_rss_list')->where('dl_status', 'ACTIVE')->get()->result_array();
			$item_count = array();
					
			foreach ($sources as $source) {				
				$sql = "SELECT SUM(se_social_effect) AS total_tweets FROM engine_social_effect WHERE se_domain_ID = '{$source['dl_ID']}' AND (se_tweet_date BETWEEN '{$startdate}' AND '{$enddate}')";
				$count = $this->default_db->query($sql)->row_array();			
				$item_count[$count['total_tweets']] = $source['dl_domain'];
			}
			
			$this->redis->setex($key, $this->cache_ttl, msgpack_pack($item_count));
		}
		
		krsort($item_count);
		
		return array_slice($item_count, 0, $limit, TRUE);
	}
	
	function get_most_viewed_today($limit = 10) {		
		$startdate = date("Y-m-d H:i:s", time() - ( 60 * 60 * 24 * 1 ));
		$enddate = date("Y-m-d H:i:s", time() - ( 60 * 60 * 24 * 0 ));
		$view_data = array();
		$sql = "SELECT * FROM engine_social_effect WHERE (se_posted_date BETWEEN '{$startdate}' AND '{$enddate}') AND se_post_ID <> ''";
		$key = md5($sql);
		$cache = $this->redis->get($this->cache_namespace . "-mvt-" . $key);
		
		if($cache) {
			$view_data = msgpack_unpack($cache);
		} else {
			$posts = $this->default_db->query($sql)->result_array();
			
			foreach ($posts as $post) {
                $counter = $this->post_viewed($post['se_post_ID']);
				$view_data[$counter] = array(
									'post_id' => $post['se_post_ID'],
									'title' => $post['se_title'],
									'posted_date' => $post['se_posted_date'],
									'view' => $counter
							   );
			}
			
			$this->redis->setex($this->cache_namespace . "-mvt-" . $key, $this->cache_ttl, msgpack_pack($view_data));
		}
		
		krsort($view_data);
		
		return array_slice($view_data, 0, $limit);
	}

    function get_most_viewed_last_hour($hour = 24,$limit = 10) {
        $view_data = array();
        $time = time() -(60*60*24);
        $sql = "SELECT * FROM engine_social_effect WHERE se_posted_date > '".date('Y-m-d H:i:s',$time)."' AND se_post_ID <> ''";
        $key = md5($sql);
        $cache = $this->redis->get($this->cache_namespace . "-mvlh-" . $key);

        if($cache) {
            $view_data = msgpack_unpack($cache);
        } else {
            $posts = $this->default_db->query($sql)->result_array();

            foreach ($posts as $post) {
                $counter = $this->post_viewed($post['se_post_ID']);
                $view_data[$counter] = array(
                    'post_id' => $post['se_post_ID'],
                    'title' => $post['se_title'],
                    'view' => $counter
                );
            }

            $this->redis->setex($this->cache_namespace . "-mvlh-" . $key, $this->cache_ttl, msgpack_pack($view_data));
        }

        krsort($view_data);

        return array_slice($view_data, 0, $limit);
    }
	
	function get_most_active_category($limit = 10) {
		$startdate = date("Y-m-d H:i:s", time() - ( 60 * 60 * 24 * 1 ));
		$enddate = date("Y-m-d H:i:s", time() - ( 60 * 60 * 24 * 0 ));
		$categories = $this->get_channels();
		$item_count = array();
		
		$key = $this->cache_namespace . "-mac";		
		$cache = $this->redis->get($key);
		
		if($cache) {
			$item_count = msgpack_unpack($cache);
		} else {
			foreach ($categories as $cat) {
				if($cat['type'] == 'parent')
					$sql = "SELECT COUNT(se_ID) AS total FROM engine_social_effect WHERE se_category = '{$cat['id']}' AND (se_homepop_date BETWEEN '{$startdate}' AND '{$enddate}')";
				else
					$sql = "SELECT COUNT(se_ID) AS total FROM engine_social_effect WHERE se_topic = '{$cat['name']}' AND (se_homepop_date BETWEEN '{$startdate}' AND '{$enddate}')";
				
				$count = $this->default_db->query($sql)->row_array();
						 
				$item_count[$count['total']] = $cat['name'];
			}
			
			$this->redis->setex($key, $this->cache_ttl, msgpack_pack($item_count));
		}
		
		krsort($item_count);
		
		return array_slice($item_count, 0, $limit, TRUE);
	}
	
	function get_channels() {
		$this->load->library('mongo_db');
		
        $channels = $this->mongo_db->where(array('_deleted' => FALSE))->order_by(array('setposition' => 'ASC'))->get('channel_tree_new');
		$ch_array = array();
		
		foreach ($channels as $ch) {
			$ch_array[] = array('id' => $ch['id'], 'name' => $ch['name'], 'type' => 'parent');
			
			if(count($ch['child']) > 0){
				foreach ($ch['child'] as $child) {
					$ch_array[] = array('id' => $child['id'], 'name' => $child['name'], 'type' => 'child');
				}
			}
		}
		
		return $ch_array;
    }

	function post_viewed($id) {
		$this->load->library('mongo_db');
		
    	$res = $this->mongo_db->select(array('counter'))->where(array('post_id' => $id))->get('post_counter');
    	
    	return $res[0]['counter'];
    }	
}
