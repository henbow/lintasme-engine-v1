<?php
class User_model extends Model {
	var $default_db = NULL;
	
	function __construct() {
		parent::__construct();
		$this->default_db = $this->load->database("default", TRUE);
	}
	
	function auth($username, $password) {
		$this->default_db->where('uname', $username);
		$this->default_db->where('password', md5($password));
		$this->default_db->where('status', '1');
		
		$user = $this->default_db->get('backend_users');
		
		if($user->num_rows() > 0) {
			return $user->row_array();
		}
		
		return FALSE;
	}
	
	function set_activity_log($user_id, $login_date, $ip_address, $user_agent) {
		$data = array(
			'user_id' => $user_id,
			'login_date' => $login_date,
			'ip_address' => $ip_address,
			'user_agent' => $user_agent
		);
		
		return $this->default_db->insert('user_activity_logs', $data);
	}
	
	function update_activity_log($user_id, $logout_date) {
		$data = array(
			'logout_date' => $logout_date
		);
		
		return $this->default_db->where('user_id', $user_id)->update('user_activity_logs', $data);
	}
}
