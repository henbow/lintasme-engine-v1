<?php

class Daemon_model extends Model {
	var $default_db = NULL;
	
	function __construct() {
		parent::__construct();
		$this->default_db = $this->load->database("default", TRUE);
	}
	
	function get_daemons() {
		return $this->default_db->order_by('name')->get('daemon_manager')->result_array();
	}
	
	function get_daemons_total() {
		return $this->default_db->get('daemon_manager')->num_rows();
	}
	
	function get_daemon_by_id($id) {
		return $this->default_db->where('daemon_id', $id)->get('daemon_manager')->row_array();
	}
	
	function add_daemon($data) {
		return $this->default_db->insert('daemon_manager', $data);
	}
	
	function edit_daemon($id, $data) {
		return $this->default_db->where('daemon_id', $id)->update('daemon_manager', $data);
	}
}
