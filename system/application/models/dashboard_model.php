<?php
class Dashboard_model extends Model {
	var $namespace = 'lcache';
	
	function __construct() {
		parent::__construct();
		
		$this->config->load('redis');
		$this->load->library('NaiveBayesClassifier', array(
			'store' => array(
				'mode'	=> 'redis',
				'mintrainratio' => $this->config->item('rmintrainratio'),
				'db'	=> array(
					'db_host'	=> $this->config->item('rhost'),
					'db_port'	=> $this->config->item('rport'),
					'namespace'	=> $this->config->item('rnamespace')
				)
			),
			'debug' => $this->config->item('rdebug')
		), 'redis');
	}
	
	function get_newest_feed_cache($key) {
		return $this->redis->get($this->namespace . "-newestfeeds-" . $key);
	}
	
	function set_newest_feed_cache($key, $data, $ttl = 600) {
		return $this->redis->set($this->namespace . "-newestfeeds-" . $key, $ttl, $data);
	}
	
	function get_keys() {
		return $this->redis->keys('*');
	}
}
