<?php
class Domain_model extends Model {
	var $default_db = NULL;
	
	function __construct() {
		parent::__construct();
		$this->default_db = $this->load->database("default", TRUE);
	}
	
	function limit($limit, $offset) {
		$this->default_db->limit($limit, $offset);
		
		return $this;
	}
	
	function order_by($field, $sort) {
		$this->default_db->order_by($field, $sort);
		
		return $this;
	}
	
	function domain_active_only() {
		$this->default_db->where("dl_status = 'ACTIVE'");
		return $this;
	}
	
	function get_domain_name($id) {
		$domain = $this->default_db->where('dl_ID', $id)->get('domain_rss_list')->row_array();
		return $domain['dl_domain'];
	}
	
	function get_domain($id = '') {
		if($id != '') $this->default_db->where('dl_ID', $id);
		
		$this->default_db->order_by('dl_domain', 'asc');
		
		return $this->default_db->get('domain_rss_list')->result_array();
	}
	
	function get_total_domain() {
		return $this->default_db->get('domain_rss_list')->num_rows();
	}
	
	function add_domain(
						$domain, 
						$category, 
						$category_name, 
						$topic, 
						$rss_url, 
						$insert_date, 
						$update_every, 
						$update_type, 
						$type = 'rss', 
						$whitelist, 
						$channel, 
						$autotweet, 
						$social_effect, 
						$crawl_type,
						$title_pattern,
						$desc_pattern,
						$img_pattern,
						$status) {
		$data = array(
					'dl_domain' => $domain,
					'dl_category' => $category,
					'dl_category_name' => $category_name,
					'dl_topic' => $topic,
					'dl_rss_url' => $rss_url,
					'dl_insert_date' => $insert_date,
					'dl_update_every' => $update_every,
					'dl_update_type' => $update_type,
					'dl_last_scanned' => date('Y-m-d H:i:s'),
					'dl_white_list' => $whitelist,
					'dl_channel_popular' => $channel,
					'dl_autotweet' => $autotweet,
					'dl_social_effect' => $social_effect,
					'dl_type' => $type,
					'dl_crawl_type' => $crawl_type,
					'dl_title_pattern' => $title_pattern,
					'dl_desc_pattern' => $desc_pattern,
					'dl_img_pattern' => $img_pattern,
					'dl_status' => $status
				);
		
		$insert = $this->default_db->insert('domain_rss_list', $data);
				
		return $insert;
	}
	
	function edit_domain($id, 
						$domain, 
						$category, 
						$category_name, 
						$topic, 
						$rss_url, 
						$update_every, 
						$update_type, 
						$type = 'rss', 
						$whitelist, 
						$channel, 
						$autotweet, 
						$social_effect, 
						$crawl_type,
						$title_pattern,
						$desc_pattern,
						$img_pattern,
						$status) {
		$data = array(
					'dl_domain' => $domain,
					'dl_category' => $category,
					'dl_category_name' => $category_name,
					'dl_topic' => $topic,
					'dl_rss_url' => $rss_url,
					'dl_update_every' => $update_every,
					'dl_update_type' => $update_type,
					'dl_white_list' => $whitelist,
					'dl_channel_popular' => $channel,
					'dl_autotweet' => $autotweet,
					'dl_social_effect' => $social_effect,
					'dl_type' => $type,
					'dl_crawl_type' => $crawl_type,
					'dl_title_pattern' => $title_pattern,
					'dl_desc_pattern' => $desc_pattern,
					'dl_img_pattern' => $img_pattern,
					'dl_status' => $status
				);

		$this->default_db->where('se_domain_ID', $id)->update('social_effect', array('se_category' => $category));
		
		$this->default_db->where('dl_ID', $id);
		return $this->default_db->update('domain_rss_list', $data);
	}
	
	function delete_domain($id) {
		$this->default_db->where('dl_ID', $id);
		
		return $this->default_db->delete('domain_rss_list');
	}
		
	function get_category($format = '') {
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, "http://www.lintas.me/rest/listtopic");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		
		$result = curl_exec($ch);
		
		curl_close($ch);
		
		return ($format == 'array') ? json_decode($result, TRUE) : $result;
	}

	function get_category_setting($format = '') {
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, "http://www.lintas.me/rest/listtopic");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		
		$result = curl_exec($ch);
		
		curl_close($ch);
		
		$cat = json_decode($result, TRUE);
		$data = array();
		
		foreach ($cat as $cat) {
			$data[] = array (
				'id' => $cat['id'],
				'displayName' => $cat['displayName'],
				'name' => $cat['name']
			);
		}
		
		$data[] = array(
			'id' => '4d82a1d37205fb674a000023-5',
			'displayName' => 'Bola',
			'name' => 'bola'
		);
		
		return ($format == 'array') ? $data : json_encode($data);
	}

	function get_sub_category($catname = '') {
		if($catname == '') return array();
		
		$ch = curl_init();
              
        curl_setopt($ch, CURLOPT_URL, "http://www.lintas.me/rest/explore_topic/".$catname);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
          
        //execute post
        $topics = json_decode(curl_exec($ch), TRUE);
          
        //close connection
        curl_close($ch);
		
		return $topics;
	}
	
	function get_category_name($cid) {
		$cat = $this->default_db->where('category', $cid)->get('autotweet_setting')->row_array();
				
		return isset($cat['category_name']) ? $cat['category_name'] : "";
	}
	
	function get_article_view($pid) {
		$ch = curl_init();
		
		curl_setopt($ch,CURLOPT_URL, "http://www.lintas.me/rest/getsinglestream/&pid=$pid");
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);
		
		$result = json_decode(curl_exec($ch), TRUE);
		$result = isset($result['result']['views']) ? $result['result']['views'] : 0; 
		
		curl_close($ch);
		
		return intval($result);
	}
	
	function get_postid_by_tag($tag) {
		return $this->default_db->where(array('tag' => trim($tag), 'post_ID <>' => ''))->get('post_tags')->result_array();
	}
	
	function get_setting() {
		return $this->default_db->get('settings')->result_array();
	}
	
	function edit_setting($params) {
		if(count($params) > 0 ) {
			foreach ($params as $key => $value) {
				$this->default_db->where('alias', $key);
				$update = $this->default_db->update('settings', array('value' => $value));
			} 
			
			return $update;
		}	
		
		return FALSE;
	}
	
	function get_search($keyword, $field) {
		if($field == 'topic') {
			list($cat, $topic) = explode('%%', $keyword);
			$this->default_db->like('dl_category_name', $cat);
			if($topic != '') $this->default_db->or_like('dl_topic', $topic);
		} else {
			$this->default_db->like($field, $keyword);
		}
		
		$this->default_db->order_by('dl_domain', 'asc');
		return $this->default_db->get('domain_rss_list')->result_array();
	}
	
	function get_total_search($keyword, $field) {
		if($field == 'topic') {
			list($cat, $topic) = explode('%%', $keyword);
			$this->default_db->like('dl_category_name', $cat);
			if($topic != '') $this->default_db->or_like('dl_topic', $topic);
		} else {
			$this->default_db->like($field, $keyword);
		}
		$this->default_db->order_by('dl_domain', 'asc');
		return $this->default_db->get('domain_rss_list')->num_rows();
	}
	
	function update_domain_view($domain) {
		$dom  = $this->default_db->where('domain', $domain)->get('domain_view')->row_array();
		$view = intval($dom['views']);
		
		$update = $this->default_db->where('domain', $domain)->update('domain_view', array('views' => $view + 1));
		
		if($update == TRUE) {
			$dom  = $this->default_db->where('domain', $domain)->get('domain_view')->row_array();
			return intval($dom['views']);
		}
		
		return $update;
	}
	
	function update_post_view($post_id) {
		$post = $this->default_db->where('post_id', $post_id)->get('post_view')->row_array();
		$view = intval($post['views']);
		
		$update = $this->default_db->where('post_id', $post_id)->update('post_view', array('views' => $view + 1));
		
		if($update == TRUE) {
			$dom  = $this->default_db->where('post_id', $post_id)->get('post_view')->row_array();
			return intval($dom['views']);
		}
		
		return $update;
	}

	function check_rss_exist($url) {
		$rss = $this->default_db->where('dl_rss_url', $url)->get('domain_rss_list')->num_rows();
		
		return $rss > 0 ? TRUE : FALSE;
	}

    function unset_popular_social_effect($postid){
        $res = $this->default_db->where(array('se_post_ID' => $postid))->update('social_effect',array('se_home_popular' => 0));

        return $res > 0 ? TRUE : FALSE;
    }

    function json_output($input, $format_json = FALSE) {
        $this->load->helper('json');

        $this->output->set_header("HTTP/1.0 200 OK");
        $this->output->set_header("HTTP/1.1 200 OK");
        $this->output->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s', time()).' GMT');
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Content-type: application/json");
        if($format_json === TRUE) {
            $this->output->set_output(json_readable_encode($input));
        } else {
            $this->output->set_output(json_encode($input));
        }
    }
}