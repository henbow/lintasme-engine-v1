<?php
class Keyword_model extends Model {
    function __construct() {
        parent::__construct();
        $this->load->database("default");

        $this->offset = 0;
        $this->limit = 20;
    }

    function getBadKeywordList(){
        $ret = $this->db
            ->offset($this->offset)
            ->limit($this->limit)
            ->order_by('id','DESC')
            ->get('bad_keyword')
            ->result_array();
        return $ret;
    }

    function getBadKeywordTotal(){
        $ret = $this->db
            ->count_all('bad_keyword');
        return $ret;
    }

    function addBadKeyword($keyword){
        $ins['name'] = strtolower($keyword);
        $cek = $this->getBadKeyword($keyword);
        if(!empty($cek)){
            return;
        }

        $this->db->insert('bad_keyword',$ins);
        $ret = $this->db->select('name')->get('bad_keyword')->result_array();
        $bad_keyword = array();
        foreach($ret as $key => $val){
            $bad_keyword[] = $val['name'];
        }

        return $bad_keyword;
    }

    function getBadKeyword($keyword){
        $keyword = strtolower($keyword);
        $ret = $this->db->where(array('name' => $keyword))->get('bad_keyword')->result_array();
        return $ret;
    }

    function deleteBadKeyword($id){
        $this->db->delete('bad_keyword', array('id' => $id));
    }

    function setOffset($offset){
        $this->offset = $offset;
        return $this;
    }

    function setLimit($limit){
        $this->limit = $limit;
        return $this;
    }

    function result(){
        return $this->result;
    }

}