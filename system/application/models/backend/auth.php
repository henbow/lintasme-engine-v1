<?php
class Auth extends Model {
    function __construct() {
        parent::__construct();
        $this->load->library('mongo_db');
        if(session_id() == '') {
            session_cache_expire(1440);
            session_start();
        }
    }

    function isLogin(){
        if(!empty($_SESSION['backend_user'])){
            return true;
        }else{
            return false;
        }
    }

    function setLogin($params) {
        $password = sha1($params['password']);
        $ret = $this->mongo_db->select(array('_id',
            'external_user',
            'nama',
            'username',
            'profile',
            'active',
            'auth_post',
            'auth_edit',
            'auth_del',
            'auth_filter',
            'auth_popular',
            'auth_top5',
            'auth_comment',
            'auth_feed',
            'auth_newuser',
            'auth_poll',
            'auth_stat',
            'auth_nav',
            'auth_tag',
            'post_only_ch',
            'auth_tw_global',
            'auth_tw_tech',
            'auth_tw_woman',
            'auth_tw_buzz',
            'auth_tw_bola',
            'auth_tw_auto',
            'auth_tw_style'
        ))->where(array('username' => $params['username'], 'password' => $password,'active' => true))->get('backend_user');
        if(empty($ret)) {
            return false;
        } else {
            $_SESSION['backend_user'] = $ret[0];
            $_SESSION['backend_user']['id'] = $ret[0]['_id']->{'$id'};
            return $ret;
        }
    }

    function setLogout(){
        unset($_SESSION['backend_user']);
        session_destroy();
        return true;
    }
}