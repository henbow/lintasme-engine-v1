<?php

class Tag_model extends Model {
	var $tag_set = "lat-tags";
	var $stopword_set = "lfe2-stopwords";
	
	function __construct() {
		parent::__construct();
		
		$this->load->library('redis_connector');
	}
	
	/**
	 * Add tags to database
	 * 
	 * @access public
	 * @param array $tags
	 * @return array added tags
	 * 
	 */
	function add_tag($tags) {
        $added = array();
		if(is_array($tags)) {
			foreach ($tags as $value) {
				if($value != ''){
                    $sadd = $this->redis_connector->redis->sadd($this->tag_set, trim($value));
                    if($sadd == '1'){
                        $added[] = trim($value);
                    }
                }
			}

		}else{
            $sadd = $this->redis_connector->redis->sadd($this->tag_set, $tags);
            if($sadd == '1'){
                $added[] = trim($tags);
            }
        }

        return $added;
	}
	
	/**
	 * Delete tag from database
	 * 
	 * @access public
	 * @param string $tag
	 * @return boolean
	 * 
	 */
	function delete_tags($tag) {
		return $this->redis_connector->redis->srem($this->tag_set, $tag) == '1' ? TRUE : FALSE;
	}
	
	/**
	 * Get all tags
	 * 
	 * @access public
	 * @return array
	 */
	function get_tags() {
		return $this->redis_connector->redis->sMembers($this->tag_set);
	}
	
	/**
	 * Get tags by alphabet
	 * 
	 * @access public
	 * @return array
	 */
	function get_tags_by_alphabet($alphabet = 'a') {
		return preg_grep('/^'.$alphabet.'/', $this->redis_connector->redis->sMembers($this->tag_set));
	}
	
	function get_tags_by_keyword($keywords) {
		return preg_grep('/('.$keywords.')/', $this->redis_connector->redis->sMembers($this->tag_set));
	}
	
	function get_tags_count() {
		return $this->redis_connector->redis->sCard($this->tag_set);
	}
	
	function add_stopwords($words) {
		if(is_array($words)) {
			foreach ($words as $value) {
				if($value != '') $sadd = $this->redis_connector->redis->sadd($this->stopword_set, trim($value));
			}
			
			return $sadd == '1' ? TRUE : FALSE;
		}
		
		return $this->redis_connector->redis->sadd($this->stopword_set, $words) == '1' ? TRUE : FALSE;
	}
	
	function delete_stopwords($word) {
		return $this->redis_connector->redis->srem($this->stopword_set, $word) == '1' ? TRUE : FALSE;
	}
	
	function get_stopwords() {
		return $this->redis_connector->redis->sMembers($this->stopword_set);
	}
	
	function get_stopwords_count() {
		return $this->redis_connector->redis->sCard($this->stopword_set);
	}
}

/**
 * End of file
 * 
 */