<?php
class Redis_model extends Model {

    function storeCache($key, $value, $time=600) {
        $this->load->config('redis');
        $redisext 			= new redis();
        $host 				= $this->config->item('redis_host');
        $redisext->pconnect($host, 6379, 2.5);
        $keying 			= md5($key);
        $value				= serialize($value);
        //$value				= $this->msgpack_pack($value);
        $ret  				= $redisext->setex($keying, $time, $value);
    }

    function getCache($key) {
        $this->load->config('redis');
        $redisext 			= new redis();
//		$host 				= $this->config->item('redis_host');
        $host 				= "192.168.1.10";
        $redisext->pconnect($host, 6379, 2.5);
        $keying 			= md5($key);
        $ret  				= $redisext->get($keying);
        return unserialize($ret);
    }

    function delCache($key) {
        require_once APPPATH."libraries/redisent.php";
        $this->load->config('redis');
        $host 				= $this->config->item('redis_host');
        $rediscache 		= new redisent\Redis($host);
        $keying 			= md5($key);
        $ret  				= $rediscache->del($keying);
    }

}