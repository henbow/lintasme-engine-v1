<?php
class Modelpublisher extends Model {
    function __construct() {

    }

    function getDomainStat($domain = 'blog.lintas.me',$num = 7,$options = array()){
        $this->db = $this->load->database('local', TRUE);

        if(!empty($options['range'])){
            $days = date('Y-m-d',$options['range']['start']);
            $days_array = $this->getRangeDays($options['range']['start'],$options['range']['end']);
            $days_end = date('Y-m-d',$options['range']['end']);
            $q_end_param = " and date_start <= '".$days_end."' ";
        }else{
            $days = date('Y-m-d',strtotime( $num." days ago"));
            $days_array = $this->getPastDays($num);
            $q_end_param = "";
        }

        foreach($days_array as $key => $val){
            $day_txt = $val['day_txt'];
            $day_axis[] = $day_txt;
            $day_view[$day_txt] = 0;
        }

        $ret = $this->db->query("
        select
          date_start,stat_post
        from publisher_statistik
        where
          domain = '".$domain."'
          and date_start >= '".$days."'
          ".$q_end_param."
        order by date_start ASC
      ")->result_array();


        foreach($ret as $key => $val){
            $day_txt = $val['date_start'];
            $day_view[$day_txt] = $val['stat_post'];
        }
        foreach($day_view as $key => $val){
            $day_pageview[] = (int) $val;
        }

        $rv['axis'] = $day_axis;
        $rv['view'] = $day_pageview;
        return $rv;
    }

    function getRangeDays($start_dt = 1347210000,$end_date = 1347555600){
        $diff = ($end_date-$start_dt)/86400;

        for ($i=0; $i <= $diff; $i++) {
            if($i!=0){
                $start_dt = $start_dt + 86400;
            }
            $day['timestamp'] = $start_dt;
            $day['day_txt'] = date('Y-m-d',$day['timestamp']);
            $days[] = $day;
        }

        return $days;
    }
}