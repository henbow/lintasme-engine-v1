<?php

class Local_model extends Model {
	var $default_title_pattern = '//channel/item/title';
	var $default_desc_pattern = '//channel/item/description';
	var $default_img_pattern = '//channel/item/description';
	var $limit = '';
	var $offset = '';
	var $location = '';
	var $local_db = NULL;
	
	function __construct() {
		parent::__construct();
		$this->local_db = $this->load->database("noprefix", TRUE);
	}
	
	function limit($limit, $offset) {
		$this->limit = $limit;
		$this->offset = $offset;
		
		return $this;
	}
	
	function order_by($field, $sort) {
		$this->local_db->order_by($field, $sort);
		
		return $this;
	}
	
	function get_news_by_location($location, $fetch_results = TRUE) {
		$news = array();		
		$city_ids = array();
			
		foreach ($location as $loc) {
			$loc  = strtolower($loc);	
			$city = $this->local_db->where('kota', $loc)->get('local_kota');
			if($city->num_rows() > 0) {
				$local = $city->row_array();
				$city_ids = $this->local_db->where('id', $local['id'])->get('local_kota')->result_array();
				break;
			}
			
			$prov = $this->local_db->select('local_province.*')->where('local_province.name', $loc)->from('local_province')->join('local_kota','local_province.id=local_kota.province_id')->get();
			if($prov->num_rows() > 0) {
				$local = $prov->row_array();
				$city_ids = $this->local_db->where('province_id', $local['id'])->get('local_kota')->result_array();
				break;
			}
			
			$coun = $this->local_db->where('name', $loc)->get('local_country');
			if($coun->num_rows() > 0) {
				$local = $coun->row_array();
				$city_ids = $this->local_db->where('country_id', $local['id'])->get('local_kota')->result_array();
				break;
			}			
		}
		
		$this->location = $loc;
				
		if(count($city_ids) > 0) {
			foreach ($city_ids as $city_id) {
				$this->local_db->or_where('id_kota', $city_id['id']);
			}
			
			$feeds = $this->local_db->get('local_feeds')->result_array();
			
			foreach ($feeds as $feed) {
				 $this->local_db->or_where('feed_id', $feed['id']);
			}
			
			if($this->limit !== '' && $this->offset !== '') {
				$this->local_db->limit($this->limit, $this->offset);
			}
			
			$news = $this->local_db->order_by('pubdate','desc')->get('local_feed_items')->result_array();
			
			$this->limit = ''; 
			$this->offset = '';
		}
		
		return $news;
	}

	function get_total_news_by_location($location) {
		$news = $this->get_news_by_location($location);
		
		return count($news);
	}
	
	function get_current_location() {
		return $this->location;
	}
	
	function get_city($id = '') {
		if($id != '') $this->local_db->where('id', $id);
		
		return $this->local_db->get('local_kota')->result_array();
	}
	
	function get_total_city() {
		return $this->local_db->get('local_kota')->num_rows();
	}
	
	function get_city_by_position($lat, $lng) {
		$this->local_db->like('longitude', $lng, 'after');
		$this->local_db->like('latitude', $lat, 'after');
		
		return $this->local_db->get('local_kota')->row_array();
	}
	
	function get_province() {
		return $this->local_db->get('local_province')->result_array();
	}
	
	function get_country() {
		return $this->local_db->get('local_country')->result_array();
	}
	
	function get_city_name($id) {
		$city = $this->local_db->where('id', $id)->get('local_kota')->row_array();
		
		return isset($city['kota']) ? $city['kota'] : '';
	}
	
	function get_province_name($id) {
		$prov = $this->local_db->where('id', $id)->get('local_province')->row_array();
		
		return $prov['name'];
	}
	
	function get_country_name($id) {
		$prov = $this->local_db->where('id', $id)->get('local_country')->row_array();
		
		return $prov['name'];
	}
	
	function add_city($country, $province, $name, $desc, $long, $lat, $image, $status = '1') {
		$data = array(
			'country_id' => $country,
			'province_id' => $province,
			'kota' => $name,
			'desc' => $desc,
			'longitude' => $long,
			'latitude' => $lat,
			'image' => $image,
			'status' => $status
		);
		
		return $this->local_db->insert('local_kota', $data);
	}
	
	function edit_city($id, $country, $province, $name, $desc, $long, $lat, $image, $status = '1') {
		$data = array(
			'country_id' => $country,
			'province_id' => $province,
			'kota' => $name,
			'desc' => $desc,
			'longitude' => $long,
			'latitude' => $lat,
			'image' => $image,
			'status' => $status
		);
		
		return $this->local_db->where('id', $id)->update('local_kota', $data);
	}
	
	function delete_city($id) {
		return $this->local_db->delete('local_kota', array('id' => $id));
	}
	
	function get_domain($id = '') {
		if($id != '') $this->local_db->where('id', $id);
		if($this->limit !== '' && $this->offset !== '') {
			$this->local_db->limit($this->limit, $this->offset);
		}
		
		return $this->local_db->order_by('id_kota','asc')->get('local_feeds')->result_array();
	}
	
	function get_total_domain() {
		return $this->local_db->get('local_feeds')->num_rows();
	}
	
	function add_domain($domain, $category, $category_name, $topic, $topic_name, $city, $rss_url, $date, $crawltype, $title_patt, $desc_patt, $img_patt, $status) {
		$data = array(
			'id_kota' => $city,
			'domain' => $domain,
			'category_id' => $category,
			'category_name' => $category_name,
			'topic_id' => $topic,
			'topic_name' => $topic_name,
			'rss' => $rss_url,
			'insert_date' => $date,
			'crawl_type' => $crawltype,
			'title_pattern' => $title_patt == "" ? $this->default_title_pattern : $title_patt,
			'desc_pattern' => $desc_patt == "" ? $this->default_desc_pattern : $desc_patt,
			'img_pattern' => $img_patt == "" ? $this->default_img_pattern : $img_patt,
			'status' => $status
		);
		
		return $this->local_db->insert('local_feeds', $data);
	}
	
	function edit_domain($id, $domain, $category, $category_name, $topic, $topic_name, $city, $rss_url, $crawltype, $title_patt, $desc_patt, $img_patt, $status) {
		$data = array(
			'id_kota' => $city,
			'domain' => $domain,
			'category_id' => $category,
			'category_name' => $category_name,
			'topic_id' => $topic,
			'topic_name' => $topic_name,
			'rss' => $rss_url,
			'crawl_type' => $crawltype,
			'title_pattern' => $title_patt == "" ? $this->default_title_pattern : $title_patt,
			'desc_pattern' => $desc_patt == "" ? $this->default_desc_pattern : $desc_patt,
			'img_pattern' => $img_patt == "" ? $this->default_img_pattern : $img_patt,
			'status' => $status
		);
		
		return $this->local_db->where('id', $id)->update('local_feeds', $data);
	}
	
	function delete_domain($id) {
		return $this->local_db->delete('local_feeds', array('id' => $id));
	}
}
