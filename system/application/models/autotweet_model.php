<?php

class Autotweet_model extends Model {
	var $table = 'autotweet_setting';
	var $default_db = NULL;
	
	function __construct() {
		parent::__construct();
		$this->default_db = $this->load->database("default", TRUE);
	}
	
	function get_channel_setting($category_id) {
		return $this->default_db
		->where('category', $category_id)->get($this->table)->row_array();		
	}
	
	
	function update_autotweet_setting($cid, $start, $end, $fullday) {		
		$data = array(
					'start_time' => $start,
					'end_time' => $end,
					'fullday_tweet' => $fullday
				);
		
		return $this->default_db->where('category', $cid)->update('autotweet_setting', $data);
	}
	
	function set_fullday_tweet($cid) {
		return $this->default_db->where('category', $cid)->update('autotweet_setting', array('fullday_tweet' => '1'));
	}
	
	function unset_fullday_tweet($cid) {
		return $this->default_db->where('category', $cid)->update('autotweet_setting', array('fullday_tweet' => '0'));
	}
}
