<?php

class Schedule_model extends Model {
	var $default_db = NULL;
	
	function __construct() {
		parent::__construct();
		$this->default_db = $this->load->database("default", TRUE);
	}
	
	function get_schedule() {
		return $this->default_db->get('scheduler')->result_array();
	}
	
	function get_schedules_total() {
		return $this->default_db->get('schedule')->num_rows();
	}
	
	function get_schedule_by_id($id) {
		return $this->default_db->where('id', $id)->get('schedule')->row_array();
	}
	
	function add_schedule($data) {
		return $this->default_db->insert('schedule', $data);
	}
	
	function edit_schedule($id, $data) {
		return $this->default_db->where('schedule_id', $id)->update('schedule', $data);
	}
}
