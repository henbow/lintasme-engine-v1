<?php
/**
 * Social effect for publisher website
 * @author Hendro (hendro@lintas.me)
 */
 
ini_set('display_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('Asia/Jakarta');

require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/DBActiveRecord.php';
require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/function.php';
require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/poster.php';

$db = new DB_active_record;
$poster = new Poster;
$max_home_popular = 2;
$limit_post = 1;
$expired_news = 1;

echo "====================================================================================".PHP_EOL;
echo "LintasMe Social Effect [Home Popular] is started...".PHP_EOL;
echo "====================================================================================".PHP_EOL;
echo PHP_EOL;

while(TRUE){	
	$start = microtime(TRUE);
	$yesterday = date("Y-m-d", time() - ( 60 * 60 * 24 * $expired_news ));
	$ch_delay_setting = $db->get('publisher_rss_settings', array('alias' => 'home_popular_delay'))->row_array();
	$ch_delay = isset($ch_delay_setting['value']) ? intval($ch_delay_setting['value']) * 60 : 600;
	$tweetsCount = array(); 
	$sql = "SELECT * FROM publisher_social_effect WHERE 
			(pnr_date BETWEEN '".$yesterday." 00:00:00' AND '".date('Y-m-d')." 23:59:59') AND
			pnr_home_popular = '0' AND (pnr_status = 'OK' OR pnr_status='DUPLICATE') AND
			pnr_post_ID <> ''
			ORDER BY pnr_date DESC";
	$publishers = $db->reset_all()->query($sql)->result_array();
	
	echo "Process ".count($publishers)." URLs...".PHP_EOL;
	
	if(count($publishers) > 0) {
		$channel_name = "";
		foreach ($publishers as $pub) {
			$s = microtime(TRUE);
			
			echo "Get tweets count from => {$pub['pnr_origlink']} ".PHP_EOL;
			
			$tweets = get_tweets($pub['pnr_origlink']);
			$tweetsCount[$pub['pnr_pub_member']][$pub['pnr_ID']] = $tweets;
			$saveSE = $db->update_data('publisher_social_effect', array('pnr_social_effect' => $tweets), array('pnr_ID' => $pub['pnr_ID']));
			
			$e = microtime(TRUE);
			
			echo "[".$tweets." TWEETS] - ". (round($e - $s, 2)) . " seconds" . PHP_EOL; 
		}
		
		$max_per_publisher = array();
		
		foreach ($tweetsCount as $pub_id => $data) {
			$max = max($data);
			$max_per_publisher[array_search($max, $data)] = $max;
		}
		
		arsort($max_per_publisher);
		
		foreach ($max_per_publisher as $rssID => $max) {
			$article = $db->get('publisher_social_effect', array('pnr_ID' => $rssID))->row_array();
			$postID = $poster->checkPost($article['pnr_origlink']);
			$pdet = json_decode($poster->getPostDetail($postID), TRUE);
			
			if(strlen($postID) > 0) {
				$db->update_data('publisher_social_effect', array('pnr_home_popular' => '1', 'pnr_homepop_date' => date('Y-m-d H:i:s')), array('pnr_ID' => $article['pnr_ID']));
				
				$updToChannelPop = $poster->set_popular($postID, 'home', $article['pnr_pub_member']);
				$status = json_decode($updToChannelPop, TRUE);						
			}					
			
			echo PHP_EOL;
			echo "Post ID: " . $postID . PHP_EOL;
			echo "Source URL: " . $article['pnr_origlink'] . PHP_EOL;
			echo "Post URL: http://www.lintas.me/article/" . $postID . PHP_EOL;
			echo "Tweets Count: " . $max . PHP_EOL;
			echo "Status: " . ($status['error'] == "" ? "OK" : "FAILED" ) . PHP_EOL . PHP_EOL;
			
			echo "====================================================================================".PHP_EOL;
			echo "Time: ".date('Y-m-d H:i:s').PHP_EOL;
			echo 'Sleep for '.($ch_delay / 60).' minutes.... zzzzzz....'.PHP_EOL;
			echo "====================================================================================".PHP_EOL;
			echo PHP_EOL;
		
			sleep($ch_delay);
			flush();
		}
	}

	$end = microtime(TRUE); 
	
	echo "Execution time: " . ($end - $start) . " seconds".PHP_EOL;
	echo 'Collecting cycles: ' . gc_collect_cycles();
	echo PHP_EOL;
	
	unset($start, $end, $ch_delay, $yesterday, $publishers, $ch_delay, $ch_delay_setting);
	flush();
}

/**
 * End of file
 * 
 */
