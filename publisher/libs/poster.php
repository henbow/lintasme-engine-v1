<?php
//require_once(dirname(__FILE__).DIRECTORY_SEPARATOR."HumbleHttpAgent.php");
//require_once(dirname(__FILE__).DIRECTORY_SEPARATOR."Readability.php");
//require_once(dirname(__FILE__).DIRECTORY_SEPARATOR."simple_html_dom.php");

class Poster
{
	public $newPostUrl = "http://www.lintas.me/rest/newPost";
	public $maxContentChars = 600;
	
	function __construct()
	{
		
	}

	function getRandomUser()
	{
        //$get = file_get_contents("http://www.lintas.me/rest/getRandomUser");
        //$rt = json_decode($get);
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://www.lintas.me/rest/getRandomUser'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);

        $rt = json_decode($resp);

        return $rt;
	}

    function getRandomUserSingle()
    {
        $ret = $this->getRandomUser();
        return $ret[0]->id;
    }

    function newPost($params)
    {
        $this->posting_start = time();
        
		if(isset($params['title'])){
			//$content = strlen(trim($params['content'])) > $this->maxContentChars ? substr((trim($params['content'])), 0, $this->maxContentChars).'...' : trim($params['content']);
	        $fields = array(
	                    'is_backecd' => 1,
	                    'origin_id' => $params['category'],
	                    'url' => $params['url'],
	                    'post_title' => $params['title'],
	                    'post_content' => trim($params['content']),
	                    'post_img_url' => $params['image'],
	                    'post_tag_no_auto' => $params['tags'],
	                    'kind' => 'article',
	                    'backecd_user' => '505935efe0c8ba4515004028'
	                );
				        
	        if(!empty($params['post_as'])) {
	            $fields['rand_user'] = $params['post_as'];
	        } else {
	            $fields['rand_user'] = $this->getRandomUserSingle();
	        }
	        
	        $fields_string = '';
	        //url-ify the data for the POST
	        foreach($fields as $key => $value) { $fields_string .= $key.'='.$value.'&'; }
	        rtrim($fields_string, '&');
			
	        //open connection
	        $ch = curl_init();
	
	        //set the url, number of POST vars, POST data
	        curl_setopt($ch,CURLOPT_URL, $this->newPostUrl);
	        curl_setopt($ch,CURLOPT_POST, count($fields));
	        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			//curl_setopt($ch,CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.97 Safari/537.22 AlexaToolbar/alxg-3.1");
	        curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);
	         
	        //execute post
	        $result = curl_exec($ch);
	
	        //close connection
	        curl_close($ch);
	
	        $this->posting_end = time();
	        $this->posting_duration = $this->posting_end - $this->posting_start;
			
			//print $url.PHP_EOL;
			//print_r($result);
			
	        if($result == "Disallowed Key Characters.") {
	            $res['error'] = TRUE;
	            $res['error_msg'] = $result;
	            return $res;
	        } else {
	            $res = json_decode($result);
	            if(!empty($params['set_popular'])){
	             	echo "set_popular=".$params['set_popular']." ";
	            }
	            
	            return $res;
	        }
		} else {
			return json_encode(array('status' => 'FAILED'));
		}
    }

    function updateLog($feeditems_id,$result){
        $q = mysql_query("
            update
                publisher_feed_items
            set
                status = 1,
                post_status = '".$result->status."',
                post_id = '".$result->detail_post->id."',
                crawl_start = '".date("Y-m-d H:i:s",$this->crawler_start)."',
                crawl_end = '".date("Y-m-d H:i:s",$this->crawler_end)."',
                crawl_duration = '".$this->crawler_duration."',
                post_duration = '".$this->posting_duration."'
            where
                id = '".$feeditems_id."'
        ");
    }
    
    function set_popular($post_id, $type = 'channel', $pub_id){
      	//set POST variables
      	$url = 'http://www.lintas.me/rest/setPopular';
      	$fields = array(
                    'backend_id' => '505935efe0c8ba4515004028',
                    'post_id' => $post_id,
                    'set_popular' => $type,
                    'publisher_id' => $pub_id
                );
		
      	$fields_string = '';
      	//url-ify the data for the POST
      	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
      	rtrim($fields_string, '&');

      	//open connection
      	$ch = curl_init();

      	//set the url, number of POST vars, POST data
      	curl_setopt($ch,CURLOPT_URL, $url);
      	curl_setopt($ch,CURLOPT_POST, count($fields));
      	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
      	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

      	// debug post data
      	
      	//echo "\n";
      	//echo "Posting New Post : \n";
      	//echo "Field String : ".$fields_string;
      	//echo "\n";
       
      	//execute post
      	$result = curl_exec($ch);

      	//close connection
      	curl_close($ch);
		
      	return $result;
    }

	function post_tweet($post_id, $is_weekend, $min, $max)
    {
      	//set POST variables
      	$url = 'http://www.lintas.me/rest/setPopularCli';
      	$fields = array(
                    'backend_id' => '505935efe0c8ba4515004028',
                    'post_id' => $post_id,
                    'isweekend' => $is_weekend,
                    'hour_min' => $min,
                    'hour_max' => $max
                );
		
      	$fields_string = '';
      	//url-ify the data for the POST
      	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
      	rtrim($fields_string, '&');

      	//open connection
      	$ch = curl_init();

      	//set the url, number of POST vars, POST data
      	curl_setopt($ch,CURLOPT_URL, $url);
      	curl_setopt($ch,CURLOPT_POST, count($fields));
      	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
      	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

      	// debug post data
      	
      	//echo "\n";
      	//echo "Posting New Post : \n";
      	//echo "Field String : ".$fields_string;
      	//echo "\n";
       
      	//execute post
      	$result = curl_exec($ch);

      	//close connection
      	curl_close($ch);
		
      	return $result;
    }

    function runSingleCrawl(){
        $ret = $this->getUrlToCrawl();
        
        $set_popular = $ret['set_popular'];
        $feeditems_id = $ret['id'];
        echo date("Y-m-d H:i:s",time())." ".$feeditems_id." ".$ret['url']." ";
        $url = $ret['url'];
        $check = $this->checkPost($ret['url']);
        mysql_query("update publisher_feed_items set status = '8' where id='".$feeditems_id."'");
        
        if(empty($check)){

            $ret = $this->newPost($ret);
            
            //print_r($ret);
            //die();

            if($ret['error']){
                echo $ret['error_msg'];
                die();
            }
            mysql_query("update publisher_feed_items set status = '7' where id='".$feeditems_id."'");
            print_r($ret);
            echo $ret->status." ".$ret->detail_post->id."\n";
            $ret = $this->updateLog($feeditems_id,$ret);
        }else{
          $result->status = "DUPLICATE";
          $result->detail_post->id = $check;
          $this->crawler_start = time();
          $this->crawler_end = time();
          $this->crawler_duration = 0;
          $this->posting_duration = 0;
          
          
          $ret = $this->updateLog($feeditems_id,$result);
          mysql_query("update publisher_feed_items set status = '2' where id='".$feeditems_id."'");
          if($set_popular == 'channel'){
            mysql_query("update publisher_feed_items set status = '3' where id='".$feeditems_id."'");
            $this->set_popular($check);
            echo $result->status." set_popular=".$set_popular." ".$result->detail_post->id."\n";
          }else{
            echo $result->status." ".$result->detail_post->id."\n";
          }          
          
          $this->runSingleCrawl();
        }
    }
    
    function checkPost($urltarget){
        //set POST variables
        $url = 'http://www.lintas.me/rest/checkPost';
        $fields = array(
                    'url' => $urltarget
                );
        $fields_string = '';
        //url-ify the data for the POST
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);
        
        return $result;
    }
    
	function getPostDetail($pid) {
		//set POST variables
        $url = 'http://www.lintas.me/rest/getsinglestream/&pid='.$pid;
        $fields = array( 'url' => $url );

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);
        
        return $result;
	}
	
	function updatePostImage($post_id, $img_url) {
		$url = 'http://www.lintas.me/rest/updateThumb';
      	$fields = array(
                    'post_id' => $post_id,
                    'img_url' => $img_url
                );
		
      	$fields_string = '';
      	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
      	rtrim($fields_string, '&');

      	$ch = curl_init();

      	curl_setopt($ch,CURLOPT_URL, $url);
      	curl_setopt($ch,CURLOPT_POST, count($fields));
      	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
      	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

      	$result = curl_exec($ch);

      	curl_close($ch);
		
      	return $result;
	}

    function runCrawl($limit = 200)
    {
        for ($i=0; $i < $limit ; $i++) { 
            $this->runSingleCrawl();
        }
    }
    
}

/**
 * End of file
 */