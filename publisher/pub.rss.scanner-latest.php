<?php
/**
 * RSS Scanner for publisher website
 * @author Hendro (hendro@lintas.me)
 */

ini_set('display_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('Asia/Jakarta');

require_once __DIR__ . DIRECTORY_SEPARATOR . 'libs/DBActiveRecord.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'libs/poster.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'libs/config.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '../shared_libs/Configuration.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'libs/function.php'; //'../shared_libs/helpers.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '../shared_libs/TextStatistics.php';

$db        = new DB_active_record;
$poster    = new Poster;
$tagAPI    = "http://engine.lintas.me/?c=autotag&m=process&content=";
$filterAPI = "http://engine.lintas.me/filter/classify/?train=true";
$cat_url   = "http://www.lintas.me/rest/getChannelName/";

echo "Start Publisher RSS Scanner ".PHP_EOL;
while(TRUE) {
	echo '==========================================================================================================='.PHP_EOL;
	echo 'Start crawling date: '.date('Y-m-d H:i:s').PHP_EOL;
	
	$start = microtime(TRUE);
	$saveRss = FALSE;
	$rssNum = 10;
	$delay_setting = $db->get('publisher_rss_settings', array('alias' => 'rss_scan_delay'))->row_array();
	$delay = isset($delay_setting['value']) ? intval($delay_setting['value']) * 60 : 1800;
	$where = array('status' => '3');
	
	if(isset($argv[1])) {
		if($argv[1] != '') {
			for($i = 1; $i < count($argv); $i++) {
				$param = explode('=', $argv[$i]);
				
				switch ($param[0]) {
					case '-p':
						$where += array('publisher_member' => $param[1]);
						break;
					
					case '-s':
						$where = array('status' => $param[1]);
						break;
						
					case '-cg':
						$where += array('crawl_group' => $param[1]);
						break;
					
					default:
						break;
				}
			}	
		}
	}
	
	$publisher = $db->get('publisher_feed', $where)->result_array();
	
	if(count($publisher) > 0) {
            foreach ($publisher as $pub) {		
                $url = trim($pub['rss_url'], '/ ');
                $start_rss_crawl = microtime(TRUE);

                echo PHP_EOL."Scan RSS: ".$url;

                if($pub['publisher_member'] == '37') continue;

                $tag_param = array(
                                'title' => $pub['title_tag'],
                                'description' => $pub['description_tag'],
                                'image' => $pub['image_tag']
                             );

                $arrayrss = rss2array($url, $pub['use_reader'], $tag_param, $rssNum);
                $result   = isset($arrayrss['item']) ? $arrayrss['item'] : array();
                $limit    = count($result) < $rssNum ? count($result) : $rssNum;  

                if(count($result) > 0) {
                    for($i = 0; $i < $limit; $i++) {
                        $origUrl  = strpos($result[$i]['link'], 'feedproxy.')  > 0 ||
                                    strpos($result[$i]['link'], 'feeds.')  > 0 || 
                                    strpos($result[$i]['link'], 'feeds09.')  > 0 || 
                                    strpos($result[$i]['link'], 'feedsportal.')  > 0 ? 
                                    get_final_url($result[$i]['link']) : $result[$i]['link'];
                        $origUrl  = strpos($origUrl, 'infobunda.com') > 0 ||
                                    strpos($origUrl, 'bidanku.com') > 0 ||
                                    strpos($origUrl, 'mobile88.co.id') > 0 ? 
                                    $origUrl : preg_replace('/\?.*/', '', $origUrl);

                        $sql = "SELECT * FROM publisher_social_effect WHERE pnr_origlink = '{$origUrl}' AND pnr_latest = '1'";

                        $url_exist = $db->reset_all()->query($sql);
                        $url_count = $url_exist === FALSE ? 0 : $url_exist->num_rows;

                        echo PHP_EOL."--- Crawl URL: " . $origUrl . ' => ';

                        if(strpos($origUrl, 'lintas.me') === FALSE && strpos($result[$i]['link'], 'lintas.me') === FALSE) {
                            if($url_count > 0 && strlen($poster->checkPost($origUrl)) > 0) {
                                echo "DUPLICATE";
                                continue;
                            } else {					
                                if($poster->checkPost($origUrl) == '') {
                                    $content = $result[$i]['description'];
                                    $tag_url = $tagAPI . urlencode($content);
                                    $tag_ch = curl_init();

                                    curl_setopt($tag_ch, CURLOPT_URL, $tag_url);
                                    curl_setopt($tag_ch, CURLOPT_RETURNTRANSFER, TRUE);

                                    $tag_results = json_decode(curl_exec($tag_ch), TRUE);

                                    curl_close($tag_ch);

                                    $tags = array();
                                    if(count($tag_results) > 0) {
                                        foreach($tag_results as $word => $count) {
                                            $tags[] = $word;
                                        }
                                    }

                                    // Filtering
                                    $fields = array('text_data' => $content);
                                    $fields_string = '';

                                    foreach($fields as $key => $value) {
                                        $fields_string .= $key.'='.$value.'&';
                                    }

                                    rtrim($fields_string, '&');

                                    $fch = curl_init();

                                    curl_setopt($fch,CURLOPT_URL, $filterAPI);
                                    curl_setopt($fch,CURLOPT_POST, count($fields));
                                    curl_setopt($fch,CURLOPT_POSTFIELDS, $fields_string);
                                    curl_setopt($fch,CURLOPT_RETURNTRANSFER, TRUE);

                                    $filter_result = json_decode(curl_exec($fch), TRUE);
                                    $filter_result = $filter_result['result'];

                                    curl_close($fch);

                                    $filtered_as = "";
                                    if(count($filter_result) > 0) {
                                        foreach ($filter_result as $key => $value) {
                                            $filtered_as = $key.":".$value;
                                        }
                                    } 

                                    if($url_count == 0) {
                                        $cat_ch = curl_init();

                                        curl_setopt($cat_ch,CURLOPT_URL, $cat_url . $pub['category']);
                                        curl_setopt($cat_ch,CURLOPT_RETURNTRANSFER, TRUE);

                                        $catname = json_decode(curl_exec($cat_ch), TRUE);

                                        curl_close($cat_ch);

                                        $data = array (
                                                    'pnr_pub_member' => $pub['publisher_member'],
                                                    'pnr_category' => $pub['category'],
                                                    'pnr_topic' => $pub['topic'],
                                                    'pnr_title' => $result[$i]['title'],
                                                    'pnr_description' => $result[$i]['description'],
                                                    'pnr_tags' => trim(implode(',', $tags).",".$catname, ','),
                                                    'pnr_image' => $result[$i]['image'],
                                                    'pnr_author' => $result[$i]['author'],
                                                    'pnr_link' => $result[$i]['link'],
                                                    'pnr_origlink' => $origUrl,
                                                    'pnr_date' => date('Y-m-d H:i:s'),
                                                    'pnr_pub_date' => $result[$i]['pubdate'],
                                                    'pnr_filtered_as' => $filtered_as,
                                                    'pnr_status' => 'PENDING'
                                                );

                                        $saveRss = $db->insert_data('publisher_social_effect', $data);

                                        if($saveRss) echo "OK";
                                    }
                                } else {
                                        echo "DUPLICATE";
                                }

                                sleep(1);
                            }
                        } else {
                                echo "FORBIDDEN";
                        }
                    }
                } else {
                    print_r($result);
                }

                $end_rss_crawl = microtime(TRUE);
                $rss_crawl_time = $end_rss_crawl - $start_rss_crawl;

                echo PHP_EOL;
                echo 'RSS Crawling Time: ' . (ceil($rss_crawl_time / 60)) . ' minutes' . PHP_EOL;
                echo PHP_EOL;

                flush();
            }
	}
	
	$end = microtime(TRUE);
	$execTime = $end - $start;
	$remainingDelay = $delay - ceil($execTime);
	$remainingDelay = $remainingDelay < 0 ? 0 : $remainingDelay;
	
	// print_r($rssItems);
	echo ($saveRss) ? PHP_EOL.'Success post Publisher RSSs to Lintas.Me'.PHP_EOL : PHP_EOL.''.PHP_EOL;
	echo 'End crawling date: '.date('Y-m-d H:i:s').PHP_EOL;
	echo 'Execution Time: ' . (ceil($execTime / 60)) . ' minutes' . PHP_EOL;
	echo 'Sleep for '.(ceil($remainingDelay / 60)).' minutes....'.PHP_EOL; 
	echo '==========================================================================================================='.PHP_EOL;
	echo PHP_EOL;

	if($execTime > $delay) {
		$delay = 0;
	} else {
		$delay = $remainingDelay;
	}

	sleep(ceil($delay));
	flush();
	
	unset($start, $end, $execTime, $remainingDelay, $saveRss, $delay_setting, $where, $publisher, $param);
}
/**
 * End of file
 */
