<?php
ini_set('display_errors', 0);
error_reporting(0);
date_default_timezone_set('Asia/Jakarta');

require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/DBActiveRecord.php';
require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/poster.php';
require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/function.php';

$dbposter  = new DB_active_record;
$poster = new Poster;
$postToLM = FALSE;

echo "Start Poster ".PHP_EOL;

while(TRUE) {
	echo "=========================================================================".PHP_EOL;
	echo "Start Date : " . date('Y-m-d H:i:s').PHP_EOL;
	echo "=========================================================================".PHP_EOL;
	
	$delay = 15 * 60;
	$quantity_post = 1;
	$start = microtime(TRUE);
	$expired_in = 1;
	$now = date('Y-m-d');
	
	list($year, $month, $day) = explode('-', $now);
	
	$where = array('status' => '3');
	
	if(isset($argv[1])) {
		if($argv[1] != '') {
			for($i = 1; $i < count($argv); $i++) {
				$param = explode('=', $argv[$i]);
				
				switch ($param[0]) {					
					case 'group':
						$where += array('crawl_group' => $param[1]);
						break;
				}
			}	
		}
	}
	
	$domains = $dbposter->reset_all()->get('publisher_feed', $where)->result_array();
	$curr_num = 1;
	
	if(count($domains) > 0) {	
		foreach($domains as $dom) {
			echo PHP_EOL . $curr_num;
						
			$exec_query = $dbposter->reset_all()
							->query("SELECT publisher_social_effect.* FROM publisher_social_effect 
									  WHERE pnr_pub_member = '{$dom['publisher_member']}' AND pnr_status <> 'DUPLICATE' 
									  AND pnr_status <> 'OK' AND pnr_status <> 'FAILED' 
									  AND DATE(pnr_date) BETWEEN '".date('Y-m-d', strtotime($year . "-" . $month . "-" . ( $day - $expired_in )))."' 
									  AND DATE(NOW()) ORDER BY pnr_date DESC LIMIT {$quantity_post}");
			
			if($dom['publisher_member'] == '37') continue;
			
			if($exec_query !== FALSE){
				$rss_items = $exec_query->result_array();
								
				foreach ($rss_items as $rss) {
					if(strlen($rss['pnr_origlink']) > 5) {
						echo " - " . $dom['rss_url'] . " => Post ".$rss['pnr_origlink']." to Lintas.Me => ";
						
						$params = array(
									//'title' => preg_replace('/[^a-z0-9\-\s\:]+/i','', ($rss['pnr_title'])),
                                    'title' => $rss['pnr_title'],
									'content' => word_limiter(preg_replace('/[^(\x20-\x7F)]*/','',(str_replace(array('&#13;'), '', $rss['pnr_description']))), 200, '...'),
									'category' => $rss['pnr_category'],
									'image' => $rss['pnr_image'],
									'tags' => $rss['pnr_tags'],
									'url' => $rss['pnr_origlink'],
								);
						
						$postToLM = $poster->newPost($params);
						
						$RSSData = array (
										'pnr_latest' => '1',
										'pnr_post_ID' => isset($postToLM->detail_post->id) ? $postToLM->detail_post->id : '',
										'pnr_status' => isset($postToLM->status) ? $postToLM->status : 'FAILED',
										'pnr_posted_date' => isset($postToLM->detail_post->creation_date) ? date('Y-m-d H:i:s', $postToLM->detail_post->creation_date) : date('Y-m-d H:i:s')
									);
						
						$updateRSS = $dbposter->update_data('publisher_social_effect', $RSSData, array('pnr_ID' => $rss['pnr_ID']));
						
						$tags = explode(',', $rss['pnr_tags']);
                        $tags = array_unique($tags);
						if(count($tags) > 0) {
							foreach ($tags as $tag_item) {
								if($RSSData['pnr_post_ID'] != '') {								        
							        $saveTags = save_tags($tag_item, $RSSData['pnr_post_ID']);
								}
							}
						}
						
						if($updateRSS) {
							echo isset($postToLM->status) ? "[".$postToLM->status."]" : '[FAILED]';
						} else {
							echo PHP_EOL . $curr_num . " - No new RSS entry in \"{$dom['rss_url']}\".";
						}
						
						$log_data = array(
									'log_rss_item_ID' => $rss['pnr_ID'],
									'log_url' => $rss['pnr_origlink'],
									'log_error_code' => !isset($postToLM->status) ? $postToLM->error : ($postToLM->status == 'ERROR' ? '2' : '0'),
									'log_message' => !isset($postToLM->detail_post) ? $postToLM->error_msg : ($postToLM->detail_post == 'DELETED' ? $postToLM->detail_post . ' - ' . $postToLM->msg->message : 'Success posted to Lintas.me'),
									'log_insert_date' => date('Y-m-d H:i:s')
								);
						
						$dbposter->insert_data('publisher_post_log', $log_data);
						
						sleep(1);
					} else {
						echo PHP_EOL . $curr_num . " - No new RSS entry in \"{$dom['rss_url']}\".";
					}
				}
			} else {
				echo PHP_EOL . $curr_num . " - No new RSS entry in \"{$dom['rss_url']}\".";
			}
			
			flush();
			$curr_num++;
		}
	}
	
	$end = microtime(TRUE);
	$execTime = $end - $start;
	$delay = round($delay / 60);
	
	echo PHP_EOL;
	echo PHP_EOL;
	echo "=========================================================================".PHP_EOL;
	echo "End Date : " . date('Y-m-d H:i:s').PHP_EOL;
	echo ($postToLM) ? PHP_EOL.'Success post article to Lintas.Me'.PHP_EOL : '';
	echo PHP_EOL . 	'Execution Time: ' . $execTime . ' seconds' . PHP_EOL;
	echo "Delay for {$delay} minutes.".PHP_EOL;
	echo "=========================================================================".PHP_EOL;
	
	sleep($delay * 60);
	flush();
}
/**
 * End of file
 */