#!/usr/bin/php

<?php
/**
 * RSS Scanner for non publisher domains
 * @author Hendro (hendro@lintas.me)
 * 
 */

ini_set('display_errors', 1);
error_reporting(E_ALL); //E_ERROR | E_WARNING | E_PARSE);
date_default_timezone_set('Asia/Jakarta');

require_once __DIR__ . '/../shared_libs/ActiveRecord.php';
require_once __DIR__ . '/../shared_libs/Configuration.php';
require_once __DIR__ . '/../shared_libs/helpers.php';
require_once __DIR__ . '/../shared_libs/TextStatistics.php';

require_once __DIR__ . '/../shared_libs/vendor/autoload.php';
require_once __DIR__ . '/../shared_libs/monolog/Logger.php';
require_once __DIR__ . '/../shared_libs/monolog/Handler/HandlerInterface.php';
require_once __DIR__ . '/../shared_libs/monolog/Handler/AbstractHandler.php';
require_once __DIR__ . '/../shared_libs/monolog/Handler/AbstractProcessingHandler.php';
require_once __DIR__ . '/../shared_libs/monolog/Handler/StreamHandler.php';

require_once __DIR__ . '/../shared_libs/monolog/Formatter/FormatterInterface.php';
require_once __DIR__ . '/../shared_libs/monolog/Formatter/NormalizerFormatter.php';
require_once __DIR__ . '/../shared_libs/monolog/Formatter/LineFormatter.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use ActiveRecord\Config;
use SharedLibs\TextStatistics;

Config::initialize(function($cfg) {
    $cfg->set_model_directory(__DIR__ . '/../shared_libs/models');
    $cfg->set_connections(array(
    	'development' => 'mysql://'.Configuration::$db_user.':'.Configuration::$db_pass.'@'.Configuration::$db_host.'/'.Configuration::$db_name
    ));
});

$opts = getopt('i:c:l:');
$save_rss = FALSE;
$time_to_hour = array ( 'hour' => 1, 'day' => 24, 'week' => 168 );
$delay_setting = Engine_Settings::find('first', array('conditions' => array('alias=?','rss_scan_delay')));
$delay = isset($delay_setting->value) ? intval($delay_setting->value) * 60 : 600;
$rssnum_setting = Engine_Settings::find('first', array('conditions' => array('alias=?','rss_scan_num')));
$rss_num = isset($rssnum_setting->value) ? intval($rssnum_setting->value) : 10;
$tag_api = Configuration::$internal_api_url . "?c=autotag&m=process";
$img_api = Configuration::$internal_api_url . "uploader/upload/?url=";
$train_api = Configuration::$internal_api_url . "filter/train";
// $filter_api = Configuration::$internal_api_url . "filter/classify/?train=true";
$start  = microtime(TRUE);
$num = 1;

$log_path = Configuration::$log_path.(isset($opts['l']) ? $opts['l'] : 'DEBUG_rss_scanner.log');
$log = new Logger('rss_scanner');
$log->pushHandler(new StreamHandler($log_path));

$statistics = new TextStatistics;

/**
 * Process script parameters
 * 
 */
$where = array('dl_status=?', 'ACTIVE');

if(isset($opts['l']) == FALSE) {
	$log->addWarning("Log filename is not defined. Use [-l] parameter to define log filename.");
	$log->addInfo("");
}

if(isset($opts['i'])) {
	$where = array('dl_status=? AND dl_ID=?', 'ACTIVE', $opts['i']);
	$log->addInfo("Crawl by domain ID = ".$opts['i']);
	$log->addInfo("");
}

if(isset($opts['c'])) {
	$where = array('dl_status=? AND dl_category=?', 'ACTIVE', $opts['c']);
	$log->addInfo("Crawl by Category ID = ".$opts['c']);
	$log->addInfo("");
}

$domains = Engine_Domain_Rss_List::all(array('conditions' => $where));

$log->addInfo("Scan " . count($domains) . " URLs");

foreach ($domains as $pub) {
	$url = trim($pub->dl_rss_url);
	
	$log->addInfo($num." - Scan RSS: ".$url);
	
	// Get last-scanned date for current rss
	$last_scanned	   = $pub->dl_last_scanned ? intval($pub->dl_last_scanned) : 0;
	$last_scanned_date = date('Y-m-d H:i:s', $last_scanned);
	$update_type	   = $pub->dl_update_type != '' ? $pub->dl_update_type : 'hour';
	
	// Get delay for current rss
	$rss_scan_delay = intval($pub->dl_update_every) * $time_to_hour[$update_type] * 60 * 60;
	$next_scan_time = $last_scanned + $rss_scan_delay;
	
	$log->addInfo("Scan interval: {$pub->dl_update_every} {$update_type} or {$rss_scan_delay} seconds");
	$log->addInfo("Last scan: {$last_scanned_date} ({$last_scanned})");
	$log->addInfo("Next scan: ".date('Y-m-d H:i:s', $next_scan_time). "(".$next_scan_time.")");
	$log->addInfo("Now: ".date('Y-m-d H:i:s', time())." (".time().")");
	
	// If last-scanned date + scan delay less than current time AND if scan delay is not zero, skip to next rss item or do otherwise.
	if($next_scan_time > time() && $rss_scan_delay > 0) {
            $log->addInfo("SKIPPED TO NEXT RSS");
            $log->addInfo("");
            $num++;
            continue;
	}
	
        $array_rss = rss_reader($url, $rss_num);	
	if(isset($array_rss[0]['error'])) {
            $log->addWarning($array_rss[0]['messages']);
            $num++;
            continue;
	}
	
	$result = isset($array_rss['item']) ? $array_rss['item'] : array();
	$limit = count($result) < $rss_num ? count($result) : $rss_num; 
        
	if(count($result) > 0) {
            for($i = 0; $i < $limit; $i++) {
                if(isset($result[$i])) {
                    if(strlen($result[$i]['title']) > 3) {
                        $orig_link = strpos($result[$i]['link'], 'feedproxy.') !== FALSE ||
                                     strpos($result[$i]['link'], 'feeds.') !== FALSE || 
                                     strpos($result[$i]['link'], 'feeds09.') !== FALSE || 
                                     strpos($result[$i]['link'], 'feedsportal.') !== FALSE ? 
                                     preg_replace('/\?.*/', '', get_final_url($result[$i]['link'])) : $result[$i]['link'];

                        $url_exist = Engine_Social_Effect::find('first', array('conditions' => array('se_origlink = ?', $orig_link)));

                        $crawl_msg = "Crawl URL: ".$orig_link. ' => ';

                        if($url_exist !== NULL) {
                                $crawl_msg .= "ALREADY EXISTS";
                                $log->addInfo($crawl_msg);
                                continue;
                        }

                        $content = trim(strip_tags($result[$i]['description']));

                        // Autotagging start here
                        $tag_fields = array('content' => $content);
                        $tag_fields_string = '';

                        foreach($tag_fields as $key => $value) {
                            if(is_numeric($value) === FALSE) $tag_fields_string .= $key.'='.$value.'&';
                        }

                        rtrim($tag_fields_string, '&');

                        $ch = curl_init();

                        curl_setopt($ch,CURLOPT_URL, $tag_api);
                        curl_setopt($ch,CURLOPT_POST, count($tag_fields));
                        curl_setopt($ch,CURLOPT_POSTFIELDS, $tag_fields_string);
                        curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);

                        $tag_results = json_decode(curl_exec($ch), TRUE);

                        curl_close($ch);

                        $tags = array();
                        if(count($tag_results) > 0) {
                            foreach($tag_results as $word => $count) {
                                $tags[] = $word;
                            }
                        }
                        // Autotagging finish here

                        // Image processing
                        $ich = curl_init(); 

                        curl_setopt($ich,CURLOPT_URL, $img_api . $result[$i]['image']);
                        curl_setopt($ich,CURLOPT_RETURNTRANSFER, TRUE);

                        $image_output = json_decode(curl_exec($ich), TRUE);

                        curl_close($ich);

                        // Train and Filtering by Machine Learning
                        $category = Api_Categories::find('first', array('conditions' => array('id = ?', $pub->dl_category)));
                        $fields = array('content' => $content, 'classified_as' => $category->name);
                        $fields_string = '';

                        foreach($fields as $key => $value) {
                                $fields_string .= $key.'='.$value.'&';
                        }

                        rtrim($fields_string, '&');

                        $tch = curl_init();

                        curl_setopt($tch,CURLOPT_URL, $train_api);
                        curl_setopt($tch,CURLOPT_POST, count($fields));
                        curl_setopt($tch,CURLOPT_POSTFIELDS, $fields_string);
                        curl_setopt($tch,CURLOPT_RETURNTRANSFER, TRUE);
                        curl_exec($tch);
                        curl_close($tch);
                        
                        /*
                        $fields = array('text_data' => $content);
                        $fields_string = '';

                        foreach($fields as $key => $value) {
                                $fields_string .= $key.'='.$value.'&';
                        }

                        rtrim($fields_string, '&');

                        $fch = curl_init();

                        curl_setopt($fch,CURLOPT_URL, $filter_api);
                        curl_setopt($fch,CURLOPT_POST, count($fields));
                        curl_setopt($fch,CURLOPT_POSTFIELDS, $fields_string);
                        curl_setopt($fch,CURLOPT_RETURNTRANSFER, TRUE);

                        $filter_result = json_decode(curl_exec($fch), TRUE);
                        $filter_result = $filter_result['result'];

                        curl_close($fch);

                        $filtered_as = "";
                        if(count($filter_result) > 0) {
                                foreach ($filter_result as $key => $value) {
                                        $filtered_as = $key.":".$value;
                                }
                        }
                        */

                        if($url_exist === NULL) {
                            $slice_url = explode('/', $orig_link);
                            $source = str_replace("www.","",$slice_url[2]);

                            $data = array (
                                'se_domain_ID' 		  => $pub->dl_id,
                                'se_category' 		  => $pub->dl_category,
                                'se_title' 		  => $result[$i]['title'],
                                'se_image'                => $result[$i]['image'],
                                'se_image_local'          => isset($image_output['result']['original']) ? $image_output['result']['original'] : "",
                                'se_image_local_w'        => isset($image_output['info']['original']['width']) ? $image_output['info']['original']['width'] : "",
                                'se_image_local_h'        => isset($image_output['info']['original']['height']) ? $image_output['info']['original']['height'] : "",
                                'se_image_local_resize'   => isset($image_output['result']['resize']) ? $image_output['result']['resize'] : "",
                                'se_image_local_resize_w' => isset($image_output['info']['resize']['width']) ? $image_output['info']['resize']['width'] : "",
                                'se_image_local_resize_h' => isset($image_output['info']['resize']['height']) ? $image_output['info']['resize']['height'] : "",
                                'se_description' 	  => $content,
                                'se_description_html' 	  => trim($result[$i]['description_html']),
                                'se_excerpt' 		  => trim($result[$i]['excerpt']),
                                'se_link' 		  => $result[$i]['link'],
                                'se_guid' 		  => $result[$i]['guid'],
                                'se_origlink' 		  => $orig_link,
                                'se_author' 		  => isset($result[$i]['dc_creator']) ? $result[$i]['dc_creator'] : '',
                                'se_format' 		  => isset($result[$i]['dc_format']) ? $result[$i]['dc_format'] : '',
                                'se_lang' 		  => isset($result[$i]['dc_language']) ? $result[$i]['dc_language'] : '',
                                'se_pubdate' 		  => isset($result[$i]['pubDate']) ? date('Y-m-d H:i:s', strtotime($result[$i]['pubDate'])) : date('Y-m-d H:i:s'),
                                'se_insert_date' 	  => time(),
                                'se_filtered_as' 	  => $category->name,
                                'se_source' 		  => $source,
                                'se_tags'		  => trim(implode(',', $tags).','.$category->name, ', '),
                                'se_readability_score'	  => $result[$i]['readability_score']
                            );

                            $engine_se = new Engine_Social_Effect($data);
                            $save_rss  = $engine_se->save();

                            $new_tag = new Api_Se_Tags;
                            $api_tag = new Api_Tags;

                            foreach(explode(',', $data['se_tags']) as $tag_string) {
                                $tag_name = str_replace(' ', '-', strtolower($tag_string)); 
                                $tag      = Api_Tags::find('first', array('conditions' => array('name=?', $tag_name)));		

                                if(!isset($tag->name)) {			
                                    $api_tag->name         = $tag_name;
                                    $api_tag->display_name = $tag_string;
                                    $api_tag->save();
                                }

                                $new_tag->se_id  = $engine_se->se_id;
                                $new_tag->tag_id = isset($tag->id) ? $tag->id : $api_tag->id;		
                                $new_tag->save();
                            }

                            if($save_rss) $crawl_msg .= "OK";
                            else $crawl_msg .= "FAILED";

                            $log->addInfo($crawl_msg);
                        }
                    }
                }
            }
	} else {
            $log->addInfo("No new RSS feed.");
	}
	
	$domain = Engine_Domain_Rss_List::find('first', array('conditions' => array('dl_ID=?', $pub->dl_id)));
	$domain->dl_last_scanned = time();
	$domain->save();
	
	$log->addInfo("");
	
	$num++;
}

$end = microtime(TRUE);
$exec_time = $end - $start;
$delay = round($delay / 60);

$log->addInfo(($save_rss) ? 'Success save all contents to database' : '');
$log->addInfo('Execution Time: ' . ceil($exec_time/60) . ' minutes');	

/**
 * End of file
 */