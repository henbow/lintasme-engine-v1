<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('Asia/Jakarta');

require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/DBActiveRecord.php';
require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/poster.php';
require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/function.php';

$db  = new DB_active_record;
$poster = new Poster;
$updatePostImage = FALSE;
$tagAPI = "http://engine.lintas.me/?c=autotag&m=process&content=";
$filterAPI = "http://engine.lintas.me/filter/classify/?train=true";
$cat_url = "http://www.lintas.me/rest/getChannelName/";

echo "Start Post Updater ".PHP_EOL;

while(TRUE) {
	
	echo "=========================================================================".PHP_EOL;
	echo "Start Date : " . date('Y-m-d H:i:s').PHP_EOL;
	echo "=========================================================================".PHP_EOL;
	
	$and = '';
	if(isset($argv[1])) {
		$and = " AND pnr_pub_member = '".$argv[1]."'";
	}
	
	$startdate = date("Y-m-d", time() - ( 60 * 60 * 24 * 2 ));
	$enddate = date("Y-m-d", time() - ( 60 * 60 * 24 * 0 ));
		
	$rss_items = $db->reset_all()
				 ->query("SELECT * FROM publisher_social_effect WHERE 
						  (pnr_status = '' OR pnr_status = 'OK_NO_ID_IMAGE' OR pnr_status = 'OK_NO_IMAGE' OR pnr_status = 'DUPLICATE')
						  {$and} AND DATE(pnr_date) BETWEEN '{$startdate}' AND '{$enddate}' ORDER BY pnr_date DESC")
				 ->result_array();
	$curr_num = 1;
	
	if(count($rss_items) > 0){
		foreach ($rss_items as $rss) {
			echo PHP_EOL.PHP_EOL.$rss['pnr_link'].PHP_EOL;
			echo "http://www.lintas.me/article/";
			
			if($rss['pnr_image'] != '' && $rss['pnr_post_ID'] != '') {
				$updatePostImage = json_decode($poster->updatePostImage($rss['pnr_post_ID'], $rss['pnr_image']), TRUE);
				
				echo $rss['pnr_post_ID']." => ";
				
				if(isset($updatePostImage['result']['original'])) {
					$updateRSS = $db->update_data('publisher_social_effect', array( 'pnr_status' => 'OK'), array('pnr_ID' => $rss['pnr_ID']));
					
					if($updateRSS) {
						 echo ($updatePostImage['result']['original'] ? "[OK]" : '[FAILED]').PHP_EOL;
					}
				}
				
				// print_r($updatePostImage);
			} else {
				$domain = $db->reset_all()->query("SELECT * FROM publisher_feed WHERE publisher_member = {$rss['pnr_pub_member']} AND status = '3'")->row_array();
				$html = str_replace('property="og:','property="og_',file_get_contents($rss['pnr_link']));
				$dom  = new DOMDocument();
				
			    @$dom->loadHTML($html);
				
				$xpath = new DOMXPath($dom);
				$imgurl = (string) $xpath->evaluate('string('.str_replace('property="og:','property="og_',$domain['default_image_tag']).')');
				
				if(strpos($imgurl, 'http') === FALSE && $imgurl != '') {
					$parse = parse_url($rss['pnr_link']);
					$imgurl = "http://".$parse['host']."/".str_replace(' ', '%20', $imgurl);
				}
				
				$post_ID = $rss['pnr_post_ID'] == '' ? $poster->checkPost($rss['pnr_link']) : $rss['pnr_post_ID'];
				
				if($post_ID == '') {
					$tag_url = $tagAPI . urlencode($rss['pnr_description']);
					$tag_ch = curl_init();
						
					curl_setopt($tag_ch, CURLOPT_URL, $tag_url);
					curl_setopt($tag_ch, CURLOPT_RETURNTRANSFER, TRUE);
					
					$tag_results = json_decode(curl_exec($tag_ch), TRUE);
						
					curl_close($tag_ch);
					
					$tags = array();
					if(count($tag_results) > 0) {
						foreach($tag_results as $word => $count) {
							$tags[] = $word;
						}
					}
					
					$cat_ch = curl_init();
					        	
			        curl_setopt($cat_ch,CURLOPT_URL, $cat_url . $rss['pnr_category']);
			        curl_setopt($cat_ch,CURLOPT_RETURNTRANSFER, TRUE);
			        
			        $catname = json_decode(curl_exec($cat_ch), TRUE);
			        	
			        curl_close($cat_ch);
					
					$params = array (
								'title' => $rss['pnr_title'],
								'content' => $rss['pnr_description'] == '' ? $rss['pnr_title'] : trim(word_limiter(str_replace('&#13;', '', $rss['pnr_description']), 100, '...')),
								'category' => $rss['pnr_category'],
								'tags' => trim(implode(',', $tags).",".$catname, ','),
								'image' => $rss['pnr_image'],
								'url' => $rss['pnr_link'],
								'rand_user' => ""
							);
							
					$posttoLM = $poster->newPost($params);
					$post_ID  = isset($posttoLM->detail_post->id) ? $posttoLM->detail_post->id : "";
					
					//echo PHP_EOL; 
					//print_r($params);
					//print_r($posttoLM);
					//echo PHP_EOL;
				}
				
				echo $post_ID." use pattern ".$domain['default_image_tag']." => ";
				echo $imgurl.PHP_EOL;
				
				if(strpos($imgurl, 'http') !== FALSE && $post_ID != '') {
					$updatePostImage = json_decode($poster->updatePostImage($post_ID, $imgurl), TRUE);
					
					if(isset($updatePostImage['result']['original'])) {
						$updateRSS = $db->update_data('publisher_social_effect', array('pnr_post_ID' => $post_ID, 'pnr_image' => $imgurl, 'pnr_status' => 'OK'), array('pnr_ID' => $rss['pnr_ID']));
						
						if($updateRSS) {
							echo ($updatePostImage['result']['original'] ? "[OK]" : '[FAILED]').PHP_EOL;
						}
					} else {
						echo '[FAILED]'.PHP_EOL;
					}
				} else {
					echo '[FAILED UPDATE POST]'.PHP_EOL;
				}
				
				$db->update_data('publisher_social_effect', array('pnr_status' => 'OK_FAILED_GET_IMG'), array('pnr_ID' => $rss['pnr_ID']));
			}

			sleep(2);
			flush();
		}
	}

	echo "=========================================================================".PHP_EOL;
	echo "End Date : " . date('Y-m-d H:i:s').PHP_EOL;
	echo "=========================================================================".PHP_EOL;
	
	sleep(300);	
	flush();
}
/**
 * End of file
 */