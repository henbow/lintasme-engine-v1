<?php
/**
 * Extract content from RSS feed and format it as array
 * @access public
 * @param $rss_url string RSS feed URL
 * @param $limit integer Limit number of content to be extracted
 * @param $min_words integer 
 * @return array
 * 
 */
function rss_reader($rss_url, $limit = 10, $min_words = 30) {
    if(!url_exists($rss_url)) return FALSE;

    $output_rss = array();
    $statistics = new \SharedLibs\TextStatistics;
    $target_url = urlencode(trim($rss_url));
    $resource   = sprintf(Configuration::$reader_url, $target_url, $limit);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $resource);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $resultdata = json_decode(curl_exec($ch), TRUE);
    $results    = $resultdata['rss']['channel']['item'];
    curl_close($ch);
    
    print_r($results);
      
    $scan_num_limit = count($results) < $limit ? count($results) : $limit;

    if(isset($results[0])) {
        for($i = 0; $i < $scan_num_limit; $i++) {
            if(isset($results[$i]['media_content'][0]['@attributes']['url'])) {
                    $image = $results[$i]['media_content'][0]['@attributes']['url'];
            } else {
                $dom = new DOMDocument();
                @$dom->loadHTML($results[$i]['content_encoded']);

                $xpath = new DOMXPath($dom);
                $image = $xpath->evaluate("string(//img/@src)");
            }

            $readability_score = $statistics->automated_readability_index($results[$i]['content_encoded']);
            $words_count       = $statistics->word_count($statistics->cleanText);
            $content           = clean_text($results[$i]['content_encoded']);
            $excerpt           = substr(get_excerpt($content, 55, "..."), strpos($content, "."));			
            $excerpt           = clean_text($excerpt, TRUE);
            
            if($words_count >= $min_words && $content !== '' && $excerpt !== '') {
                $output_rss['item'][$i]['title']             = clean_text($results[$i]['title'], TRUE);
                $output_rss['item'][$i]['description']       = $content;
                $output_rss['item'][$i]['description_html']  = $results[$i]['content_encoded'];
                $output_rss['item'][$i]['excerpt']           = $excerpt;
                $output_rss['item'][$i]['link']              = $results[$i]['link'];
                $output_rss['item'][$i]['guid']              = $results[$i]['guid'];
                $output_rss['item'][$i]['origlink']          = preg_replace('/\?./', '', get_final_url($results[$i]['link']));
                $output_rss['item'][$i]['pubdate']           = isset($results[$i]['pubDate']) ? $results[$i]['pubDate'] : date("D, d M Y H:i:s T");
                $output_rss['item'][$i]['image']             = $image ? preg_replace('/\?.*/', '', $image) : '';
                $output_rss['item'][$i]['author']            = isset($results[$i]['dc_creator']) ? $results[$i]['dc_creator'] : '';
                $output_rss['item'][$i]['language']          = isset($results[$i]['dc_language']) ? $results[$i]['dc_language'] : '';
                $output_rss['item'][$i]['format']            = isset($results[$i]['dc_format']) ? $results[$i]['dc_format'] : '';
                $output_rss['item'][$i]['identifier']        = isset($results[$i]['dc_identifier']) ? $results[$i]['dc_identifier'] : '';
                $output_rss['item'][$i]['readability_score'] = $readability_score;
            }
        }
    } else {
        if(isset($results['media_content'][0]['@attributes']['url'])) {
            $image = $results['media_content'][0]['@attributes']['url'];
        } else {
            $dom = new DOMDocument();
            @$dom->loadHTML($results['content_encoded']);

            $xpath = new DOMXPath($dom);
            $image = $xpath->evaluate("string(//img/@src)");
        }

        $readability_score = $statistics->automated_readability_index($results['content_encoded']);
        $words_count       = $statistics->word_count($statistics->cleanText);
        $content           = clean_text($results['content_encoded']);
        $excerpt           = substr(get_excerpt($content, 55, "..."), strpos($content, "."));			
        $excerpt           = clean_text($excerpt, TRUE);

        if($words_count >= $min_words && $content !== '' && $excerpt !== '') {
            $output_rss['item'][$i]['title']             = clean_text($results['title'], TRUE);
            $output_rss['item'][$i]['description']       = $content;
            $output_rss['item'][$i]['description_html']  = $results['content_encoded'];
            $output_rss['item'][$i]['excerpt']           = $excerpt;
            $output_rss['item'][$i]['link']              = $results['link'];
            $output_rss['item'][$i]['guid']              = $results['guid'];
            $output_rss['item'][$i]['origlink']          = preg_replace('/\?./', '', get_final_url($results['link']));
            $output_rss['item'][$i]['pubdate']           = isset($results[$i]['pubDate']) ? $results['pubDate'] : date("D, d M Y H:i:s T");
            $output_rss['item'][$i]['image']             = $image ? preg_replace('/\?.*/', '', $image) : '';
            $output_rss['item'][$i]['author']            = isset($results['dc_creator']) ? $results['dc_creator'] : '';
            $output_rss['item'][$i]['language']          = isset($results['dc_language']) ? $results['dc_language'] : '';
            $output_rss['item'][$i]['format']            = isset($results['dc_format']) ? $results['dc_format'] : '';
            $output_rss['item'][$i]['identifier']        = isset($results['dc_identifier']) ? $results['dc_identifier'] : '';
            $output_rss['item'][$i]['readability_score'] = $readability_score;
        }
    }
    
    print_r($output_rss);

    return $output_rss;
}

/**
 * Extract content from single URL and format it as array
 * @access public
 * @param $rss_url string URL to crawl
 * @param $limit integer Limit number of content to be extracted
 * @param $min_words integer 
 * @return array
 * 
 */
function crawl_single_url($url, $min_words = 20) {
    if(!url_exists($url)) return FALSE;
    
    $output_rss = array();
    $statistics = new \SharedLibs\TextStatistics;
    $target_url = urlencode(trim($url));
    $resource   = sprintf(Configuration::$reader_url, $target_url, 1);
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $resource);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $data    = json_decode(curl_exec($ch), TRUE);
    $results = $data['rss']['channel']['item'];
    curl_close($ch);
        
    if(isset($results['media_content'][0]['@attributes']['url'])) {
        $image = $results['media_content'][0]['@attributes']['url'];
    } else {
        $dom = new DOMDocument();
        @$dom->loadHTML($results['content_encoded']);

        $xpath = new DOMXPath($dom);
        $image = $xpath->evaluate("string(//img/@src)");
    }

    $readability_score = $statistics->automated_readability_index($results['content_encoded']);
    $words_count       = $statistics->word_count($statistics->cleanText);
    $content           = clean_text($results['content_encoded']);
    //$excerpt_text      = strpos($content, ". ") === FALSE ? $content : substr($content, strpos($content, ". "), 300);			
    $excerpt           = clean_text(get_excerpt($content, 55, "..."), TRUE);

    //if($words_count >= $min_words && $content !== '' && $excerpt !== '') {
        $output_rss['title']             = clean_text($results['title'], TRUE);
        $output_rss['description']       = $content;
        $output_rss['description_html']  = $results['content_encoded'];
        $output_rss['excerpt']           = $excerpt;
        $output_rss['link']              = $results['link'];
        $output_rss['guid']              = $results['guid'];
        $output_rss['origlink']          = preg_replace('/\?./', '', get_final_url($results['link']));
        $output_rss['pubdate']           = isset($results['pubDate']) ? $results['pubDate'] : date("D, d M Y H:i:s T");
        $output_rss['image']             = $image ? preg_replace('/\?./', '', $image) : '';
        $output_rss['author']            = isset($results['dc_creator']) ? $results['dc_creator'] : '';
        $output_rss['language']          = isset($results['dc_language']) ? $results['dc_language'] : '';
        $output_rss['format']            = isset($results['dc_format']) ? $results['dc_format'] : '';
        $output_rss['identifier']        = isset($results['dc_identifier']) ? $results['dc_identifier'] : '';
        $output_rss['readability_score'] = $readability_score;
    //}

    return $output_rss;
}

/**
 * Create excerpt from text
 * @access public
 * 
 */
function get_excerpt($text, $num_words=55, $more=null) {
	if (null === $more) $more = '&hellip;';
	$text = strip_tags($text);
	//TODO: Check if word count is based on single characters (East Asian characters)
	/*
	if (1==2) {
		$text = trim(preg_replace("/[\n\r\t ]+/", ' ', $text), ' ');
		preg_match_all('/./u', $text, $words_array);
		$words_array = array_slice($words_array[0], 0, $num_words + 1);
		$sep = '';
	} else {
		$words_array = preg_split("/[\n\r\t ]+/", $text, $num_words + 1, PREG_SPLIT_NO_EMPTY);
		$sep = ' ';
	}
	*/
	$words_array = preg_split("/[\n\r\t ]+/", $text, $num_words + 1, PREG_SPLIT_NO_EMPTY);
	$sep = ' ';
	if (count($words_array) > $num_words) {
		array_pop($words_array);
		$text = implode($sep, $words_array);
		$text = $text.$more;
	} else {
		$text = implode($sep, $words_array);
	}
	// trim whitespace at beginning or end of string
	// See: http://stackoverflow.com/questions/4166896/trim-unicode-whitespace-in-php-5-2
	$text = preg_replace('/^[\pZ\pC]+|[\pZ\pC]+$/u', '', $text);
	return $text;
}

/**
 * Clean extracted text
 * @access public
 * @param $strText string
 * @return string
 * 
 */
function clean_text($strText, $title = FALSE) {
    $targets = array('\r\n','\n','\r','\t');
    $results = array(" "," "," ","");
	
    $strText = strip_tags($strText);
    $strText = str_ireplace(array('[unable to retrieve full-text content]', '[embedded content]', 'embedded content', 'embedded_content'), '', $strText); // remove sentence "[unable to retrieve full text content]" from content
    $strText = str_replace($targets, $results, $strText);
    $strText = preg_replace('/[\*]+/', ' ', $strText); // Remove multiple wild-chars
    $strText = preg_replace('/[_]+/', ' ', $strText); // Remove multiple underscores
    $strText = preg_replace('/[ ]+/', ' ', $strText); // Remove multiple spaces
    $strText = html_entity_decode($strText);
	
    $trans = get_html_translation_table(HTML_ENTITIES); 
    $trans[chr(130)] = '&sbquo;';    // Single Low-9 Quotation Mark 
    $trans[chr(131)] = '&fnof;';    // Latin Small Letter F With Hook 
    $trans[chr(132)] = '&bdquo;';    // Double Low-9 Quotation Mark 
    $trans[chr(133)] = '&hellip;';    // Horizontal Ellipsis 
    $trans[chr(134)] = '&dagger;';    // Dagger 
    $trans[chr(135)] = '&Dagger;';    // Double Dagger 
    $trans[chr(136)] = '&circ;';    // Modifier Letter Circumflex Accent 
    $trans[chr(137)] = '&permil;';    // Per Mille Sign 
    $trans[chr(138)] = '&Scaron;';    // Latin Capital Letter S With Caron 
    $trans[chr(139)] = '&lsaquo;';    // Single Left-Pointing Angle Quotation Mark 
    $trans[chr(140)] = '&OElig;';    // Latin Capital Ligature OE 
    $trans[chr(145)] = '&lsquo;';    // Left Single Quotation Mark 
    $trans[chr(146)] = '&rsquo;';    // Right Single Quotation Mark 
    $trans[chr(147)] = '&ldquo;';    // Left Double Quotation Mark 
    $trans[chr(148)] = '&rdquo;';    // Right Double Quotation Mark 
    $trans[chr(149)] = '&bull;';    // Bullet 
    $trans[chr(150)] = '&ndash;';    // En Dash 
    $trans[chr(151)] = '&mdash;';    // Em Dash 
    $trans[chr(152)] = '&tilde;';    // Small Tilde 
    $trans[chr(153)] = '&trade;';    // Trade Mark Sign 
    $trans[chr(154)] = '&scaron;';    // Latin Small Letter S With Caron 
    $trans[chr(155)] = '&rsaquo;';    // Single Right-Pointing Angle Quotation Mark 
    $trans[chr(156)] = '&oelig;';    // Latin Small Ligature OE 
    $trans[chr(159)] = '&Yuml;';    // Latin Capital Letter Y With Diaeresis 
    $trans['euro'] = '&euro;';    // euro currency symbol 
    ksort($trans); 
     
    foreach ($trans as $k => $v) {
        $strText = str_replace($v, $k, $strText);
    }
	
    $strText = str_replace(array_keys(Configuration::$to_replace), array_values(Configuration::$to_replace), $strText);
    $strText = str_replace(array("á","à","â","ã","ª","ä"),"a",$strText);
    $strText = str_replace(array("Â","Â","Á","À","Â","Ã","Ä"),"A",$strText);
    $strText = str_replace(array("Í","Ì","Î","Ï"),"I",$strText);
    $strText = str_replace(array("í","ì","î","ï"),"i",$strText);
    $strText = str_replace(array("é","è","ê","ë"),"e",$strText);
    $strText = str_replace(array("É","È","Ê","Ë"),"E",$strText);
    $strText = str_replace(array("ó","ò","ô","õ","º","ö"),"o",$strText);
    $strText = str_replace(array("Ó","Ò","Ô","Õ","Ö"),"O",$strText);
    $strText = str_replace(array("ú","ù","û","ü"),"u",$strText);
    $strText = str_replace(array("Ú","Ù","Û","Ü"),"U",$strText);
    $strText = str_replace(array("’","‘","‹","›‚"),"'",$strText);
    $strText = str_replace(array("“","”","«","»","„"),'"',$strText);
    $strText = str_replace("–","-",$strText);
    $strText = str_replace(" "," ",$strText);
    $strText = str_replace("ç","c",$strText);
    $strText = str_replace("Ç","C",$strText);
    $strText = str_replace("ñ","n",$strText);
    $strText = str_replace("Ñ","N",$strText);	
    
    // Remove unwanted characters from beginning of string.
    if($title === TRUE) {
        $strText = ucwords(ltrim($strText, ';:.`\\/])}, ')); 
    } else {
        $strText = ucfirst(ltrim($strText, ';:.`\\/[](){}, '));
    }
    
    $strText = trim(trim(trim($strText), ';:-_,.* '));
    
    return $strText;
}

function get_tweets($url) {
	$json_string = @file_get_contents('http://urls.api.twitter.com/1/urls/count.json?url=' . $url);
	$json = json_decode($json_string, true);
	return intval( $json['count'] );
}

function url_exists($url=NULL)  
{  
    if($url == NULL) return false;  
    $ch = curl_init($url);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
    $data = curl_exec($ch);  
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);  
    curl_close($ch);  
    if($httpcode>=200 && $httpcode<300){  
        return true;  
    } else {  
        return false;  
    }  
}

function get_redirect_url($url){
    $redirect_url = null; 

    $url_parts = @parse_url($url);
    if (!$url_parts) return false;
    if (!isset($url_parts['host'])) return false; //can't process relative URLs
    if (!isset($url_parts['path'])) $url_parts['path'] = '/';

    $sock = @fsockopen($url_parts['host'], (isset($url_parts['port']) ? (int)$url_parts['port'] : 80), $errno, $errstr, 30);
    if (!$sock) return false;

    $request = "HEAD " . $url_parts['path'] . (isset($url_parts['query']) ? '?'.$url_parts['query'] : '') . " HTTP/1.1\r\n"; 
    $request .= 'Host: ' . $url_parts['host'] . "\r\n"; 
    $request .= "Connection: Close\r\n\r\n"; 
    @fwrite($sock, $request);
    $response = '';
    while(!feof($sock)) $response .= @fread($sock, 8192);
    fclose($sock);

    if (preg_match('/^Location: (.+?)$/m', $response, $matches)){
        if ( substr($matches[1], 0, 1) == "/" )
            return $url_parts['scheme'] . "://" . $url_parts['host'] . trim($matches[1]);
        else
            return trim($matches[1]);

    } else {
        return false;
    }

}

/**
 * get_all_redirects()
 * Follows and collects all redirects, in order, for the given URL. 
 *
 * @param string $url
 * @return array
 */
function get_all_redirects($url){
    $redirects = array();
    while ($newurl = get_redirect_url($url)){
        if (in_array($newurl, $redirects)){
            break;
        }
        $redirects[] = $newurl;
        $url = $newurl;
    }
    return $redirects;
}

/**
 * get_final_url()
 * Gets the address that the URL ultimately leads to. 
 * Returns $url itself if it isn't a redirect.
 *
 * @param string $url
 * @return string
 */
function get_final_url($url){
    $redirects = get_all_redirects($url);
    if (count($redirects)>0){
        return array_pop($redirects);
    } else {
        return $url;
    }
}

function get_final_location($url)
{
	$headers = @get_headers($url);
	$pattern = '/Location\s*:\s*(https?:[^;\s\n\r]+)/i';

	if ($locations = preg_grep($pattern, $headers))
	{
		preg_match($pattern, end($locations), $redirect);
		return $redirect[1];
	}

	return $url;
}

/**
 * End of file
 * 
 */