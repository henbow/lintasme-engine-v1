<?php
 
class Configuration
{
    /**
     * Database configuration
     * 
     */
    public static $db_host 			  = '192.168.1.3';
    public static $db_user 			  = 'lcm';
    public static $db_pass 			  = 'lcm12345';
    public static $db_name 			  = 'lintasengine';
    public static $db_port 			  = '3306';
	
    /**
     * Internal API
     * 
     */
    public static $internal_api_url   = "http://brain.kurioapps.com/";
    
    /**
     * Readability Config
     * 
     */
    public static $api_url            = "https://readability.com/api/content/v1/parser?url=%s&token=%s";
    public static $readability_tokens = array( "096764936b88f2426210975b96311199b3c9c90c", );

    /**
     * API URLs
     */
    public static $reader_url         = "http://brain.kurioapps.com/reader/makefulltextfeed.php?url=%s&max=%s&links=preserve&exc=&summary=1&format=json";

    /**
     * Log path
     */
    public static $log_path           = "/home/kurio/daemons-log/";

     /**
      * Characters to replace
      */
    public static $to_replace         = array (	
                                            '&13;'	=> "",
                                            '&13' 	=> "",
                                            '&#13;'	=> "",
                                            '&#x2019;' 	=> "'",
                                            '&#8217;' 	=> "'",
                                            '&lsquo;'	=> "'",
                                            '&rsquo;'	=> "'",
                                            '&#8220;'	=> '"',
                                            '&#8221;'	=> '"',
                                            '&ldquo;'	=> '"',
                                            '&rdquo;'	=> '"',
                                            '&hellip;'	=> ".",
                                            '&#xA0;'	=> " ",
                                            '&nbsp;'	=> " ",
                                            "’"		=> "'",
                                            "***"	=> "",
                                            '&sbquo;'	=> ",",
                                            '&bdquo;'	=> ",,",
                                            '&dagger;'	=> "",
                                            '&Dagger;'	=> "",
                                            '&circ;'	=> "",
                                            '&permil;'	=> "permil",
                                            '&Scaron;'	=> "S",
                                            '&scaron;'	=> "s",
                                            '&lsaquo;'	=> "<",
                                            '&rsaquo;'	=> ">",
                                            '&OElig;'	=> "OE",
                                            '&oelig;'	=> "oe",
                                            '&ndash;'	=> "-",
                                            '&mdash;'	=> "-",
                                            '&tilde;'	=> "~",
                                            '&Yuml;'	=> "Y",
                                            '&euro;'	=> "Euro",
                                            '&amp;'     => "&",
                                            '&gt;'      => ">",
                                            '&lt;'      => "<",
                                            '&amp;nbsp' => " ",
                                        );
}

/**
 * End of file DBConfig.php
 * Location: ./libs/DBConfig.php
 * 
 */
