#!/bin/bash
USER="skrip"
PASS="lcmskrip"
CMD=$1

VAR=$(expect -c "
spawn su $USER
expect \"Password:\" 
send \"$PASS\r\"
expect \"\\\\$\"
send \"$CMD\r\"
expect -re \"$USER.*\"
send \"exit\"
")

echo "$VAR"
