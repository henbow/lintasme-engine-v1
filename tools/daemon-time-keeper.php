<?php
/**
 * This script is to auto-restart the daemons according to daemon setting.
 * @author Hendro Wibowo (hendro@lintas.me)
 * 
 */

ini_set('display_errors', 1);
ini_set("memory_limit","520M");
error_reporting(E_ALL);
date_default_timezone_set('Asia/Jakarta');

require dirname(__FILE__).DIRECTORY_SEPARATOR.'../domain/libs/DBActiveRecord.php';

$opts = getopt("e:");
$expire = isset($opts['e']) ? intval($opts['e']) : 2;
$db = new DB_active_record();
$daemons = $db->get('engine_daemon_manager')->result_array();
$host = "http://engine.lintas.me";

while(TRUE) {
	exec('ps aux | grep engine.lintas.me', $outps);
	
	$expired_ps = array();
	if (count($outps) > 0) {
	    for ($i = 0; $i < count($outps); $i++) {
	    	$ps = preg_split('/\s+/', $outps[$i]);
	        $pid = $ps[1];
	        $cpu = $ps[2];
	        $mem = $ps[3];
	        $time = date('Y-m-d H:i:s', strtotime($ps[8]));
	        $runtime = time() - strtotime($ps[8]);
			$runtime_in_days = round($runtime / 60 / 60 / 24, 0, PHP_ROUND_HALF_DOWN);
	        $command = $ps[10] . " " . $ps[11] . " " . (isset($ps[12]) ? " " . $ps[12] : "") . (isset($ps[13]) ? " " . $ps[13] : "");
			
			if($runtime_in_days > $expire) {
				$expired_ps[] = array(
					'pid' => $pid,
					'start' => $time,
					'runtime' => $runtime_in_days,
					'command' => str_replace('  ', ' ', $command)
				);
			}
	    }
	}
	
	if(count($daemons) > 0) {
		foreach ($daemons as $daemon) {
			$php_command = trim("php -q ".$daemon['script_path']." ".$daemon['script_params']);
			foreach($expired_ps as $eps) {
				if($eps['command'] == $php_command) {
					echo "Kill: ".$php_command." with PID [".$eps['pid']."]".PHP_EOL;
					exec('curl "'.$host.'/daemon/restart/'.$eps['pid'].'/'.$daemon['daemon_id'].'"');
					sleep(30);
				}
			}
		}
	}
	
	sleep(3 * 3600);
}
/**
 * End of file
 * 
 */