<?php
$commands = array(
	// Crawler/RSS Scanner
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.rss.scanner.php domain.rss.scanner-bisnis.log domain channel=bisnis',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.rss.scanner.php domain.rss.scanner-design.log domain channel=design',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.rss.scanner.php domain.rss.scanner-entertainment.log domain channel=entertainment',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.rss.scanner.php domain.rss.scanner-fun.log domain channel=fun',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.rss.scanner.php domain.rss.scanner-games.log domain channel=games',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.rss.scanner.php domain.rss.scanner-internet.log domain channel=internet',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.rss.scanner.php domain.rss.scanner-lifestyle.log domain channel=lifestyle',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.rss.scanner.php domain.rss.scanner-news.log domain channel=news',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.rss.scanner.php domain.rss.scanner-otomotif.log domain channel=otomotif',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.rss.scanner.php domain.rss.scanner-sports.log domain channel=sports',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.rss.scanner.php domain.rss.scanner-tech.log domain channel=technology',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.rss.scanner.php domain.rss.scanner-woman.log domain channel=woman',
	
	// Poster
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.poster.php domain.poster-bisnis.log domain channel=bisnis',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.poster.php domain.poster-design.log domain channel=design',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.poster.php domain.poster-entertainment.log domain channel=entertainment',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.poster.php domain.poster-fun.log domain channel=fun',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.poster.php domain.poster-games.log domain channel=games',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.poster.php domain.poster-internet.log domain channel=internet',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.poster.php domain.poster-lifestyle.log domain channel=lifestyle',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.poster.php domain.poster-news.log domain channel=news',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.poster.php domain.poster-otomotif.log domain channel=otomotif',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.poster.php domain.poster-sports.log domain channel=sports',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.poster.php domain.poster-tech.log domain channel=technology',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.poster.php domain.poster-woman.log domain channel=woman',

	// Set Popular
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.set.popular.php domain.set.popular.log domain',

	// Social Effect Checker
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.social.effect.php domain.social.effect.log domain',

	// Autotweet
	'/var/www/engine.lintas.me/tools/execute-daemon.exp domain.autotweet.php domain.autotweet.log domain',

	// Publisher Crawlers
	'/var/www/engine.lintas.me/tools/execute-daemon.exp pub.rss.scanner-latest.php pub.rss.scanner-1.log publisher -cg=1',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp pub.rss.scanner-latest.php pub.rss.scanner-2.log publisher -cg=2',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp pub.rss.scanner-latest.php pub.rss.scanner-3.log publisher -cg=3',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp pub.rss.scanner-latest.php pub.rss.scanner-4.log publisher -cg=4',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp pub.rss.scanner-latest.php pub.rss.scanner-5.log publisher -cg=5',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp pub.rss.scanner-latest.php pub.rss.scanner-6.log publisher -cg=6',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp pub.rss.scanner-latest.php pub.rss.scanner-7.log publisher -cg=7',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp pub.rss.scanner-latest.php pub.rss.scanner-8.log publisher -cg=8',
	'/var/www/engine.lintas.me/tools/execute-daemon.exp pub.rss.scanner-latest.php pub.rss.scanner-9.log publisher -cg=9',

	// Publisher Poster
	'/var/www/engine.lintas.me/tools/execute-daemon.exp pub.poster.php pub.poster.log publisher',

	// Publisher Social Effect
	'/var/www/engine.lintas.me/tools/execute-daemon.exp pub.social.effect.php pub.social.effect.log publisher'
);

foreach($commands as $command)
{
	exec($command, $output);

	print_r($output);
	print PHP_EOL;
}

exit();
?>