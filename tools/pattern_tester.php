<?php
ini_set('display_errors', 0);
error_reporting(0);

require '../domain/libs/function.php';

$url = 'http://'.str_replace('http://', '', $_POST['url']);
$type = $_POST['type'];
$titlep = $_POST['title_pattern'];
$descp  = $_POST['desc_pattern'];
$imgp   = $_POST['image_pattern'];
$tag_param = array(
				'title' => $titlep ? $titlep : '//channel/item/title',
				'description' => $descp ? $descp : '//channel/item/description',
				'image' => $imgp ? $imgp : '//channel/item/description'
			);

$arrayrss = rss2array($url, $type, $tag_param, 2);

$output = array();

foreach ($arrayrss['item'] as $key => $value) {
	$output[] = array($key => $value);
}
header("Access-Control-Allow-Origin: *");
header('Content-type: application/json');
echo json_encode($output[0][0]);
?>
