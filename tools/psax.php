<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

$psname = isset($_GET['ps']) ? $_GET['ps'] : "engine.lintas.me\/[domain|publisher|local]";
exec('ps aux | grep "'.urldecode($psname).'"', $psOutput);

$allps = array();
if (count($psOutput) > 0) {
    foreach ($psOutput as $ps) {    	
    	$ps = preg_split('/\s+/', $ps);
        $pid = $ps[1];
        $cpu = $ps[2];
        $mem = $ps[3];
        $time = $ps[8];
        $runtime = $ps[9];
        $command = $ps[10] . " " . $ps[11] . " " . (isset($ps[12]) ? " " . $ps[12] : "") . (isset($ps[13]) ? " " . $ps[13] : "");
		
		if(strpos($command, 'php') === 0) {
            $allps['ps_items'][] = array(
							'pid' => $pid,
							'cpu_usage' => $cpu,
							'memory_usage' => $mem,
							'starttime' => date('Y-m-d H:i:s', strtotime($time)),
							'stime' => strtotime($time),
							'runtime' => time() - strtotime($time),
							'process_name' => $command
						);
		}
    }
}

header('Content-type: application/json');
echo json_encode($allps);
/**
 * End of file
 */