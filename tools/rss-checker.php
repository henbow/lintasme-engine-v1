<?php
while(TRUE) {
libxml_use_internal_errors(TRUE);
$xml = file_get_contents($argv[1]);
$loadxml = simplexml_load_string($xml);

if($loadxml === FALSE) {
	echo "Errors: ".PHP_EOL;
	foreach(libxml_get_errors() as $error) {
        echo $error->message;
    }
} else {
	echo var_dump($loadxml).PHP_EOL;
}
}