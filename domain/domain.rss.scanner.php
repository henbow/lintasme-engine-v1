<?php
/**
 * RSS Scanner for non publisher domains
 * @author Hendro (hendro@lintas.me)
 */
ini_set('display_errors', 1);
ini_set("memory_limit","520M");
error_reporting(E_ALL);
date_default_timezone_set('Asia/Jakarta');

require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/DBActiveRecord.php';
require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/poster.class.php';
require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/function.php';
 
$db  = new DB_active_record;
$poster = new Poster;
$saveRss = FALSE;
$hourVal = array(
				'hour' => 1,
				'day' => 24,
				'week' => 168
			);

// ===========================================
// Start RSS scanner
// ===========================================
echo "Start RSS Scanner ";
while(TRUE) {
	$delay_setting = $db->get('engine_settings', array('alias' => 'rss_scan_delay'))->row_array();
	$delay = isset($delay_setting['value']) ? intval($delay_setting['value']) * 60 : 600;
	$rssnum_setting = $db->get('engine_settings', array('alias' => 'rss_scan_num'))->row_array();
	$rssNum = isset($rssnum_setting['value']) ? intval($rssnum_setting['value']) : 10;
	$tagAPI = "http://engine.lintas.me/?c=autotag&m=process";
	$filterAPI = "http://engine.lintas.me/filter/classify/?train=true";
	$start  = microtime(TRUE);
	$num = 1;
	
	$where = array('dl_status' => 'ACTIVE');
		
	if(isset($argv[1])) {
            if($argv[1] != '') {
                for($i = 1; $i < count($argv); $i++) {
                    $param = explode('=', $argv[$i]);

                    switch ($param[0]) {
                        case 'id': 
                            $where += array('dl_ID' => $param[1]);
                            echo "=> domain ID = ".$param[1];
                            break;

                        case 'channel':
                            $where += array('dl_category_name' => $param[1]);
                            echo "=> Channel = ".$param[1];
                            break;

                        case 'channel-only':
                            $where += array('dl_category_name' => $param[1], 'dl_topic' => '');
                            echo "=> Channel Only = ".$param[1];
                            break;

                        case 'subchannel':
                            $where += array('dl_topic' => $param[1]);
                            echo "=> Sub Channel = ".$param[1];
                            break;

                        default:
                            // $where += array('crawl_group' => '1');
                            break;
                    }
                }	
            }
	}
	
	echo PHP_EOL;
	
	// Find domains that has status ACTIVE
	$pubs = $db->get('engine_domain_rss_list', $where)->result_array();
	
	// Loop them...
	foreach ($pubs as $pub) {
		$url = trim($pub['dl_rss_url'], '/ ');
		
		echo PHP_EOL.PHP_EOL.$num." - Scan RSS: ".$url;
		
		$xml = @simplexml_load_string(file_get_contents("http://".$url));
		
		if($xml === FALSE) {
			$num++;
			
			echo PHP_EOL."RSS feed is not valid.";
			continue;
		}
				
		// Get last-scanned date for current rss
		$last_scanned = strtotime($pub['dl_last_scanned']);
		$update_type = $pub['dl_update_type'] != '' ? $pub['dl_update_type'] : 'hour';
				
		// Get delay for current rss
		$rss_scan_delay = intval($pub['dl_update_every']) * $hourVal[$update_type] * 60 * 60;
		
		echo PHP_EOL."Scan interval: {$pub['dl_update_every']} {$update_type} or {$rss_scan_delay} seconds";
		echo PHP_EOL."Last scan: {$pub['dl_last_scanned']} (".strtotime($pub['dl_last_scanned']).")";
		echo PHP_EOL."Next scan: ".date('Y-m-d H:i:s', $last_scanned + $rss_scan_delay). "(".($last_scanned + $rss_scan_delay).")";
		echo PHP_EOL."Now: ".date('Y-m-d H:i:s', time())." (".time().")";
		
		// If last-scanned date + scan delay less than current time AND if scan delay is not zero, skip to next rss item or do otherwise.
		if(($last_scanned + $rss_scan_delay) > time() && $rss_scan_delay != 0) {
			echo PHP_EOL."SKIPPED TO NEXT RSS";
			$num++;
			continue;
		}
		
		$tag_param = array(
						'title' => $pub['dl_title_pattern'],
						'description' => $pub['dl_desc_pattern'],
						'image' => $pub['dl_img_pattern']
					);
		
		$arrayrss = rss2array($url, $pub['dl_crawl_type'], $tag_param, $rssNum);
		$result = isset($arrayrss['item']) ? $arrayrss['item'] : array();
		$limit = count($result) < $rssNum ? count($result) : $rssNum; 
		
		if(count($result) > 0) {
			for($i = 0; $i < $limit; $i++) {
				if(strlen($result[$i]['title']) > 3){
					$origLink = strpos($result[$i]['link'], 'feedproxy.') !== FALSE ||
							   strpos($result[$i]['link'], 'feeds.') !== FALSE || 
							   strpos($result[$i]['link'], 'feeds09.') !== FALSE || 
							   strpos($result[$i]['link'], 'feedsportal.') !== FALSE ? 
							   get_final_url($result[$i]['link']) : $result[$i]['link'];
					$origLink = strpos($origLink, 'bidanku.com') === FALSE ? preg_replace('/\?.*/', '', $origLink) : $origLink;
					
					$sql = "SELECT * FROM engine_social_effect WHERE se_origlink = '{$origLink}' AND se_latest = '1'";
										
					$url_exist = $db->reset_all()->query($sql);
					$url_count = $url_exist === FALSE ? 0 : $url_exist->num_rows;
					
					echo PHP_EOL."--- Crawl URL: ".$origLink. ' => ';
					
					if($url_count > 0) {
						echo "ALREADY EXISTS";
						
						$feed_data = $url_exist->row_array();
						$post_id = $feed_data['se_post_ID'];
						$img_url = $feed_data['se_image'];
						$status = $feed_data['se_status'];
						
						if($status == 'OK' || $status == 'DUPLICATE' || $status == 'PENDING') {
							if($img_url == '') {
								if($pub['dl_crawl_type'] == '0' || $pub['dl_crawl_type'] == '1') {
									continue;
								}
								
								echo PHP_EOL;
								echo "Post ID: ";
								
								$pattern = str_replace('property="og:', 'property="og_', $pub['dl_img_pattern']);
								$html = str_replace('property="og:','property="og_',file_get_contents($feed_data['se_origlink']));
								$dom  = new DOMDocument();
								@$dom->loadHTML($html);
								
								$xpath = new DOMXPath($dom);
								$imgurl = (string) $xpath->evaluate('string(' . $pattern . ')');
								
							    if(strpos($imgurl, 'http') === FALSE && $imgurl != '') {
									$parse = parse_url($feed_data['se_link']);
									$imgurl = "http://".$parse['host']."/".str_replace(' ', '%20', $imgurl);
								}
								
								$post_ID = $feed_data['se_post_ID'] == '' ? $poster->checkPost($feed_data['se_origlink']) : $feed_data['se_post_ID'];			
								$status = $imgurl == '' ? "NO_IMAGE_FOUND" : "OK";
								
								echo $post_ID;
								echo PHP_EOL;
								echo "Pattern: ".$pattern;
								echo PHP_EOL;
								echo "Orig Image URL: ".$imgurl;
								echo PHP_EOL;
								
								if($imgurl != '' && $post_ID != '') {				
									$updatePostImage = json_decode($poster->updatePostImage($post_ID, $imgurl), TRUE);			
									
									if(isset($updatePostImage['result']['original'])) {
										$updateData = array('se_post_ID' => $post_ID, 'se_image' => $imgurl, 'se_status' => $status);
										$updateRSS = $db->update_data('engine_social_effect', $updateData, array('se_ID' => $feed_data['se_ID']));
										
										if($updateRSS) {
											echo (isset($updatePostImage['result']['original']) ? "[OK]" : '[FAILED]').PHP_EOL;
											echo "Status: ".(isset($updatePostImage['status']) ? $updatePostImage['status'] : '').PHP_EOL;
											echo "Image: ".(isset($updatePostImage['result']['original']) ? $updatePostImage['result']['original'] : '').PHP_EOL;
										}
									}	
								}
							}
						}
						
						continue;
					}
					
					$content = str_replace(array('[unable to retrieve full-text content]', "\n", "\t"), '', preg_replace(array('/[^a-zA-Z0-9]/', '/\-\[\]\(\)\|\{\}\: \s\*/'), array(' ', ' '), trim(strip_tags($result[$i]['description']))));
					
					// Autotagging
					$fields = array('content' => $content);
					$fields_string = '';
					
					foreach($fields as $key => $value) {
						$fields_string .= $key.'='.$value.'&';
					}
					
					rtrim($fields_string, '&');
					
					$ch = curl_init();
					
					curl_setopt($ch,CURLOPT_URL, $tagAPI);
					curl_setopt($ch,CURLOPT_POST, count($fields));
					curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
					curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);
					 
					$tag_results = json_decode(curl_exec($ch), TRUE);
					
					curl_close($ch);
	
					$tags = array();
					if(count($tag_results) > 0) {
						foreach($tag_results as $word => $count) {
                            if($word != "android"){
                                $tags[] = $word;
                            }
						}
					}
					
					// Filtering
					$fields = array('text_data' => $content);
					$fields_string = '';
					
					foreach($fields as $key => $value) {
						$fields_string .= $key.'='.$value.'&';
					}
					
					rtrim($fields_string, '&');
					
					$fch = curl_init();
					
					curl_setopt($fch,CURLOPT_URL, $filterAPI);
					curl_setopt($fch,CURLOPT_POST, count($fields));
					curl_setopt($fch,CURLOPT_POSTFIELDS, $fields_string);
					curl_setopt($fch,CURLOPT_RETURNTRANSFER, TRUE);
					 
					$filter_result = json_decode(curl_exec($fch), TRUE);
					$filter_result = $filter_result['result'];
					
					curl_close($fch);
	
					$filtered_as = "";
					if(count($filter_result) > 0) {
						foreach ($filter_result as $key => $value) {
							$filtered_as = $key.":".$value;
						}
					} 
							
					if($url_count == 0) {
                        $expurl = explode('/', $origLink);
                        $source = str_replace("www.","",$expurl[2]);

				        $data = array (
				        	'se_domain_ID' => $pub['dl_ID'],
				        	'se_category' => $pub['dl_category'],
				        	'se_topic' => $pub['dl_topic'] == '' ? $pub['dl_category_name'] : $pub['dl_topic'],
							'se_title' => $result[$i]['title'],
				        	'se_image' => $result[$i]['image'],
							'se_description' => str_replace(array('[unable to retrieve full-text content]', "\n", "\t"), '', trim(strip_tags($result[$i]['description']))),
							'se_description_html' => str_replace('[unable to retrieve full-text content]', '', trim($result[$i]['description_html'])),
				        	'se_tags' => trim(implode(',', $tags).','.($pub['dl_topic'] == '' ? $pub['dl_category_name'] : $pub['dl_category_name'].','.$pub['dl_topic']), ','),
							'se_link' => $result[$i]['link'],
							'se_guid' => $result[$i]['guid'],
							'se_origlink' => $origLink,
							'se_author' => isset($result[$i]['dc_creator']) ? $result[$i]['dc_creator'] : '',
							'se_format' => isset($result[$i]['dc_format']) ? $result[$i]['dc_format'] : '',
				        	'se_lang' => isset($result[$i]['dc_language']) ? $result[$i]['dc_language'] : '',
							'se_pubdate' => isset($result[$i]['pubDate']) ? date('Y-m-d H:i:s', strtotime($result[$i]['pubDate'])) : date('Y-m-d H:i:s'),
							'se_insert_date' => date('Y-m-d H:i:s'),
							'se_latest' => '1',
							'se_filtered_as' => $filtered_as,
				        	'se_status' => 'PENDING',
                            'se_source' => $source
						);
				        				        
				        $saveRss = $db->insert_data('engine_social_effect', $data);	
				        
				        if($saveRss) echo "OK";
						else echo "FAILED";
					} 
				}
			}

		} else {
			echo PHP_EOL."--- Unable to retrieve RSS feeds.".PHP_EOL;
		}

		$arrayrss = null;
		$result = null;	
		$limit  = null;
		
		$setLastScan = $db->update_data('engine_domain_rss_list', array('dl_last_scanned' => date('Y-m-d H:i:s')), array('dl_ID' => $pub['dl_ID']));
		
		$num++;
		
		flush();
	}
	
	$end = microtime(TRUE);
	$execTime = $end - $start;
	$delay = round($delay / 60);
	
	echo ($saveRss) ? PHP_EOL.'Success save all RSSs to database'.PHP_EOL : '';
	echo PHP_EOL . 	'Execution Time: ' . ($execTime/60) . ' minutes' . PHP_EOL;	
	echo "Delay for {$delay} minutes.".PHP_EOL;
	
	sleep($delay * 60);
	flush();
}

/**
 * End of file
 */