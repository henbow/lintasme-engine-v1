<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('Asia/Jakarta');

require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/DBActiveRecord.php';
require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/poster.class.php';

$db  = new DB_active_record;
$poster = new Poster;
$set_popular = FALSE;

echo "Start Set Popular Engine ".PHP_EOL;

while (TRUE) {
	$domains = $db->get('engine_domain_rss_list', array('dl_status' => 'ACTIVE', 'dl_white_list' => '1'))->result_array();
	$expired_in = $db->get('engine_settings', array('alias' => 'article_expired_in'))->row_array();	
	$delay_setting = $db->get('engine_settings', array('alias' => 'home_popular_delay'))->row_array();
	$homepop_setting = $db->get('engine_settings', array('alias' => 'post_home_popular'))->row_array();
	$start_homepop_setting = $db->get('engine_settings', array('alias' => 'set_home_pop_start'))->row_array();
	$end_homepop_setting = $db->get('engine_settings', array('alias' => 'set_home_pop_end'))->row_array();
	$now = date('Y-m-d');
	$set_popular_trigger = FALSE;
        
        //print_r($domains);
	
	list($year, $month, $day) = explode('-', $now);
	
	if($homepop_setting['value'] == '1') {
		$starttime = strtotime(date('Y-m-d') . " " . $start_homepop_setting['value'] . ":0:0");
		$endtime   = strtotime(date("Y-m-d", time() + ( 60 * 60 * 24 * 1 )) . " " . $end_homepop_setting['value'] . ":0:0");
		
		if(time() >= $starttime && time() <= $endtime) {
			$set_popular_trigger = TRUE;
		}
	}
        	
	if(count($domains) > 0 && $set_popular_trigger === TRUE) {
		foreach ($domains as $dom) {
			$rss_items = $db->reset_all()
                                        ->query("SELECT * FROM engine_social_effect 
                                                WHERE se_domain_ID = '{$dom['dl_ID']}' AND se_post_ID <> '' AND se_home_popular <> '1'
                                                AND DATE(se_insert_date) BETWEEN '".date('Y-m-d', strtotime($year . "-" . $month . "-" . ( $day - intval($expired_in['value']) )))."' AND '{$now}' 
                                                LIMIT 1")->result_array();
                        
			if(count($rss_items) > 0) {
				foreach($rss_items as $rss) {
					if(strlen($rss['se_origlink']) > 3){
						$set_popular = json_decode($poster->set_popular($rss['se_post_ID'],'home'), TRUE);
						$RSSData = array();
						$set_popular['error'] = '';
						
						if($set_popular['error'] == '') {
							echo PHP_EOL;
							echo "Title: ".$rss['se_title'].PHP_EOL;
							echo "Lintas URL: http://lintas.me/article/".$rss['se_post_ID'].PHP_EOL;
							echo "Orig URL: ".$rss['se_origlink'].PHP_EOL;
							echo "Date: ".date('Y-m-d H:i:s').PHP_EOL;
							echo "Posted to Home Popular ".PHP_EOL;
							
							$RSSData['se_home_popular'] = '1';
							$RSSData['se_homepop_date'] = date('Y-m-d H:i:s');
						}
					
						$updateRSS = $db->update_data('engine_social_effect', $RSSData, array('se_ID' => $rss['se_ID']));
						
						echo "Delay for " . intval($delay_setting['value']) . " minutes" . PHP_EOL;
						echo PHP_EOL;
						
						sleep(intval($delay_setting['value']) * 60);
					}
				}
			}
		}
	} 
	
	sleep(10);
	flush();
}