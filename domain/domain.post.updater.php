<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('Asia/Jakarta');

require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/DBActiveRecord.php';
require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/poster.class.php';

$db  = new DB_active_record;
$poster = new Poster;

echo "Start Post Updater ".PHP_EOL;

while(TRUE) {	
	echo "=========================================================================".PHP_EOL;
	echo "Start Date : " . date('Y-m-d H:i:s').PHP_EOL;
	echo "=========================================================================".PHP_EOL;
	
	$exp = 7;
	$startdate = date("Y-m-d", time() - ( 60 * 60 * 24 * $exp ));
	$enddate = date("Y-m-d", time() - ( 60 * 60 * 24 * 0 ));
	
	switch ($argv[1]) {
		case 'TITLE':
			$updatePostTitle = FALSE;
			
			echo PHP_EOL;
			echo "Update Title";
			echo PHP_EOL;
			
			$rss_items = $db->reset_all()
						 ->query("SELECT * FROM engine_social_effect WHERE se_post_ID <> '' AND se_title LIKE '%&quot;%' AND
								  DATE(se_insert_date) BETWEEN '{$startdate}' AND '{$enddate}' ORDER BY se_insert_date DESC")
						 ->result_array();
			$curr_num = 1;
			
			if(count($rss_items) > 0) {
				foreach ($rss_items as $rss) {
					echo PHP_EOL;
					echo "Update title from http://www.lintas.me/article/";
					
					$post_ID = $rss['se_post_ID'] == '' ? $poster->checkPost($rss['se_origlink']) : $rss['se_post_ID'];
					$old_title = $rss['se_title'];
					$new_title = html_entity_decode($rss['se_title']);
					
					echo $post_ID;
					echo PHP_EOL;
					echo "Old Title: ".$old_title;
					echo PHP_EOL;
					echo "New Title: ".$new_title;
					echo PHP_EOL;
					echo "Post Date: ".$rss['se_posted_date'];
					echo PHP_EOL;
					
					$updatePostTitle = json_decode($poster->updatePostTitle($post_ID, $new_title), TRUE);
					
					print_r($updatePostTitle);
					
					if($updatePostTitle['status'] == '200') {
						$updateData = array(
							'se_post_ID' => $post_ID, 
							'se_title' => $new_title
						);
						$updateRSS = $db->update_data('engine_social_effect', $updateData, array('se_ID' => $rss['se_ID']));
						
						if($updateRSS) {
							echo (isset($updatePostTitle['status'] ) ? "[OK]" : '[FAILED]').PHP_EOL;
						}
					}	
					
					usleep(300000);
					flush();
				}
			}
			break;
			
		default:
		case 'IMAGE':
			$updatePostImage = FALSE;
			
			$rss_items = $db->reset_all()
						 ->query("SELECT * FROM engine_social_effect WHERE se_post_ID <> '' AND se_image = '' AND
								  DATE(se_insert_date) BETWEEN '{$startdate}' AND '{$enddate}' ORDER BY se_insert_date DESC")
						 ->result_array();
			$curr_num = 1;
			
			if(count($rss_items) > 0) {
				foreach ($rss_items as $rss) {
					$domain = $db->reset_all()->query("SELECT * FROM engine_domain_rss_list WHERE dl_ID = {$rss['se_domain_ID']} AND dl_status = 'ACTIVE'")->row_array();
					
					if($domain['dl_crawl_type'] == '0' || $domain['dl_crawl_type'] == '1') {
						continue;
					}
					
					echo PHP_EOL.$rss['se_origlink'].PHP_EOL;
					echo "http://www.lintas.me/article/";
					
					$pattern = str_replace('property="og:', 'property="og_', $domain['dl_img_pattern']);
					$html = str_replace('property="og:','property="og_',file_get_contents($rss['se_origlink']));
					$dom  = new DOMDocument();
					@$dom->loadHTML($html);
					
					$xpath = new DOMXPath($dom);
					$imgurl = (string) $xpath->evaluate('string(' . $pattern . ')');
					
				    if(strpos($imgurl, 'http') === FALSE && $imgurl != '') {
						$parse = parse_url($rss['se_link']);
						$imgurl = "http://".$parse['host']."/".str_replace(' ', '%20', $imgurl);
					}
					
					$post_ID = $rss['se_post_ID'] == '' ? $poster->checkPost($rss['se_origlink']) : $rss['se_post_ID'];
					$status = $imgurl == '' ? "NO_IMAGE_FOUND" : "OK";
					
					echo $post_ID." use pattern ".$pattern." => ";
					echo $imgurl . PHP_EOL;
					
					if($imgurl != '' && $post_ID != '') {				
						$updatePostImage = json_decode($poster->updatePostImage($post_ID, $imgurl), TRUE);
						
						if(isset($updatePostImage['result']['original'])) {
							$updateData = array(
								'se_post_ID' => $post_ID, 
								'se_image' => $imgurl, 
								'se_status' => $status
							);
							$updateRSS = $db->update_data('engine_social_effect', $updateData, array('se_ID' => $rss['se_ID']));
							
							if($updateRSS) {
								echo (isset($updatePostImage['result']['original']) ? "[OK]" : '[FAILED]').PHP_EOL;
								echo "Status: ".(isset($updatePostImage['status']) ? $updatePostImage['status'] : '').PHP_EOL;
								echo "Image: ".(isset($updatePostImage['result']['original']) ? $updatePostImage['result']['original'] : '').PHP_EOL;
							}
						}	
					}
					
					sleep(1);
					flush();
				}
			}
			break;
	}	
	
	echo "==========================================================================================================================".PHP_EOL;
	echo PHP_EOL;
	
	sleep(300);	
	flush();
}
/**
 * End of file
 */