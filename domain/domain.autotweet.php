<?php
ini_set('display_errors', 0);
error_reporting(0);
date_default_timezone_set('Asia/Jakarta');

require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/DBActiveRecord.php';
require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/poster.class.php';

$db = new DB_active_record;
$poster = new Poster;
$postToLM = FALSE;

while(TRUE) {
	echo PHP_EOL;
	echo "====================================================================================================".PHP_EOL;
	
	$delay_setting = $db->get('engine_settings', array('alias' => 'autotweet_delay'))->row_array();
	$delay = isset($delay_setting['value']) ? intval($delay_setting['value']) * 60 : 600;
	$domains = $db->reset_all()
				->query("SELECT DISTINCT(dl_category) AS dl_category FROM 
						    engine_domain_rss_list
						 WHERE 
							dl_status = 'ACTIVE'
						 AND dl_autotweet = '1'")->result_array();
	
	if(count($domains) > 0) {
		foreach($domains as $dom) {
			$yesterday = date("Y-m-d", time() - ( 60 * 60 * 6 ));
			
			$rss_items = $db->reset_all()
						    ->query("SELECT * FROM 
									    engine_social_effect	
								    JOIN
									   engine_domain_rss_list
									ON dl_ID = se_domain_ID									
									WHERE 
										dl_autotweet = '1' AND
										(se_insert_date BETWEEN '{$yesterday} 00:00:01' AND NOW()) AND 
										se_post_ID <> '' AND
										se_tweeted = '0' AND
										se_category = '{$dom['dl_category']}'
									LIMIT 1")->result_array();
			
			$RSSData = array();
			$isWeekend = FALSE;
			
			if(count($rss_items) > 0) {
				foreach ($rss_items as $rss) {
					$day = date('D');
					$autotweet_setting = $db->reset_all()->query("SELECT * FROM engine_autotweet_setting WHERE category_name='{$rss['dl_category_name']}'")->row_array();
					
					$starttime = strtotime(date('Y-m-d') . " " . $autotweet_setting['start_time'] . ":0:0");
					$endtime   = strtotime(date("Y-m-d", time() + ( 60 * 60 * 24 * 1 )) . " " . $autotweet_setting['end_time'] . ":0:0");
					
					//if(time() >= $starttime && time() <= $endtime) {
						if($autotweet_setting['fullday_tweet'] == '1') {
							$isWeekend = TRUE;
						} else {
							$isWeekend = ( $day == 'Sat' OR $day == 'Sun' ) ? TRUE : FALSE;
						}
						
						$post_tweet = json_decode($poster->post_tweet($rss['se_post_ID'], $isWeekend, intval($autotweet_setting['start_time']), intval($autotweet_setting['end_time'])), TRUE);
						
						if($post_tweet['istweet'] == '1') {
							$RSSData['se_tweeted'] = '1';
							$RSSData['se_tweet_date'] = date('Y-m-d H:i:s');
							
							$updateRSS = $db->update_data('engine_social_effect', $RSSData, array('se_ID' => $rss['se_ID']));
							
							if($updateRSS) {
								echo PHP_EOL;
								echo "Post ID: ".$rss['se_post_ID'].PHP_EOL;
								echo "Title: ".$rss['se_title'].PHP_EOL;
								echo "Topic: ".ucwords($rss['se_topic']).PHP_EOL;
								echo "URL: ".$rss['se_origlink'].PHP_EOL;
								echo "Tweeted: ".($post_tweet['istweet'] == '' ? 'NO' : 'YES').PHP_EOL;
								echo "Weekend: ".($isWeekend == TRUE ? "YES" : "NO").PHP_EOL;
								echo "Insert Time: ".$rss['se_insert_date'].PHP_EOL;
								echo "Time: ".date('Y-m-d H:i:s').PHP_EOL;
								echo PHP_EOL;
							} else {
								echo PHP_EOL;
								echo "No more post left to tweeted.";
								echo PHP_EOL;
							}
						} else {
							$RSSData['se_tweeted'] = '1';
							$RSSData['se_tweet_date'] = date('Y-m-d H:i:s');
							$RSSData['se_status'] = "FAILED_TWEET";
							
							$updateRSS = $db->update_data('engine_social_effect', $RSSData, array('se_ID' => $rss['se_ID']));
							
							echo PHP_EOL;
							echo "Post ID: ".$rss['se_post_ID'].PHP_EOL;
							echo "Title: ".$rss['se_title'].PHP_EOL;
							echo "Topic: ".ucwords($rss['se_topic']).PHP_EOL;
							echo "URL: ".$rss['se_origlink'].PHP_EOL;
							echo "Tweeted: NO".PHP_EOL;
							echo "Time: ".date('Y-m-d H:i:s').PHP_EOL;
							echo PHP_EOL;
						}
					//} else {
					//	echo "Category: ".$rss['dl_category_name'].PHP_EOL;
					//	echo "Start: ". date('Y-m-d H:i:s', $starttime).PHP_EOL;
					//	echo "End: ". date('Y-m-d H:i:s', $endtime).PHP_EOL;
					//	echo PHP_EOL;
					//	continue;
					//}
					
					sleep(30);
					flush();
				}	
			}	
			
			$last_channel = $dom['dl_category'];
		}
	}

	$delay = round($delay / 60);
	
	echo "Delay for {$delay} minutes.".PHP_EOL;
	echo "====================================================================================================".PHP_EOL;
	
	sleep($delay * 60);
	
	flush();
}



/**
 * End of file
 */