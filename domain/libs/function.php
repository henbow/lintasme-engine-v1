<?php
function rss2array($url, $use_reader, $tag_param, $limit = 5) {	
	$json = array();
	$rssAPI = "http://engine.lintas.me/readers/v32/makefulltextfeed.php?url=%s&max=%s&links=preserve&format=json";
	
	switch ($use_reader) {
		case '1':
			//echo PHP_EOL."Use Reader".PHP_EOL;
		
			$result = @json_decode(file_get_contents(sprintf($rssAPI, $url, $limit)), TRUE);
			$json['item'] = array();
			$i = 0;
			
			if(isset($result['rss']['channel']['item'])) {
				foreach ($result['rss']['channel']['item'] as $item) {
				    $title = $item['title'];
				    $description = $item['description'];
					$content = isset($item['content']) ? $item['content'] : "";
				    $link = $item['link'];
				    $guid = $item['guid'];
				    $author = $item['dc_creator'];
					$language = $item['dc_language'];
				    $format = $item['dc_format'];
				    $identifier = $item['dc_identifier'];
					$pubdate = isset($item['pubDate']) ? date('Y-m-d H:i:s', strtotime($item['pubDate'])) : "";
					$origlink = get_final_url($link);
					
					$dom = new DOMDocument();
				    @$dom->loadHTML((string) $description);
					
					$xpath = new DOMXPath($dom);
					$imgurl = $xpath->evaluate("string(//img/@src)");
				   
				    $json['item'][$i]['title'] = $title;
				    $json['item'][$i]['description'] = strip_tags($description);
                    $json['item'][$i]['description_html'] = $description;
				    $json['item'][$i]['link'] = $link;
				    $json['item'][$i]['guid'] = $guid;
					$json['item'][$i]['origlink'] = $origlink;
					$json['item'][$i]['pubdate'] = $pubdate;
					$json['item'][$i]['image'] = $imgurl;
					$json['item'][$i]['author'] = $author;
				    $json['item'][$i]['language'] = $language;
				    $json['item'][$i]['format'] = $format;
				    $json['item'][$i]['identifier'] = $identifier;
					
					$i++;
				}
			}
					
	        $json['use_reader'] = TRUE;
			break;
			
		case '2':
			//echo PHP_EOL."Use Reader and find image in original URL with pattern {$tag_param['image']}".PHP_EOL;
		
			$result = @json_decode(file_get_contents(sprintf($rssAPI, $url, $limit)), TRUE);
			$json['item'] = array();
			$i = 0;
			
			if(isset($result['rss']['channel']['item'])) {
				foreach ($result['rss']['channel']['item'] as $item) {			   
				    $title = $item['title'];
				    $description = $item['description'];
					$content = isset($item['content']) ? $item['content'] : "";
				    $link = $item['link'];
				    $guid = $item['guid'];
				    $author = $item['dc_creator'];
				    $language = $item['dc_language'];
				    $format = $item['dc_format'];
				    $identifier = $item['dc_identifier'];
					$pubdate = isset($item['pubDate']) ? date('Y-m-d H:i:s', strtotime($item['pubDate'])) : "";
					$origlink = get_final_url($link);
					
					$ich = curl_init();
					curl_setopt($ich, CURLOPT_URL, $origlink);
	        		curl_setopt($ich, CURLOPT_RETURNTRANSFER, TRUE);
			        curl_setopt($ich, CURLOPT_CONNECTTIMEOUT ,10); 
				    curl_setopt($ich, CURLOPT_TIMEOUT, 30);	
					$html = curl_exec($ich);
					curl_close($ich);
					
					$dom = new DOMDocument();
				    @$dom->loadHTML($html);
					
					$xpath = new DOMXPath($dom);
					$imgurl = (string) $xpath->evaluate('string('.$tag_param['image'].')');
					
					if(strpos($imgurl, 'http') === FALSE && $imgurl != '') {
						$parse = parse_url($url);
						$imgurl = "http://".$parse['host']."/".str_replace(' ', '%20', $imgurl);
					}
				   
				    $json['item'][$i]['title'] = $title;
				    $json['item'][$i]['description'] = strip_tags($description);
                    $json['item'][$i]['description_html'] = $description;
				    $json['item'][$i]['link'] = $link;
				    $json['item'][$i]['guid'] = $guid;
					$json['item'][$i]['origlink'] = $origlink;
					$json['item'][$i]['pubdate'] = $pubdate;
					$json['item'][$i]['image'] = $imgurl;
					$json['item'][$i]['author'] = $author;
				    $json['item'][$i]['language'] = $language;
				    $json['item'][$i]['format'] = $format;
				    $json['item'][$i]['identifier'] = $identifier;
					
					$i++;
				}
			}
					
	        $json['use_reader'] = TRUE;
			break;
		
		case '3':
			//echo PHP_EOL."Read RSS directly and find image in original URL with pattern {$tag_param['image']}".PHP_EOL;

			$rss = file_get_contents($url);
			$result = str_replace(array('content:', 'media:', 'dc:'), array('content_', 'media_', 'dc_'), $rss);
			$xml = @simplexml_load_string($result);
			
			if($xml !== FALSE) {				
				$titles = $xml->xpath($tag_param['title']);
				$descriptions = $xml->xpath(str_replace(':', '_', $tag_param['description']));
				$links = $xml->xpath('//channel/item/link');
				$guids = $xml->xpath('//channel/item/guid');
				$pubdates = $xml->xpath('//channel/item/pubDate');
				$author = $xml->xpath('//channel/item/dc_creator');
				$language = $xml->xpath('//channel/item/dc_language');
				$format = $xml->xpath('//channel/item/dc_format');
				$identifier = $xml->xpath('//channel/item/dc_identifier');
				$limit = count($titles) < $limit ? count($titles) : $limit;
				
				$json['item'] = array();
				
				for($i = 0; $i < $limit; $i++){				   
				   $origlink = get_final_url((string) $links[$i]);
				   $imgurl = "";
				   
				   $ich = curl_init();
				   curl_setopt($ich, CURLOPT_URL, $origlink);
	        	   curl_setopt($ich, CURLOPT_RETURNTRANSFER, TRUE);
			       curl_setopt($ich, CURLOPT_CONNECTTIMEOUT ,10); 
				   curl_setopt($ich, CURLOPT_TIMEOUT, 30);	
				   $html = curl_exec($ich);
				   curl_close($ich);
				   
				   $dom = new DOMDocument();
				   @$dom->loadHTML($html);
					
				   $xpath = new DOMXPath($dom);
				   $imgurl = (string) $xpath->evaluate('string('.$tag_param['image'].')');
				   
				   //echo "Title: ".((string) $titles[$i]).PHP_EOL;
				   //echo "Image URL: ".$imgurl.PHP_EOL;
				   
				   if(strpos($imgurl, 'http') === FALSE && $imgurl != '') {
						$parse = parse_url($url);
						$imgurl = "http://".$parse['host']."/".str_replace(' ', '%20', $imgurl);
				   }
				   
				   $json['item'][$i]['title'] = (string) $titles[$i];
				   $json['item'][$i]['description'] = (string) strip_tags($descriptions[$i]);
                   $json['item'][$i]['description_html'] = (string) $descriptions[$i];
				   $json['item'][$i]['link'] = (string) $links[$i];
				   $json['item'][$i]['guid'] = count($guids) > 0 ? (string) $guids[$i] : "";
				   $json['item'][$i]['origlink'] = $origlink;
				   $json['item'][$i]['pubdate'] = count($pubdates) > 0 ? date('Y-m-d H:i:s', strtotime((string) $pubdates[$i])) : "";
				   $json['item'][$i]['image'] = $imgurl;
				   $json['item'][$i]['author'] = $author;
				   $json['item'][$i]['language'] = $language;
				   $json['item'][$i]['format'] = $format;
				   $json['item'][$i]['identifier'] = $identifier;
				}
				
				$json['use_reader'] = FALSE;
			}
			break;
		
		case '0':
		default:
			//echo PHP_EOL."Read RSS directly".PHP_EOL;

			$rss = file_get_contents($url);
			$result = str_replace(array('content:', 'media:', 'dc:'), array('content_', 'media_', 'dc_'), $rss);
			$xml = simplexml_load_string($result);
					
			if($xml === FALSE) {				
				echo "RSS feed is not valid.".PHP_EOL;
				foreach(libxml_get_errors() as $error) {
			        echo $error->message;
			    }
				echo PHP_EOL;
			} else {
				$imgparam = $tag_param['image'];
				$attr = FALSE;
				
				if(strpos($imgparam, '@')) {
					$attr = TRUE;
				}
				
				$titles = $xml->xpath($tag_param['title']);
				$descriptions = $xml->xpath(str_replace(':', '_', $tag_param['description']));
				$links = $xml->xpath('//channel/item/link');
				$guids = $xml->xpath('//channel/item/guid');
				$pubdates = $xml->xpath('//channel/item/pubDate');
				$author = $xml->xpath('//channel/item/dc_creator');
				$language = $xml->xpath('//channel/item/dc_language');
				$format = $xml->xpath('//channel/item/dc_format');
				$identifier = $xml->xpath('//channel/item/dc_identifier');
				$images = $xml->xpath(str_replace(':', '_', $imgparam));
				$limit = count($titles) < $limit ? count($titles) : $limit;
				
				// $datas = $xml->xpath('//channel');
				// echo ' ===> //channel/item/'.str_replace(':', '_', $imgparam).PHP_EOL;
				// print_r($images);die;
				
				$json['item'] = array();
				
				for($i = 0; $i < $limit; $i++){
				   if($i == $limit) break;
				   
				   $imgurl = "";
				   if($attr === TRUE) {
					    foreach ($images[$i] as $key => $value) {
							if($key == $attr) $imgurl = (string) $value;
						}
				   } else {
				   	    $dom = new DOMDocument();
					    @$dom->loadHTML((string) $descriptions[$i]);
						
						$xpath = new DOMXPath($dom);
						$imgurl = $xpath->evaluate("string(//img/@src)");
						//print_r($src);
				   }
				   
				   if(strpos($imgurl, 'http') === FALSE && $imgurl != '') {
						$parse = parse_url($url);
						$imgurl = "http://".$parse['host']."/".str_replace(' ', '%20', $imgurl);
				   }
				   
				   //echo "Title: ".((string) $titles[$i]).PHP_EOL;
				   //echo "Image URL: ".$imgurl.PHP_EOL;
				   
				   $json['item'][$i]['title'] = (string) $titles[$i];
				   $json['item'][$i]['description'] = (string) strip_tags($descriptions[$i]);
                   $json['item'][$i]['description_html'] = (string) $descriptions[$i];
				   $json['item'][$i]['link'] = (string) $links[$i];
				   $json['item'][$i]['guid'] = count($guids) > 0 ? (string) $guids[$i] : "";
				   $json['item'][$i]['origlink'] = preg_replace('/\?.*/', '', get_final_url($json['item'][$i]['link']));
				   $json['item'][$i]['pubdate'] = count($pubdates) > 0 ? date('Y-m-d H:i:s', strtotime((string) $pubdates[$i])) : "";
				   $json['item'][$i]['image'] = $imgurl;
				   $json['item'][$i]['author'] = $author;
				   $json['item'][$i]['language'] = $language;
				   $json['item'][$i]['format'] = $format;
				   $json['item'][$i]['identifier'] = $identifier;
				}
				
				$json['use_reader'] = FALSE;
			}
			break;
	}
	
	return $json;
}

function get_tweets($url) {
	$json_string = @file_get_contents('http://urls.api.twitter.com/1/urls/count.json?url=' . $url);
	$json = json_decode($json_string, true);
	return intval( $json['count'] );
}

function get_redirect_url($url){
    $redirect_url = null; 

    $url_parts = @parse_url($url);
    if (!$url_parts) return false;
    if (!isset($url_parts['host'])) return false; //can't process relative URLs
    if (!isset($url_parts['path'])) $url_parts['path'] = '/';

    $sock = @fsockopen($url_parts['host'], (isset($url_parts['port']) ? (int)$url_parts['port'] : 80), $errno, $errstr, 30);
    if (!$sock) return false;

    $request = "HEAD " . $url_parts['path'] . (isset($url_parts['query']) ? '?'.$url_parts['query'] : '') . " HTTP/1.1\r\n"; 
    $request .= 'Host: ' . $url_parts['host'] . "\r\n"; 
    $request .= "Connection: Close\r\n\r\n"; 
    @fwrite($sock, $request);
    $response = '';
    while(!feof($sock)) $response .= @fread($sock, 8192);
    fclose($sock);

    if (preg_match('/^Location: (.+?)$/m', $response, $matches)){
        if ( substr($matches[1], 0, 1) == "/" )
            return $url_parts['scheme'] . "://" . $url_parts['host'] . trim($matches[1]);
        else
            return trim($matches[1]);

    } else {
        return false;
    }

}

/**
 * get_all_redirects()
 * Follows and collects all redirects, in order, for the given URL. 
 *
 * @param string $url
 * @return array
 */
function get_all_redirects($url){
    $redirects = array();
    while ($newurl = get_redirect_url($url)){
        if (in_array($newurl, $redirects)){
            break;
        }
        $redirects[] = $newurl;
        $url = $newurl;
    }
    return $redirects;
}

/**
 * get_final_url()
 * Gets the address that the URL ultimately leads to. 
 * Returns $url itself if it isn't a redirect.
 *
 * @param string $url
 * @return string
 */
function get_final_url($url){
    $redirects = get_all_redirects($url);
    if (count($redirects)>0){
        return array_pop($redirects);
    } else {
        return $url;
    }
}

function get_final_location($url)
{
	$headers = @get_headers($url);
	$pattern = '/Location\s*:\s*(https?:[^;\s\n\r]+)/i';

	if ($locations = preg_grep($pattern, $headers))
	{
		preg_match($pattern, end($locations), $redirect);
		return $redirect[1];
	}

	return $url;
}

/**
 * End of file
 * 
 */