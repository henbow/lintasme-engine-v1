<?php
//require_once(dirname(__FILE__).DIRECTORY_SEPARATOR."HumbleHttpAgent.php");
//require_once(dirname(__FILE__).DIRECTORY_SEPARATOR."Readability.php");
//require_once(dirname(__FILE__).DIRECTORY_SEPARATOR."simple_html_dom.php");

class Poster
{
	
	public $extractorUrl = "http://engine.lintas.me/?c=crawler&m=process&url=";
	public $newPostUrl = "http://devel.lintas.me/rest/newPost";
	public $maxContentChars = 500;
	
	function getCrawlBoilerpipe($url)
	{
		$ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, $this->extractorUrl . $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);

        $result = curl_exec($ch);
		
        curl_close($ch);
		
		return json_decode($result);
	}

	function getRandomUser()
	{
        //$get = file_get_contents("http://www.lintas.me/rest/getRandomUser");
        //$rt = json_decode($get);
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://www.lintas.me/rest/getRandomUser'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);

        $rt = json_decode($resp);
        
        return $rt;
	}

    function getRandomUserSingle()
    {
        $ret = $this->getRandomUser();
        return $ret[0]->id;
    }

    function newPost($params)
    {
    	$this->posting_start = time();

    	$fields = array(
                    'is_backecd' => 1,
                    'origin_id' => $params['category'],
                    'url' => $params['url'],
                    'post_title' => $params['title'],
                    'post_content' => substr((trim($params['description'])), 0, $this->maxContentChars).'...',
                    'post_img_url' => $params['image'],
                    'post_tag_no_auto' => $params['tags'],
                    'kind' => 'article',
                    'backecd_user' => '51763a7d1b971a5c2000038f'
                );

        $post_img = json_decode(file_get_contents('http://di.lintas.me/upload?url='.$params['image']));

        $fields['local_thumbnail_url'] = $post_img->result->original;
        $fields['local_thumbnail_info'] = @ (array) $post_img->info;

        
        if(!empty($params['post_as'])) {
            $fields['rand_user'] = $params['post_as'];
        } else {
            $fields['rand_user'] = $this->getRandomUserSingle();
        }
        
        print_r($fields);;
        
        $fields_string = '';
        
        //url-ify the data for the POST
        foreach($fields as $key => $value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');
		
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $this->newPostUrl);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		    curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);
         
        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);

        $this->posting_end = time();
        $this->posting_duration = $this->posting_end - $this->posting_start;
		
		//print $url.PHP_EOL;
		//print_r($result);
		
        if($result == "Disallowed Key Characters.") {
            $res['error'] = TRUE;
            $res['error_msg'] = $result;
            return $res;
        } else {
            $res = json_decode($result);
            if(!empty($params['set_popular'])){
             	echo "set_popular=".$params['set_popular']." ";
            }
            
            return $res;
        }
    }
    
    function set_popular($post_id, $type = 'channel', $is_weekend = '', $min = '', $max = '')
    {
      	//set POST variables
      	$url = 'http://www.lintas.me/rest/setPopular';
		
		if($is_weekend == '' && $min == '' & $max == '') {
			$fields = array(
	                    'backend_id' => '51763a7d1b971a5c2000038f',
	                    'post_id' => $post_id,
	                    'set_popular' => $type
	                );
		} else { 
	      	$fields = array(
	                    'backend_id' => '51763a7d1b971a5c2000038f',
	                    'post_id' => $post_id,
	                    'set_popular' => $type,
	                    'isweekend' => $is_weekend,
	                    'hour_min' => $min,
	                    'hour_max' => $max
	                );
		}
		
      	$fields_string = '';
      	//url-ify the data for the POST
      	foreach($fields as $key => $value) { $fields_string .= $key.'='.$value.'&'; }
      	rtrim($fields_string, '&');

      	//open connection
      	$ch = curl_init();

      	//set the url, number of POST vars, POST data
      	curl_setopt($ch,CURLOPT_URL, $url);
      	curl_setopt($ch,CURLOPT_POST, count($fields));
      	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
      	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

      	// debug post data
      	
      	//echo "\n";
      	//echo "Posting New Post : \n";
      	//echo "Field String : ".$fields_string;
      	//echo "\n";
       
      	//execute post
      	$result = curl_exec($ch);

      	//close connection
      	curl_close($ch);
		
      	return $result;
    }

	function post_tweet($post_id, $is_weekend, $min, $max)
    {
      	//set POST variables
      	$url = 'http://www.lintas.me/rest/setPopularCli';
      	$fields = array(
                    'backend_id' => '51763a7d1b971a5c2000038f',
                    'post_id' => $post_id,
                    'isweekend' => $is_weekend,
                    'hour_min' => $min,
                    'hour_max' => $max
                );
		
      	$fields_string = '';
      	//url-ify the data for the POST
      	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
      	rtrim($fields_string, '&');

      	//open connection
      	$ch = curl_init();

      	//set the url, number of POST vars, POST data
      	curl_setopt($ch,CURLOPT_URL, $url);
      	curl_setopt($ch,CURLOPT_POST, count($fields));
      	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
      	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

      	// debug post data
      	
      	//echo "\n";
      	//echo "Posting New Post : \n";
      	//echo "Field String : ".$fields_string;
      	//echo "\n";
       
      	//execute post
      	$result = curl_exec($ch);

      	//close connection
      	curl_close($ch);
		
      	return $result;
    }

	function updatePostImage($post_id, $img_url) {
		$url = 'http://www.lintas.me/rest/updatePost';
      	$fields = array(
                    'post_id' => $post_id,
                    'img_url' => $img_url
                );
		
      	$fields_string = '';
      	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
      	rtrim($fields_string, '&');

      	$ch = curl_init();

      	curl_setopt($ch,CURLOPT_URL, $url);
      	curl_setopt($ch,CURLOPT_POST, count($fields));
      	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
      	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

      	$result = curl_exec($ch);

      	curl_close($ch);
		 
      	return $result;
	}
	
	function updatePostTitle($post_id, $title) {
		$url = 'http://www.lintas.me/rest/updatePost';
      	$fields = array (
                    'post_id' => $post_id,
                    'post_title' => $title
                  );
		
      	$fields_string = '';
      	foreach($fields as $key => $value) { $fields_string .= $key.'='.$value.'&'; }
      	rtrim($fields_string, '&');

      	$ch = curl_init();

      	curl_setopt($ch,CURLOPT_URL, $url);
      	curl_setopt($ch,CURLOPT_POST, count($fields));
      	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
      	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

      	$result = curl_exec($ch);

      	curl_close($ch);
		
      	return $result;
	}

    function checkPost($urltarget)
    {
        //set POST variables
        $url = 'http://www.lintas.me/rest/checkPost';
        $fields = array(
                    'url' => $urltarget
                );
        $fields_string = '';
        //url-ify the data for the POST
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);
        
        return $result;
    }
    
	function getPostDetail($pid) 
	{
		//set POST variables
        $url = 'http://www.lintas.me/rest/getsinglestream/&pid='.$pid;
        $fields = array( 'url' => $url );

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);
        
        return $result;
	}
    
}

/**
 * End of file
 */