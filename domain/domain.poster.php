<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('Asia/Jakarta');

require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/DBActiveRecord.php';
require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/poster.class.php';

$db  = new DB_active_record;
$poster = new Poster;
$postToLM = FALSE;

echo "Start Poster ".PHP_EOL;

while(TRUE) {
	echo "=========================================================================".PHP_EOL;
	echo "Start Date : " . date('Y-m-d H:i:s').PHP_EOL;
	echo "=========================================================================".PHP_EOL;
	
	$delay_setting = $db->get('engine_settings', array('alias' => 'poster_delay'))->row_array();
	$delay = isset($delay_setting['value']) ? intval($delay_setting['value']) * 60 : 600;
	$quantity_post_setting = $db->get('engine_settings', array('alias' => 'quantity_per_post'))->row_array();
	$quantity_post = isset($quantity_post_setting['value']) ? intval($quantity_post_setting['value']) : 1;
	$start = microtime(TRUE);
	$expired_in = $db->get('engine_settings', array('alias' => 'article_expired_in'))->row_array();	
	$now = date('Y-m-d');
	
	list($year, $month, $day) = explode('-', $now);
	
	$where = array('dl_status' => 'ACTIVE');
	
	if(isset($argv[1])) {
		if($argv[1] != '') {
			for($i = 1; $i < count($argv); $i++) {
				$param = explode('=', $argv[$i]);
				
				switch ($param[0]) {
					case 'id':
						$where += array('dl_ID' => $param[1]);
						break;
					
					case 'channel':
						$where += array('dl_category_name' => $param[1]);
						break;
						
					case 'channel-only':
						$where += array('dl_category_name' => $param[1], 'dl_topic' => '');
						break;
						
					case 'subchannel':
						$where += array('dl_topic' => $param[1]);
						break;
					
					default:
						// $where += array('crawl_group' => '1');
						break;
				}
			}	
		}
	}
	
	$domains = $db->reset_all()->get('engine_domain_rss_list', $where)->result_array();
	$curr_num = 1;
	
	if(count($domains) > 0) {
		foreach($domains as $dom) {
			echo PHP_EOL . $curr_num;
			
			$exec_query = $db->reset_all()
						    ->query("SELECT engine_social_effect.* FROM engine_social_effect 
									  WHERE se_domain_ID = '{$dom['dl_ID']}' AND se_status <> 'DUPLICATE' 
									  AND se_status <> 'OK' AND se_status <> 'FAILED' 
									  AND DATE(se_insert_date) BETWEEN '".date('Y-m-d', strtotime($year . "-" . $month . "-" . ( $day - intval($expired_in['value']) )))."' 
									  AND '{$now}' LIMIT {$quantity_post}");
			
			if($exec_query !== FALSE) {
				if($exec_query->num_rows > 0){
					$rss_items = $exec_query->result_array();
					
					foreach ($rss_items as $rss) {
						echo " - " . $dom['dl_domain'] . " => Post ".$rss['se_origlink']." to Lintas.Me => ";
						
						if(strpos($rss['se_origlink'], 'lintas.me') === FALSE) {
							if(strlen($rss['se_origlink']) > 3) {
								$params = array (
											'title' => $rss['se_title'],
											'description' => preg_replace('/[^(\x20-\x7F)]*/','',(str_replace(array('&#13;'), '', $rss['se_description']))),
											'category' => $rss['se_category'],
											'image' => $rss['se_image'],
											'tags' => $rss['se_tags'],
											'url' => $rss['se_origlink'],
										);
								
								$postToLM = $poster->newPost($params);

                                if(isset($postToLM->detail_post->large_images)){
                                    $img = $postToLM->detail_post->large_images;
                                    $img = str_replace("http://i.brta.in/images/", "", $img);
                                    $img = str_replace("big/", "", $img);
                                }

								$RSSData = array(
												'se_latest' => '1',
												'se_post_ID' => isset($postToLM->detail_post->id) ? $postToLM->detail_post->id : '',
												'se_status' => isset($postToLM->status) ? $postToLM->status : 'FAILED',
												'se_posted_date' => isset($postToLM->detail_post->creation_date) ? date('Y-m-d H:i:s', $postToLM->detail_post->creation_date) : date('Y-m-d H:i:s'),
                                                'se_image_local' => isset($postToLM->detail_post->large_images) ? $img : ''
											);
								
								$updateRSS = $db->update_data('engine_social_effect', $RSSData, array('se_ID' => $rss['se_ID']));
								
								$tags = explode(',', $rss['se_tags']);
								if(count($tags) > 0) {
									foreach ($tags as $tag_item) {
										if($RSSData['se_post_ID'] != '') {
											$tag_data = array (
									        	'tag' => $tag_item,
									        	'post_ID' => $RSSData['se_post_ID']
											);
									        
									        $saveTags = $db->insert_data('engine_post_tags', $tag_data);
										}
									}
								}
								
								if($updateRSS) {
									echo isset($postToLM->status) ? "[".$postToLM->status."]" : '[FAILED]';
								}
		
								$message = isset($postToLM->msg->message) ? ' - ' . $postToLM->msg->message : '';
								$log_data = array(
											'log_rss_item_ID' => $rss['se_ID'],
											'log_url' => $rss['se_origlink'],
											'log_error_code' => !isset($postToLM->status) ? $postToLM['error'] : ($postToLM->status == 'ERROR' ? '2' : '0'),
											'log_message' => !isset($postToLM->status) ? 
																$postToLM['error_msg'] : (
																	$postToLM->detail_post == 'DELETED' ? 
																		$postToLM->detail_post . $message : 'Success posted to Lintas.me'
																),
											'log_date' => date('Y-m-d H:i:s')
										);
								
								$db->insert_data('engine_post_log', $log_data);
									
								sleep(1);
							} else {
								echo "[INVALID]";
							}
						}
					}
				} else {
					echo " - {$dom['dl_domain']} => No new RSS entry in \"{$dom['dl_rss_url']}\".";
				}
			} else {
				echo " - {$dom['dl_domain']} => No new RSS entry in \"{$dom['dl_rss_url']}\".";
			}
			
			flush();
			$curr_num++;
		}
	}
	
	$end = microtime(TRUE);
	$execTime = $end - $start;
	$delay = round($delay / 60);
	
	echo PHP_EOL;
	echo PHP_EOL;
	echo "=========================================================================".PHP_EOL;
	echo "End Date : " . date('Y-m-d H:i:s').PHP_EOL;
	echo ($postToLM) ? PHP_EOL.'Success post article to Lintas.Me'.PHP_EOL : '';
	echo PHP_EOL . 	'Execution Time: ' . $execTime . ' seconds' . PHP_EOL;
	echo "Delay for {$delay} minutes.".PHP_EOL;
	echo "=========================================================================".PHP_EOL;
	
	sleep($delay * 60);
	flush();
}
/**
 * End of file
 */