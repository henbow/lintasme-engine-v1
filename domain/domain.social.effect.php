<?php
ini_set('display_errors', 0);
error_reporting(0);
date_default_timezone_set('Asia/Jakarta');

require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/DBActiveRecord.php';
require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/function.php';
require dirname(__FILE__).DIRECTORY_SEPARATOR.'libs/poster.class.php';

$db = new DB_active_record;
$poster = new Poster;
$max_home_popular = 2;

echo "Start Domain Social Effect ".PHP_EOL;
while(TRUE) {
	$expired_setting = $db->get('engine_settings', array('alias' => 'article_expired_in'))->row_array();
	$delay_setting = $db->get('engine_settings', array('alias' => 'social_effect_delay'))->row_array();
	$homepop_setting = $db->get('engine_settings', array('alias' => 'post_home_popular'))->row_array();
	$start_homepop_setting = $db->get('engine_settings', array('alias' => 'set_home_pop_start'))->row_array();
	$end_homepop_setting = $db->get('engine_settings', array('alias' => 'set_home_pop_end'))->row_array();
	$delay = isset($delay_setting['value']) ? intval($delay_setting['value']) * 60 : 600;
	$set_popular_trigger = FALSE;
	
	echo "LintasMe Domain Social Effect is started...".PHP_EOL;
	
	$tweetsCount = array(); 
	$expired_in = $db->get('engine_settings', array('alias' => 'article_expired_in'))->row_array();
	$now = date('Y-m-d');
	$start_date = date("Y-m-d H:i:s", time() - ( 60 * 60 * 24 * intval($expired_in['value']) ));
	$end_date = date("Y-m-d H:i:s", time() - ( 60 * 60 * 24 * 0 ));
	
	list($year, $month, $day) = explode('-', $now);
	
	if($homepop_setting['value'] == '1') {
		$day = date('D');
		$starttime = strtotime(date('Y-m-d') . " " . $start_homepop_setting['value'] . ":0:0");
		$endtime   = strtotime(date("Y-m-d", time() + ( 60 * 60 * 24 * 1 )) . " " . $end_homepop_setting['value'] . ":0:0");
		
		if(time() >= $starttime && time() <= $endtime) {
			$set_popular_trigger = TRUE;
		} else {
			$set_popular_trigger = ( $day == 'Sat' OR $day == 'Sun' ) ? TRUE : FALSE;
		}
	}
	
	$domains = $db->reset_all()->query("
					SELECT * FROM 
						engine_social_effect 
					JOIN 
						engine_domain_rss_list ON dl_ID = se_domain_ID 
					WHERE 
						dl_status = 'ACTIVE' AND dl_social_effect = '1' AND 
						se_home_popular <> '1' AND se_channel_popular <> '1' AND 
						(se_status = 'OK' OR se_status = 'DUPLICATE') AND 
						se_insert_date BETWEEN '$start_date' AND '$end_date' 
			   ")->result_array(); 
	
	echo "Process ".count($domains)." URLs...".PHP_EOL;
	
	if(count($domains) > 0) {
		$channel_name = "";
		$set_popular = 'channel';
		
		foreach ($domains as $dom) {
			if(strlen($dom['se_origlink']) > 3) {
				$s = microtime(TRUE);
				
				echo "Get tweets count from => {$dom['se_origlink']} ".PHP_EOL;
				
				$tweets = get_tweets($dom['se_origlink']);
				$tweetsCount[$dom['se_domain_ID']][$dom['se_ID']] = $tweets;
				$saveSE = $db->update_data('engine_social_effect', array('se_social_effect' => $tweets), array('se_ID' => $dom['se_ID']));
				
				$e = microtime(TRUE);
				
				echo "[".$tweets." TWEETS] - ". (round($e - $s, 2)) . " seconds" . PHP_EOL;
			}
		}
		
		foreach ($tweetsCount as $pub_id => $data) {
			$max = max($data);
			$rssID = array_search($max, $data);
			$article = $db->get('engine_social_effect', array('se_ID' => $rssID))->row_array();
			$domain  = $db->get('engine_domain_rss_list', array('dl_ID' => $pub_id))->row_array();
			$postID = $poster->checkPost($article['se_origlink']);
			//$pdet = json_decode($poster->getPostDetail($postID), TRUE);
			
			if($set_popular_trigger === TRUE && ($domain['dl_social_effect'] == '1' || $domain['dl_white_list'] == '1')) {
				$set_popular = 'home';
			}
			
			if(strlen($postID) > 0) {
				$status = json_decode($poster->set_popular($postID, $set_popular), TRUE);
				
				if($status['error'] == "") {
					$db->update_data('engine_social_effect', array('se_'.$set_popular.'_popular' => '1', 'se_'.$set_popular.'pop_date' => date('Y-m-d H:i:s')), array('se_ID' => $article['se_ID']));
				}
				
				echo PHP_EOL;
				echo "Post ID: " . $postID . PHP_EOL;
				echo "URL: " . $article['se_origlink'] . PHP_EOL;
				echo "Popular: " . ucfirst($set_popular) . PHP_EOL;
				echo "Tweets: " . $max . PHP_EOL;
				echo "Time: " . date('Y-m-d H:i:s') . PHP_EOL;
				echo "Status: " . ($status['error'] == "" ? "OK" : "FAILED" ) . PHP_EOL;
				
				if($set_popular == 'home') {
					echo "Delay for ".($delay/60)." minutes.".PHP_EOL;
					sleep($delay);
				} else sleep(5); 
			}			
		}
	}

	if($set_popular == 'channel') sleep($delay);
	flush();
}

/**
 * End of file
 * 
 */
