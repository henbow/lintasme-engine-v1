<?php
 
class DBConfig
{
    public static $db_host = '192.168.1.3';
    public static $db_user = 'lcm';
    public static $db_pass = 'lcm12345';
    public static $db_name = 'lintasengine';
    public static $db_port = '3306';
    public static $db_pconnect = TRUE;
    
    // Available options is mysqli, mysql. Currently unused.
    public static $db_driver = 'mysqli';
}

/**
 * End of file DBConfig.php
 * Location: ./libs/DBConfig.php
 * 
 */
