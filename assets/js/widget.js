var lintas_widget = function (config){
  
  var w = config.width;
  var layout = config.layout;
  var tit = config.title;
  var vis = config.visible_item * 1;
  var h = 30 + (vis * 82);
  var num = config.content_num;
  var tipe = config.tipe;
  var keyword = config.keyword;
  
  
  if(layout == 'moveup'){
    if(tipe == 'youmightmiss'){
      var strCode = '<iframe id="mightmiss" scrolling="no" style="border: none;width: '+ w +'px;height : '+ h +'px;" src="http://devhome.lintas.me/webplugin/youmightmiss_widget/?title='+ tit +'&limit='+ num +'&visible='+ vis +'&bg1='+ config.themes.head.background.replace("#","%23") +'&bg2='+ config.themes.item.background.replace("#","%23") +'&bg3='+ config.themes.item.background2.replace("#","%23") +'&font1='+ config.themes.head.color.replace("#","%23") +'&font2='+ config.themes.item.color.replace("#","%23") +'&font3='+ config.themes.item.color2.replace("#","%23") +'"></iframe>';
    }
    if(tipe == 'popularnews'){
      var strCode = '<iframe id="mightmiss" scrolling="no" style="border: none;width: '+ w +'px;height : '+ h +'px;" src="http://devhome.lintas.me/webplugin/popularnews_widget/?what='+ keyword +'&title='+ tit +'&limit='+ num +'&visible='+ vis +'&bg1='+ config.themes.head.background.replace("#","%23") +'&bg2='+ config.themes.item.background.replace("#","%23") +'&bg3='+ config.themes.item.background2.replace("#","%23") +'&font1='+ config.themes.head.color.replace("#","%23") +'&font2='+ config.themes.item.color.replace("#","%23") +'&font3='+ config.themes.item.color2.replace("#","%23") +'"></iframe>';
    }
  }
  
  if(layout == 'ticker'){
    if(tipe == 'youmightmiss'){
      var strCode = '<iframe id="mightmiss" scrolling="no" style="border: none;width: '+ w +'px;height : 34px;" src="http://devhome.lintas.me/webplugin/youmightmiss_ticker/?title='+ tit +'&limit='+ num +'&visible='+ vis +'&width='+ w +'"></iframe>';
    }
    if(tipe == 'popularnews'){
      var strCode = '<iframe id="mightmiss" scrolling="no" style="border: none;width: '+ w +'px;height : 34px;" src="http://devhome.lintas.me/webplugin/popularnews_ticker/?what='+ keyword +'&title='+ tit +'&limit='+ num +'&visible='+ vis +'&width='+ w +'"></iframe>';
    }
  }
  
  if(config.async){
    document.getElementById(config.loader_id).innerHTML = strCode;
  }else{
    document.write(strCode);
  }
} 

var lintas_trending = function (config){
  if(config.async){
    var strCode = '<iframe id="lme_trending" scrolling="no" style="border: none;width: 300px;height : 50px;" src="http://devhome.lintas.me/webplugin/trending"></iframe>';
    document.getElementById(config.loader_id).innerHTML = strCode;
  }else{
    document.write('<iframe id="lme_trending" scrolling="no" style="border: none;width: 300px;height : 50px;" src="http://devhome.lintas.me/webplugin/trending"></iframe>');
  }
  
}

var lintasme_trending = function (config){
  if(config.async){
    var strCode = '<iframe id="lme_trending" scrolling="no" style="border: none;width: ' + config.width + 'px;height : 34px;" src="http://devhome.lintas.me/webplugin/trending_widget/?limit=' + config.limit + '&bg='+ config.background.replace("#","%23") +'"></iframe>';
    document.getElementById(config.loader_id).innerHTML = strCode;
  }else{
    document.write('<iframe id="lme_trending" scrolling="no" style="border: none;width: ' + config.width + 'px;height : 34px;" src="http://devhome.lintas.me/webplugin/trending_widget/?limit=' + config.limit + '&bg='+ config.background.replace("#","%23") +'"></iframe>');
  }
}
