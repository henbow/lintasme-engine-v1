$(document).ready(function(){
  function do_search(q){
      if(q != ""){
        $.ajax({
                type: "GET",
                url: 'http://devhome.lintas.me/xhr/follow_search/' + q,
                beforeSend: function(){
  
                },
                success: function(data){
                  $(".search-sugest").show();
                  $(".search-sugest").html(data.view);
                  
                  $(".followdomain").click(function() {
                    var followbtn = $(this);
                    var domain = $(this).attr('data-domain');
                    $.ajax({
                          type: "POST",
                          url: 'http://www.lintas.me/xhr/join/domain/Y',
                          data : {tag:domain},
                          beforeSend: function(){

                          },
                          success: function(data){
                              if(data == 'JOIN'){
                                followbtn.hide();
                                followbtn.parent().children('.following').show();
                              }
                          }
                    });
                  });
                  
                  $(".followtopic").click(function() {
                    var followbtn = $(this);
                    var topic = $(this).attr('data-topic');
                    $.ajax({
                          type: "POST",
                          url: 'http://www.lintas.me/xhr/join_ch/Y',
                          data : {ch:topic},
                          beforeSend: function(){

                          },
                          success: function(data){
                              if(data == 'JOIN'){
                                followbtn.hide();
                                followbtn.parent().children('.following').show();
                              }
                          }
                    });
                  });
                  
                  
                  $(".followtag").click(function() {
                    var followbtn = $(this);
                    var tag = $(this).attr('data-tag');
                    $.ajax({
                          type: "POST",
                          url: 'http://www.lintas.me/xhr/join/tag/Y',
                          data : {tag:tag},
                          beforeSend: function(){

                          },
                          success: function(data){
                              if(data == 'JOIN'){
                                followbtn.hide();
                                followbtn.parent().children('.following').show();
                              }
                          }
                    });
                  });
                  
                  $(".followvideo").click(function() {
                    var followbtn = $(this);
                    var channelid = $(this).attr('data-channelid');
                    $.ajax({
                          type: "POST",
                          url: 'http://www.lintas.me/xhr/xhr_join_video_channel/',
                          data : {chid:channelid},
                          beforeSend: function(){

                          },
                          success: function(data){
                              if(data.status){
                                followbtn.hide();
                                followbtn.parent().children('.following').show();
                              }else{
                                alert(data.error_msg);
                              }
                          }
                    });
                  });
                  
                }
        });
      }
    } 
    
    $("#searchfield").keyup( function() {
      if ($(this).val()==''){
        $(".search-sugest").hide();
        return false;
      }
      var q = $(this).val();

        clearTimeout($.data(this, "timer"));
        var ms = 1000; 
        var wait = setTimeout(function() {
          do_search(q);
        }, ms);

        $.data(this, "timer", wait);
    });
});
