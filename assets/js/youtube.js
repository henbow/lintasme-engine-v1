      /*
       * Polling the player for information
       */
      
      // Update a particular HTML element with a new value
      function updateHTML(elmId, value) {
        document.getElementById(elmId).innerHTML = value;
      }
      
      // This function is called when an error is thrown by the player
      function onPlayerError(errorCode) {
        alert("An error occured of type:" + errorCode);
      }
      
      function update_db(id,persen){
        /*
        var accessID = document.getElementById("accessId");
        
        $.ajax({
                type: "POST",
                url: '/xhr/videostat',
                data : {persen:persen,id:id,accessId:accessID.value},
                beforeSend: function(){
                    // action before send
                },
                success: function(data){
                    // action success post
                    
                }
        });
        */
      }
      
      function update_view(id,persen){
        /*
        var accessID = document.getElementById("accessId");
        
        $.ajax({
                type: "POST",
                url: '/xhr/videostat/1',
                data : {persen:persen,id:id,accessId:accessID.value},
                beforeSend: function(){
                    // action before send
                },
                success: function(data){
                    // action success post
                    
                }
        });
        */
      }
      
      // This function is called when the player changes state
      function onPlayerStateChange(newState) {
        //updateHTML("playerState", newState);
        console.log(newState);
        
        
        if(newState == 0){
          $(".stoped").show();
        }
        //var persen = Math.round((ytplayer.getCurrentTime() / ytplayer.getDuration()) * 100);
        /*
        var postVideoID = document.getElementById("postvideoId");
        if(newState == 2){
          //alert(persen);
          console.log(persen + " "  + postVideoID.value);
          update_db(postVideoID.value,persen);
        }
        if(newState == 5){
          //alert(persen);
          console.log(persen + " "  + postVideoID.value);
          update_db(postVideoID.value,persen);
        }
        if(newState == 0){
          //alert(persen);
          console.log(persen + " "  + postVideoID.value);
          //update_db(postVideoID.value,persen);
        }
        if(newState == 1){
          // update view to first start
          //alert(persen);
          console.log(persen + " "  + postVideoID.value);
          update_view(postVideoID.value,persen);
        }
        */
      }
      
      
      
      // Display information about the current state of the player
      function updatePlayerInfo() {
        // Also check that at least one function exists since when IE unloads the
        // page, it will destroy the SWF before clearing the interval.
        if(ytplayer && ytplayer.getDuration) {
          //updateHTML("videoDuration", ytplayer.getDuration());
          //updateHTML("videoCurrentTime", ytplayer.getCurrentTime());
          //updateHTML("bytesTotal", ytplayer.getVideoBytesTotal());
          //updateHTML("startBytes", ytplayer.getVideoStartBytes());
          //updateHTML("bytesLoaded", ytplayer.getVideoBytesLoaded());
          var persen = Math.round((ytplayer.getCurrentTime() / ytplayer.getDuration()) * 100);

          //updateHTML("persenPlay", persen);
        }
      }
      
      // This function is automatically called by the player once it loads
      function onYouTubePlayerReady(playerId) {
        ytplayer = document.getElementById("ytPlayer");
        // This causes the updatePlayerInfo function to be called every 250ms to
        // get fresh data from the player
        //setInterval(updatePlayerInfo, 5000);
        //updatePlayerInfo();
        ytplayer.addEventListener("onStateChange", "onPlayerStateChange");
        //ytplayer.playVideo();
        //ytplayer.addEventListener("onError", "onPlayerError");
      }
      
      // The "main method" of this sample. Called when someone clicks "Run".
      function loadPlayer() {
        // The video to load
        var videoID = document.getElementById("videoId");
        
        // Lets Flash from another domain call JavaScript
        var params = { allowScriptAccess: "always" ,wmode: "opaque",autohide : "0",autoplay : "1",cc_load_policy : "1",controls :"0",showinfo : 0};

        // The element id of the Flash embed
        var atts = { id: "ytPlayer" };
        // All of the magic handled by SWFObject (http://code.google.com/p/swfobject/)
        swfobject.embedSWF("http://www.youtube.com/v/" + videoID.value + 
                           "?version=3&enablejsapi=1&playerapiid=player1&showinfo=0&rel=0", 
                           "videoDiv", "640", "360", "9", null, null, params, atts);
      }
      function _run() {
        loadPlayer();
      }
      google.setOnLoadCallback(_run);
      